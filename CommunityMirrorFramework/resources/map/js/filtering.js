	// Jede Checkbox überprüfen und wenn ausgewählt entsprechende GMarker auf der Karte anzeigen
	function filtering(map) {
		for ( i=0; i<4; i++) {
			for ( j=0; j < resultMarkers.length; j++) {
				if ( document.checkboxen.elements[i].checked) {
					if ( resultMarkers[j].id == document.checkboxen.elements[i].value) {
							resultMarkers[j].setVisible(true);
						}
					}
				else {			
					if ( !document.checkboxen.elements[i].checked) {
					if ( resultMarkers[j].id == document.checkboxen.elements[i].value) {
						resultMarkers[j].setVisible(false);
						}			
					}
				}
			}
		}
		if ( aBoxChosen() == true ) {
			adjustZoom(map);
		}
	}
	
	function aBoxChosen () {
		var aBox = false;
		for ( b=0; b<4; b++ ) {
			if ( document.checkboxen.elements[b].checked) {
				aBox = true;
			}
		}
		return aBox;
	}
			