	//Passt die Ansicht den Suchergebnissen an
	function adjustZoom(map) {
		var resultBounds = new google.maps.LatLngBounds();
		for ( a=0; a < resultMarkers.length; a++) {
			if( resultMarkers[a].getVisible() == true ) {
				resultBounds.extend(resultMarkers[a].position);
			}
		}
		map.fitBounds(resultBounds);
		zoomChangeBoundsListener = 
		google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        if (this.getZoom() > 13) {
			this.setZoom(13);
			}
		});
	}