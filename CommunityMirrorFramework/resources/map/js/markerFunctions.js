	var gMarkers = [];
	var resultMarkers =[];

	//Infofenster Inhalt
	var infoWindowContent = [
		['<div class="info_content">' + '<h3>London Eye</h3>' + '<p>The London Eye is a giant Ferris wheel</p>' + '</div>'],
		['<div class="info_content">' + '<h3>London Eye</h3>' + '<p>The London Eye is a giant Ferris wheel</p>' + '</div>'],
		['<div class="info_content">' + '<h3>London Eye</h3>' + '<p>The London Eye is a giant Ferris wheel</p>' + '</div>'],
		['<div class="info_content">' + '<h3>London Eye</h3>' + '<p>The London Eye is a giant Ferris wheel</p>' + '</div>'],
		['<div class="info_content">' + '<h3>Palace of Westminster</h3>' + '<p>The Palace of Westminster is the meeting place of the House of Commons</p>' + '</div>'],
		[''],
		['USA'],
	];
			
	//Interator, platziert alle Marker auf der Karte
	function setMarkers ( map ) {
			bounds = new google.maps.LatLngBounds();
		for( i=0; i < markers.length; i++) {
			var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
			bounds.extend(position);
			var marker = new google.maps.Marker({
			position: position,
			map: map,
			title: markers[i][0],
			id: markers[i][3],
			//animation: google.maps.Animation.DROP,
			});
			
		//Alle GMarker in das Array schreiben
		gMarkers[i] = marker;
		
		//Antippen eines Markers registrieren und jedem Marker ein InfoWindow zuordnen
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infoWindow.setContent(infoWindowContent[i][0]);
				infoWindow.open(map, marker);
			}
		})(marker, i));
		
		//Die Karte auf die Marker zentrieren
		map.fitBounds(bounds);
		}
		
		//Infowindows initialisieren
		var infoWindow = new google.maps.InfoWindow(), marker, i;
	}
	
	//Marker Clusterer	
	function initClusterer (map) {
		var mcOptions = {gridSize: 50, maxZoom: 15};
		markerClusterer = new MarkerClusterer(map, resultMarkers, mcOptions);
		}
		
	function recluster (map) {
		markerClusterer.setMap(null);
		markerClusterer = new MarkerClusterer(map, resultMarkers);
	}
	
	//Zeigt alle Marker auf der Karte an und setzt die Ergebnismenge zurück
	function resetAllMarkers(map) {
		for ( i=0; i < gMarkers.length; i++ ) {
			gMarkers[i].setMap(map);
			}
		for ( i=0; i < resultMarkers.length; i++ ) {
			resultMarkers[i].setMap(null);
			}
			resultMarkers = [];
		}