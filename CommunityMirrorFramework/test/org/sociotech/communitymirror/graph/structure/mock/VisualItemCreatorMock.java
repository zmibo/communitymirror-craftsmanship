package org.sociotech.communitymirror.graph.structure.mock;

import java.util.Set;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.graph.creation.GraphVisualItemCreator;
import org.sociotech.communitymirror.graph.structure.GraphNode;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

public class VisualItemCreatorMock implements GraphVisualItemCreator {

    @Override
    public VisualItem<?> createVisualItem(Item dataItem, VisualState visualState, GraphNode node) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<Item> getRelatedItemsFor(Item dataItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<Item> getSelectedRelatedItemsToConnectFor(Item dataItem, Set<Item> connectedItems) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void destroyVisualItem(VisualItem<?> visualItem) {
        // TODO Auto-generated method stub

    }

}
