
package org.sociotech.communitymirror.graph.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.sociotech.communitymirror.graph.structure.mock.VisualItemCreatorMock;

public class GraphNodeTest {

    private Graph graph;

    @Before
    public void setUp() throws Exception {
        this.graph = new Graph(new VisualItemCreatorMock());
    }

    @Test
    public void testGraphContainment() {
        GraphNode node = new GraphNode(graph);
        assertEquals(graph, node.getGraph());

        // TODO test empty edges

        // TODO test empty visual components
    }

    @Test
    public void testConnection() {
        GraphNode node1 = new GraphNode(graph);
        GraphNode node2 = new GraphNode(graph);

        assertEquals(0, node1.getConnectedNodes().size());
        assertEquals(0, node2.getConnectedNodes().size());

        GraphEdge edge1 = node1.connectTo(node2);

        assertEquals(1, node1.getConnectedNodes().size());
        assertEquals(1, node2.getConnectedNodes().size());

        GraphEdge edge2 = node1.connectTo(node2);

        assertTrue(edge1 == edge2);

        assertEquals(1, node1.getConnectedNodes().size());
        assertEquals(1, node2.getConnectedNodes().size());

        // opposite direction
        GraphEdge edge3 = node2.connectTo(node1);

        assertTrue(edge1 == edge3);

        assertEquals(1, node1.getConnectedNodes().size());
        assertEquals(1, node2.getConnectedNodes().size());
    }

    @Test
    public void testEdgeAccess() {
        GraphNode node1 = new GraphNode(graph);
        GraphNode node2 = new GraphNode(graph);
        GraphNode node3 = new GraphNode(graph);
        GraphNode node4 = new GraphNode(graph);

        GraphEdge edge1 = node1.connectTo(node2);
        GraphEdge edge2 = node1.connectTo(node4);

        assertEquals(edge1, node1.getEdgeTo(node2));
        assertEquals(edge2, node1.getEdgeTo(node4));

        assertEquals(null, node1.getEdgeTo(node3));

    }

    @Test
    public void testEdgeAccessUntilLevel() {
        GraphNode node1 = new GraphNode(graph);
        GraphNode node2 = new GraphNode(graph);
        GraphNode node3 = new GraphNode(graph);
        GraphNode node4 = new GraphNode(graph);
        GraphNode node5 = new GraphNode(graph);
        GraphNode node6 = new GraphNode(graph);

        node1.connectTo(node2);
        node1.connectTo(node5);
        node2.connectTo(node3);
        node2.connectTo(node4);
        node6.connectTo(node3);

        assertTrue(node1.getConnectedNodesUntilLevel(0).isEmpty());
        assertTrue(node1.getConnectedNodesUntilLevel(-1).isEmpty());

        assertEquals(2, node1.getConnectedNodesUntilLevel(1).size());
        assertTrue(node1.getConnectedNodesUntilLevel(1).contains(node2));
        assertTrue(node1.getConnectedNodesUntilLevel(1).contains(node5));

        // TODO expected 4
        // assertEquals(4, node1.getConnectedNodesUntilLevel(2).size());
        assertTrue(node1.getConnectedNodesUntilLevel(2).contains(node3));
        assertTrue(node1.getConnectedNodesUntilLevel(2).contains(node4));
        assertTrue(node1.getConnectedNodesUntilLevel(2).contains(node5));
        assertTrue(node1.getConnectedNodesUntilLevel(2).contains(node2));
        // TODO Bugfix
        // assertFalse(node1.getConnectedNodesUntilLevel(2).contains(node1));

        // TODO expected 5
        // assertEquals(5, node1.getConnectedNodesUntilLevel(3).size());
        assertTrue(node1.getConnectedNodesUntilLevel(3).contains(node3));
        assertTrue(node1.getConnectedNodesUntilLevel(3).contains(node4));
        assertTrue(node1.getConnectedNodesUntilLevel(3).contains(node5));
        assertTrue(node1.getConnectedNodesUntilLevel(3).contains(node2));
        assertTrue(node1.getConnectedNodesUntilLevel(3).contains(node6));
        // TODO Bugfix
        // assertFalse(node1.getConnectedNodesUntilLevel(3).contains(node1));

    }

    @Test
    public void testEdgeAccessInLevel() {
        GraphNode node1 = new GraphNode(graph);
        GraphNode node2 = new GraphNode(graph);
        GraphNode node3 = new GraphNode(graph);
        GraphNode node4 = new GraphNode(graph);
        GraphNode node5 = new GraphNode(graph);
        GraphNode node6 = new GraphNode(graph);

        node1.connectTo(node2);
        node1.connectTo(node5);
        node2.connectTo(node3);
        node2.connectTo(node4);
        node6.connectTo(node3);

        assertTrue(node1.getConnectedNodesInLevel(0).isEmpty());
        assertTrue(node1.getConnectedNodesInLevel(-1).isEmpty());

        assertEquals(2, node1.getConnectedNodesInLevel(1).size());
        assertTrue(node1.getConnectedNodesInLevel(1).contains(node2));
        assertTrue(node1.getConnectedNodesInLevel(1).contains(node5));

        assertEquals(2, node1.getConnectedNodesInLevel(2).size());
        assertTrue(node1.getConnectedNodesInLevel(2).contains(node3));
        assertTrue(node1.getConnectedNodesInLevel(2).contains(node4));
        assertFalse(node1.getConnectedNodesInLevel(2).contains(node1));

        assertEquals(1, node1.getConnectedNodesInLevel(3).size());
        assertTrue(node1.getConnectedNodesInLevel(3).contains(node6));
        assertFalse(node1.getConnectedNodesInLevel(3).contains(node3));
        assertFalse(node1.getConnectedNodesInLevel(3).contains(node4));
        assertFalse(node1.getConnectedNodesInLevel(3).contains(node5));
        assertFalse(node1.getConnectedNodesInLevel(3).contains(node2));
        assertFalse(node1.getConnectedNodesInLevel(3).contains(node1));
    }

    @Test
    public void testVisualStateSequence() {
        GraphNode node = new GraphNode(graph);

        assertTrue(node.getPreviousVisualState() == null);
        assertTrue(node.getNextVisualState().isPreview());

        node.nextState();

        assertTrue(node.getPreviousVisualState().isPreview());
        assertTrue(node.getNextVisualState().isDetail());

    }
}
