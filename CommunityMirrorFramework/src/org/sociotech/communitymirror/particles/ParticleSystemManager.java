/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * Fishification: Visualizing Activity Streams Using the Aquarium Metaphor
 * Project: <http://www.fishification.net>
 * Source: <https://github.com/soziotech/Fishification>
 *
 * Fishification is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at <http://www.eclipse.org/legal/epl-v10.html>.
 *
 * The accompanying materials of Fishification are made available under the terms
 * of Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * You should have received a copy of the license along with this
 * work.  If not, see <http://creativecommons.org/licenses/by-sa/3.0/>.
 */

package org.sociotech.communitymirror.particles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

/**
 * Created by i21bmabu on 13.02.14.
 */
public abstract class ParticleSystemManager<T extends VisualComponent, S extends ParticleSystem<T>> extends VisualGroup {

    public final static int  PARTICLESYSTEMS_MAX_DEFAULT = 5;
    public final static int  FREQUENCY_DEFAULT           = 10;
    public final static long NANO_TO_SECONDS             = 1000000000l;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(CommunityMirror.class.getName());

    @SuppressWarnings("unused")
	private int  mMaxConcurrentParticleSystems = PARTICLESYSTEMS_MAX_DEFAULT;
    private long mDuration                     = FREQUENCY_DEFAULT * NANO_TO_SECONDS;

    private long    mOldNanoTime = 0;

    public ParticleSystemManager(int maxConcurrentParticleSystems, int frequencyInSeconds) {
        mMaxConcurrentParticleSystems = maxConcurrentParticleSystems;
        mDuration = frequencyInSeconds * NANO_TO_SECONDS;
    }

    @Override
    protected void onInit() {
        super.onInit();
    }

    /**
     * Adds bubble particle systems to aquarium over time.
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);

        long elapsedTime = mOldNanoTime + mDuration;
        if (nanoTime > elapsedTime) {
            produceParticleSystem();
            mOldNanoTime = nanoTime;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        logger.debug("Destroying particle system manager.");
        removeVisualComponent(this);
        removeNode(this);
    }

    protected abstract ParticleSystem<T> produceParticleSystem();

}
