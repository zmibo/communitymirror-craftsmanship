/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.particles;

/**
 * Defines a 2D vector.
 */
public class Vec2D {

    private double x;
    private double y;

    public Vec2D() {
        x = 0.0;
        y = 0.0;
    }

    public Vec2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void add(Vec2D v) {
        x += v.x;
        y += v.y;
    }

    public void div(double value) {
        x /= value;
        y /= value;
    }

    public void mult(double value) {
        x *= value;
        y *= value;
    }
}
