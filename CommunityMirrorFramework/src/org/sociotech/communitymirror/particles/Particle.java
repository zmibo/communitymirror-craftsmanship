/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.particles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

public class Particle<T extends VisualComponent> extends VisualGroup {

    public final static int LIFESPAN_DEFAULT = 3000;
    public final static int MASS_DEFAULT     = 1;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger();

    ParticleSystem<?> mParticleSystem;
    private final T mParticleComponent;
    Vec2D mPosition;
    Vec2D mVelocity;
    Vec2D mAcceleration;
    double mLifeSpan = LIFESPAN_DEFAULT;
    double mass      = MASS_DEFAULT;

    public Particle(ParticleSystem<?> particleSystem, T particleComponent, Vec2D initialPosition, Vec2D initialVelocity, double lifeSpan) {
        mParticleSystem = particleSystem;
        mParticleComponent = particleComponent;
        mPosition = initialPosition;
        mVelocity = initialVelocity;
        mAcceleration = new Vec2D(0, 0);
        mLifeSpan = lifeSpan;
    }

    void applyForce(Vec2D force) {
        force.div(mass);
        mAcceleration.add(force);
    }

    @Override
    protected void onInit() {
        super.onInit();

        // Add visual component
        mParticleComponent.init();
        addNode(mParticleComponent);
        addVisualComponent(mParticleComponent);

        // Set position of particle
        setTranslateX(mPosition.getX());
        setTranslateY(mPosition.getY());
    }

    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);

        // Add acceleration to velocity
        mVelocity.add(mAcceleration);

        // Reset acceleration
        mAcceleration.mult(0);

        double fpsFactor = 60 / framesPerSecond;
        mLifeSpan -= 2.0 * fpsFactor;

        // Set position
        //TODO: dependency to FPS
        double posX = getTranslateX();
        double posY = getTranslateY();
        setTranslateX(posX + mVelocity.getX()*fpsFactor);
        setTranslateY(posY + mVelocity.getY()*fpsFactor);

        // Apply new force
        applyForce(onCalculateNewForce());

        // Destroy after particle exceeded its life span
        if (mLifeSpan < 0) {
            destroy();
        }
    }

    /**
     * Applies a new force to the particle's speed.
     *
     * @return The force vector.
     */
    protected Vec2D onCalculateNewForce() {
        return new Vec2D(0, 0);
    }

    @Override
    protected void onDestroy() {

        logger.debug("Removing particle component.");
        super.onDestroy();

        //mParticleComponent.destroy();
        //removeNode(mParticleComponent);

        logger.debug("Destroying particle.");
        mParticleSystem.removeVisualComponent(this);
        //mParticleSystem.removeNode(this);
    }
}