/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.particles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

import java.util.List;

public class ParticleSystem<T extends VisualComponent> extends VisualGroup {

    private final ParticleSystemManager<T, ParticleSystem<T>> mParticleSystemManager;
    private final int                                         mMinParticles;
    private final int                                         mMaxParticles;

    private static final Logger logger = LogManager.getLogger();

    public ParticleSystem(int minParticles, int maxParticles) {
        mParticleSystemManager = null;
        mMinParticles = minParticles;
        mMaxParticles = maxParticles;
    }

    public ParticleSystem(ParticleSystemManager<T, ParticleSystem<T>> particleSystemManager, int minParticles, int maxParticles) {
        mParticleSystemManager = particleSystemManager;

        mMinParticles = minParticles;
        mMaxParticles = maxParticles;
    }

    /**
     * Adds a new particle to the particle system.
     * @param particle
     */
    public void addParticle(Particle<T> particle) {
        particle.init();
        addNode(particle);
        addVisualComponent(particle);
    }

    public void applyForce(Vec2D f) {

        for (VisualComponent visualComponent : getVisualComponents()) {
            if (visualComponent instanceof Particle<?>) {
                Particle<?> particleEntity = (Particle<?>) visualComponent;
                particleEntity.applyForce(f);
            }

        }
    }

    public int getMinParticles() {
        return mMinParticles;
    }

    public int getMaxParticles() {
        return mMaxParticles;
    }

    public int getAmountParticles() {
        return getVisualComponents().size();
    }

    @Override
    protected void onUpdate(double framesPerSecond, long nanoSeconds) {
        super.onUpdate(framesPerSecond, nanoSeconds);

        final List<VisualComponent> visualComponents = getVisualComponents();
        if (visualComponents != null && visualComponents.isEmpty()) {

            // Destroy particle system as soon as all child components are destroyed
            logger.debug("Destroying particle system as all child components are destroyed.");
            destroy();
        }
    }

    @Override
    protected void onDestroy() {

        logger.debug("Destroying particle system.");
        super.onDestroy();

        if(mParticleSystemManager != null) {
            mParticleSystemManager.removeVisualComponent(this);
        }
    }
}
