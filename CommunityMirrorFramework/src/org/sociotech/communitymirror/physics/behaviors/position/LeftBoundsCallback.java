package org.sociotech.communitymirror.physics.behaviors.position;

import javafx.geometry.Bounds;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Interface that must be implemented to be called back by the {@link CallBackOnBoundsExitBehavior}
 * when a component leaves special bounds.
 * 
 * @author Peter Lachenmaier
 */
public interface LeftBoundsCallback {
	/**
	 * Will be called if the configured component leaves the configured bounds.
	 * 
	 * @param bounds Bounds the object left.
	 * @param leavingObject Object leaving the bounds
	 */
	public void leftBounds(Bounds bounds, PhysicsApplicable leavingObject);
}
