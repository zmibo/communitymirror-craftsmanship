package org.sociotech.communitymirror.physics.behaviors.position;

import javafx.geometry.Bounds;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Interface that must be implemented to be called back by the {@link CallBackOnBoundsTraversedBehavior}
 * when a component traverses special bounds.
 * 
 * @author Eva Lösch
 */
public interface TraversedBoundsCallback {
	/**
	 * Will be called if the configured component traverses the configured bounds.
	 * 
	 * @param bounds Bounds the object traversed.
	 * @param leavingObject Object traversing the bounds
	 */
	public void traversedBounds(Bounds bounds, PhysicsApplicable traversingObject);
}
