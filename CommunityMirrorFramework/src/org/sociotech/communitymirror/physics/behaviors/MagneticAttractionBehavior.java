package org.sociotech.communitymirror.physics.behaviors;

import javafx.geometry.Bounds;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.visualitems.VisualComponent;

public class MagneticAttractionBehavior extends DriftBehavior {

	private VisualComponent targetComponent;
	private boolean jump = false;
	private double deltaX = 0;
	private double deltaY = 0;
	private double offset = 3;
	double targetCenterMinX;
	double targetCenterMaxX;
	double targetCenterMinY;
	double targetCenterMaxY;
	double componentCenterX;
	double componentCenterY;

	public MagneticAttractionBehavior(double velocity, double acceleration,
			double targetVelocity, VisualComponent targetComponent) {
		super(0, 0, velocity, acceleration, targetVelocity);
		this.targetComponent = targetComponent;
	}

	private void update(PhysicsApplicable component) {
		// calculate center of component and target component
		this.calculateCenterValues(component);

		// update deltas for movement
		this.updateDetas();

		// check if should be switched from drifting to jumping
		this.updateJump();

		// update drift behavior
		this.updateDriftBehavior();
	}

	private void calculateCenterValues(PhysicsApplicable component) {
		// get bounds of this component
		Bounds compBounds = component.getPositionBounds();
		// calculate center of this component
		componentCenterX = compBounds.getMinX()
				+ (compBounds.getMaxX() - compBounds.getMinX()) / 2;
		componentCenterY = compBounds.getMinY()
				+ (compBounds.getMaxY() - compBounds.getMinY()) / 2;

		// get bounds of the target component
		Bounds targetBounds = targetComponent.getPositionBounds();
		// calculate center of target component
		double targetCenterX = targetBounds.getMinX()
				+ (targetBounds.getMaxX() - targetBounds.getMinX()) / 2;
		double targetCenterY = targetBounds.getMinY()
				+ (targetBounds.getMaxY() - targetBounds.getMinY()) / 2;

		// define a center area of the target component
		// with an offset to the actual center point of the target component
		targetCenterMinX = targetCenterX - offset;
		targetCenterMaxX = targetCenterX + offset;
		targetCenterMinY = targetCenterY - offset;
		targetCenterMaxY = targetCenterY + offset;
	}

	private void updateDetas() {
		// if this component is at the left side of the other component
		if (componentCenterX < targetCenterMinX) {
			// move component to the right
			this.deltaX = targetCenterMinX - componentCenterX;
		}
		// if this component is at the right side of the other component
		if (componentCenterX > targetCenterMaxX) {
			// move component to the left
			this.deltaX = targetCenterMaxX - componentCenterX;
		}
		// if centerX of component is within centerX of target component
		if (componentCenterX >= targetCenterMinX
				&& componentCenterX <= targetCenterMaxX) {
			// don't move horizontally any more
			this.deltaX = 0;
		}
		// if this component is above the other component
		if (componentCenterY < targetCenterMinY) {
			// move component down
			this.deltaY = targetCenterMinY - componentCenterY;
		}
		// if this component is below the other component
		if (componentCenterY > targetCenterMaxY) {
			// move component up
			this.deltaY = targetCenterMaxY - componentCenterY;
		}
		// if centerY of this component is within centerY of target component
		if (componentCenterY >= targetCenterMinY
				&& componentCenterY <= targetCenterMaxY) {
			// don't move vertically any more
			this.deltaY = 0;
		}
	}

	private void updateJump() {
		// if component has been on center of target component once
		if (this.deltaX == 0 && this.deltaY == 0) {
			// switch to drift behavior and stay with it
			this.jump = true;
		}
	}

	private void updateDriftBehavior() {
		// if drift behavior is switched on
		if (!this.jump) {
			double maxDelta = Math.max(Math.abs(deltaX), Math.abs(deltaY));
			if (this.deltaX == 0) {
				// don't move horizontally any more
				this.setXDirection(0);
			} else {
				// move component horizontally
				this.setXDirection(deltaX / maxDelta);
			}
			if (this.deltaY == 0) {
				// don't move vertically any more
				this.setYDirection(0);
			} else {
				// move component vertically
				this.setYDirection(deltaY / maxDelta);
			}
		}
	}

	private void resetInteractionTime(PhysicsApplicable component) {
		// reset interaction time
		if (component instanceof VisualComponent) {
			((VisualComponent) component).resetInteractionTime();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo
	 * (org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		// reset interaction time
		this.resetInteractionTime(component);

		// first update values
		this.update(component);

		// if component has not been on center of target component once
		if (!this.jump) {
			// use drift behavior
			super.onApplyTo(component);
		}
		// if component has already been on center of target component once
		else {
			// jump to center
			component.move(this.deltaX, this.deltaY);
		}
	}
}
