package org.sociotech.communitymirror.physics.behaviors;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;

/**
 * Behavior that fades out a component and destroys it when no interaction is performed on it.
 * 
 * @author Peter Lachenmaier
 */
public class StopMoveOnInteractionBehavior extends PhysicsBehavior {

	/**
	 * Duration for the fade out.
	 */
	private long timeToRestart = -1;

	/**
	 * Creates a behavior that stops movement of the component if an interaction is performed in the last timeToRestart
	 * millis. Its important to add this behavior after all other behavior that move the component.
	 *  
	 * @param timeToRestart Time in millis after the stop is deactivated. -1 to keep stop permanently.
	 */
	public StopMoveOnInteractionBehavior(long timeToRestart) {
		this.timeToRestart = timeToRestart;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		long timeSinceLastInteraction = component.getTimeSinceLastInteractionProperty().longValue();
		
		if(timeSinceLastInteraction < timeToRestart || (timeSinceLastInteraction < Long.MAX_VALUE && timeToRestart == -1)) {
			// reset move
			component.resetMove();
		}
	}
	
	// factory methods
	
	/**
	 * Creates a stop move behavior that permanently stops the movement after an interaction.
	 * 
	 * @return The newly created stop move behavior.
	 */
	public static StopMoveOnInteractionBehavior createStopMoveBehavior() {
		return new StopMoveOnInteractionBehavior(-1);
	}

	/**
	 * Creates a behavior that stops movement of the component if an interaction is performed in the last timeToRestart
	 * millis. Its important to add this behavior after all other behavior that move the component.
	 *  
	 * @param timeToRestart Time in millis after the stop is deactivated. -1 to keep stop permanently.
	 * 
	 * @return The newly created stop move behavior.
	 */
	public static StopMoveOnInteractionBehavior createStopMoveBehavior(long timeToRestart) {
		return new StopMoveOnInteractionBehavior(timeToRestart);
	}
}
