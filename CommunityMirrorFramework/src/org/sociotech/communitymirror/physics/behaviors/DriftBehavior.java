package org.sociotech.communitymirror.physics.behaviors;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Behavior that let a component drift in a defined direction.
 * 
 * @author Peter Lachenmaier
 */
public class DriftBehavior extends AcceleratablePhysicsBehavior {

	/**
	 * The property for the x direction.
	 */
	private final DoubleProperty xDirection;

	/**
	 * The property for the y direction.
	 */
	private final DoubleProperty yDirection;
	
	/**
	 * Creates a drift behavior with the given acceleration and velocity in the given direction. Acceleration will be performed
	 * until the target velocity will be reached. Velocity 1.0 means 1 pixel per second.
	 * 
	 * @param xDirection Drift in x direction
	 * @param yDirection Drift in y direction
	 * @param velocity Current velocity
	 * @param acceleration Acceleration to apply in every step
	 * @param targetVelocity Velocity to reach due to acceleration
	 */
	public DriftBehavior(double xDirection, double yDirection, double velocity, double acceleration, double targetVelocity) {
		super(velocity, acceleration, targetVelocity);
		this.xDirection     = new SimpleDoubleProperty(xDirection);
		this.yDirection     = new SimpleDoubleProperty(yDirection);
	}
	
	/**
	 * Creates a drift behavior with the given velocity in the given direction. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. 
	 * 
	 * @param xDirection Drift in x direction
	 * @param yDirection Drift in y direction
	 * @param velocity Current velocity
	 */
	public DriftBehavior(double xDirection, double yDirection, double velocity) {
		// zero acceleration and target velocity is initial velocity
		this(xDirection, yDirection, velocity, 0.0, velocity);
	}
	
	/**
	 * Creates a drift behavior with the given velocity in the given direction. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. Default velocity is 1.0 
	 * 
	 * @param xDirection Drift in x direction
	 * @param yDirection Drift in y direction
	 */
	public DriftBehavior(double xDirection, double yDirection) {
		// 1.0 velocity
		this(xDirection, yDirection, 1.0);
	}
		
	/**
	 * Returns the direction x property.
	 * 
	 * @return The direction x property.
	 */
	public DoubleProperty xDirectionProperty() {
		return xDirection;
	}
	
	/**
	 * Returns the direction y property.
	 * 
	 * @return The direction y property.
	 */
	public DoubleProperty yDirectionProperty() {
		return yDirection;
	}	
	
	/**
	 * Sets the value for the x direction.
	 * 
	 * @param xDirection X direction
	 */
	public void setXDirection(double xDirection) {
		this.xDirection.set(xDirection);
	}
	
	/**
	 * Sets the value for the y direction.
	 * 
	 * @param yDirection Y direction
	 */
	public void setYDirection(double yDirection) {
		this.yDirection.set(yDirection);
	}
		
	/**
	 * Creates a drift behavior in horizontal direction. (Left to right)
	 * 
	 * @return The created drift behavior.
	 */
	public static DriftBehavior createHorizontalDriftBehavior() {
		return new DriftBehavior(1.0, 0.0);
	}
	
	/**
	 * Creates a drift behavior in vertical direction. (Top to bottom)
	 * 
	 * @return The created drift behavior.
	 */
	public static DriftBehavior createVerticalDriftBehavior() {
		return new DriftBehavior(0.0, 1.0);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		// simply move the component with the current velocity corrected by the speed factor in x and y direction
		component.move(getSpeedCorrection() * velocityProperty().get() * xDirection.get(), getSpeedCorrection() * velocityProperty().get() * yDirection.get());
	}
}
