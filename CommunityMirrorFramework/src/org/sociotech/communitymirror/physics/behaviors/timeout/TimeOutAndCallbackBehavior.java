package org.sociotech.communitymirror.physics.behaviors.timeout;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;

/**
 * Behavior that times out and calls back when no interaction is performed on it.
 * 
 * @author Peter Lachenmaier
 */
public class TimeOutAndCallbackBehavior extends PhysicsBehavior {

	/**
	 * Time to wait until the time out callback.
	 */
	private long  waitToTimeOut;
	
	/**
	 * Object that will be called after time out 
	 */
	private TimeOutCallBack callback;
	
	/**
	 * Creates a behavior that calls back if no interaction is performed on it.
	 * (All times in millis)
	 *  
	 * @param waitToTimeOut Time to wait until the fade out starts.
	 * @param callback The callback after time out happened
	 */
	public TimeOutAndCallbackBehavior(long waitToTimeOut, TimeOutCallBack callback) {
		this.waitToTimeOut = waitToTimeOut;
		this.callback = callback;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		long timeSinceLastInteraction = component.getTimeSinceLastInteractionProperty().longValue();
		
		if(waitToTimeOut > 0 && timeSinceLastInteraction > waitToTimeOut) {
			// callback
			callback.timedOut(component);
		}
	}
}
