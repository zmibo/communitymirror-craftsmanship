package org.sociotech.communitymirror.physics.behaviors.destroy;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;

/**
 * Behavior that fades out a component and destroys it when no interaction is performed on it.
 * 
 * @author Peter Lachenmaier
 */
public class FadeOutAndDieBehavior extends PhysicsBehavior {

	/**
	 * Duration for the fade out.
	 */
	private long  fadeOutTime;
	
	/**
	 * Time to wait until the fade out starts.
	 */
	private long  waitToFadeOut;
	
	/**
	 * Opacity to reset the opacity to after an interaction is performed.
	 */
	private double  initialOpacity;
	
	/**
	 * Whether to destroy the component when it is invisible or not.
	 */
	private boolean destroy = false;

	/**
	 * Creates a behavior that fades out the component, if no interaction is performed on it,
	 * and destroys it after it is invisible when destroy is set to true. (All times in millis)
	 *  
	 * @param fadeOutTime Duration for the fade out.
	 * @param waitToFadeOut Time to wait until the fade out starts.
	 * @param initialOpacity Opacity to reset the opacity to after an interaction is performed. 
	 * @param destroy Whether to destroy the component when it is invisible or not.
	 */
	public FadeOutAndDieBehavior(long fadeOutTime, long waitToFadeOut, double initialOpacity, boolean destroy) {
		this.fadeOutTime = fadeOutTime;
		this.waitToFadeOut = waitToFadeOut;
		this.initialOpacity = initialOpacity;
		this.destroy = destroy;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		long timeSinceLastInteraction = component.getTimeSinceLastInteractionProperty().longValue();
		
		if(timeSinceLastInteraction < waitToFadeOut) {
			if(component.getOpacityProperty().get() != initialOpacity) {
				// reset opacity
				component.getOpacityProperty().set(initialOpacity);
			}
		} else if (timeSinceLastInteraction > waitToFadeOut + fadeOutTime && destroy) {
			// destroy component
			component.destroy();
		} else {
			// update opacity value -> fade out
			double targetOpacity = (1.0 - ((double) (timeSinceLastInteraction - waitToFadeOut) / (double) fadeOutTime)) * initialOpacity;
			component.getOpacityProperty().set(targetOpacity);
		}
	}
	
	// factory methods
	
	/**
	 * Creates a fade out and die behavior.
	 * 
	 * @param fadeOutTime Duration for the fade out.
	 * @param waitToFadeOut Time to wait until the fade out starts.
	 * @param initialOpacity Opacity to reset the opacity to after an interaction is performed. 
	 * 
	 * @return The newly created fade out and die behavior.
	 */
	public static FadeOutAndDieBehavior createFadeOutAndDieBehavior(long fadeOutTime, long waitToFadeOut, double initialOpacity) {
		return new FadeOutAndDieBehavior(fadeOutTime,
										 waitToFadeOut,
										 initialOpacity,
										 true);
	}

	/**
	 * Creates a fade out and die behavior with 0.0 opacity as initial opacity.
	 * 
	 * @param fadeOutTime Duration for the fade out.
	 * @param waitToFadeOut Time to wait until the fade out starts.
	 * 
	 * @return The newly created fade out and die behavior.
	 */
	public static FadeOutAndDieBehavior createFadeOutAndDieBehavior(long fadeOutTime, long waitToFadeOut) {
		return new FadeOutAndDieBehavior(fadeOutTime,
										 waitToFadeOut,
										 0.0,
										 true);
	}
}
