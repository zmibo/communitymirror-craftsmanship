package org.sociotech.communitymirror.communication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.sociotech.communityinteraction.eventcreators.CommunicationEventCreator;
import org.sociotech.communitymirror.CommunityMirror;

/**
 * Class to create a server socket and receive text messages from connected clients.
 *
 * @author Lösch
 *
 */
public class ServerToReceive {

	final int maxNumberOfClients = 3;

	final CommunicationEventCreator communicationEventCreator = CommunityMirror.getCommunicationEventCreator();

	public ServerToReceive(int port) {
		this.startServer(port);
	}

	/**
	 * Creates a new server socket and allows multiple client sockets to connect to this server socket.
	 *
	 * @param port	number of the port of the server socket
	 */
	private void startServer(int port) {
		final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(maxNumberOfClients);

		Runnable serverTask = new Runnable() {
			@Override
			public void run() {
				ServerSocket serverSocket = null;
				try {
					serverSocket = new ServerSocket(port, maxNumberOfClients);
					System.out.println("Waiting for clients to connect...");
					while (true) {
						Socket clientSocket = serverSocket.accept();
						clientProcessingPool.submit(new ClientTask(clientSocket));
					}
				} catch (IOException e) {
					System.err.println("Unable to process client request");
					e.printStackTrace();
				} finally {
					if(serverSocket != null) {
						try {
							serverSocket.close();
						} catch(IOException e) {
							System.err.println("Unable to close server socket");
							e.printStackTrace();
						}
					}
				}
			}
		};
		Thread serverThread = new Thread(serverTask);
		serverThread.start();
	}

	/**
	 * Runnable task to receive text messages from a client socket connected to this server socket and publish a message received event.
	 * @author Lösch
	 *
	 */
	private class ClientTask implements Runnable {

		private final Socket clientSocket;

		private ClientTask(Socket clientSocket) {
			this.clientSocket = clientSocket;
		}

		@Override
		public void run() {
			System.out.println("Got a client !");
			Scanner in = null;
			try {
				in = new Scanner(this.clientSocket.getInputStream());
				while (true) {
					// receive data from client
					String message = in.nextLine();
					// publish MessageReceivedEvent
					communicationEventCreator.createAndPublishMessageReceivedEvent(message);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(in != null) {
					in.close();
				}
			}
		}
	}
}
