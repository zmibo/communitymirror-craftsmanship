/**
 *
 */

package org.sociotech.communitymirror.graph.layout;

import java.util.List;

import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.VisualItem;

import com.facebook.rebound.BaseSpringSystem;

import javafx.scene.Group;

/**
 * @author User
 *
 */
public class CircleNodeSpringAngleLayouter extends CircleNodeSpringLayouter {

    public CircleNodeSpringAngleLayouter(List<VisualItem<?>> components, VisualItem<?> center, double radius,
            BaseSpringSystem springSystem, VisualGroup parentForSprings, Group parentForBackground, double startAngle,
            double microSpringLengthFactor, double previewSpringLengthFactor, double detailSpringLengthFactor,
            double backgroundCircleFactor, boolean reposition, String backgroundCircleColor,
            double backgroundCircleOpacity) {
        super(components, center, radius, springSystem, parentForSprings, parentForBackground, startAngle,
                microSpringLengthFactor, previewSpringLengthFactor, detailSpringLengthFactor, backgroundCircleFactor,
                reposition, backgroundCircleColor, backgroundCircleOpacity);
    }

    /**
     * Returns the starting angle for the layout.
     *
     * @return The starting angle.
     */
    @Override
    protected double getStartAngle() {
        return 2 * Math.PI * 135 / 360;
    }

    /**
     * Returns the angle between elements for the layout.
     *
     * @return The angle between elements.
     */
    @Override
    protected double getBetweenAngle() {
        return 2 * Math.PI * 35 / 360;
    }
}
