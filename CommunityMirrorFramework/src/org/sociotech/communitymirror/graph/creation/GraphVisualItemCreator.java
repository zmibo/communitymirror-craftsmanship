package org.sociotech.communitymirror.graph.creation;

import java.util.Set;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.graph.structure.GraphNode;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Interface that must be implemented to create elements for a graph.
 * 
 * @author Peter Lachenmaier
 */
public interface GraphVisualItemCreator {
	
	/**
	 * Creates a new visual item based on the given data item with the given visual state.
	 * 
	 * @param dataItem Data item to create the visual representation for
	 * @param visualState Visual state
	 * @param node The node the created visual item will belong to
	 * @return The new created visual item or null in error case
	 */
	public VisualItem<?> createVisualItem(Item dataItem, VisualState visualState, GraphNode node);
	
	/**
	 * Returns all items that are related to the given data item
	 * 
	 * @param dataItem Item to find related items for
	 * 
	 * @return Set of related items, might be empty.
	 */
	public Set<Item> getRelatedItemsFor(Item dataItem);
	
	/**
	 * Returns all items that are related to the given data item
	 * 
	 * @param dataItem Item to find related items for
	 * @param connectedItems Set of already connected related items
	 * 
	 * @return Set of related items, might be empty.
	 */
	public Set<Item> getSelectedRelatedItemsToConnectFor(Item dataItem, Set<Item> connectedItems);
	
	/**
	 * Destroys the given visual item and removes all references to it.
	 * 
	 * @param visualItem Visual item to destroy
	 */
	public void destroyVisualItem(VisualItem<?> visualItem);
}
