package org.sociotech.communitymirror.graph.structure;


/**
 * Representing one edge between two nodes in a {@link Graph}.
 * 
 * @author Peter Lachenmaier
 */
public class GraphEdge extends GraphElement {
	
	/**
	 * Beginning node. 
	 */
	protected final GraphNode from;
	
	/**
	 * Ending node. 
	 */
	protected final GraphNode to;

	/**
	 * Whether an angle is saved or not 
	 */
	private boolean angleSaved;

	/**
	 * The saved angle between the two nodes
	 */
	private double angle;
	
	/**
	 * Creates an edge between from and to.
	 * 
	 * @param from The node where the edge starts.
	 * @param to The node where the edge ends.
	 */
	public GraphEdge(GraphNode from, GraphNode to) {
		if(from == null) {
			throw new NullPointerException("From node must be defined.");
		}
		if(to == null) {
			throw new NullPointerException("To node must be defined.");
		}
		if(from.getGraph() != to.getGraph()) {
			throw new IllegalArgumentException("Nodes must be in same graph to be connected by an edge");
		}
		this.from = from;
		this.to = to;
		// maintain bidirectional associations
		this.from.add(this);
		this.to.add(this);
		this.setGraph(from.getGraph());
	}
	
	/**
	 * Returns the from node.
	 * 
	 * @return The from node
	 */
	public GraphNode getFrom() {
		return from;
	}

	/**
	 * Returns the from node.
	 * 
	 * @return The from node
	 */
	public GraphNode getTo() {
		return to;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof GraphEdge)) {
			return false;
		}
		
		// currently edges are bidirectional
		return (((GraphEdge)obj).from.equals(this.from) && ((GraphEdge)obj).to.equals(this.to))
			|| (((GraphEdge)obj).to.equals(this.from) && ((GraphEdge)obj).from.equals(this.to));
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 17 * from.hashCode() * to.hashCode();
	}
	
	/**
	 * True if this edge contains the given node.
	 * 
	 * @param node Node to check
	 * @return True if this edge contains the given node.
	 */
	public boolean containsNode(GraphNode node) {
		return this.to == node || this.from == node;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.graph.GraphElement#destroy()
	 */
	@Override
	public void destroy() {
		// currently nothing to do
	}

	/**
	 * Returns whether an angle is saved or not
	 * 
	 * @return Whether an angle is saved or not
	 */
	public boolean hasAngle() {
		return angleSaved;
	}

	/**
	 * Returns the angle of this edge
	 * 
	 * @param perspective To or From
	 * 
	 * @return The angle between to and from.
	 */
	public double getAngle(GraphNode perspective) {
		if(perspective == to) {
			return angle - Math.PI;
		} else if(perspective == from) {
			return angle;
		} else {
			throw new IllegalArgumentException("Perspective must be one of the connected nodes.");
		}
	}

	/**
	 * Sets the angle of this edge.
	 * 
	 * @param angle The angle
	 * @param perspective The perspective (must be on of the connected nodes)
	 */
	public void setAngle(double angle, GraphNode perspective) {
		if(perspective == to) {
			this.angle = Math.PI + angle;
		} else if(perspective == from) {
			this.angle = angle;	
		} else {
			throw new IllegalArgumentException("Perspective must be one of the connected nodes.");
		}
		angleSaved = true;
	}
}
