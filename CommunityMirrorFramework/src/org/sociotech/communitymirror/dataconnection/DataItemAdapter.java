package org.sociotech.communitymirror.dataconnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.sociotech.communitymashup.data.Item;

/**
 * Adapter to observe changes to data items. 
 * 
 * @author Peter Lachenmaier
 */
public class DataItemAdapter extends EContentAdapter {

	private final Logger logger = LogManager.getLogger(DataItemAdapter.class.getName());
	
	/**
	 * Object that observes data changes and will be notified. 
	 */
	private final DataItemChangedInterface observer;
	
	/**
	 * The data item to be observed. 
	 */
	private final Item dataItem;
	
	public DataItemAdapter(Item dataItem, DataItemChangedInterface observer) {
		this.observer = observer;
		this.dataItem = dataItem;
		// add adapter to data item
		if(this.dataItem != null) {
			this.dataItem.eAdapters().add(this);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.util.EContentAdapter#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification) {
		// handle notification to observer
		if(this.observer != null) {		
			observer.notifyChanged(notification);
		}
		logger.debug("Handled notification: " + notification);
	}

	/**
	 * Disconnects this adapter from the used data item.
	 */
	public void disconnect() {
		// remove from adapter list
		if(this.dataItem != null){
			this.dataItem.eAdapters().remove(this);
		}
		
	}
	
}
