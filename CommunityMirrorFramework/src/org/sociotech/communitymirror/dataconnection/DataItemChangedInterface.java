package org.sociotech.communitymirror.dataconnection;

import org.eclipse.emf.common.notify.Notification;

/**
 * Interface to easily observe changes of CommunityMashup data items.
 * 
 * @author Peter Lachenmaier
 */
public interface DataItemChangedInterface {

	/**
	 * This method will be called after the adapted data item has changed.
	 * 
	 * @param notification EMF notification containing details of the chage.
	 */
	public void notifyChanged(Notification notification);

}
