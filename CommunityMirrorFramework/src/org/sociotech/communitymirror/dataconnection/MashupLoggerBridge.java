package org.sociotech.communitymirror.dataconnection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Bridge to log messages form CommunityMashup components in the application specific way.
 * 
 * @author Peter Lachenmaier
 */
public class MashupLoggerBridge implements LogService{

	// TODO: should this be class specific instead of generic CommunityMashup logger
    /**
	 * The log4j logger reference.
	 */
	private final Logger logger = LogManager.getLogger("CommunityMashup");

	/* (non-Javadoc)
	 * @see org.osgi.service.log.LogService#log(int, java.lang.String)
	 */
	@Override
	public void log(int osgiLogLevel, String message) {
		//System.out.println("Level " + osgiLogLevel + ": " + message);
		
		// initialize with default
		Level outputLevel = Level.ALL;
		
		// map osgi level
		switch (osgiLogLevel) {
			case LogService.LOG_DEBUG:
				outputLevel = Level.DEBUG;
				break;
	
			case LogService.LOG_INFO:
				outputLevel = Level.INFO;
				break;
	
			case LogService.LOG_ERROR:
				outputLevel = Level.ERROR;
				break;
	
			case LogService.LOG_WARNING:
				outputLevel = Level.WARN;
				break;
	
			default:
				break;
		}
		
		logger.log(outputLevel, message);
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.log.LogService#log(int, java.lang.String, java.lang.Throwable)
	 */
	@Override
	public void log(int arg0, String arg1, Throwable arg2) {
		this.log(arg0, arg1);
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.log.LogService#log(org.osgi.framework.ServiceReference, int, java.lang.String)
	 */
	@Override
	public void log(@SuppressWarnings("rawtypes") ServiceReference arg0, int arg1, String arg2) {
		this.log(arg1, arg2);
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.log.LogService#log(org.osgi.framework.ServiceReference, int, java.lang.String, java.lang.Throwable)
	 */
	@Override
	public void log(@SuppressWarnings("rawtypes") ServiceReference arg0, int arg1, String arg2, Throwable arg3) {
		this.log(arg1, arg2);
	}

}
