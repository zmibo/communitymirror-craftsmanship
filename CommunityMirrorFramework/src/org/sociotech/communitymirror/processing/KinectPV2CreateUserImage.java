package org.sociotech.communitymirror.processing;
import java.util.ArrayList;

import KinectPV2.KJoint;
import KinectPV2.KSkeleton;
import KinectPV2.KinectPV2;
import processing.core.PApplet;
import processing.core.PImage;

public class KinectPV2CreateUserImage extends PApplet {

	/**
	 *
	 */
	private static final long serialVersionUID = 764104156190726062L;

	public static void main(String[] args) {
		PApplet.main("org.sociotech.communitymirror.processing.KinectPV2CreateUserImage");
	}

	KinectPV2 kinect;
	PImage colorImage;
	PImage scaledBodyTrackImg;

	public void settings() {
		size(1304, 1080*6);
	}

	public void setup() {
		kinect = new KinectPV2(this);

		kinect.enableColorImg(true);
		kinect.enableBodyTrackImg(true);

		kinect.enableSkeletonColorMap(true);
		kinect.enableSkeletonDepthMap(true);

		kinect.init();
	}

	public void draw() {
		background(255);

		// get color image
		colorImage = kinect.getColorImage();

		// create and display a user image for each tracked user
		@SuppressWarnings("unchecked")
		ArrayList<PImage> bodyTrackList = kinect.getBodyTrackUser();
		for(int userId = 0; userId< bodyTrackList.size(); userId++){
			// get body track image of user
			PImage bodyTrackImg = (PImage) bodyTrackList.get(userId);

//			// create silhouette user image
//			createUserImage(userId, bodyTrackImg, false);

			// create color user image
			createUserImage(userId, bodyTrackImg, true);
		}
	}

	private PImage createUserImage(int userId,  PImage bodyTrackImg, boolean useColorImage) {

			// resize body track image to 1304, 1080
			double resizeFactor = (double)colorImage.height/(double)bodyTrackImg.height;
			int newDepthWidth = (int) (bodyTrackImg.width * resizeFactor);
			int newDepthHeight = (int) (bodyTrackImg.height * resizeFactor);

			scaledBodyTrackImg = new PImage(newDepthWidth, newDepthHeight);
			scaledBodyTrackImg.copy(bodyTrackImg, 0, 0, bodyTrackImg.width, bodyTrackImg.height, 0, 0, newDepthWidth, newDepthHeight);
			scaledBodyTrackImg.loadPixels();

			// set user image
			PImage userImage = useColorImage ? colorImage : scaledBodyTrackImg;

			// cut user color image to size of body track image
			if(useColorImage){
				float jointColorX = 0;
				ArrayList<KSkeleton> skeletonArrayColor = kinect.getSkeletonColorMap();
				if (skeletonArrayColor.size() > userId) {
					KSkeleton skeletonColor = (KSkeleton) skeletonArrayColor.get(userId);
					if (skeletonColor.isTracked()) {
						KJoint[] joints = skeletonColor.getJoints();
						jointColorX = joints[KinectPV2.JointType_SpineMid].getX();
					}
				}
				float shiftDepthX = 0;
				ArrayList<KSkeleton> skeletonArrayDepth = kinect.getSkeletonDepthMap();
				if (skeletonArrayDepth.size() > userId) {
					KSkeleton skeletonDepth = (KSkeleton) skeletonArrayDepth.get(userId);
					if (skeletonDepth.isTracked()) {
						KJoint[] joints = skeletonDepth.getJoints();
						float jointDepthX = joints[KinectPV2.JointType_SpineMid].getX();
						// find joint position in scaled depth image
						jointDepthX = jointDepthX / 512 * newDepthWidth;
						// calculate shift from color to depth
						shiftDepthX = (jointColorX - jointDepthX);
					}
				}
				userImage = userImage.get((int) shiftDepthX, 0, newDepthWidth, newDepthHeight);
			}

			// make other pixels transparent and find min/max - values
			// initialize min/max-values
			int minX = userImage.width;
			int minY = userImage.height;
			int maxX = 0;
			int maxY = 0;
			// go line by line through all pixels of body track image
			for (int x = 0; x < scaledBodyTrackImg.width; x++) {
				for (int y = 0; y < scaledBodyTrackImg.height; y++) {
					int index = y * scaledBodyTrackImg.width + x;
					int pixelColor = scaledBodyTrackImg.pixels[index];
					// if pixel belongs to user
					if (pixelColor != 0) {
						// update minX, minY and maxX, maxY
						minX = x < minX ? x : minX;
						minY = y < minY ? y : minY;
						maxX = x > maxX ? x : maxX;
						maxY = y > maxY ? x : maxY;
					} else {
						// make other pixels in color image transparent
						int i = y * userImage.width + x;
						userImage.loadPixels();
						userImage.pixels[i] = 0x00FFFFFF;
					}
				}
			}

			// cut user area from color image
			userImage.updatePixels();
			int w = Math.max((Math.min(userImage.width, maxX) - minX), 0);
			int h = Math.max((Math.min(userImage.height, maxY) - minY), 0);
			userImage = userImage.get(minX, minY, w, h);

			image(userImage, 0, 500 * userId, userImage.width/2, userImage.height/2);
			image(bodyTrackImg, 500, 500 * userId, bodyTrackImg.width, bodyTrackImg.height);
			//image(userImage, minX, minY + userImage.height * userId);

			return userImage;
	}
}
