package org.sociotech.communitymirror.processing;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

import KinectPV2.KJoint;
import KinectPV2.KSkeleton;
import KinectPV2.KinectPV2;
import javafx.concurrent.Task;
import processing.core.PImage;
import processing.core.PVector;

public class KinectV2UserUpdator extends UserUpdator {
	private PImage bodyTrackImg;
	private PImage colorImage;
	private KinectPV2 kinect;
	private long[] loggingTimes;
	
	private int oldDepthWidth;
	private int oldDepthHeight;
	private int newDepthWidth;
	private int newDepthHeight;
	private PImage scaledBodyTrackImg;
	private PImage userImage;
	private float jointPositionX_InUserImage;
	private float jointPositionY_InUserImage;
	private float jointPositionX2_InUserImage;
	private float jointPositionY2_InUserImage;
	private int minX;
	private int minY;
	private int maxX;
	private int maxY;
	private PVector jointDepth1_PVector;
	private PVector jointColor1_PVector;
	private PVector jointColor2_PVector;
	private PVector correctedPosition1_mm;
	private PVector correctedPosition2_mm;

	public KinectV2UserUpdator(VisualUserRepresentation visualUserRepresentation, PImage bodyTrackImg,
			PImage colorImage, KinectPV2 kinect, long[] loggingTimes) {
		super(visualUserRepresentation);
		this.bodyTrackImg = bodyTrackImg;
		this.colorImage = colorImage;
		this.kinect = kinect;
		this.loggingTimes = loggingTimes;
		
		this.updateUser();
	}

	private void updateUser() {

		//long timeBeforeUpdate = System.nanoTime();

		if (bodyTrackImg != null) {
			ExecutorService exec = Executors.newSingleThreadExecutor();
			
			// (0) get all joint needed joint positions
			exec.submit(new Task<Void>() {
				protected Void call() {
					getJointPositions(KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);
					return null;
				}
			});

			// (1a) resize body track image to map color image
			exec.submit(new Task<Void>() {
				protected Void call() {
					createScaledBodyTrackImage();
					return null;
				}
			});

			// (1b) get joint positions in depth image
			exec.submit(new Task<Void>() {
				protected Void call() {
					cutUserImageToSizeOfBodyTrackImage();
					return null;
				}
			});

			// (1c) make other pixels transparent and find min/max - values
			exec.submit(new Task<Void>() {
				protected Void call() {
					makeOtherPixelsTransparent();
					return null;
				}
			});

			// (1d) cut user area from color image
			exec.submit(new Task<Void>() {
				protected Void call() {
					cutUserArea();
					return null;
				}
			});

			// (2) create JavaFX Image
			exec.submit(new Task<Void>() {
				protected Void call() {
					createNextVisualUserRepresentationImage(visualUserRepresentation, userImage.pixels, userImage.width,
							userImage.height);
					return null;
				}
			});

			// (3) update visibility, position, positionInUserImage and
			// orientation
			exec.submit(new Task<Void>() {
				protected Void call() {
					updateTrackedUser();
					return null;
				}
			});

			//long timeAfterUpdate = System.nanoTime();
			//System.out.println("updateTime: " + (timeAfterUpdate - timeBeforeUpdate));
		} else {
			// update not tracked user
			updateNotTrackedUser();
		}
	}
	
	/**
	 * needs skeleton3d.getJoints
	 * needs kinect
	 * 
	 * @param userId
	 * @param joint1_Type
	 * @param joint2_Type
	 * @return
	 */
	private void getJointPositions(int joint1_Type, int joint2_Type) {
		// get list of 3d skeletons
		ArrayList<KSkeleton> skeletons3d = kinect.getSkeleton3d();
			// (1) get 3d joint positions
			// get tilt angle of camera from configuration
			double tilt = CommunityMirror.getConfiguration().getDouble("sensing.kinect.tilt", 0);
			KSkeleton skeleton = skeletons3d.get(userId);
			KJoint[] joints = skeleton.getJoints();

			// get position of first joint
			KJoint joint1 = joints[joint1_Type];
			PVector position1 = new PVector(joint1.getX(), joint1.getY(), joint1.getZ());
			// correct position according to tilt
			PVector correctedPosition1 = correctJointPositionsAccordingToCameraTilt(tilt, position1);
			// convert position values from m to mm
			correctedPosition1_mm = new PVector(correctedPosition1.x *1000, correctedPosition1.y*1000, correctedPosition1.z*1000);

			// get position of second joint
			KJoint joint2 = joints[joint2_Type];
			PVector position2 = new PVector(joint2.getX(), joint2.getY(), joint2.getZ());
			// correct position according to tilt
			PVector correctedPosition2 = correctJointPositionsAccordingToCameraTilt(tilt, position2);
			// convert position values from m to mm
			correctedPosition2_mm = new PVector(correctedPosition2.x *1000, correctedPosition2.y*1000, correctedPosition2.z*1000);
			
			// (2) get joint position in depth image
			jointDepth1_PVector = kinect.MapCameraPointToDepthSpace(position1);
//			System.out.println("jointDepth1_PVector: " + jointDepth1_PVector);
			
//			ArrayList<KSkeleton> skeleltonsDepth = kinect.getSkeletonDepthMap();
//			KSkeleton skelDepth = skeleltonsDepth.get(userId);
//			KJoint[] depthJoints = skelDepth.getJoints();
//			KJoint depthJoint1 = depthJoints[joint1_Type];
//			jointDepth1_PVector = depthJoint1.getPosition();
//			System.out.println("jointDepth1_PVector: " + jointDepth1_PVector);

//			 (3) get joint positions in color image
			jointColor1_PVector = kinect.MapCameraPointToColorSpace(position1);
			jointColor2_PVector = kinect.MapCameraPointToColorSpace(position2);
			
//			System.out.println("jointColor1_PVector: " + jointColor1_PVector);
//			System.out.println("jointColor2_PVector: " + jointColor2_PVector);
			
//			ArrayList<KSkeleton> skeleltonsColor = kinect.getSkeletonColorMap();
//			KSkeleton skelColor = skeleltonsColor.get(userId);
//			KJoint[] colorJoints = skelColor.getJoints();
//			
//			KJoint colorJoint1 = colorJoints[joint1_Type];
//			jointColor1_PVector = colorJoint1.getPosition();
//			KJoint colorJoint2 = colorJoints[joint2_Type];
//			jointColor2_PVector = colorJoint2.getPosition();
//			
//			System.out.println("**jointColor1_PVector: " + jointColor1_PVector);
//			System.out.println("**jointColor2_PVector: " + jointColor2_PVector);
			
			// log joint positions to csv file
			logJointPositions(joints, tilt);
	}	

	/**
	 * @param joints
	 */
	private void logJointPositions(KJoint[] joints, double tilt) {
		// if last logging entry was within the last second, do nothing
		long time = System.currentTimeMillis();
		if(time - this.loggingTimes[userId] < 500){
			return;
		}
		// update logging time
		this.loggingTimes[userId] = time;
		
		// get positions for logging
		KJoint head = joints[KinectPV2.JointType_Head];
		PVector head_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(head.getX(), head.getY(), head.getZ()));
		
		KJoint spineShoulder = joints[KinectPV2.JointType_SpineShoulder];
		PVector spineShoulder_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(spineShoulder.getX(), spineShoulder.getY(), spineShoulder.getZ()));
		
		KJoint shoulderLeft = joints[KinectPV2.JointType_ShoulderLeft];
		PVector shoulderLeft_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(shoulderLeft.getX(), shoulderLeft.getY(), shoulderLeft.getZ()));
		
		KJoint shoulderRight = joints[KinectPV2.JointType_ShoulderRight];
		PVector shoulderRight_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(shoulderRight.getX(), shoulderRight.getY(), shoulderRight.getZ()));
		
		KJoint spineMid = joints[KinectPV2.JointType_SpineMid];
		PVector spineMid_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(spineMid.getX(), spineMid.getY(), spineMid.getZ()));
		
		KJoint spineBase = joints[KinectPV2.JointType_SpineBase];
		PVector spineBase_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(spineBase.getX(), spineBase.getY(), spineBase.getZ()));
		
		KJoint hipLeft = joints[KinectPV2.JointType_HipLeft];
		PVector hipLeft_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(hipLeft.getX(), hipLeft.getY(), hipLeft.getZ()));
		
		KJoint hipRight = joints[KinectPV2.JointType_HipRight];
		PVector hipRight_correctedPosition = this.correctJointPositionsAccordingToCameraTilt(tilt, new PVector(hipRight.getX(), hipRight.getY(), hipRight.getZ()));
		
		// log kinect data
		UserActivityLogger userLogger = CommunityMirror.getUserActivityLogger();
		userLogger.logActivity(
				userLogger.detail("TimeStamp", new Timestamp(Calendar.getInstance().getTime().getTime()).toString()),
				userLogger.detail("LoggingType", "KinectData"),
				userLogger.detail("ScreenPosition", CommunityMirror.getConfiguration().getInt("screenPosition", 0)),
				userLogger.detail("UserID", userId),

				userLogger.detail("HeadPositionX", head_correctedPosition.x * 1000),
				userLogger.detail("HeadPositionY", head_correctedPosition.y * 1000),
				userLogger.detail("HeadPositionZ", head_correctedPosition.z * 1000),

				userLogger.detail("ShoulderLeftPositionX", shoulderLeft_correctedPosition.x * 1000),
				userLogger.detail("ShoulderLeftPositionY", shoulderLeft_correctedPosition.y * 1000),
				userLogger.detail("ShoulderLeftPositionZ", shoulderLeft_correctedPosition.z * 1000),

				userLogger.detail("ShoulderRightPositionX", shoulderRight_correctedPosition.x * 1000),
				userLogger.detail("ShoulderRightPositionY", shoulderRight_correctedPosition.y * 1000),
				userLogger.detail("ShoulderRightPositionZ", shoulderRight_correctedPosition.z * 1000),

				userLogger.detail("SpineShoulderPositionX", spineShoulder_correctedPosition.x * 1000),
				userLogger.detail("SpineShoulderPositionY", spineShoulder_correctedPosition.y * 1000),
				userLogger.detail("SpineShoulderPositionZ", spineShoulder_correctedPosition.z * 1000),

				userLogger.detail("SpineMidPositionX", spineMid_correctedPosition.x * 1000),
				userLogger.detail("SpineMidPositionY", spineMid_correctedPosition.y * 1000),
				userLogger.detail("SpineMidPositionZ", spineMid_correctedPosition.z * 1000),

				userLogger.detail("SpineBasePositionX", spineBase_correctedPosition.x * 1000),
				userLogger.detail("SpineBasePositionY", spineBase_correctedPosition.y * 1000),
				userLogger.detail("SpineBasePositionZ", spineBase_correctedPosition.z * 1000),

				userLogger.detail("HipLeftPositionX", hipLeft_correctedPosition.x * 1000),
				userLogger.detail("HipLeftPositionY", hipLeft_correctedPosition.y * 1000),
				userLogger.detail("HipLeftPositionZ", hipLeft_correctedPosition.z * 1000),

				userLogger.detail("HipRightPositionX", hipRight_correctedPosition.x * 1000),
				userLogger.detail("HipRightPositionY", hipRight_correctedPosition.y * 1000),
				userLogger.detail("HipRightPositionZ", hipRight_correctedPosition.z * 1000));
	}

	/**
	 * This method corrects a given position vector according to a given
	 * downward tilt of camera. The given position vector is transformed to
	 * match the coordinate system of a not tilted camera.
	 * 
	 * @param tilt
	 *            : the angle by which the camera is tilted downwards
	 * @param position
	 *            : the position vector that should be corrected
	 * @return the corrected position vector
	 */
	private PVector correctJointPositionsAccordingToCameraTilt(double tilt, PVector position) {
		if(tilt == 0){
			return position;
		}
		
		float posX = position.x;
		float posY = position.y;
		float posZ = position.z;

		double sin = Math.sin(Math.toRadians(tilt));
		double cos = Math.cos(Math.toRadians(tilt));

		float correctedPosY = (float) (0 * posX + cos * posY - sin * posZ);
		float correctedPosZ = (float) (0 * posX + sin * posY + cos * posZ);

		return new PVector(posX, correctedPosY, correctedPosZ);
	}

	/**
	 * 
	 */
	private void createScaledBodyTrackImage() {
		// (1) resize body track image to map color image
		double verticalAngleOfViewColor = 53.8;
		double verticalAngleOfViewDepth = 60;
		double adaptedColorImageHeight = colorImage.height * verticalAngleOfViewDepth / verticalAngleOfViewColor;
		double resizeFactor = (double) adaptedColorImageHeight / (double) bodyTrackImg.height;
		oldDepthWidth = bodyTrackImg.width; // 512
		oldDepthHeight = bodyTrackImg.height; // 424
		newDepthWidth = (int) (bodyTrackImg.width * resizeFactor);
		newDepthHeight = (int) (bodyTrackImg.height * resizeFactor);

		// long timeBeforeScaling = System.currentTimeMillis();

		// scaledBodyTrackImg = new PImage(newDepthWidth, newDepthHeight);
		// scaledBodyTrackImg.copy(bodyTrackImg, 0, 0, bodyTrackImg.width,
		// bodyTrackImg.height, 0, 0, newDepthWidth,
		// newDepthHeight);

		scaledBodyTrackImg = scaleImage(bodyTrackImg, newDepthWidth, newDepthHeight);
		scaledBodyTrackImg.updatePixels();

		// long timeAfterScaling = System.currentTimeMillis();
		// System.out.println("scalingTime: " + (timeAfterScaling -
		// timeBeforeScaling));

		scaledBodyTrackImg.loadPixels();
	}

	private PImage scaleImage(PImage original, int newWidth, int newHeight) {
		PImage output = new PImage(newWidth, newHeight);
		original.loadPixels();
		output.loadPixels();

		// YD compensates for the x loop by subtracting the width back out
		int YD = (original.height / newHeight) * original.width - original.width;
		int YR = original.height % newHeight;
		int XD = original.width / newWidth;
		int XR = original.width % newWidth;
		int outOffset = 0;
		int inOffset = 0;

		for (int y = newHeight, YE = 0; y > 0; y--) {
			for (int x = newWidth, XE = 0; x > 0; x--) {
				output.pixels[outOffset++] = original.pixels[inOffset];
				inOffset += XD;
				XE += XR;
				if (XE >= newWidth) {
					XE -= newWidth;
					inOffset++;
				}
			}
			inOffset += YD;
			YE += YR;
			if (YE >= newHeight) {
				YE -= newHeight;
				inOffset += original.width;
			}
		}
		return output;
	}

	/**
	 * 
	 */
	private void cutUserImageToSizeOfBodyTrackImage() {
		// (2a) find joint position in scaled depth image
		float jointDepthX = jointDepth1_PVector.x / oldDepthWidth * newDepthWidth;
		float jointDepthY = jointDepth1_PVector.y / oldDepthHeight * newDepthHeight;

		// (2b) calculate shift from color to depth
		float shiftDepthX = (jointColor1_PVector.x - jointDepthX);
		float shiftDepthY = (jointColor1_PVector.y - jointDepthY);

		// (2c) cut user image to size of body track image
		userImage = colorImage;
		userImage = userImage.get((int) shiftDepthX, (int) shiftDepthY, newDepthWidth, userImage.height);
		userImage.loadPixels();

		// (2d) calculate joint position in user image
		jointPositionX_InUserImage = jointColor1_PVector.x - shiftDepthX;
		jointPositionY_InUserImage = jointColor1_PVector.y - shiftDepthY;

		// (2e) calculate joint position in user image
		jointPositionX2_InUserImage = jointColor2_PVector.x - shiftDepthX;
		jointPositionY2_InUserImage = jointColor2_PVector.y - shiftDepthY;
	}

	/**
	 * 
	 */
	private void makeOtherPixelsTransparent() {
		// (3) make other pixels transparent and find min/max - values
		// get user index color
		ArrayList<KSkeleton> skeletonArrayDepth = kinect.getSkeletonDepthMap();
		KSkeleton skeletonDepth = (KSkeleton) skeletonArrayDepth.get(userId);
		int userIndexColor = skeletonDepth.getIndexColor();
		
		// find min/max-values
		minX = userImage.width;
		minY = userImage.height;
		maxX = 0;
		maxY = 0;
		// go line by line through all pixels of body track image
		for (int x = 0; x < newDepthWidth; x++) {
			for (int y = 0; y < newDepthHeight; y++) {
				int index = y * newDepthWidth + x;
				int pixelColor = scaledBodyTrackImg.pixels[index];
				int i = y * userImage.width + x;
				// if pixel belongs to user
				if (pixelColor != 0) {
					// update minX, minY and maxX, maxY
					minX = x < minX ? x : minX;
					minY = y < minY ? y : minY;
					maxX = x > maxX ? x : maxX;
					maxY = y > maxY ? y : maxY;
					// copy pixels from scaledBodyTRackImg to userImage
					boolean useColorImage = visualUserRepresentation.getCurrentState().getImageCreator().isRGBImage();
					if (!useColorImage) {
						userImage.pixels[i] = userIndexColor;
					}
				} else {
					// make other pixels in color image transparent
					if (i < userImage.pixels.length) {
						userImage.pixels[i] = 0x00FFFFFF;
					}
				}
			}
		}
	}

	/**
	 * 
	 */
	private void cutUserArea() {
		// (4) cut user area from color image
		userImage.updatePixels();
		int w = Math.max((Math.min(userImage.width, maxX) - minX), 0);
		int h = Math.max((Math.min(userImage.height, maxY) - minY), 0);
		userImage = userImage.get(minX, minY, w, h);

		// calculate and store joint position in cut user area image
		positionInUserImage[0] = jointPositionX_InUserImage - minX;
		positionInUserImage[1] = jointPositionY_InUserImage - minY;

		// calculate and store joint position in cut user area image
		position2InUserImage[0] = jointPositionX2_InUserImage - minX;
		position2InUserImage[1] = jointPositionY2_InUserImage - minY;

		// load pixels
		userImage.loadPixels();
	}
	
	/**
	 * 
	 */
	protected void updateTrackedUser() {
		// update user
		CommunityMirror.getUserController().updateUser(userId, true, correctedPosition1_mm.array(), correctedPosition2_mm.array(), null,
				positionInUserImage, position2InUserImage);
	}

	/**
	 * Creates a new PImage for a given user. The image is created as color
	 * image if createRGB is true and else as silhouette image.
	 * 
	 * @return the new created PImage for the user
	 */
	@SuppressWarnings("unused")
	private PImage createUserPImage() {

		//long timeBeforeUpdate = System.nanoTime();
	
		// (0) get and log all needed joint positions
		getJointPositions(KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);

		// (1) resize body track image to map color image
		createScaledBodyTrackImage();

		// (2) get joint positions in depth image
		cutUserImageToSizeOfBodyTrackImage();

		// (3) make other pixels transparent and find min/max - values
		makeOtherPixelsTransparent();

		// (4) cut user area from color image
		cutUserArea();

		//long timeAfterUpdate = System.nanoTime();
		//System.out.println("creationTime: " + (timeAfterUpdate - timeBeforeUpdate));

		// (5) create an set javafx user representation image
		createNextVisualUserRepresentationImage(visualUserRepresentation, userImage.pixels, userImage.width,
				userImage.height);

		// (6) update user
		updateTrackedUser();

		return userImage;
	}

}
