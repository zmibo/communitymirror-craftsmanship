package org.sociotech.communitymirror.processing;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

import SimpleOpenNI.SimpleOpenNI;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Class for receiving and using data from windows kinect version 1 using processing libraries.
 * 
 * @author loesch
 *
 */
public class KinectV1Processing extends KinectProcessing {

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1031834452727101463L;

	private final Logger logger = LogManager.getLogger(KinectV1Processing.class
			.getName());

	SimpleOpenNI context;
	PImage cam;
	// kinect dimension: 640 x 480;
	int imageWidth = 640;
	int imageHeight = 480;

	// holds for each pixel userId of user represented by this pixel
	// or holds 0 else
	private int[] userMap;

	public KinectV1Processing(
			List<VisualUserRepresentation> visualuserRepresentations) {
		super(visualuserRepresentations);
		setup();
	}

	public void setup() {
		logger.info("setup processing kinect");
		//context = new SimpleOpenNI(this);
		context = new SimpleOpenNI(new PApplet());
		context.enableDepth();
		context.enableRGB();
		context.alternativeViewPointDepthToImage();
		if (!context.enableUser()) {
			logger.warn("Kinect not connected!");
			//exit();
		} else {
			// mirror the image to be more intuitive
			context.setMirror(true);
			// set scene map array
			userMap = new int[imageWidth * imageHeight];
		}
		
		startUpdateService();
	}

	private void startUpdateService(){
		ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(new Runnable(){
            @Override
            public void run(){;
                updateUsers();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
	}

	private void updateUsers() {
		// create list of tracked stati of all users
		boolean[] trackedStati = new boolean[] { false, false, false, false,
				false, false };
		// update kinect data
		context.update();

		// get all tracked users
		int[] trackedUsers = context.getUsers();
		logger.debug("number of users: " + trackedUsers.length);

		// update all tracked users
		for (int i = 0; i < trackedUsers.length; i++) {
			int userId = trackedUsers[i];
			//update all tracked users
			updatTrackedUser(userId);
			trackedStati[userId - 1] = true;
		}

		// update all not tracked users
		updateNotTrackedUsers(trackedStati);
	}
	
	private void updatTrackedUser(int userId) {
		// create task to create user image
		@SuppressWarnings("unused")
		KinectV1UserUpdator task = new KinectV1UserUpdator(
				visualuserRepresentations.get(userId - 1), context, userMap);
	}

	/**
	 * 
	 * @param trackedStati
	 */
	private void updateNotTrackedUsers(boolean[] trackedStati) {
		for (int i = 0; i < trackedStati.length; i++) {
			if (!trackedStati[i]) {
				// update tracked status in user object
				//int userId = i + 1;
				CommunityMirror.getUserController().updateUser(i, false, null, null, null, null, null);
			}
		}
	}
}
