
package org.sociotech.communitymirror.configuration;

import org.apache.commons.configuration2.CombinedConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.combined.CombinedConfigurationBuilder;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class to load a mirror configuration from a file.
 *
 * @author Peter Lachenmaier
 */
public class ConfigurationLoader {
    /**
     * Class specific logger.
     */
    private static final Logger logger = LogManager.getLogger(ConfigurationLoader.class.getName());

    /**
     * Loads the configuration from the configured path and returns it.
     *
     * @param path
     *            Path of the configuration file
     * @return The loaded configuration. Null if the configuration does not
     *         exist.
     */
    public static CombinedConfiguration load(String path) {

        // create a configuration builder
        org.apache.commons.configuration2.builder.fluent.Parameters params = new org.apache.commons.configuration2.builder.fluent.Parameters();
        CombinedConfigurationBuilder builder = new CombinedConfigurationBuilder().configure(params.fileBased()
                .setURL(org.sociotech.communitymirror.CommunityMirror.class.getClassLoader().getResource(path)));

        // load configuration
        try {
            return builder.getConfiguration();
        } catch (ConfigurationException e) {
            ConfigurationLoader.logger.error("Could not load a configuration from " + path, e);
            return null;
        }

    }

    static public String[] getStringArray(Configuration config, String aname) {
        String tmps = config.getString(aname);
        if (tmps == null) {
            return null;
        }
        String[] result = tmps.split("[\\s]*,[\\s]*");
        return result;
    }
}
