package org.sociotech.communitymirror.configuration;

import javafx.stage.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: i21bmabu
 * Date: 30.09.13
 * Time: 17:13
 * To change this template use File | Settings | File Templates.
 */
public class ViewPort {
    private Stage stage;

    public ViewPort(Stage stage) {
        //To change body of created methods use File | Settings | File Templates.
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
