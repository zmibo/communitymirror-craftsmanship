
package org.sociotech.communitymirror.configuration;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.io.Resources;

/**
 * Helper class to load resources from configured folders.
 *
 * @author Peter Lachenmaier
 */
public class ResourceLoader {

    /**
     * The list of folders to check for resources, in order.
     */
    private List<String> resourceFolders;

    /**
     * Creates a resource loader with a specific folder and a default folder
     * as fallback if the needed resource is not contained in the specific
     * folder.
     *
     * @param specificFolder
     *            The specific folder to look for resources.
     * @param defaultFolder
     *            The default folder to look for resources.
     */
    public ResourceLoader(String specificFolder, String defaultFolder) {
        this(Arrays.asList(specificFolder, defaultFolder));
    }

    /**
     * Creates a resource loader with a specific folder and a default folder
     * as fallback if the needed resource is not contained in the specific
     * folder.
     *
     * @param resourceFolders
     *            The folders that are checked, in order, for requested
     *            resources. For example: first application-specific resource
     *            folder, second general fallback or default folder.
     */
    public ResourceLoader(Iterable<String> resourceFolders) {
        this.resourceFolders = new ArrayList<>();
        for (String resourceFolder : resourceFolders) {
            if (!resourceFolder.endsWith("/")) {
                resourceFolder = resourceFolder + "/";
            }
            this.resourceFolders.add(resourceFolder);
        }
    }

    /**
     * Returns a list of all files found within a specific path.
     *
     * @param resourcePath
     *            The resource path (relative to the configured resource
     *            folders) that should be enumerated.
     *
     * @return A list of all files (as URLs) found in the requested directory.
     */
    @SuppressWarnings("deprecation")
    public List<URL> getFolderContents(String resourcePath) {
        URL resource = null;
        List<URL> result = new ArrayList<>();
        Set<String> foundFilenames = new HashSet<>();
        for (String resourceFolder : this.resourceFolders) {
            try {
                resource = Resources.getResource(resourceFolder + resourcePath);
                if (resource != null) {
                    File foundDir = new File(resource.getPath());
                    if (foundDir.isDirectory()) {
                        File[] files = foundDir.listFiles();
                        for (File foundFile : files) {
                            if (!foundFilenames.contains(foundFile.getName())) {
                                result.add(foundFile.toURL());
                                foundFilenames.add(foundFile.getName());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // do nothing. move on to next folder.
            }
        }
        return result;
    }

    /**
     * Returns the url string of the resource identified by the given resource
     * path. Looks first in
     * the specific folder and if not found in the default folder.
     *
     * @param resourcePath
     *            Resource path relative to the configured specific and default
     *            folder.
     *
     * @return The Url of the resource identified by the given relative resource
     *         path.
     */
    public String getResourceURLString(String resourcePath) {
        URL resource = this.getResourceURL(resourcePath);

        if (resource != null) {
            return resource.toExternalForm();
        }

        return null;
    }

    /**
     * Returns the url of the resource identified by the given resource path.
     * Looks first in
     * the specific folder and if not found in the default folder.
     *
     * @param resourcePath
     *            Resource path relative to the configured specific and default
     *            folder.
     *
     * @return The Url of the resource identified by the given relative resource
     *         path.
     */
    public URL getResourceURL(String resourcePath) {
        URL resource = null;
        for (String resourceFolder : this.resourceFolders) {
            try {
                resource = Resources.getResource(resourceFolder + resourcePath);
            } catch (Exception e) {
                // do nothing. Resourse stays null
            }
            if (resource != null) {
                break;
            }
        }
        return resource;
    }
}
