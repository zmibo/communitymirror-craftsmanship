/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.screen;


public class ScreenSettings {

    private final int     m_monitorId;
    private final boolean m_fullscreenMode;
    private final boolean m_decorated;

    public ScreenSettings(int monitorId, boolean fullscreenMode, boolean decorated) {

        m_monitorId = monitorId;
        m_fullscreenMode = fullscreenMode;
        m_decorated = decorated;
    }

    public int getMonitorId() {
        return m_monitorId;
    }

    public boolean isFullscreenMode() {
        return m_fullscreenMode;
    }

    public boolean isDecorated() {
        return m_decorated;
    }
}
