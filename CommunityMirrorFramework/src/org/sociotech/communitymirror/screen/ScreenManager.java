/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.screen;

import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ScreenManager {

    public static Stage getStageForMonitor(ScreenSettings screenSettings) {

        int monitorId = org.sociotech.communitymirror.utils.Math.clamp(screenSettings.getMonitorId(), 0,
                                                                       Screen.getScreens().size());
        Stage stage = new Stage();

        // Get Screen for Monitor
        Screen screen = Screen.getScreens().get(monitorId);

        // Set Fullscreen Mode
        stage.setFullScreen(screenSettings.isFullscreenMode());

        // Set Decoration Mode
        if(!screenSettings.isDecorated()) {
            stage.initStyle(StageStyle.UNDECORATED);
        }

        // Set the stage's position to the Monitor's position
        stage.setX(screen.getVisualBounds().getMinX());
        stage.setY(screen.getVisualBounds().getMinY());

        // Set the stage's dimensions to the Monitor's dimensions
        if(!screenSettings.isFullscreenMode()) {
            stage.setWidth(screen.getVisualBounds().getWidth());
            stage.setHeight(screen.getVisualBounds().getHeight());
        } else {
            stage.setWidth(screen.getBounds().getWidth());
            stage.setHeight(screen.getBounds().getHeight());
        }

        return stage;
    }
}

