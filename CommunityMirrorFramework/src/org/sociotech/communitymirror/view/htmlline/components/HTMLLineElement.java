package org.sociotech.communitymirror.view.htmlline.components;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.view.htmlline.visualitems.HTMLItem;
import org.sociotech.communitymirror.view.picture.components.ItemLine;
import org.sociotech.communitymirror.view.picture.components.ItemLineElement;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * A visual html item that can be used as line element.
 * 
 * @author Peter Lachenmaier
 *
 */
public class HTMLLineElement extends HTMLItem implements ItemLineElement {

	private ItemLine line;

	/**
	 * Creates a new line element with the given state and height as representation
	 * of the given data item.
	 * 
	 * @param dataItem Data item to be represented
	 * @param state Visual state
	 * @param height Supposed height 
	 */
	public HTMLLineElement(Item dataItem, VisualState state, double height, String baseUrl, String tpl) {
		super(dataItem, state, height, baseUrl, tpl);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getX()
	 */
	@Override
	public double getX() {
		return this.getLayoutX();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setX(double)
	 */
	@Override
	public void setX(double x) {
		this.setLayoutX(x);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setY(double)
	 */
	@Override
	public void setY(double y) {
		this.setLayoutY(y);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getWidth()
	 */
	@Override
	public double getWidth() {
		return width;
		
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setLine(org.sociotech.communitymirror.view.picture.components.ItemLine)
	 */
	@Override
	public void setLine(ItemLine itemLine) {
		this.line = itemLine;		
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getLine()
	 */
	@Override
	public ItemLine getLine() {
		return line;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#isInAnimation()
	 */
	@Override
	public boolean isInAnimation() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#isValid()
	 */
	@Override
	public boolean isValid() {
		return true;
	}
}
