package org.sociotech.communitymirror.view.htmlline;

import javafx.geometry.VPos;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import org.apache.commons.configuration2.Configuration;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymirror.utils.RandomFactory;
import org.sociotech.communitymirror.view.htmlline.components.HTMLLineElementRenderer;
import org.sociotech.communitymirror.view.picture.PictureView;
import org.sociotech.communitymirror.view.picture.components.ItemLine;
import org.sociotech.communitymirror.view.picture.components.ItemLineElement;
import org.sociotech.communitymirror.visualstates.VisualState;


/**
 * Basic line view implementation with HTML Renderings
 * 
 * @author Peter Lachenmaier
 */
public class HTMLLineView extends PictureView {

	private EList<InformationObject> informationObjects = null;
	private HTMLLineElementRenderer htmlRenderer;

	/**
	 * Creates a generic html line view-
	 * 
	 * @param width The width of the view
	 * @param height The height of the view
	 * @param configuration The overall configuration
	 * @param dataSet The data set
	 */
	public HTMLLineView(double width, double height,
			Configuration configuration, DataSet dataSet) {
		super(width, height, configuration, dataSet);
		htmlRenderer = new HTMLLineElementRenderer();
		htmlRenderer.setBaseUrl(configuration.getString("mashup.htmlinterface"));
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.PictureView#produceNextLineElement(double, org.sociotech.communitymirror.view.picture.components.ItemLine)
	 */
	@Override
	public ItemLineElement produceNextLineElement(double height, ItemLine line) {
		
		this.updateReferencesOnDemand();
		
		// check if there are tweets to render
		if(informationObjects == null || informationObjects.isEmpty()) {
			return null;
		}
		
		// pick a random information object to render
		int randIndex = RandomFactory.getRandomInt(informationObjects.size());

		// set height for rendering
		htmlRenderer.setHeight(height);

		return htmlRenderer.renderItem(informationObjects.get(randIndex), VisualState.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.PictureView#onInit()
	 */
	@Override
	protected void onInit() {
		
		// get title from configuration if set
		String titleVal = "CommunityMirror";
		if(configuration.getString("view.title") != null) {
			titleVal = configuration.getString("view.title");
		}
		
		// add title
		Text title = new Text(titleVal);
		addNode(title);
		
		title.setFont(new Font(70.0));
		title.setFill(Color.WHITE);
		title.setTextOrigin(VPos.CENTER);
		title.setLayoutY(topOffset/2.0);
		title.setLayoutX(getWidth() - title.getLayoutBounds().getWidth() - 30.0);
		
		// set space between lines
		spaceBetween = 10.0;
		
		super.onInit();
		
		// initialize data
		updateReferencesOnDemand();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.PictureView#updateReferences()
	 */
	@Override
	protected void updateReferences() {
		informationObjects = dataSet.getInformationObjects();		
	}

	
}
