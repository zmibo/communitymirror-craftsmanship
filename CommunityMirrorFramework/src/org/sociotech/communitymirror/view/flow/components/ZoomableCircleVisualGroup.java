package org.sociotech.communitymirror.view.flow.components;

import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communitymirror.visualgroups.layout.CircleVisualGroup;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;

import com.facebook.rebound.BaseSpringSystem;

/**
 * Visual group with circle layout that can be zoomed. Zoo gestures changing the radius of the layout.
 * You need to call {@link VisualComponent#makeZoomable()} to enable the interaction.
 *
 * @author Peter Lachenmaier
 */
public class ZoomableCircleVisualGroup extends CircleVisualGroup {

	/**
	 * Invisible circle in the background for zoom interaction.
	 */
	private Circle backgroudCircle;

	/**
	 * Maximum radius to zoom to
	 */
	private double maxRadius;

	/**
	 * Minimum radius to zoom to
	 */
	private double minRadius;

	/**
	 * Creates a zoomable visual group with circular layout for the given list of components.
	 *
	 * @param components Components to align around center
	 * @param center Component in the center
	 * @param radius Radius of the layout circle
	 * @param springSystem Refernce to the spring system to be used for spring connections
	 */
	public ZoomableCircleVisualGroup(List<VisualComponent> components,
			VisualComponent center, List<VisualSpringConnection> springs, double radius, BaseSpringSystem springSystem) {
		super(components, center, springs, radius, springSystem);
		// calculate a max and min for zooming
		maxRadius = 2.0 * radius;
		minRadius = 0.5 * radius;
	}

	/**
	 * Creates a zoomable visual group with circular layout for the given list of components.
	 *
	 * @author Eva Loesch
	 *
	 * @param components Components to align around center
	 * @param center Component in the center
	 * @param radius Radius of the layout circle
	 * @param springSystem Refernce to the spring system to be used for spring connections
	 */
	public ZoomableCircleVisualGroup(List<VisualComponent> components,
			VisualComponent center, double radius, BaseSpringSystem springSystem) {
		super(components, center, null, radius, springSystem);
		// calculate a max and min for zooming
		maxRadius = 2.0 * radius;
		minRadius = 0.5 * radius;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualgroups.layout.CircleVisualGroup#onInit()
	 */
	@Override
	protected void onInit() {

		super.onInit();

		// add invisible background circle to receive correct interaction events
		backgroudCircle = new Circle();
		backgroudCircle.setRadius(getRadius());
		backgroudCircle.setFill(Color.RED);
		backgroudCircle.setVisible(false);

		backgroudCircle.centerXProperty().bindBidirectional(this.getCenterComponent().getSpringConnectorXProperty());
		backgroudCircle.centerYProperty().bindBidirectional(this.getCenterComponent().getSpringConnectorYProperty());

		this.addNode(backgroudCircle);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onZoomZoomed(org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent)
	 */
	@Override
	public void onZoomZoomed(ZoomZoomedEvent zoomEvent) {
		double newRadius = this.getRadius() * zoomEvent.getZoomFactor();
		// limit zoom
		newRadius = Math.min(newRadius, maxRadius);
		newRadius = Math.max(newRadius, minRadius);

		this.setRadius(newRadius);

		backgroudCircle.setRadius(this.getRadius());
	}
}
