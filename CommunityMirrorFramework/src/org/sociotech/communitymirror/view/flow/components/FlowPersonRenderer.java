
package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymirror.view.flow.visualitems.person.DetailPersonItem;
import org.sociotech.communitymirror.view.flow.visualitems.person.MicroPersonItem;
import org.sociotech.communitymirror.view.flow.visualitems.person.PersonItem;
import org.sociotech.communitymirror.view.flow.visualitems.person.PreviewPersonItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.VisualState;

public class FlowPersonRenderer extends VisualItemRenderer<Person, PersonItem> {

    @Override
    public PersonItem renderItem(Person item, VisualState state) {

        PersonItem personItem = null;

        if (state instanceof Micro) {
            personItem = new MicroPersonItem(item);
        } else if (state instanceof Detail) {
            personItem = new DetailPersonItem(item);
        } else {
            personItem = new PreviewPersonItem(item);
        }

        personItem.setVisualState(state);
        return personItem;
    }

}
