
package org.sociotech.communitymirror.view.flow.components;

import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.physics.behaviors.destroy.FadeOutAndDieBehavior;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.content.MicroContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.MicroMicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.person.MicroPersonItem;
import org.sociotech.communitymirror.visualitems.VisualGroup;

import com.google.common.io.Resources;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Creates the visual representation of the of the novice mode (marking menu).
 *
 * @author Christian Voss
 *
 */
public class FilterNoviceVisualGroup extends VisualGroup {
    /**
     * Logger reference
     */
    protected Logger logger = LogManager.getLogger(VisualGroup.class.getName());

    /**
     * The visual representation of the center group within the novice mode.
     */
    private VisualGroup centerGroup = new VisualGroup();

    /**
     * The visual representation of the bottom group within the novice mode.
     */
    private VisualGroup bottomGroup = new VisualGroup();

    /**
     * The X coordinate of the marking menu center.
     */
    private double X;

    /**
     * The Y coordinate of the marking menu center.
     */
    private double Y;

    /**
     * The period of time it takes for the novice mode to fade out.
     */
    private long fadeOutTime;

    /**
     * The period of time without interaction it takes to start the fading out
     * process.
     */
    private long waitToFadeOut;

    /**
     * The node for the center image.
     */
    private ImageView imageViewCenter = new ImageView();

    /**
     * The node for the bottom image.
     */
    private ImageView imageViewBottom = new ImageView();

    /**
     * The central arrow image for the novice mode.
     */
    private Image centerImage;

    /**
     * The keyboard image representing the search function.
     */
    private Image bottomImage;

    /**
     * The opacity of the shown dummy items.
     */
    private double opacity = 0.5;

    /**
     * The scale of the shown dummy items.
     */
    private double scale = 0.5;

    /**
     * Creates the Visual Group with the given starting Position.
     *
     * @param X
     * @param Y
     * @param fadeOutTime
     * @param waitToFade
     */
    public FilterNoviceVisualGroup(double X, double Y, long fadeOutTime, long waitToFade) {
        this.X = X;
        this.Y = Y;
        this.fadeOutTime = fadeOutTime / 2;
        this.waitToFadeOut = waitToFade / 2;

        URL arrowUrl, searchUrl;
        String arrowPath = "images/arrow.png";
        String searchPath = "images/searchmask.png";
        if (this.themeResources != null) {
            arrowUrl = this.themeResources.getResourceURL(arrowPath);
            searchUrl = this.themeResources.getResourceURL(searchPath);
        } else {
            arrowUrl = Resources.getResource("flow/" + arrowPath);
            searchUrl = Resources.getResource("flow/" + searchPath);
            this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
        }
        this.centerImage = ImageHelper.createImage(arrowUrl, true);
        this.bottomImage = ImageHelper.createImage(searchUrl, true);
    }

    /**
     * Creates the visible elements of the marking menu novice mode.
     */
    public void createElement() {

        this.imageViewCenter.setImage(this.centerImage);
        this.imageViewCenter.setOpacity(this.opacity);
        this.imageViewCenter.setScaleX(this.scale / 2);
        this.imageViewCenter.setScaleY(this.scale / 2);
        this.centerGroup.addNode(this.imageViewCenter);

        this.imageViewBottom.setImage(this.bottomImage);
        this.imageViewBottom.setOpacity(this.opacity);
        this.imageViewBottom.setScaleX(this.scale / 1.45);
        this.imageViewBottom.setScaleY(this.scale / 1.45);
        this.bottomGroup.addNode(this.imageViewBottom);

        MicroPersonItem personItem = new MicroPersonItem(null);
        MicroContentItem eventItem = new MicroContentItem(null);
        MicroMicroPostItem microItem = new MicroMicroPostItem(null);

        personItem.init();
        eventItem.init();
        microItem.init();

        personItem.setScaleX(this.scale);
        personItem.setScaleY(this.scale);
        eventItem.setScaleX(this.scale);
        eventItem.setScaleY(this.scale);
        microItem.setScaleX(this.scale);
        microItem.setScaleY(this.scale);

        personItem.setOpacity(this.opacity);
        eventItem.setOpacity(this.opacity);
        microItem.setOpacity(this.opacity);

        // ============ Positioning ============

        // ----------- right -----------
        personItem.setPositionX(this.X + 100);
        personItem.setPositionY(this.Y - 90);
        // ----------- top -----------
        eventItem.setPositionX(this.X - 90);
        eventItem.setPositionY(this.Y - 275);
        // ----------- left -----------
        microItem.setPositionX(this.X - 275);
        microItem.setPositionY(this.Y - 90);
        // ----------- bottom -----------
        this.bottomGroup.setPositionX(this.X - 375);
        this.bottomGroup.setPositionY(this.Y + 90);
        // ----------- center -----------
        this.centerGroup.setPositionX(this.X - 225);
        this.centerGroup.setPositionY(this.Y - 225);

        this.addVisualComponent(eventItem, true);
        this.addVisualComponent(personItem, true);
        this.addVisualComponent(microItem, true);
        this.addVisualComponent(this.centerGroup, true);
        this.addVisualComponent(this.bottomGroup, true);

        this.addPhysicsBehavior(FadeOutAndDieBehavior.createFadeOutAndDieBehavior(this.fadeOutTime, this.waitToFadeOut,
                this.getOpacity()));
        this.resetInteractionTime();
    }
}
