
package org.sociotech.communitymirror.view.flow.components;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.configuration.ThemeResourceLoader;
import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.position.CallBackOnBoundsExitBehavior;
import org.sociotech.communitymirror.physics.behaviors.position.LeftBoundsCallback;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.view.flow.visualitems.PileEventListener;
import org.sociotech.communitymirror.view.flow.visualitems.WorkspaceNodeInteractionEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Class containing visual items laying on top of each, therefore forming a pile
 * 
 * @author Andrea Nutsi
 *
 */
public class VisualComponentsPile<I extends Item, V extends FXMLController> extends VisualGroup
        implements LeftBoundsCallback {

    /**
     * The background of the pile
     */
    protected Rectangle rectangle;

    /**
     * Definition of the call back method when specified bounds are left
     */
    protected CallBackOnBoundsExitBehavior addInteractionOnPileExit;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(VisualComponentsPile.class.getName());

    private List<PileEventListener> pileEventListeners = new LinkedList<>();

    public PileController controller;

    /**
     * Creates a new pile with the given visual components
     * 
     * @param components
     *            The visual components to be visualized as pile
     */
    public VisualComponentsPile(Configuration configuration, List<VisualComponent> components) {
        this.configuration = configuration;

        // create new theme resource loader
        this.themeResources = new ThemeResourceLoader(configuration);

        this.rectangle = new Rectangle();
        this.rectangle.setHeight(292);
        this.rectangle.setWidth(292);
        this.rectangle.setOpacity(0);
        this.rectangle.setFill(Color.GHOSTWHITE);
        this.rectangle.setLayoutX(0/*-50*/);
        this.rectangle.setLayoutY(0/*-50*/);

        this.addNode(this.rectangle);

        AnchorPane pile;
        FXMLLoader loader = new FXMLLoader();
        try {
            URL location;
            String fxmlPath = "fxml/pile.fxml";
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            pile = (AnchorPane) loader.load(location.openStream());
            this.controller = loader.getController();
            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = pile.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(pile.getChildren().get(0));
            }
            this.controller.setDragPointVisible(false);
            this.addInteractionOnPileExit = CallBackOnBoundsExitBehavior
                    .createCallBackOnBoundsExitBehaviorWithOffset(this.rectangle.getBoundsInLocal(), this, 0/*-50*/);

        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for Pile" + e.getStackTrace());
        }

        // add all components
        if (components != null) {
            for (VisualComponent component : components) {
                this.addVisualComponentToPile(component, true);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.sociotech.communitymirror.visualitems.VisualGroup#addVisualComponent(
     * org.sociotech.communitymirror.visualitems.VisualComponent)
     */
    public synchronized void addVisualComponentToPile(VisualComponent visualComponent, boolean initializePile) {

        if (initializePile) {
            this.addVisualComponent(visualComponent);
        } else {
            this.addVisualComponentAtListHead(visualComponent, false);
        }

        // angles between -20 and 20
        // component.rotate((Math.random() * 41) - 20);
        visualComponent.addPhysicsBehavior(this.addInteractionOnPileExit);

        visualComponent.setPositionX(60 + ((Math.random() * 10) - 5));
        visualComponent.setPositionY(50 + ((Math.random() * 10) - 5));

        this.addNode(visualComponent);

        if (!initializePile) {
            visualComponent.putToBack();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.sociotech.communitymirror.physics.behaviors.position.
     * LeftBoundsCallback#leftBounds(javafx.geometry.Bounds,
     * org.sociotech.communitymirror.physics.PhysicsApplicable)
     */
    @Override
    public void leftBounds(Bounds bounds, PhysicsApplicable leavingObject) {
        if (leavingObject instanceof FlowVisualItem<?, ?>) {
            @SuppressWarnings("unchecked")
            FlowVisualItem<InformationObject, FXMLController> leavingItem = (FlowVisualItem<InformationObject, FXMLController>) leavingObject;

            leavingItem.removePhysicsBehavior(this.addInteractionOnPileExit);

            this.removeVisualComponent(leavingItem);

            if (this.getVisualComponents().size() < 2) {
                this.notifyListenersPileEmpty();
            }

            this.notifyListenersPileLeft(new WorkspaceNodeInteractionEvent<>(leavingItem, null));
        }
    }

    /**
     * Adds a Pile-EventListener to this Pile
     * 
     * @param pileEventListener
     *            The Pile-EventListener
     */
    public void addPileEventListener(PileEventListener pileEventListener) {
        this.pileEventListeners.add(pileEventListener);
    }

    /**
     * Notifies all listeners registered to this pile about an item leaving the
     * pile
     * 
     * @param event
     *            The event the listeners are notified about
     */
    protected synchronized void notifyListenersPileLeft(
            WorkspaceNodeInteractionEvent<? extends Item, ? extends FXMLController> event) {
        for (int i = 0; i < this.pileEventListeners.size(); i++) {
            PileEventListener listener = this.pileEventListeners.get(i);
            listener.onPileLeft(event);
        }
    }

    /**
     * Notifies all listeners registered to this pile about an item leaving the
     * pile
     * 
     * @param event
     *            The event the listeners are notified about
     */
    protected synchronized void notifyListenersPileEmpty() {
        for (int i = 0; i < this.pileEventListeners.size(); i++) {
            PileEventListener listener = this.pileEventListeners.get(i);
            listener.onPileEmpty(this);
        }
    }

}
