package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.view.flow.visualitems.tag.MicroTagItem;
import org.sociotech.communitymirror.view.flow.visualitems.tag.TagItem;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

public class FlowTagRenderer extends VisualItemRenderer<Tag, VisualItem<Tag>>{

	@Override
	public TagItem renderItem(Tag item, VisualState state) {
		
		TagItem tagItem = null;
		
		tagItem = new MicroTagItem(item);
		
//		if(state instanceof Micro){
//			tagItem = new MicroTagItem(item);
//		}
//		else if(state instanceof Detail){
//			tagItem = new DetailTagItem(item);
//		}
//		else{
//			tagItem = new PreviewTagItem(item);
//		}
		
		tagItem.setVisualState(state);
		return tagItem;
	}

}
