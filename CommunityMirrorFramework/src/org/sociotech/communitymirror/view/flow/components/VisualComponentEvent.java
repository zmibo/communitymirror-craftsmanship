package org.sociotech.communitymirror.view.flow.components;

import java.util.EventObject;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * An event object used for events that occurred on visual components
 * 
 * @author Eva Loesch 
 */
public class VisualComponentEvent extends EventObject{

	/**
	 * The unique serial version ID of this event.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The visual component this workspace node interaction event has occurred on.
	 */
	private VisualComponent sourceNode;

	/**
	 * The CIEvent which has has triggered this event.
	 */
	private CIEvent sourceEvent;
	
	
	public VisualComponentEvent(VisualComponent sourceNode, CIEvent sourceEvent) {
		super(sourceNode);
		this.sourceNode = sourceNode;
		this.setSourceEvent(sourceEvent);
	}

	/**
	 * Gets the visual component this event has occurred on.
	 */
	public VisualComponent getSourceNode() {
		return sourceNode;
	}

	/**
	 * Sets the visual component this event has occurred on.
	 * 
	 * @param sourceNode: the visual component that should be set as the source node of this event
	 */
	public void setSourceNode(VisualComponent sourceNode) {
		this.sourceNode = sourceNode;
	}

	/**
	 * Gets the CIEvent that has triggered this event.
	 */
	public CIEvent getSourceEvent() {
		return sourceEvent;
	}

	/**
	 * Sets the CIEvent that has triggered this event. 
	 * @param sourceEvent : CIEvent that should be set as source event of this event.
	 * 
	 */
	public void setSourceEvent(CIEvent sourceEvent) {
		this.sourceEvent = sourceEvent;
	}

}

