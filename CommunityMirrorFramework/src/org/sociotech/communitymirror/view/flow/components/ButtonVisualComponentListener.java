package org.sociotech.communitymirror.view.flow.components;

import java.util.EventListener;

/**
 * The interface of an event listener that notifys a flow view workspace about user interaction on its workspace node visual items.
 * 
 * @author Eva Loesch 
 */
public interface ButtonVisualComponentListener extends EventListener{
	
	/**
	 * Notifies about a button pressed event.
	 * @param visualComponentEvent: The visual component event that has occurred 
	 */
	void onButtonPressed(VisualComponentEvent visualComponentEvent);
}