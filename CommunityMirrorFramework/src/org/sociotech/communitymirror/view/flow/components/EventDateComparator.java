/*******************************************************************************
 * Copyright (c) 2013 Peter Lachenmaier - Cooperation Systems Center Munich (CSCM).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Peter Lachenmaier - Design and initial implementation
 ******************************************************************************/
package org.sociotech.communitymirror.view.flow.components;

import java.util.Comparator;
import java.util.Date;

import org.sociotech.communitymashup.data.Event;
import org.sociotech.communitymashup.data.Item;

/**
 * Comparator to compare Event items according to their date
 * 
 * @author Andrea Nutsi
 */
public class EventDateComparator implements Comparator<Item> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Item item1, Item item2) {
		
		if(item1 == null && item2 == null)
		{
			return 0;
		}
		
		if(item1 == null)
		{
			return -1;
		}
		
		if(item2 == null)
		{
			return 1;
		}
		
		Date date1;
		Date date2;
		
		if(item1 instanceof Event && item2 instanceof Event){
			date1 = ((Event)item1).getDate();
			date2 = ((Event)item2).getDate();
		}else{
			return 0;
		}
		
		
		
		// date may be not set
		if(date1 == null && date2 == null)
		{
			return 0;
		}
		
		if(date1 == null)
		{
			return -1;
		}
		
		if(date2 == null)
		{
			return 1;
		}
		
		// compare dates
		return date1.compareTo(date2);
	}

}

