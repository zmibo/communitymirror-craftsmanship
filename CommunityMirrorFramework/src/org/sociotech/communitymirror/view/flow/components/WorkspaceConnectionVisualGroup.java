package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * An abstract super class of visual groups containing elements that represent a connection of items within a workspace visual group
 * 
 * @author Eva Loesch
 */
public abstract class WorkspaceConnectionVisualGroup extends VisualGroup{
	
	/**
	 * The visual item that represents the start node  of the connection.
	 * @author Eva Loesch 
	 */
	private FlowVisualItem<? extends Item, ? extends FXMLController> startNode;
	
	/**
	 * The visual item that represents the end node of the connection.
	 * @author Eva Loesch 
	 */
	private FlowVisualItem<? extends Item, ? extends FXMLController> endNode;
	
	
	public WorkspaceConnectionVisualGroup(FlowVisualItem<? extends Item, ? extends FXMLController> startNode, FlowVisualItem<? extends Item, ? extends FXMLController> endNode)
	{
		this.startNode = startNode;
		this.endNode = endNode;
	}

	/**
	 * Gets the visual item that represents the start node of the connection.
	 * @author Eva Loesch 
	 */
	public FlowVisualItem<? extends Item, ? extends FXMLController> getStartNode() {
		return startNode;
	}

	/**
	 * Sets the visual item that represents the start node of the connection.
	 * 
	 * @param FlowVisualItem<? extends Item, ? extends ItemController> startNode : visual item that should be set as start node.
	 * @author Eva Loesch 
	 */
	public void setStartNode(FlowVisualItem<? extends Item, ? extends FXMLController> startNode) {
		this.startNode = startNode;
	}

	/**
	 * Gets the visual item that represents the end node of the connection.
	 * @author Eva Loesch 
	 */
	public FlowVisualItem<? extends Item, ? extends FXMLController> getEndNode() {
		return endNode;
	}

	/**
	 * Sets the visual item that represents the end node of the connection.
	 * 
	 * @param FlowVisualItem<? extends Item, ? extends ItemController> endNode : visual item that should be set as end node.
	 * @author Eva Loesch 
	 */
	public void setEndNode(FlowVisualItem<? extends Item, ? extends FXMLController> endNode) {
		this.endNode = endNode;
	}
	
	

}
