
package org.sociotech.communitymirror.view.flow.components;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymirror.configuration.ConfigurationLoader;
import org.sociotech.communitymirror.view.flow.visualitems.blogpost.DetailBlogPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.blogpost.MicroBlogPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.blogpost.PreviewBlogPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.content.DetailContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.content.MicroContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.content.PreviewContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.content.SpotlightContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.DetailMicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.MicroMicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.PreviewMicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.SpotlightMicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.picture.DetailPictureItem;
import org.sociotech.communitymirror.view.flow.visualitems.picture.MicroPictureItem;
import org.sociotech.communitymirror.view.flow.visualitems.picture.PreviewPictureItem;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.Spotlight;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Renderer for Contents in the flow view that chooses the correct visualization
 * based on metainformations
 * of the given content.
 *
 * @author Peter Lachenmaier, Andrea Nutsi, Eva Loesch
 *
 */
public class FlowContentRenderer extends VisualItemRenderer<Content, VisualItem<Content>> {

    /**
     * The metatags to be used for microposts
     */
    private String[] microPostMetaTags;

    /**
     * The metatag to be used for blogposts
     */
    private String[] blogPostMetaTags;

    /**
     * The metatag to be used for events
     */
    private String[] contentMetaTags;

    /**
     * The metatag to be used for pictures
     */
    private String[] pictureMetaTags;

    public FlowContentRenderer(Configuration configuration) {
        this.microPostMetaTags = ConfigurationLoader.getStringArray(configuration,
                "view.flow.particles.content.micropost.metatag");
        this.blogPostMetaTags = ConfigurationLoader.getStringArray(configuration,
                "view.flow.particles.content.blogpost.metatag");
        this.contentMetaTags = ConfigurationLoader.getStringArray(configuration,
                "view.flow.particles.content.content.metatag");
        this.pictureMetaTags = ConfigurationLoader.getStringArray(configuration,
                "view.flow.particles.content.picture.metatag");
    }

    @Override
    public VisualItem<Content> renderItem(Content item, VisualState state) {
        for (String contentMetaTag : this.contentMetaTags) {
            if (item.hasMetaTag(contentMetaTag)) {
                return this.renderEvent(item, state);
            }
        }
        for (String microPostMetaTag : this.microPostMetaTags) {
            if (item.hasMetaTag(microPostMetaTag)) {
                return this.renderMicroPost(item, state);
            }
        }
        for (String blogPostMetaTag : this.blogPostMetaTags) {
            if (item.hasMetaTag(blogPostMetaTag)) {
                return this.renderBlogPost(item, state);
            }
        }
        if (this.pictureMetaTags != null) {
            for (String pictureMetaTag : this.pictureMetaTags) {
                if (item.hasMetaTag(pictureMetaTag)) {
                    return this.renderPicture(item, state);
                }
            }
        }
        return null;
    }

    /**
     * Renders an event item according to its state
     *
     * @param item
     *            The data item
     * @param state
     *            the visual state
     * @return
     */
    protected VisualItem<Content> renderEvent(Content item, VisualState state) {

        VisualItem<Content> contentItem = null;

        if (state instanceof Spotlight) {
            contentItem = new SpotlightContentItem(item);
        } else if (state instanceof Micro) {
            // TODO maybe create class BROWSINGEventItem
            contentItem = new MicroContentItem(item);
        } else if (state instanceof Detail) {
            contentItem = new DetailContentItem(item);
        } else {
            contentItem = new PreviewContentItem(item);
        }

        contentItem.setVisualState(state);
        return contentItem;
    }

    /**
     * Renders a mircopost item according to its state
     *
     * @param item
     *            The data item
     * @param state
     *            the visual state
     * @return
     */
    private VisualItem<Content> renderMicroPost(Content item, VisualState state) {

        VisualItem<Content> contentItem = null;

        if (state instanceof Spotlight) {
            contentItem = new SpotlightMicroPostItem(item);
        } else if (state instanceof Micro) {
            // TODO maybe create class BROWSINGMicroPostItem
            contentItem = new MicroMicroPostItem(item);
        } else if (state instanceof Detail) {
            contentItem = new DetailMicroPostItem(item);
        } else {
            contentItem = new PreviewMicroPostItem(item);
        }

        contentItem.setVisualState(state);
        return contentItem;
    }

    /**
     * Renders a comment item according to its state
     *
     * @param item
     *            The data item
     * @param state
     *            the visual state
     * @return
     */
    // private VisualItem<Content> renderComment(Content item, VisualState
    // state) {
    //
    // VisualItem<Content> contentItem = null;
    //
    // if(state instanceof Spotlight){
    // contentItem = new SpotlightCommentItem(item);
    // }
    // else if(state instanceof Micro){
    // // TODO maybe create class BROWSINGCommentItem
    // contentItem = new MicroCommentItem(item);
    // }
    // else if(state instanceof Detail){
    // contentItem = new DetailCommentItem(item);
    // }
    // else{
    // contentItem = new PreviewCommentItem(item);
    // }
    //
    // contentItem.setVisualState(state);
    // return contentItem;
    // }

    /**
     * Renders a blog post item according to its state
     *
     * @param item
     *            The data item
     * @param state
     *            the visual state
     * @return
     */
    private VisualItem<Content> renderBlogPost(Content item, VisualState state) {

        VisualItem<Content> contentItem = null;

        if (state instanceof Micro) {
            contentItem = new MicroBlogPostItem(item);
        } else if (state instanceof Detail) {
            contentItem = new DetailBlogPostItem(item);
        } else {
            contentItem = new PreviewBlogPostItem(item);
        }

        contentItem.setVisualState(state);
        return contentItem;
    }

    /**
     * Renders a picture item according to its state
     *
     * @param item
     *            The data item
     * @param state
     *            the visual state
     * @return
     */
    private VisualItem<Content> renderPicture(Content item, VisualState state) {

        VisualItem<Content> contentItem = null;
        // TODO: include all visual states
        if (state instanceof Micro) {
            contentItem = new MicroPictureItem(item);
        } else if (state instanceof Detail) {
            contentItem = new DetailPictureItem(item);
        } else {
            contentItem = new PreviewPictureItem(item);
        }

        contentItem.setVisualState(state);
        return contentItem;
    }
}
