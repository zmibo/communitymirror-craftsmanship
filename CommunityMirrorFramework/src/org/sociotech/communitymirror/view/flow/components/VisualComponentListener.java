
package org.sociotech.communitymirror.view.flow.components;

import java.util.EventListener;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * The interface of an event listener that notifies registered listeners about
 * user interaction on a visual component.
 *
 * @author Eva Loesch
 */
public interface VisualComponentListener extends EventListener {

    /**
     * Notifies about an event on a visual component.
     *
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event this listener should notify about
     */
    void onObservedVisualComponentEvent(VisualComponent sourceComponent, CIEvent event);

}
