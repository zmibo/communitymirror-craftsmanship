package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import com.facebook.rebound.BaseSpringSystem;

/**
 * A workspace connection visual group that directly connects two items within a flow view workspace using spring.
 * 
 * @author Eva Loesch
 */
public class DirectWorkspaceConnectionVisualGroup extends WorkspaceConnectionVisualGroup{

	public DirectWorkspaceConnectionVisualGroup(FlowVisualItem<? extends Item, ? extends FXMLController> startNode,
			FlowVisualItem<? extends Item, ? extends FXMLController> endNode, BaseSpringSystem springSystem) {
		
		super(startNode, endNode);
	}

}
