package org.sociotech.communitymirror.view.flow.activityLogger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.activitylogger.ActivityKeyValue;
import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;

/**
 * CSV Logger for user activity.
 * 
 * @author Andrea Nutsi, Eva Loesch
 *
 */
public class CSVUserActivityLogger extends UserActivityLogger{
	
	private Logger csvMessageLogger = LogManager.getLogger(CSVUserActivityLogger.class.getName() + ".message");
	private Logger csvDetailLogger = LogManager.getLogger(CSVUserActivityLogger.class.getName() + ".detail");
	
	private final static String csvSeparator = ";";
	
	protected Logger getMessageLogger()
	{
		return this.csvMessageLogger;
	}
	
	protected Logger getDetailLogger()
	{
		return this.csvDetailLogger;
	}
	
	   /**
     * Logs a user activity.
     * 
     * @param message Description of the user activity
     */
	@Override
    public void logActivity(String message) {
    	if(message == null) return;
    	
    	// log as info
    	this.getMessageLogger().info(message);
    }
    
    /**
     * Logs the activity and all given details.
     * 
     * @param message Message to log
     * @param details Details in key value pairs
     */
	@Override
    public void logActivity(String message, ActivityKeyValue<?> ... details) {
		
		String detailsCSVEntry = detailsToCSV(details);
    	this.logActivity(message + csvSeparator + detailsCSVEntry); 	
    }
	
    /**
     * Logs all given details.
     * 
     * @param details Details in key value pairs
     */
	//@Override
    public void logActivity(ActivityKeyValue<?> ... details) {
		
		String detailsCSVEntry = detailsToCSV(details);
    	this.logActivity(detailsCSVEntry); 	
    }
	
	private String detailsToCSV(ActivityKeyValue<?> ... details)
	{
		String csvString = "";
    	for(ActivityKeyValue<?> detail : details) {
    		csvString = csvString + detail.getValue() + csvSeparator;
    	}
		
		return csvString;
	}
}
