package org.sociotech.communitymirror.view.flow;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communityinteraction.behaviors.CommunicationBehavior;
import org.sociotech.communityinteraction.events.communication.MessageReceivedEvent;
import org.sociotech.communityinteraction.interactionhandling.HandleCommunication;
import org.sociotech.communityinteraction.interfaces.CommunicationConsumer;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.communication.ClientToSend;
import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.DriftBehavior;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;
import org.sociotech.communitymirror.physics.behaviors.position.CallBackOnBoundsTraversedBehavior;
import org.sociotech.communitymirror.physics.behaviors.position.TraversedBoundsCallback;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;
import org.sociotech.communitymirror.visualstates.VisualState;

import javafx.application.Platform;
import javafx.geometry.Bounds;

/**
 * A FlowView that is able to communicate with CommunicativeFlowViews of other
 * CommunityMirror-applications.
 *
 * @author Lösch
 *
 */
public class CommunicativeFlowView extends FlowView
		implements TraversedBoundsCallback, HandleCommunication, CommunicationConsumer {

	/**
	 * @param width
	 * @param height
	 * @param configuration
	 * @param dataSet
	 */
	public CommunicativeFlowView(double width, double height, Configuration configuration, DataSet dataSet) {
		super(width, height, configuration, dataSet);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.sociotech.communitymirror.view.flow.FlowView#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();
		// create a communication behavior and add it to this component
		this.addInteractionBehavior(new CommunicationBehavior(this));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.sociotech.communitymirror.view.flow.FlowView#renderFlowVisualItem(org
	 * .sociotech.communitymashup.data.Item, double, double, double, double,
	 * org.sociotech.communitymirror.visualstates.VisualState,
	 * org.sociotech.communitymirror.physics.behaviors.DriftBehavior)
	 */
	@Override
	protected FlowVisualItem<? extends Item, ? extends FXMLController> addFlowVisualItemToView(Item item, double positionX,
			double positionY, double scaleX, double scaleY, VisualState visualState, DriftBehavior driftBehavior) {

		FlowVisualItem<? extends Item, ? extends FXMLController> flowVisualItem = super.addFlowVisualItemToView(item,
				positionX, positionY, scaleX, scaleY, visualState, driftBehavior);
		// create a CallBackOnBoundsTraversed Behavior and add it to this
		// component
		flowVisualItem.addPhysicsBehavior(CallBackOnBoundsTraversedBehavior
				.createCallBackOnBoundsTraversedBehaviorWithOffset(this.getViewBounds(), this, true, 0));
		return flowVisualItem;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.sociotech.communityinteraction.interactionhandling.
	 * HandleCommunication#onMessageReceived(org.sociotech.communityinteraction.
	 * events.communication.MessageReceivedEvent)
	 *
	 * Renders a new FlowVisualItem using the properties contained in the
	 * received message and adds it to the flow.
	 */
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				// split comma separated message
				String message = event.getMessage();
				String[] token = message.split(";");

				// get message type
				String messageType = token[0];

				if (messageType.equals("TraversedBounds")) {
					// get data item
					String ident = token[1];
					Item item = getDataSet().getItemsWithIdent(ident);

					// get visual state
					String visualStateName = token[2];
					VisualState visualState = null;
					if (visualStateName.equals("Micro")) {
						visualState = VisualState.MICRO;
					} else if (visualStateName.equals("Preview")) {
						visualState = VisualState.PREVIEW;
					} else if (visualStateName.equals("Detail")) {
						visualState = VisualState.DETAIL;
					}

					// get positionX and positionY
					double posX = Double.parseDouble(token[3]);
					posX = posX < 0 ? posX + viewBounds.getMaxX() : posX - viewBounds.getMaxX();
					double posY = Double.parseDouble(token[4]);

					// get xDirection and yDirection
					double xDirection = Double.parseDouble(token[5]);
					double yDirection = Double.parseDouble(token[6]);

					// get velocity, acceleration and target velocity
					double velocity = Double.parseDouble(token[7]);
					double acceleration = Double.parseDouble(token[8]);
					double targetVelocity = Double.parseDouble(token[9]);

					// get scaleX and scaleY
					double scaleX = Double.parseDouble(token[10]);
					double scaleY = Double.parseDouble(token[11]);

					// get width and height of original (not scaled) visual item
					double width = Double.parseDouble(token[12]);
					double height = Double.parseDouble(token[13]);

					// correct position according to scale
					posX = posX + (scaleX - 1) * (width / scaleX) / 2;
					posY = posY + (scaleY - 1) * (height / scaleY) / 2;

					// render flow visual item
					addFlowVisualItemToView(item, posX, posY, scaleX, scaleY, visualState,
							new DriftBehavior(xDirection, yDirection, velocity, acceleration, targetVelocity));
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.sociotech.communitymirror.physics.behaviors.position.
	 * TraversedBoundsCallback#traversedBounds(javafx.geometry.Bounds,
	 * org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void traversedBounds(Bounds bounds, PhysicsApplicable traversingObject) {
		// send message to socket if an item is traversing the view bounds
		if (traversingObject instanceof FlowVisualItem<?,?>) {
			sendFlowVisualItemMessage((FlowVisualItem<? extends Item, ? extends FXMLController>) traversingObject);
		}
	}

	/**
	 * Sends a text message containing information about a flow visual item that
	 * has traversed the view bounds to a server socket that the client to send
	 * is connected to.
	 *
	 * @param traversingFlowVisualItem
	 */
	public void sendFlowVisualItemMessage(
			FlowVisualItem<? extends Item, ? extends FXMLController> traversingFlowVisualItem) {

		// send message only if leaving visual item has a drift behavior
		DriftBehavior driftBehavior = null;
		for (PhysicsBehavior ph : traversingFlowVisualItem.getPhysicsBehaviors()) {
			if (ph instanceof DriftBehavior) {
				driftBehavior = (DriftBehavior) ph;
				break;
			}
		}
		if (driftBehavior != null) {

			// get properties to send
			String ident = traversingFlowVisualItem.getDataItem().getIdent();
			VisualState visualState = traversingFlowVisualItem.getVisualState();
			double positionX = traversingFlowVisualItem.getBoundsInParent().getMinX();
			double positionY = traversingFlowVisualItem.getBoundsInParent().getMinY();
			double width = traversingFlowVisualItem.getBoundsInParent().getWidth();
			double height = traversingFlowVisualItem.getBoundsInParent().getHeight();
			double scaleX = traversingFlowVisualItem.getScaleX();
			double scaleY = traversingFlowVisualItem.getScaleY();

			// build message
			String message = "TraversedBounds" + ";" + ident + ";" + visualState.toString() + ";" + positionX + ";"
					+ positionY + ";" + driftBehavior.xDirectionProperty().get() + ";"
					+ driftBehavior.yDirectionProperty().get() + ";" + driftBehavior.velocityProperty().get() + ";"
					+ driftBehavior.accelerationProperty().get() + ";" + driftBehavior.targetVelocityProperty().get()
					+ ";" + scaleX + ";" + scaleY + ";" + width + ";" + height;
			// get client to send message
			ClientToSend clientToSend = (positionX <= 0) ? CommunityMirror.getClientToSendToLeft()
					: CommunityMirror.getClientToSendToRight();
			// send message
			if (clientToSend != null) {
				clientToSend.send(message);
			}
		}
	}
}
