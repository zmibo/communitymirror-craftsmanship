
package org.sociotech.communitymirror.view.flow.visualitems.blogpost;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Connection;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

import com.google.common.io.Resources;

import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * The visualization of a blog post item
 *
 * @author Andrea Nutsi
 *
 */
public abstract class BlogPostItem extends FlowVisualItem<Content, BlogPostItemController> {

    /**
     * The data item of this blog post
     */
    protected Content dataItem;

    /**
     * The creation date of this blog post
     */
    protected String blogDate = "";

    /**
     * The title of this blog post
     */
    protected String blogTitle = "";

    /**
     * The HTML content of this blog post
     */
    protected String blogContent = "";

    /**
     * An image of this blog post
     */
    protected Image blogImage;

    /**
     * An image of this blog post
     */
    protected Image blogImageSquare;

    /**
     * An image of this blog post
     */
    protected Image blogImageRound;

    /**
     * The name of the author of this blog post
     */
    protected String blogAuthor = "";

    /**
     * The image of the author of this blog post
     */
    protected Image blogAuthorImage;

    /**
     * The number of tags of this blog post
     */
    protected String numberTags = "";

    /**
     * A String containing the tags of this blog post
     */
    protected String tags = "";

    protected BlogPostItemController controller;

    public BlogPostItem(Content dataItem) {
        super(dataItem);
        this.dataItem = dataItem;
    }

    @Override
    protected void onInit() {
        this.getBlogPostInfo();
        this.render();
        super.onInit();
    }

    private void getBlogPostInfo() {
        this.blogDate = this.dataItem.getCreatedPretty();
        this.blogTitle = this.dataItem.getName();
        this.blogContent = this.stripHTMLTags(this.dataItem.getStringValue());
        if (this.dataItem.getAuthor() != null) {
            this.blogAuthor = this.dataItem.getAuthor().getName();
        } else {
            this.blogAuthor = "";
        }
        this.numberTags = this.dataItem.getTags().size() + "";

        // tags
        EList<Tag> tags = this.dataItem.getTags();
        for (Tag tag : tags) {
            this.tags += " #" + tag.getName();
        }

        // get author image
        if (this.dataItem.getAuthor() != null) {
            // create Metatag list
            LinkedList<String> metaTagList = new LinkedList<>();
            metaTagList.add("profile_image_big_main");
            metaTagList.add("profile_image_big");

            for (String metaTag : metaTagList) {
                org.sociotech.communitymashup.data.Image mashupImage = this.dataItem.getAuthor()
                        .getAttachedImageWithMetaTagName(metaTag);
                if (mashupImage != null) {
                    String fileUrl = mashupImage.getFileUrl();
                    this.blogAuthorImage = ImageHelper.createImage(fileUrl, true);
                    if (this.blogAuthorImage != null) {
                        break;
                    }
                }
            }
        }

        // get an image for this blog post
        EList<org.sociotech.communitymashup.data.Image> images = this.dataItem.getImages();
        String fileUrl = "";
        if (!images.isEmpty()) {
            for (int i = 0; i < images.size(); i++) {
                if (!images.get(i).hasMetaTag("takeaway_qr")) {
                    fileUrl = images.get(i).getFileUrl();
                    this.blogImage = ImageHelper.createImage(fileUrl, true);
                    // if something failed try next fileUrl in dataset
                    if (this.blogImage != null) {
                        break;
                    }
                }
            }

        }

        // Automatically remove transparency from logos
        double imgWidth = 256;
        double imgHeight = imgWidth;
        if (this.blogImage != null) {
            imgWidth = this.blogImage.getWidth();
            imgHeight = this.blogImage.getHeight();
        }
        Canvas canvas = new Canvas(imgWidth, imgHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GRAY); // adjust here if background
                                // should be blended to black or
                                // white instead
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        if (this.blogImage != null) {
            gc.drawImage(this.blogImage, 0, 0);
        }
        this.blogImage = canvas.snapshot(null, null);

        // create square image
        int rawWidth = (int) Math.round(this.blogImage.getWidth());
        int rawHeight = (int) Math.round(this.blogImage.getHeight());
        int targetSize = Math.round(Math.min(rawWidth, rawHeight));
        if (targetSize > 0) {
            WritableImage newImage = new WritableImage(targetSize, targetSize);
            int xOffset = (int) Math.round((this.blogImage.getWidth() - targetSize) / 2);
            int yOffset = (int) Math.round((this.blogImage.getHeight() - targetSize) / 2);
            newImage.getPixelWriter().setPixels(0, 0, targetSize, targetSize, this.blogImage.getPixelReader(), xOffset,
                    yOffset);
            this.blogImageSquare = newImage;

            // create round image
            ImageView tempView = new ImageView();
            tempView.setImage(this.blogImageSquare);
            Circle clip = new Circle(this.blogImageSquare.getWidth() / 2, this.blogImageSquare.getWidth() / 2,
                    this.blogImageSquare.getWidth() / 2);
            tempView.setClip(clip);
            SnapshotParameters parameters = new SnapshotParameters();
            parameters.setFill(Color.TRANSPARENT);
            this.blogImageRound = tempView.snapshot(parameters, null);
        }
    }

    private String stripHTMLTags(String htmlText) {

        Pattern pattern = Pattern.compile("<[^>]*>");
        Matcher matcher = pattern.matcher(htmlText);
        final StringBuffer sb = new StringBuffer(htmlText.length());
        while (matcher.find()) {
            matcher.appendReplacement(sb, " ");
        }
        matcher.appendTail(sb);
        return StringEscapeUtils.unescapeHtml(sb.toString().trim());

    }

    /**
     * Method to override in subclasses to generate the visual representation
     */
    protected abstract void render();

    protected void setCategory(String newCategory) {
        javafx.scene.image.Image categoryIcon = null;
        if (newCategory != null) {
            String iconPath = "images/category_icon_" + newCategory + ".png";
            URL location;
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(iconPath);
            } else {
                location = Resources.getResource("flow/" + iconPath);
                this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
            }
            // If the icon file does not exist, ImageHelper will simply return
            // null. Consequently, passing null into the controller will hide
            // the category icon.
            categoryIcon = ImageHelper.createImage(location, true);
        }
        this.controller.setCategoryIcon(categoryIcon);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedOrganizations()
     */
    @Override
    protected List<Organisation> getRelatedOrganizations() {
        Content content = this.getDataItem();
        // get the organizations of all authors of this item ...
        List<Organisation> res = content.getOrganisations();
        // now add direct connections to organisation objects
        List<Connection> tmplist = content.getConnectedTo();
        for (Connection c : tmplist) {
            InformationObject o = c.getTo();
            if (o instanceof Organisation) {
                if (res == null) {
                    res = new LinkedList<>();
                }
                res.add((Organisation) o);
            }
        }

        return res;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedPersons()
     */
    @Override
    protected List<Person> getRelatedPersons() {
        Content blogPost = this.getDataItem();
        List<Person> relatedPersons = new LinkedList<>();
        relatedPersons.add(blogPost.getAuthor());
        return relatedPersons;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedTags()
     */
    @Override
    protected List<Tag> getRelatedTags() {
        Content blogPost = this.getDataItem();

        List<Tag> relatedTags = new LinkedList<>();
        for (Tag tag : blogPost.getTags()) {
            // if(tag.getTagged().size() > 1)
            relatedTags.add(tag);
        }

        return relatedTags;
    }
}
