
package org.sociotech.communitymirror.view.flow.visualitems;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymashup.data.MetaTag;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 * The visualization of an about item showing background information about the
 * mirror installation
 *
 * @author Michael Koch
 *
 */
public class MetaTagItem extends FlowVisualItem<MetaTag, MetaTagItemController> {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(MetaTagItem.class);

    /**
     * The related content items
     */
    private List<Item> relatedItems = new LinkedList<>();

    /**
     * The data item representing the metatag
     */
    private MetaTag dataItem;

    public MetaTagItem(MetaTag dataItem) {
        super(dataItem);
        this.dataItem = dataItem;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
     */
    @Override
    protected void onInit() {
        this.render();
        this.setRelatedItems();
        super.onInit();
    }

    protected void render() {
        try {
            // Get FXML for time item
            FXMLLoader loader = new FXMLLoader();
            URL location;
            String fxmlPath = "fxml/inPreview/metatag.fxml";
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            AnchorPane aboutPane = (AnchorPane) loader.load(location.openStream());

            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = aboutPane.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(aboutPane.getChildren().get(0));
            }

        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for AboutItem" + e.getStackTrace());
        }

    }

    public void setRelatedItems() {
        // get the InformationItem objects metatagged with about
        EList<Item> objects = this.dataItem.getMetaTagged();
        this.relatedItems.addAll(objects);
    }

    /**
     * Returns the list of all related items of this item or a empty list if no
     * related items exist.
     */
    @Override
    public Set<Item> getAllRelatedItems() {
        Set<Item> relatedItems = new HashSet<>();
        relatedItems.addAll(this.relatedItems);
        return relatedItems;
    }

}
