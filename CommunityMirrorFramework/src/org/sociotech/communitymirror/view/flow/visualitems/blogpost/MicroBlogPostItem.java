
package org.sociotech.communitymirror.view.flow.visualitems.blogpost;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymirror.CommunityMirror;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

/**
 * The visualization of a blog post item in the micro visual state
 *
 * @author Andrea Nutsi
 *
 */
public class MicroBlogPostItem extends BlogPostItem {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(MicroBlogPostItem.class.getName());

    public MicroBlogPostItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        try {
            // Get FXML for blog post
            FXMLLoader loader = new FXMLLoader();

            URL location;
            String fxmlPath = "fxml/inMicro/blog.fxml";
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }

            loader.setLocation(location);
            AnchorPane blogPost = (AnchorPane) loader.load(location.openStream());
            this.controller = loader.getController();

            // get and set category information from meta tags
            String categoryString = null;
            String category_icons[] = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons")
                    .split(",");
            EList<MetaTag> metaTags = this.dataItem.getMetaTags();
            for (MetaTag metaTag : metaTags) {
                for (String category : category_icons) {
                    if (category.equals(metaTag.getName())) {
                        categoryString = metaTag.getName();
                        break;
                    }
                }
            }
            this.setCategory(categoryString);

            // set the content of this blog post

            this.controller.bindToTagsCounter(this.numberTags);

            if (this.blogAuthorImage != null) {
                Circle circle = new Circle(8);
                circle.setLayoutY(8);
                circle.setLayoutX(8);
                this.controller.setBlogAuthorImageClip(circle);
                this.controller.setBlogAuthorImage(this.blogAuthorImage);
            }

            if (this.blogImage != null) {
                Circle circle = new Circle(40);
                circle.setLayoutY(40);
                circle.setLayoutX(40);
                this.controller.setBlogImageClip(circle);
                this.controller.setBlogImage(this.blogImage);
                this.controller.setBlogImageSquare(this.blogImageSquare);
                this.controller.setBlogImageRound(this.blogImageRound);
            } else {
                Circle circle = new Circle(40);
                circle.setLayoutY(3);
                circle.setLayoutX(43);
                this.controller.setBlogTitleClip(circle);
                this.controller.bindToBlogTitleField(this.blogTitle);
            }

            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = blogPost.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(blogPost.getChildren().get(0));
            }
        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for BlogPostItem" + e.getStackTrace());
        }
    }

}
