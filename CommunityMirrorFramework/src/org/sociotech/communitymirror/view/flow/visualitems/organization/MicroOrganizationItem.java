
package org.sociotech.communitymirror.view.flow.visualitems.organization;

import org.sociotech.communitymashup.data.Organisation;

/**
 * The visualization of an organization item in the micro visual state
 * 
 * @author Andrea Nutsi
 *
 */
public class MicroOrganizationItem extends OrganizationItem {
    public MicroOrganizationItem(Organisation dataItem) {
        super(dataItem);
    }
}
