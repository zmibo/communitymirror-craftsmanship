package org.sociotech.communitymirror.view.flow.visualitems;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

import org.apache.commons.lang.ArrayUtils;
import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * A visualization of a concrete star ranking value
 * @author Andrea Nutsi
 *
 */
public class StarRankItem extends VisualComponent{

	private Double starValue;
	
	public StarRankItem(Double starValue) {
		this.starValue = starValue;
		onInit();
	}

	protected void onInit() {
		render();
		super.onInit();
	}

	private void render() {
		Group group = new Group();
		// group is already used as clip and therefore cannot be added as node (would throw exception)
		//therefore we need a second group with the same stars to show the "empty" stars
		Group groupCopy = new Group();
		/***********Star 1****************/
		Polygon star1 = new Polygon();
		Double[] pointsStar1 = new Double[]{
				58.881,70.069, 36.588,58.198, 14.422,70.305, 18.533,44.889, 
				0.453,27.0, 25.287,23.164, 36.279,0.0, 47.516,23.045, 72.389,26.619, 54.501,44.698 }; 
		star1.getPoints().addAll(pointsStar1);
		star1.setLayoutX(-20);
		star1.setStroke(Color.YELLOW);
		star1.setStrokeWidth(3.0);
		star1.setFill(Color.TRANSPARENT);
		
		Polygon star1Copy = new Polygon(ArrayUtils.toPrimitive(pointsStar1));
		star1Copy.setLayoutX(star1.getLayoutX());
		star1Copy.setLayoutY(star1.getLayoutY());
		group.getChildren().add(star1Copy);
		groupCopy.getChildren().add(star1);
		/***********Star 2****************/
		Polygon star2 = new Polygon();
		Double[] pointsStar2 = new Double[]{
				130.816,70.069, 108.524,58.198, 86.358,70.305, 
				90.469,44.889, 72.389,27.0, 97.223,23.164, 108.215,0.0, 119.452,23.045, 144.325,26.619, 126.438,44.698 };
		star2.getPoints().addAll(pointsStar2);
		star2.setLayoutY(-25);
		star2.setLayoutX(-10);
		star2.setStroke(Color.YELLOW);
		star2.setStrokeWidth(3.0);
		star2.setFill(Color.TRANSPARENT);
		
		Polygon star2Copy = new Polygon(ArrayUtils.toPrimitive(pointsStar2));
		star2Copy.setLayoutX(star2.getLayoutX());
		star2Copy.setLayoutY(star2.getLayoutY());
		group.getChildren().add(star2Copy);
		groupCopy.getChildren().add(star2);
		/***********Star 3****************/
		Polygon star3 = new Polygon();
		Double[] pointsStar3 = new Double[]{
				202.753,70.069, 180.46,58.198, 158.294,70.305, 
				162.405,44.889, 144.325,27.0, 169.159,23.164, 180.151,0.0, 191.388,23.045, 216.261,26.619, 198.373,44.698 };
		star3.getPoints().addAll(pointsStar3);
		star3.setLayoutY(-40);
		star3.setStroke(Color.YELLOW);
		star3.setStrokeWidth(3.0);
		star3.setFill(Color.TRANSPARENT);
		
		Polygon star3Copy = new Polygon(ArrayUtils.toPrimitive(pointsStar3));
		star3Copy.setLayoutX(star3.getLayoutX());
		star3Copy.setLayoutY(star3.getLayoutY());
		group.getChildren().add(star3Copy);
		groupCopy.getChildren().add(star3);
		/***********Star 4****************/
		Polygon star4 = new Polygon();
		Double[] pointsStar4 = new Double[]{
				274.688,70.069, 252.396,58.198, 230.23,70.305, 
				234.341,44.889, 216.261,27.0, 241.095,23.164, 252.086,0.0, 263.325,23.045, 288.198,26.619, 270.309,44.698 };
		star4.getPoints().addAll(pointsStar4);
		star4.setLayoutY(-25);
		star4.setLayoutX(10);
		star4.setStroke(Color.YELLOW);
		star4.setStrokeWidth(3.0);
		star4.setFill(Color.TRANSPARENT);
		
		Polygon star4Copy = new Polygon(ArrayUtils.toPrimitive(pointsStar4));
		star4Copy.setLayoutX(star4.getLayoutX());
		star4Copy.setLayoutY(star4.getLayoutY());
		group.getChildren().add(star4Copy);
		groupCopy.getChildren().add(star4);
		/***********Star 5****************/
		Polygon star5 = new Polygon();
		Double[] pointsStar5 = new Double[]{
				346.623,70.069, 324.332,58.198, 302.166,70.305, 
				306.277,44.889, 288.197,27.0, 313.031,23.164, 324.021,0.0, 335.26,23.045, 360.133,26.619, 342.244,44.698 };
		star5.getPoints().addAll(pointsStar5);
		star5.setLayoutX(20);
		star5.setStroke(Color.YELLOW);
		star5.setStrokeWidth(3.0);
		star5.setFill(Color.TRANSPARENT);
		
		Polygon star5Copy = new Polygon(ArrayUtils.toPrimitive(pointsStar5));
		star5Copy.setLayoutX(star5.getLayoutX());
		group.getChildren().add(star5Copy);
		groupCopy.getChildren().add(star5);
		
		Rectangle bar = new Rectangle();
		bar.setFill(Color.YELLOW);

		//normalized value between 0 and 1
		bar.setWidth(starValue * group.getBoundsInParent().getWidth());
		//bar.setWidth(Math.random() * group.getBoundsInLocal().getWidth());
		bar.setHeight(group.getBoundsInParent().getHeight());
		
		//doing some strange layout manipulations to get it right
		bar.setLayoutX(group.getLayoutX() - 20);
		bar.setLayoutY(group.getLayoutY() - 40);
		
		group.setLayoutX(group.getLayoutX() + 20);
		group.setLayoutY(group.getLayoutY() + 40);
		
		bar.setClip(group);
		
		addNode(bar);
		addNode(groupCopy);
		
		
	}
	
	
}
