
package org.sociotech.communitymirror.view.flow.visualitems.content;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * The controller class of a ContentItem
 *
 * @author Andrea Nutsi
 *
 */
public class ContentItemController extends FXMLController implements Initializable {

    @FXML
    Text contentTime;

    @FXML
    Label contentLocation;

    @FXML
    Text contentTitle;

    @FXML
    Text contentTitleLong;

    @FXML
    WebView detailWebView;

    @FXML
    ImageView contentImage;

    @FXML
    ImageView contentImageSquare;

    @FXML
    ImageView contentImageRound;

    @FXML
    Rectangle contentRectangle;

    @FXML
    ImageView contentImageLarge;

    @FXML
    ImageView contentImageSmall;

    @FXML
    ImageView contentAuthorImage;

    @FXML
    ImageView contentAuthorImageInline;

    @FXML
    ImageView contentCoAuthorImageInline;

    @FXML
    Text contentAuthor;

    @FXML
    Text contentCoAuthor;

    @FXML
    Text contentDescription;

    @FXML
    ImageView categoryIcon;

    @FXML
    Circle categoryIconBackground;

    @FXML
    ImageView kreissegment_authorImage;

    @FXML
    Text commentsCounter;

    @FXML
    Label labelPaperIcon;

    @FXML
    Label labelCoAuthor;

    @FXML
    Label labelAuthor;

    @FXML
    Label labelComment;

    @FXML
    Line line1;

    @FXML
    Line line2;

    @FXML
    ImageView qrCode;

    @FXML
    ImageView pdfSymbol;

    @FXML
    Text inDetailCloseButton;

    @FXML
    Circle graphConnection01;
    @FXML
    Circle graphConnection02;
    @FXML
    Circle graphConnection03;
    @FXML
    Circle graphConnection04;
    @FXML
    Circle graphConnection05;
    @FXML
    Circle graphConnection06;
    @FXML
    Circle graphConnection07;
    @FXML
    Circle graphConnection08;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(ContentItemController.class.getName());

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            assert this.contentTime != null : "fx:id=\"contentTime\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentLocation != null : "fx:id=\"contentLocation\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentTitle != null : "fx:id=\"contentTitle\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentTitleLong != null : "fx:id=\"contentTitleLong\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentImage != null : "fx:id=\"contentImage\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentRectangle != null : "fx:id=\"contentRectangle\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentAuthorImage != null : "fx:id=\"contentAuthorImage\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentAuthorImageInline != null : "fx:id=\"contentAuthorImageInline\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentCoAuthorImageInline != null : "fx:id=\"contentCoAuthorImageInline\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentAuthor != null : "fx:id=\"contentAuthor\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentCoAuthor != null : "fx:id=\"contentCoAuthor\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentImageLarge != null : "fx:id=\"contentImageLarge\" was not injected: check your FXML file 'content.fxml'.";
            assert this.contentImageSmall != null : "fx:id=\"contentImageSmall\" was not injected: check your FXML file 'content.fxml'.";
            assert this.kreissegment_authorImage != null : "fx:id=\"kreissegment_authorImage\" was not injected: check your FXML file 'content.fxml'.";
            assert this.commentsCounter != null : "fx:id=\"commentsCounter\" was not injected: check your FXML file 'content.fxml'.";
            assert this.labelPaperIcon != null : "fx:id=\"labelPaperIcon\" was not injected: check your FXML file 'content.fxml'.";
            assert this.labelCoAuthor != null : "fx:id=\"labelCoAuthor\" was not injected: check your FXML file 'content.fxml'.";
            assert this.labelAuthor != null : "fx:id=\"labelAuthor\" was not injected: check your FXML file 'content.fxml'.";
            assert this.labelComment != null : "fx:id=\"labelComment\" was not injected: check your FXML file 'content.fxml'.";
            assert this.line1 != null : "fx:id=\"line1\" was not injected: check your FXML file 'content.fxml'.";
            assert this.line2 != null : "fx:id=\"line2\" was not injected: check your FXML file 'content.fxml'.";
            assert this.qrCode != null : "fx:id=\"qrCode\" was not injected: check your FXML file 'content.fxml'.";
            assert this.pdfSymbol != null : "fx:id=\"pdfSymbol\" was not injected: check your FXML file 'content.fxml'.";
        } catch (AssertionError e) {
            this.logger.error(e.getMessage());
        }

    }

    protected void bindToTitle(String title, int length) {
        this.bindToFXMLElement(this.contentTitle, StringUtils.abbreviate(title, length));
    }

    protected void bindToTitleLong(String title, int length) {
        this.bindToFXMLElement(this.contentTitleLong, StringUtils.abbreviate(title, length));
    }

    protected void bindToTitleLong(String title) {
        this.bindToFXMLElement(this.contentTitleLong, title);
    }

    protected void setContentImage(Image contentImage) {
        if (this.contentImage != null) {
            this.contentImage.setImage(contentImage);
        }
    }

    protected void setContentImageSquare(Image contentImageSquare) {
        if (this.contentImageSquare != null) {
            this.contentImageSquare.setImage(contentImageSquare);
        }
    }

    protected void setContentImageRound(Image contentImageRound) {
        if (this.contentImageRound != null) {
            this.contentImageRound.setImage(contentImageRound);
        }
    }

    protected void setContentImageClip(Node clip) {
        if (this.contentImage != null) {
            this.contentImage.setClip(clip);
        }
    }

    protected void setContentAuthorImage(Image contentAuthorImage) {
        if (this.kreissegment_authorImage != null) {
            this.kreissegment_authorImage.setVisible(true);
        }
        if (this.contentAuthorImage != null) {
            this.contentAuthorImage.setImage(contentAuthorImage);
        }
    }

    protected void setContentAuthorImageClip(Node clip) {
        if (this.contentAuthorImage != null) {
            this.contentAuthorImage.setClip(clip);
        }
    }

    protected void setContentAuthorImageInline(Image contentAuthorImage) {
        GridPane.setMargin(this.contentAuthor, new Insets(0, 0, 0, 55));
        this.contentAuthorImageInline.setImage(contentAuthorImage);
    }

    protected void setContentCoAuthorImageInline(Image contentCoAuthorImage) {
        if (this.contentCoAuthor == null) {
            return;
        }
        GridPane.setMargin(this.contentCoAuthor, new Insets(5, 0, 0, 55));
        this.contentCoAuthorImageInline.setImage(contentCoAuthorImage);
    }

    protected void setContentImageLarge(Image contentImage) {
        if (this.contentImageLarge != null) {
            this.contentImageLarge.setImage(contentImage);
        }
    }

    protected void setContentImageSmall(Image contentImage) {
        if (this.contentImageSmall != null) {
            GridPane.setMargin(this.labelPaperIcon, new Insets(0, 0, 5, 50));
            this.contentImageSmall.setImage(contentImage);
        }
    }

    protected void setQRCode(Image qrCodeImage) {
        if (this.qrCode != null) {
            if (this.pdfSymbol != null) {
                this.pdfSymbol.setVisible(true);
            }
            this.qrCode.setImage(qrCodeImage);
        }
    }

    protected void setRectangleSize() {
        if (this.contentRectangle != null) {
            this.contentRectangle.setHeight(
                    this.contentRectangle.getHeight() + this.contentDescription.getBoundsInParent().getHeight()
                            + this.contentTitle.getBoundsInLocal().getHeight());
        }
    }

    protected void bindToContentTime(String contentTime) {
        this.bindToFXMLElement(this.contentTime, contentTime);
    }

    protected void bindToContentLocation(String contentLocation) {
        if (this.contentLocation != null) {
            this.contentLocation.setText(contentLocation);
        }
    }

    protected void bindToContentAuthor(String contentAuthor) {
        this.bindToFXMLElement(this.contentAuthor, contentAuthor);
    }

    protected void bindToContentAuthorDetail(String contentAuthor) {
        if (this.labelAuthor != null) {
            this.labelAuthor.setVisible(true);
            this.bindToFXMLElement(this.contentAuthor, contentAuthor);
        }
    }

    protected void bindToContentCoAuthor(String contentCoAuthor) {
        if (this.labelCoAuthor != null) {
            this.labelCoAuthor.setVisible(true);
            this.bindToFXMLElement(this.contentCoAuthor, contentCoAuthor);
        }
    }

    protected void bindToDescription(String description, int length) {
        this.bindToFXMLElement(this.contentDescription, description, length);
    }

    protected void bindToDescription(String description) {
        this.bindToFXMLElement(this.contentDescription, description);
    }

    protected void bindToCommentsCounter(String number) {
        this.bindToFXMLElement(this.commentsCounter, number);
    }

    protected void setTitleClip(Node clip) {
        if (this.contentTitle != null) {
            this.contentTitle.setClip(clip);
        }
    }

    protected void setCategoryIcon(Image categoryIcon) {
        if (this.categoryIcon != null) {
            if (categoryIcon != null) {
                this.categoryIcon.setImage(categoryIcon);
                this.categoryIcon.setOpacity(1.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(1.0);
                }
            } else {
                this.categoryIcon.setOpacity(0.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(0.0);
                }
            }
        }
    }

    protected void setWebContent(String webContent) {
        if (this.detailWebView != null) {
            WebEngine engine = this.detailWebView.getEngine();
            engine.loadContent(webContent);
        }
    }

    public WebView getWebView() {
        return this.detailWebView;
    }

    public Text getInDetailCloseButton() {
        return this.inDetailCloseButton;
    }

    public void showGraphConnections(int count) {
        // if the FXML includes graphConnection point definitions (for example
        // detail mode might not ...)
        if (this.graphConnection01 != null) {
            this.graphConnection01.setVisible(count > 0);
            this.graphConnection02.setVisible(count > 1);
            this.graphConnection03.setVisible(count > 2);
            this.graphConnection04.setVisible(count > 3);
            this.graphConnection05.setVisible(count > 4);
            this.graphConnection06.setVisible(count > 5);
            this.graphConnection07.setVisible(count > 6);
            this.graphConnection08.setVisible(count > 7);
        }
    }

}
