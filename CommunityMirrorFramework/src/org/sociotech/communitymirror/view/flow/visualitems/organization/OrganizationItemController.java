
package org.sociotech.communitymirror.view.flow.visualitems.organization;

import java.net.URL;
import java.util.ResourceBundle;

import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * The organization controller class containing all fxml elements and
 * assertions.
 *
 * @author Peter Lachenmaier, Andrea Nutsi, Michael Koch
 */
public class OrganizationItemController extends FXMLController implements Initializable {
    @FXML
    Text organizationName;

    @FXML
    Text organizationNameShort;

    @FXML
    Text organizationDescription;

    @FXML
    WebView detailWebView;

    @FXML
    ImageView organizationLogo;

    @FXML
    ImageView organizationImageSquare;

    @FXML
    ImageView organizationImageRound;

    @FXML
    ImageView organizationLogoLarge;

    @FXML
    ImageView categoryIcon;

    @FXML
    Circle categoryIconBackground;

    @FXML
    Text inDetailCloseButton;

    @FXML
    Circle graphConnection01;
    @FXML
    Circle graphConnection02;
    @FXML
    Circle graphConnection03;
    @FXML
    Circle graphConnection04;
    @FXML
    Circle graphConnection05;
    @FXML
    Circle graphConnection06;
    @FXML
    Circle graphConnection07;
    @FXML
    Circle graphConnection08;

    @Override
    // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    }

    protected void bindToOrganizationName(String organizationName) {
        this.bindToFXMLElement(this.organizationName, organizationName);
        this.bindToFXMLElement(this.organizationNameShort, this.truncateText(organizationName, 45, true));
    }

    protected void bindToDescription(String description) {
        this.bindToFXMLElement(this.organizationDescription, description);
    }

    protected void setOrganizationLogo(Image organizationLogo) {
        if (this.organizationLogo != null) {
            this.organizationLogo.setVisible(organizationLogo != null);
            this.organizationLogo.setImage(organizationLogo);
        }
    }

    protected void setOrganizationImageSquare(Image organizationImageSquare) {
        if (this.organizationImageSquare != null) {
            this.organizationImageSquare.setVisible(organizationImageSquare != null);
            this.organizationImageSquare.setImage(organizationImageSquare);
        }
    }

    protected void setOrganizationImageRound(Image organizationImageRound) {
        if (this.organizationImageRound != null) {
            this.organizationImageRound.setVisible(organizationImageRound != null);
            this.organizationImageRound.setImage(organizationImageRound);
        }
    }

    protected void setOrganizationLogoLarge(Image organizationLogoLarge) {
        if (this.organizationLogoLarge != null) {
            this.organizationLogoLarge.setImage(organizationLogoLarge);
        }
    }

    protected void setOrganizationImageClip(Node clip) {
        if (this.organizationLogo != null) {
            this.organizationLogo.setClip(clip);
        }
    }

    protected void setCategoryIcon(Image categoryIcon) {
        if (this.categoryIcon != null) {
            if (categoryIcon != null) {
                this.categoryIcon.setImage(categoryIcon);
                this.categoryIcon.setOpacity(1.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(1.0);
                }
            } else {
                this.categoryIcon.setOpacity(0.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(0.0);
                }
            }
        }
    }

    protected void setWebContent(String webContent) {
        if (this.detailWebView != null) {
            WebEngine engine = this.detailWebView.getEngine();
            engine.setJavaScriptEnabled(true);
            engine.loadContent(webContent);
        }
    }

    public WebView getWebView() {
        return this.detailWebView;
    }

    public Text getInDetailCloseButton() {
        return this.inDetailCloseButton;
    }

    public void showGraphConnections(int count) {
        // if the FXML includes graphConnection point definitions (for example
        // detail mode might not ...)
        if (this.graphConnection01 != null) {
            this.graphConnection01.setVisible(count > 0);
            this.graphConnection02.setVisible(count > 1);
            this.graphConnection03.setVisible(count > 2);
            this.graphConnection04.setVisible(count > 3);
            this.graphConnection05.setVisible(count > 4);
            this.graphConnection06.setVisible(count > 5);
            this.graphConnection07.setVisible(count > 6);
            this.graphConnection08.setVisible(count > 7);
        }
    }

}
