
package org.sociotech.communitymirror.view.flow.visualitems;

import java.util.EventListener;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * The interface of an event listener that notifies registered listeners about
 * user interaction on a visual component.
 *
 * @author Eva Loesch
 */
public interface WorkspaceNodeListener extends EventListener {

    /**
     * Notifies about a touch tapped event on a visual item.
     * 
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event this listener should notify about
     */
    <T extends Item, V extends FXMLController> void onWorkspaceNodeTouchTapped(
            WorkspaceNodeInteractionEvent<T, V> event);

    /**
     * Notifies about a close event on a visual item.
     *
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event this listener should notify about
     */
    <T extends Item, V extends FXMLController> void onWorkspaceNodeInDetailClosed(
            WorkspaceNodeInteractionEvent<T, V> event);

    /**
     * Notifies about a zoom zoomed smaller event (with zoom factor < 1) on a
     * visual item.
     * 
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event this listener should notify about
     */
    <T extends Item, V extends FXMLController> void onWorkspaceNodeZoomZoomedSmaller(
            WorkspaceNodeInteractionEvent<T, V> event);

    /**
     * Notifies about a zoom zoomed bigger event (with zoom factor > 1) on a
     * visual item.
     * 
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event this listener should notify about
     */
    <T extends Item, V extends FXMLController> void onWorkspaceNodeZoomZoomedBigger(
            WorkspaceNodeInteractionEvent<T, V> event);
}
