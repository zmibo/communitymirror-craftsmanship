
package org.sociotech.communitymirror.view.flow.visualitems.blogpost;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymirror.CommunityMirror;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;

/**
 * The visualization of a blog post item in the detail visual state
 *
 * @author Andrea Nutsi
 *
 */
public class DetailBlogPostItem extends BlogPostItem {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(DetailBlogPostItem.class.getName());

    public DetailBlogPostItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        try {
            // Get FXML for micropost
            FXMLLoader loader = new FXMLLoader();
            URL location;
            String fxmlPath = "fxml/inDetail/blog.fxml";
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            loader.setLocation(location);
            AnchorPane blogPost = (AnchorPane) loader.load(location.openStream());
            this.controller = loader.getController();

            // get and set category information from meta tags
            String categoryString = null;
            String category_icons[] = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons")
                    .split(",");
            EList<MetaTag> metaTags = this.dataItem.getMetaTags();
            for (MetaTag metaTag : metaTags) {
                for (String category : category_icons) {
                    if (category.equals(metaTag.getName())) {
                        categoryString = metaTag.getName();
                        break;
                    }
                }
            }
            this.setCategory(categoryString);

            // set the content of this blog post

            /*
             * get and process template for details view - use FreeMarker
             * template
             * engine
             */
            Map<String, Object> templateData = new HashMap<>();
            templateData.put("category", categoryString);
            templateData.put("item", this);
            // perhaps we do not need this here ...
            templateData.put("title", this.blogTitle);
            templateData.put("description", this.blogContent);
            this.controller.setWebContent(this.processDetailTemplate("detailBlogPost.ftl", templateData));

            this.controller.bindToTagsCounter(this.numberTags);

            if (this.blogAuthorImage != null) {
                Circle circle = new Circle(8);
                circle.setLayoutY(8);
                circle.setLayoutX(8);
                this.controller.setBlogAuthorImageClip(circle);
                this.controller.setBlogAuthorImage(this.blogAuthorImage);
            }

            if (this.blogImage != null) {
                Circle circle = new Circle(40);
                circle.setLayoutY(40);
                circle.setLayoutX(40);
                this.controller.setBlogImageClip(circle);
                this.controller.setBlogImage(this.blogImage);
                this.controller.setBlogImageSquare(this.blogImageSquare);
                this.controller.setBlogImageRound(this.blogImageRound);
            }

            this.controller.bindToBlogDateField(this.blogDate);
            this.controller.bindToBlogAuthorField(this.blogAuthor);
            this.controller.bindToBlogTags(this.tags);
            this.controller.setBlogImageLarge(this.blogImage);
            this.controller.bindToBlogTitleLongField(this.blogTitle);
            this.controller.bindToBlogContent(this.blogContent);

            // show connection points for graph
            this.controller.showGraphConnections(this.getAllRelatedItems().size());

            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = blogPost.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(blogPost.getChildren().get(0));
            }

        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for BlogPostItem" + e.getStackTrace());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * shouldPerformDrag()
     */
    @Override
    protected boolean shouldPerformDrag(DragStartedEvent dragEvent) {
        WebView webView = this.controller.getWebView();
        if (webView != null) {
            Bounds boundsLocal = webView.getBoundsInLocal();
            if (boundsLocal != null) {
                Bounds boundsInScene = webView.localToScene(boundsLocal);
                if (boundsInScene != null) {
                    return !boundsInScene.contains(dragEvent.getSceneX(), dragEvent.getSceneY());
                }
            }
        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getInDetailCloseButton()
     */
    @Override
    protected Text getInDetailCloseButton() {
        return this.controller.getInDetailCloseButton();
    }

}
