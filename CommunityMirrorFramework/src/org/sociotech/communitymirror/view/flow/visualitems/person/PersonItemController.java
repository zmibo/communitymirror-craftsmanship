
package org.sociotech.communitymirror.view.flow.visualitems.person;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * The controller of a person item
 *
 * @author Andrea Nutsi
 *
 */
public class PersonItemController extends FXMLController implements Initializable {

    @FXML
    Text personName;

    @FXML
    Text personFirstName;

    @FXML
    Text personLastName;

    @FXML
    Text personDescription;

    @FXML
    Text personOrganizationName;

    @FXML
    Text personOrganizationNameShort;

    @FXML
    WebView detailWebView;

    @FXML
    ImageView personImage;

    @FXML
    ImageView personImageLarge;

    @FXML
    ImageView personImageSquare;

    @FXML
    ImageView personImageRound;

    @FXML
    Text inDetailCloseButton;

    @FXML
    ImageView categoryIcon;

    @FXML
    Circle categoryIconBackground;

    @FXML
    Text personInitials;

    @FXML
    ImageView personQR;

    @FXML
    Circle graphConnection01;
    @FXML
    Circle graphConnection02;
    @FXML
    Circle graphConnection03;
    @FXML
    Circle graphConnection04;
    @FXML
    Circle graphConnection05;
    @FXML
    Circle graphConnection06;
    @FXML
    Circle graphConnection07;
    @FXML
    Circle graphConnection08;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(PersonItemController.class.getName());

    /*
     * (non-Javadoc)
     *
     * @see javafx.fxml.Initializable#initialize(java.net.URL,
     * java.util.ResourceBundle)
     */
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            assert this.personName != null : "fx:id=\"personName\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personFirstName != null : "fx:id=\"personFirstName\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personLastName != null : "fx:id=\"personLastName\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personOrganizationName != null : "fx:id=\"personOrganizationName\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personImage != null : "fx:id=\"personImage\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personImageLarge != null : "fx:id=\"personImageLarge\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personInitials != null : "fx:id=\"personInitials\" was not injected: check your FXML file 'person.fxml'.";
            assert this.personQR != null : "fx:id=\"personQR\" was not injected: check your FXML file 'person.fxml'.";

        } catch (AssertionError e) {
            this.logger.error(e.getMessage());
        }

    }

    protected StringProperty getPersonName() {
        return this.personName.textProperty();
    }

    protected void setPersonName(String personName, double maxWidth) {
        this.personName.setText(personName);
        this.fitTextToMaximumWidth(this.personName, maxWidth);
    }

    protected void bindToPersonNameField(String name, double maxWidth) {
        this.bindToFXMLElement(this.personName, name);
        this.fitTextToMaximumWidth(this.personName, maxWidth);
    }

    protected void bindToPersonFirstName(String firstName) {
        this.bindToFXMLElement(this.personFirstName, firstName);
    }

    protected void bindToPersonLastName(String lastName) {
        this.bindToFXMLElement(this.personLastName, lastName);
    }

    protected void bindToPersonLastNameMove(String lastName) {
        this.bindToFXMLElement(this.personLastName, lastName);
        // move last name to the right of first name
        this.personLastName.layoutXProperty()
                .set(this.personLastName.layoutXProperty().get() + this.personFirstName.getBoundsInParent().getWidth());
    }

    protected void bindToPersonInitials(String initials) {
        this.bindToFXMLElement(this.personInitials, initials);
    }

    protected void bindToDescription(String description) {
        this.bindToFXMLElement(this.personDescription, description);
    }

    protected void bindToPersonOrganizationName(String personOrganizationName) {
        this.bindToFXMLElement(this.personOrganizationName, personOrganizationName);
        this.bindToFXMLElement(this.personOrganizationNameShort, this.truncateText(personOrganizationName, 35, true));
    }

    protected ImageView getPersonImage() {
        return this.personImage;
    }

    protected void setPersonImage(Image personImage) {
        if (this.personImage != null) {
            this.personImage.setImage(personImage);
        }
    }

    protected void setPersonImageLarge(Image personImageLarge) {
        if (this.personImageLarge != null) {
            this.personImageLarge.setImage(personImageLarge);
        }
    }

    protected void setPersonImageSquare(Image personImageSquare) {
        if (this.personImageSquare != null) {
            this.personImageSquare.setImage(personImageSquare);
        }
    }

    protected void setPersonImageRound(Image personImageRound) {
        if (this.personImageRound != null) {
            this.personImageRound.setImage(personImageRound);
        }
    }

    protected void setPersonImageClip(Node clip) {
        if (this.personImage != null) {
            this.personImage.setClip(clip);
        }
    }

    protected void setCategoryIcon(Image categoryIcon) {
        if (this.categoryIcon != null) {
            if (categoryIcon != null) {
                this.categoryIcon.setImage(categoryIcon);
                this.categoryIcon.setOpacity(1.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(1.0);
                }
            } else {
                this.categoryIcon.setOpacity(0.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(0.0);
                }
            }
        }
    }

    protected void setWebContent(String webContent) {
        if (this.detailWebView != null) {
            WebEngine engine = this.detailWebView.getEngine();
            engine.loadContent(webContent);
        }
    }

    protected void setPersonQR(Image personQR) {
        if (this.personQR != null) {
            this.personQR.setImage(personQR);
        }
    }

    public WebView getWebView() {
        return this.detailWebView;
    }

    public Text getInDetailCloseButton() {
        return this.inDetailCloseButton;
    }

    public void showGraphConnections(int count) {
        // if the FXML includes graphConnection point definitions (for example
        // detail mode might not ...)
        if (this.graphConnection01 != null) {
            this.graphConnection01.setVisible(count > 0);
            this.graphConnection02.setVisible(count > 1);
            this.graphConnection03.setVisible(count > 2);
            this.graphConnection04.setVisible(count > 3);
            this.graphConnection05.setVisible(count > 4);
            this.graphConnection06.setVisible(count > 5);
            this.graphConnection07.setVisible(count > 6);
            this.graphConnection08.setVisible(count > 7);
        }
    }

}
