
package org.sociotech.communitymirror.view.flow.visualitems.person;

import org.sociotech.communitymashup.data.Person;

/**
 * The visualization of a person item in the preview visual state
 *
 * @author Andrea Nutsi
 *
 */
public class PreviewPersonItem extends PersonItem {
    public PreviewPersonItem(Person dataItem) {
        super(dataItem);
    }
}
