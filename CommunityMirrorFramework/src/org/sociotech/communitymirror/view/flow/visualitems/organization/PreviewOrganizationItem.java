
package org.sociotech.communitymirror.view.flow.visualitems.organization;

import org.sociotech.communitymashup.data.Organisation;

/**
 * The visualization of an organization item in the preview visual state
 * 
 * @author Andrea Nutsi
 *
 */
public class PreviewOrganizationItem extends OrganizationItem {
    public PreviewOrganizationItem(Organisation dataItem) {
        super(dataItem);
    }
}
