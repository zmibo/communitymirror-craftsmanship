package org.sociotech.communitymirror.view.flow.visualitems.micropost;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

/**
 * The visualization of a micropost item
 * @author Andrea Nutsi
 *
 */
public abstract class MicroPostItem  extends FlowVisualItem<Content, MicroPostItemController>{

	/**
	 * The data of this micropost
	 */
	private Content dataItem;
	
	/**
	 * Image of the Author
	 */
	protected javafx.scene.image.Image authorImage = null;
	
	/**
	 * The content of this micropost
	 */
	protected String microPostContent = "";
	
	/**
	 * The number of microposts of this person
	 */
	protected String microPostsCounter = "";
	
	/**
	 * The tags of this micropost
	 */
	protected String tags = "";
	
	/**
	 * The creation date of this micropost
	 */
	protected String creationDate = "";
	
	/**
	 * The image in this mircopost
	 */
	protected javafx.scene.image.Image micropostImage = null;
	
	/**
	 * The name of the author of this micropost
	 */
	protected String author = "";
	
	public MicroPostItem(Content dataItem) {
		super(dataItem);
		this.dataItem = dataItem;
	}

	@Override
	protected void onInit() {
		if (dataItem != null){
			getMicroPostAuthor();
		
			microPostContent = dataItem.getStringValue();
		
			author = dataItem.getAuthor().getName();
		
		//count the micropost contents of this author
		EList<Content> allContents = dataItem.getAuthor().getContents();
		int counter = 0;
		if(!allContents.isEmpty()){
			for(Content content: allContents){
				if(content.hasMetaTag("twitter")){
					counter++;
				}
			}
		}
		this.microPostsCounter = counter + "";
		
		//get the image of this micropost
		EList<Image> images = this.dataItem.getImages();
		String fileUrl = "";
		if(!images.isEmpty()){
			for(int i = 0; i<images.size(); i++){
				if(!images.get(i).hasMetaTag("takeaway_qr")){
					fileUrl = images.get(i).getFileUrl();
					//Set the image of this person
					micropostImage = ImageHelper.createImage(fileUrl, true);
					//if something failed try next fileUrl in dataset
					if(micropostImage!=null) break;	
				}		
			}
			
		}
		
		//tags
		EList<Tag> tags = dataItem.getTags();
		for(Tag tag : tags){
			this.tags += " #" + tag.getName();
		}
		
		this.creationDate = dataItem.getCreatedPretty();
		}
		render();
		super.onInit();
	}
	
	/**
	 * Method to override in subclasses to generate the visual representation
	 */
	protected abstract void render();
	
	/**
	 * Gets an image of the author of this micropost
	 */
	private void getMicroPostAuthor(){
//		EList<Image> images = dataItem.getAuthor().getImages();
//		String fileUrl = "";
//		if(!images.isEmpty()){
//			for(int i = 0; i<images.size(); i++){
//				//take first image
//				fileUrl = images.get(i).getFileUrl();
//				//Set the image of this person
//				authorImage = ImageHelper.createImage(fileUrl, true);
//				//if something failed try next fileUrl in dataset
//				if(authorImage!=null) break;	
//			}
//			
//		}
		
		//create Metatag list
		LinkedList<String> metaTagList = new LinkedList<>();
		metaTagList.add("profile_image_big_main");
		metaTagList.add("profile_image_big");
		
		for(String metaTag : metaTagList){ 
			org.sociotech.communitymashup.data.Image mashupImage = dataItem.getAuthor().getAttachedImageWithMetaTagName(metaTag);
			if(mashupImage != null){
				String fileUrl = mashupImage.getFileUrl();
				authorImage = ImageHelper.createImage(fileUrl, true);
				if(authorImage!=null) break;	
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedPersons()
	 */
	@Override
	protected List<Person> getRelatedPersons()
	{
		Content microPost = (Content) this.getDataItem();
		List<Person> relatedPersons = new LinkedList<Person>();
		relatedPersons.add(microPost.getAuthor());
		
		return relatedPersons;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedTags()
	 */
	@Override
	protected List<Tag> getRelatedTags()
	{
		Content microPost = (Content) this.getDataItem();
		
		List<Tag> relatedTags = new LinkedList<Tag>();
		for(Tag tag : microPost.getTags()){
			//if(tag.getTagged().size() > 1) 
			relatedTags.add(tag);
		}
		
		return relatedTags;
	}
}
