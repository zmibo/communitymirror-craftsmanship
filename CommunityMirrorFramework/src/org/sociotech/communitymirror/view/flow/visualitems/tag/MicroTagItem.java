package org.sociotech.communitymirror.view.flow.visualitems.tag;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import org.sociotech.communitymashup.data.Tag;

import com.google.common.io.Resources;

/**
 * The visualization of a tag item in the micro visual state
 * @author Andrea Nutsi
 *
 */
public class MicroTagItem extends TagItem {

	public MicroTagItem(Tag dataItem) {
		super(dataItem);
	}


	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.tag.TagItem#render()
	 */
	protected void render() {
		try {
			// Get FXML for tag
			FXMLLoader loader = new FXMLLoader();

			URL location;
			String fxmlPath = "fxml/inMicro/tag.fxml";
            if(this.themeResources != null) {
            	location = this.themeResources.getResourceURL(fxmlPath);
            } else {
            	location = Resources.getResource("flow/" + fxmlPath);
            	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
			loader.setLocation(location);
			AnchorPane tagPane = (AnchorPane) loader.load(location.openStream());
			TagItemController controller = loader.getController();

			//bind tag name to text of tag FXML
			controller.bindToTagContentField(tagContent);

			controller.bintToTagsNumber(tagsNumber);


			//prevent ConcurrentModificationException
			int size = tagPane.getChildren().size();
			for(int i = 0; i < size; i++){
				addNode(tagPane.getChildren().get(0));
			}
		} catch (IOException e) {
			logger.error("Problems loading fxml-file for TagItem" + e.getStackTrace());
		}
	}
}
