
package org.sociotech.communitymirror.view.flow.visualitems.organization;

import org.sociotech.communitymashup.data.Organisation;

/**
 * The visualization of an organization item in the detail visual state
 *
 * @author Andrea Nutsi
 *
 */
public class DetailOrganizationItem extends OrganizationItem {
    public DetailOrganizationItem(Organisation dataItem) {
        super(dataItem);
    }
}
