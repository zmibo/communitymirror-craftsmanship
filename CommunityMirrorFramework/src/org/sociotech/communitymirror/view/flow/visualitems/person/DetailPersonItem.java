
package org.sociotech.communitymirror.view.flow.visualitems.person;

import org.sociotech.communitymashup.data.Person;

/**
 * The visualization of a person item in the detail visual state
 *
 * @author Andrea Nutsi
 *
 */
public class DetailPersonItem extends PersonItem {
    public DetailPersonItem(Person dataItem) {
        super(dataItem);
    }
}
