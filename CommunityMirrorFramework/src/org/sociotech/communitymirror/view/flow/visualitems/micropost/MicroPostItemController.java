package org.sociotech.communitymirror.view.flow.visualitems.micropost;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

/**
 * The controller of a MircoPost item
 * @author Andrea Nutsi
 *
 */
public class MicroPostItemController extends FXMLController implements Initializable{

	@FXML
	Text micropostContent;
	
	@FXML
	ImageView micropostAuthorImage;
	
	@FXML
	ImageView micropostImage;
	
	@FXML
	ImageView micropostImageLarge;
	
	@FXML
	Text micropostsCounter;
	
	@FXML
	ImageView kreissegment_authorImage;
	
	@FXML
	Ellipse micropostEllipse;
	
	@FXML
	Text micropostTags;
	
	@FXML
	Text micropostDate;
	
	@FXML
	Text micropostAuthor;
	
	
	/**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(MicroPostItemController.class.getName());

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try{
			 assert micropostContent != null : "fx:id=\"micropostContent\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostAuthorImage != null : "fx:id=\"micropostAuthorImage\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostImage != null : "fx:id=\"micropostImage\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostImageLarge != null : "fx:id=\"micropostImageLarge\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostsCounter != null : "fx:id=\"micropostsCounter\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert kreissegment_authorImage != null : "fx:id=\"kreissegment_authorImage\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostTags != null : "fx:id=\"micropostTags\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostDate != null : "fx:id=\"micropostDate\" was not injected: check your FXML file 'micropost.fxml'.";
			 assert micropostAuthor != null : "fx:id=\"micropostAuthor\" was not injected: check your FXML file 'micropost.fxml'.";
		 }catch(AssertionError e){
			 logger.error(e.getMessage());
		 }

	}
	
	protected StringProperty getMicropostContent() {
		return micropostContent.textProperty();
	}

	protected void setMicropostContent(String micropostContent) {
		this.micropostContent.setText(micropostContent);
	}
	
	protected void bindToMicropostContentField(String micropostContent, int length){
		bindToFXMLElement(this.micropostContent, StringUtils.abbreviate(micropostContent, length));
	}
	
	protected void bindToMicropostContentField(String micropostContent){
		bindToFXMLElement(this.micropostContent, micropostContent);
	}
	
	protected void bindToMicropostsCounter(String counter){
		bindToFXMLElement(this.micropostsCounter, counter);
	}
	
	protected void bindToMicropostTags(String tags){
		bindToFXMLElement(micropostTags, tags);
	}
	
	protected void setMicroPostTagClip(Node clip){
		this.micropostTags.setClip(clip);
	}
	
	protected void bindToMicropostDate(String date){
		bindToFXMLElement(this.micropostDate, date);
	}
	
	protected void bindToMicropostAuthor(String author){
		bindToFXMLElement(this.micropostAuthor, author);
	}
	
	protected void setMicropostAuthorImage(Image micropostAuthorImage){
		this.kreissegment_authorImage.setVisible(true);
		this.micropostAuthorImage.setImage(micropostAuthorImage);
	}
	
	protected void setMicropostAuthorImageClip(Node clip){
		this.micropostAuthorImage.setClip(clip);
	}
	
	protected void setMicropostImage(Image micropostImage){
		this.micropostImage.setImage(micropostImage);
	}
	
	protected void setMicropostImageClip(Node clip){
		this.micropostImage.setClip(clip);
	}
	
	protected void setMicropostImageLarge(Image micropostImage){
		this.micropostImageLarge.setImage(micropostImage);
	}
	
	/**
	 * Fits the size of the ellipse to the textual content and adapts the Position of
	 * the text accordingly.
	 * @param micropostContent The textual content of the ellipse
	 */
	protected void setEllipseSize(){
		double newRadius = (this.micropostContent.getBoundsInLocal().getHeight()/2) + 50;
		this.micropostEllipse.setRadiusY(newRadius);
		this.micropostEllipse.setRadiusX(this.micropostEllipse.getRadiusX()*1.4);
		this.micropostContent.setLayoutY(this.micropostContent.getLayoutY()- newRadius*0.6);
	}

}
