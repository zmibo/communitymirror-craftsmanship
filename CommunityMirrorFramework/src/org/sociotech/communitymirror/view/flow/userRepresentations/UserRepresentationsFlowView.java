
package org.sociotech.communitymirror.view.flow.userRepresentations;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communityinteraction.behaviors.MagneticPole;
import org.sociotech.communityinteraction.eventcreators.IntersectionEventCreator;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.physics.behaviors.SpiralDriftBehavior;
import org.sociotech.communitymirror.physics.behaviors.StayInBoundsBehavior;
import org.sociotech.communitymirror.view.flow.CommunicativeFlowView;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * A CommunicativeFlowView that is able to display visual representations of the
 * users that are in front of the screen (e.g. based on data from kinect
 * sensor).
 *
 * @author Lösch
 *
 */
public class UserRepresentationsFlowView extends CommunicativeFlowView {

    /**
     * List of all visual user representations that can be displayed on this
     * flow view.
     */
    private List<VisualUserRepresentation> visualuserRepresentations;

    /**
     * @param width
     * @param height
     * @param configuration
     * @param dataSet
     */
    public UserRepresentationsFlowView(double width, double height, Configuration configuration, DataSet dataSet) {
        super(width, height, configuration, dataSet);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.FlowView#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();
        // add all visual user representations to the visual group of flow items
        // first
        this.addAllVisualUserRepresentations();
    }

    /**
     * Adds all visual user representations to the visual group containing all
     * flow items.
     */
    private void addAllVisualUserRepresentations() {
        this.visualuserRepresentations = CommunityMirror.getVisualUserRepresentations();
        if (this.visualuserRepresentations != null) {
            for (VisualUserRepresentation visualuserRepresentation : this.visualuserRepresentations) {
                this.flowComponents.addVisualComponent(visualuserRepresentation, true);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.FlowView#onUpdate(double,
     * long)
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);
        // this.updateVisualUserRepresentations();

        // long currentTime = System.currentTimeMillis();
        // long timeSinceLastUpdate = currentTime - timeOfLastUpdate;
        // System.out.println("timeSinceLastUpdate: " + timeSinceLastUpdate);
        // timeOfLastUpdate = currentTime;

        // if overlapping event creator exists then check for overlapping events
        this.createIntersectionEvents();
    }

    // private void updateVisualUserRepresentations() {
    // for (VisualUserRepresentation userRepresentation :
    // this.visualuserRepresentations) {
    // VisualGroup parentGroup = userRepresentation.getParentVisualGroup();
    // if (userRepresentation.isVisible()) {
    // if (parentGroup == null) {
    // this.flowItems.addVisualComponent(userRepresentation, true);
    // }
    //
    // this.flowItems.addVisualComponent(userRepresentation, true);
    // } else if (!userRepresentation.isVisible()) {
    // System.out.println("remove");
    // if (parentGroup == this.flowItems) {
    // this.flowItems.removeVisualComponent(userRepresentation, true);
    // }
    // }
    // }
    // }

    /**
     * let the intersection event creator check for intersection events if it
     * exists
     */
    private void createIntersectionEvents() {
        IntersectionEventCreator intersectionEventCreator = CommunityMirror.getIntersectionEventCreator();

        if (intersectionEventCreator != null) {
            List<VisualComponent> firstSetOfComponents = new LinkedList<>();

            // create a drift item FOR TESTING!
            boolean testMagneticBehavior = false;
            if (testMagneticBehavior) {
                FlowVisualItem<? extends Item, ? extends FXMLController> spiralDriftItem = this
                        .createMagneticDriftItem();
                firstSetOfComponents.add(spiralDriftItem);
            }

            if (this.visualuserRepresentations != null) {
                firstSetOfComponents.addAll(this.visualuserRepresentations);
            }

            List<VisualComponent> secondSetOfComponents = new LinkedList<>();
            for (VisualComponent component : this.flowComponents.getVisualComponents()) {
                if (component instanceof FlowVisualItem) {
                    secondSetOfComponents.add(component);
                }
            }

            // create intersection events
            intersectionEventCreator.createIntersectionEvents(firstSetOfComponents, secondSetOfComponents);
        }
    }

    private FlowVisualItem<? extends Item, ? extends FXMLController> createMagneticDriftItem() {
        // update the list of possible items
        this.updateDataList();
        // add new (random) item to the flow
        double randomY = this.random.nextDouble();
        // prevent items flowing over lower stacks
        randomY = randomY * (this.getViewBounds().getMaxY() - 500);
        FlowVisualItem<? extends Item, ? extends FXMLController> spiralDriftItem = this.addFlowVisualItemToView(
                this.dataItems.get(this.random.nextInt(this.dataItems.size())), this.getViewBounds().getMaxX() / 2,
                this.getViewBounds().getMaxY() / 2, 1, 1, VisualState.PREVIEW, null);
        // set greater magnetic force and range
        spiralDriftItem.makeMagnetic(new MagneticField(10, 100, MagneticPole.SOUTH));
        // keep time item inside screen
        spiralDriftItem.addPhysicsBehavior(
                StayInBoundsBehavior.createStayInBoundsBehaviorWithOffset(this.getViewBounds(), 0.0));
        // add spiral drift behavior to timeItem
        spiralDriftItem.addPhysicsBehavior(new SpiralDriftBehavior(0.2, 0.0, 0.0, 0.1, 1.0));

        return spiralDriftItem;
    }
}
