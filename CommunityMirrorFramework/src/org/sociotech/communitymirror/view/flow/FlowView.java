
package org.sociotech.communitymirror.view.flow;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communityinteraction.behaviors.MagneticPole;
import org.sociotech.communityinteraction.events.InteractionEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymashup.data.observer.dataset.DataSetChangeObserver;
import org.sociotech.communitymashup.data.observer.dataset.DataSetChangedInterface;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.components.ImageComponent;
import org.sociotech.communitymirror.configuration.ConfigurationLoader;
import org.sociotech.communitymirror.graph.creation.GraphVisualItemCreator;
import org.sociotech.communitymirror.graph.structure.Graph;
import org.sociotech.communitymirror.graph.structure.GraphNode;
import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.DriftBehavior;
import org.sociotech.communitymirror.physics.behaviors.SpiralDriftBehavior;
import org.sociotech.communitymirror.physics.behaviors.StayInBoundsBehavior;
import org.sociotech.communitymirror.physics.behaviors.StopMoveOnInteractionBehavior;
import org.sociotech.communitymirror.physics.behaviors.destroy.FadeOutAndDieBehavior;
import org.sociotech.communitymirror.physics.behaviors.position.CallBackOnBoundsExitBehavior;
import org.sociotech.communitymirror.physics.behaviors.position.LeftBoundsCallback;
import org.sociotech.communitymirror.physics.behaviors.timeout.TimeOutAndCallbackBehavior;
import org.sociotech.communitymirror.physics.behaviors.timeout.TimeOutCallBack;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.view.flow.components.FlowContentRenderer;
import org.sociotech.communitymirror.view.flow.components.FlowMetaTagRenderer;
import org.sociotech.communitymirror.view.flow.components.FlowOrganizationRenderer;
import org.sociotech.communitymirror.view.flow.components.FlowPersonRenderer;
import org.sociotech.communitymirror.view.flow.components.FlowTagRenderer;
import org.sociotech.communitymirror.view.flow.components.VisualComponentEvent;
import org.sociotech.communitymirror.view.flow.components.VisualComponentsPile;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.view.flow.visualitems.MetaTagItem;
import org.sociotech.communitymirror.view.flow.visualitems.PileEventListener;
import org.sociotech.communitymirror.view.flow.visualitems.TimeItem;
import org.sociotech.communitymirror.view.flow.visualitems.WorkspaceNodeInteractionEvent;
import org.sociotech.communitymirror.view.flow.visualitems.WorkspaceNodeListener;
import org.sociotech.communitymirror.view.flow.visualitems.blogpost.BlogPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.content.ContentItem;
import org.sociotech.communitymirror.view.flow.visualitems.micropost.MicroPostItem;
import org.sociotech.communitymirror.view.flow.visualitems.organization.OrganizationItem;
import org.sociotech.communitymirror.view.flow.visualitems.person.PersonItem;
import org.sociotech.communitymirror.view.flow.visualitems.picture.PictureItem;
import org.sociotech.communitymirror.view.flow.visualitems.tag.TagItem;
import org.sociotech.communitymirror.visualgroups.layout.CircleVisualGroup;
import org.sociotech.communitymirror.visualitems.FXVisualComponent;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.components.spring.BasicSpringLooper;
import org.sociotech.communitymirror.visualitems.factory.GenericItemFactory;
import org.sociotech.communitymirror.visualitems.factory.TypeBasedVisualItemFactory;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;
import org.sociotech.communitymirror.visualstates.ComponentState;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.Preview;
import org.sociotech.communitymirror.visualstates.VisualState;

import com.facebook.rebound.BaseSpringSystem;
import com.facebook.rebound.SpringClock;
import com.google.common.io.Resources;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.CacheHint;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

/**
 * A view in which information particles are flowing over the screen
 *
 * @author Andrea Nutsi, Michael Koch
 *
 */
public class FlowView extends FXView implements LeftBoundsCallback, PileEventListener, WorkspaceNodeListener,
        GraphVisualItemCreator, TimeOutCallBack, DataSetChangedInterface {

    /**
     * The base spring system where nodes and springs of flow view workspaces
     * are added.
     */
    protected BaseSpringSystem springSystem;

    /**
     * Local random number generator.
     */
    protected Random random = new Random(System.currentTimeMillis());

    /**
     * Behavior that stops moving of items after an interaction is performed on
     * them
     */
    protected StopMoveOnInteractionBehavior stopMoveOnInteraction;

    /**
     * The factory to produce information objects
     */
    protected TypeBasedVisualItemFactory<Item, FlowVisualItem<Item, ? extends FXMLController>> factory;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(FlowView.class.getName());

    /**
     * The components pile displayed in the right lower corner
     */
    private VisualComponentsPile<InformationObject, FXMLController> rightPile;
    /**
     * The components pile displayed in the left lower corner
     */
    private VisualComponentsPile<InformationObject, FXMLController> leftPile;

    /**
     * Visual groups used on the top level of the view (to hold different
     * Components)
     */
    protected Map<String, VisualGroup> topLevelVisualGroups = new HashMap<>();

    /**
     * Visual group for all items in the flow -
     * this usually is just a shortcut for stickyFlowItems.get("flow")
     */
    protected VisualGroup flowComponents;

    /**
     * List of FlowVisualItems that should be copied when a graph is generated
     * (special items like Time or About)
     */
    protected List<FlowVisualItem<?, ?>> stickyFlowItems = new LinkedList<>();

    /**
     * The list of data items from the configured mashup source
     */
    protected List<InformationObject> dataItems;

    /**
     * The list of recently created or changed data items - to be inserted in
     * the flow
     */
    protected List<InformationObject> dataItemsUpdated = new LinkedList<>();

    /**
     * The list of data items from the configured mashup source with meta tag
     * "important"
     */
    protected List<InformationObject> dataItemsImportant;
    protected String importantMetaTag = "important";

    /**
     * The list of most recently added/modified data items from the configured
     * mashup source
     */
    protected List<InformationObject> dataItemsMostRecent;

    /**
     * List of data items that are currently in the flow
     */
    protected List<InformationObject> dataItemsInFlow;

    /**
     * The maximum number of flowing items
     */
    private int maxNumberFlowingItems;

    protected HashMap<VisualItem<?>, GraphNode> visualItemNodeMapping;

    /**
     * Keeps the date of the last change in the data set.
     */
    private Date lastDataSetChange;

    /**
     * Indicates whether persons are to be included in the flow
     */
    private boolean displayPersons = false;

    /**
     * Indicates whether organizations are to be included in the flow
     */
    private boolean displayOrganizations = false;

    /**
     * Indicates whether events are to be included in the flow
     */
    private boolean displayContents = false;

    /**
     * Indicates whether pictures are to be included in the flow
     */
    private boolean displayPictures = false;

    /**
     * Indicates whether time item is displayed
     */
    private boolean displayTime = false;

    /**
     * Indicates whether two piles should be displayed in the bottom of the view
     */
    private boolean usePiles = false;

    /**
     * Indicates whether an item in the flow should be zoomable
     */
    private boolean flowItemZoomable = false;

    /**
     * Indicates whether an item in the flow should be rotateable
     */
    private boolean flowItemRotateable = false;

    /**
     * The metatags to be used for events
     */
    private String[] contentMetaTagNames;

    /**
     * The metatags to be used for microposts
     */
    private String[] microPostMetaTagNames;

    /**
     * The metatags to be used for blogposts
     */
    private String[] blogPostMetaTagNames;

    /**
     * The metatags to be used for pictures
     */
    protected String[] pictureMetaTagNames;

    /**
     * The metatag to be used for Objects for the time particle
     */
    private String timeMetaTagName;

    /**
     * Used for alternating text flow direction (right-left and left-right)
     */
    private int directionSwitcher = 1;

    /**
     * Creates the FlowView with the given width and height.
     *
     * @param width
     *            The width of the view
     * @param height
     *            The height of the view
     * @param configuration
     *            The configuration of the view
     * @param dataSet
     *            The data set used in this view
     */
    public FlowView(double width, double height, Configuration configuration, DataSet dataSet) {
        super(width, height, configuration, dataSet);
    }

    @Override
    protected void onInit() {
        super.onInit();

        // add stylesheet
        String css = this.themeResources.getResourceURLString("theme.css");
        this.scene.getStylesheets().add(css);

        /*
         * currently the VisualComponent tree only includes a background image
         * component (if it was configured) - now add some VisualGroups to group
         * VisualComponents later - store the groups under the labels
         * "background", "flow", "area" and "front"
         */
        // TODO make this configurable
        // create and add background group
        VisualGroup visualGroup = new VisualGroup();
        visualGroup.init();
        this.addVisualComponent(visualGroup, true);
        this.topLevelVisualGroups.put("background", visualGroup);
        // create and add flow group
        visualGroup = new VisualGroup();
        visualGroup.init();
        this.addVisualComponent(visualGroup, true);
        this.topLevelVisualGroups.put("flow", visualGroup);
        this.flowComponents = visualGroup;
        // create and add area group (on top of flow group)
        visualGroup = new VisualGroup();
        visualGroup.init();
        this.addVisualComponent(visualGroup, true);
        this.topLevelVisualGroups.put("area", visualGroup);
        // create and add graph group (on top of area group)
        visualGroup = new VisualGroup();
        visualGroup.init();
        this.addVisualComponent(visualGroup, true);
        this.topLevelVisualGroups.put("graph", visualGroup);
        // create and add front group (on top of graph group)
        visualGroup = new VisualGroup();
        visualGroup.init();
        this.addVisualComponent(visualGroup, true);
        this.topLevelVisualGroups.put("front", visualGroup);

        this.importantMetaTag = this.configuration.getString("context.metatag");

        // make view touchable (to enable action on tabHoldEvent on view)
        this.makeTouchable();

        // now do some setup ...
        this.createFactories();
        this.flowItemZoomable = this.configuration.getBoolean("view.flow.flowItemZoomable", true);
        this.flowItemRotateable = this.configuration.getBoolean("view.flow.flowItemRotateable", true);

        // initialize spring system
        this.springSystem = new BaseSpringSystem(new SpringClock(), new BasicSpringLooper());

        // initialize new URLHandler for WebEngine
        System.setProperty("java.protocol.handler.pkgs", "org.sociotech.communitymirror.components.web");

        // create a stop move behavior that is added to all flow items
        // TODO put time to configuration
        this.stopMoveOnInteraction = StopMoveOnInteractionBehavior.createStopMoveBehavior(5000);

        this.setDisplayItemsFromConfiguration();
        this.setMetaTagsFromConfiguration();

        // update the list of possible items
        this.updateDataList();

        // initialize data items in flow
        this.dataItemsInFlow = new LinkedList<>();

        // now create special (sticky) items
        this.addStickyVisualComponents();

        // create piles
        this.usePiles = this.configuration.getBoolean("view.flow.showPiles", true);
        if (this.usePiles) {
            if (this.dataItems.size() > 11) {
                this.createPiles(this.dataItems.subList(11, Math.min(this.dataItems.size() - 1, 30)));
            }
        }

        this.maxNumberFlowingItems = this.configuration.getInt("view.flow.flowingItems", 10);

        // mapping between visual items and graph nodes
        this.visualItemNodeMapping = new HashMap<>();

        // create dataset listener - for being notified of changes in the
        // dataset
        new DataSetChangeObserver(this.getDataSet(), this);

        // preload item panel
        String panelMetaTag = this.configuration.getString("view.flow.panel.metatag");
        if (panelMetaTag != null && panelMetaTag.length() > 0) {
            this.preloadItemPanel(panelMetaTag);
        }
    }

    private FlowVisualItem<? extends Item, ? extends FXMLController> addSingleMovingTimeItem() {
        // add Time item
        FlowVisualItem<? extends Item, ? extends FXMLController> timeItem = this.addFlowVisualItemToView(
                this.getDataSet().getMetaTag(this.timeMetaTagName),
                "org.sociotech.communitymirror.view.flow.visualitems.TimeItem", this.getViewBounds().getMaxX() - 100,
                this.getViewBounds().getMaxY() / 3, 1, 1, VisualState.PREVIEW, null);

        // set this time item
        if (timeItem == null) {
            return null;
        }
        // this.timeItem = timeItem;
        this.stickyFlowItems.add(timeItem);

        // keep time item inside screen
        timeItem.addPhysicsBehavior(
                StayInBoundsBehavior.createStayInBoundsBehaviorWithOffset(this.getViewBounds(), 0.0));

        // add spiral drift behavior to timeItem
        timeItem.addPhysicsBehavior(new SpiralDriftBehavior(0.2, 0.0, 0.0, 0.1, 1.0));

        // stop move after interaction
        timeItem.addPhysicsBehavior(this.stopMoveOnInteraction);

        return timeItem;
    }

    protected void addStickyVisualComponents() {

        // create time item
        if (this.displayTime) {
            this.addSingleMovingTimeItem();
        }

        // create other sticky items
        String names = this.configuration.getString("view.flow.sticky.components");
        if (names != null) {
            String[] nameArray = names.split("\\s*,\\s*");
            for (String name : nameArray) {
                this.logger.info("Adding sticky component " + name);
                final String aname = "view.flow.sticky.components." + name;
                String type = this.configuration.getString(aname + ".type");
                if (type == null) {
                    type = "VisualComponent";
                }
                String classname = this.configuration.getString(aname + ".classname");
                Double scaleX = this.configuration.getDouble(aname + ".scale.x", 1.0);
                Double scaleY = this.configuration.getDouble(aname + ".scale.y", 1.0);
                Double posX = this.configuration.getDouble(aname + ".pos.x");
                if (posX < 0) {
                    // if value given for posX is negative, interpret it as
                    // "this far from the right edge of the scene"
                    posX += this.scene.getWidth();
                }
                Double posY = this.configuration.getDouble(aname + ".pos.y");
                if (posY < 0) {
                    // if value given for posY is negative, interpret it as
                    // "this far from the bottom edge of the scene"
                    posY += this.scene.getHeight();
                }

                String layer = this.configuration.getString(aname + ".layer", "flow");
                if (layer == null) {
                    layer = "flow";
                }

                VisualComponent c = this.addVisualComponentToView(classname, posX, posY, scaleX, scaleY, layer, aname);
                c.setConfiguration(this.configuration);

                // if scale != 1 then change x,y to get correct settings
                Point2D point = c.localToScene(0, 0);
                if (point.getX() != 0) {
                    c.setTranslateX(posX - point.getX());
                }
                if (point.getY() != 0) {
                    c.setTranslateY(posY - point.getY());
                }

            }
        }
    }

    /**
     * Show a text notification which will disappear after some seconds
     */
    public void showNotification(String text, double posx, double posy) {
        Label label = new Label(text);
        label.setWrapText(true);
        label.setEffect(new Glow());
        label.setMinWidth(300);
        label.setMaxWidth(300);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setStyle("-fx-font-size: 20; -fx-text-fill: white;  -fx-background-color: #00000080;"
                + "-fx-background-radius: 15; -fx-padding: 4;");
        // TODO make themeable

        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(15.0);
        dropShadow.setOffsetX(0.0);
        dropShadow.setOffsetY(0.0);
        dropShadow.setColor(Color.BLACK);
        label.setEffect(dropShadow);

        VisualComponent visualComponent = new FXVisualComponent(label);

        // TODO Find out why the below line is necessary here (it probably
        // shouldn't be).
        visualComponent.resetInteractionTime();

        visualComponent
                .addPhysicsBehavior(new FadeOutAndDieBehavior(1500L, 5000L, visualComponent.getOpacity(), false));
        visualComponent.init();
        this.addVisualComponentToView(visualComponent, posx, posy, 1.0, 1.0, "front");
    }

    /**
     * Create FlowVisualItem and set parameter for creating set of selected
     * related items according to configuration.
     */
    protected FlowVisualItem<Item, ? extends FXMLController> createFlowVisualItem(Item item, VisualState visualState) {
        return this.createFlowVisualItem(item, null, visualState);
    }

    @SuppressWarnings("unchecked")
    protected FlowVisualItem<Item, ? extends FXMLController> createFlowVisualItem(Item item, String className,
            VisualState visualState) {
        // produce flow visual item
        FlowVisualItem<Item, ? extends FXMLController> visualItem = null;
        if (className != null) {
            // try to instantiate class from given class name
            try {
                visualItem = (FlowVisualItem<Item, ? extends FXMLController>) Class.forName(className)
                        .getConstructor(MetaTag.class).newInstance(item);
                visualItem.setThemeResourceLoader(this.themeResources);
                visualItem.init();
            } catch (Throwable e) {
                this.logger.warn("Did not succeed to produce FlowVisualItem from class " + className, e);
                visualItem = null;
            }
        }
        if (visualItem == null) {
            visualItem = this.factory.produceVisualItem(item, visualState, this.themeResources);
        }
        // set parameter from configuration
        this.setRelatedItemPreferredNumbersFromConfigInItem(visualItem);

        return visualItem;
    }

    /**
     * Gets a string representation of a FlowVisualItem dependent of it's exact
     * class type
     *
     * @param visualItem
     *            the VisualItem the string representation should be returned
     * @return string representation of the given visualItem
     */
    private String getStringForFlowVisualItem(FlowVisualItem<? extends Item, ? extends FXMLController> visualItem) {
        String itemType = null;

        if (visualItem instanceof OrganizationItem) {
            itemType = "organization";
        } else if (visualItem instanceof PersonItem) {
            itemType = "person";
        } else if (visualItem instanceof ContentItem) {
            itemType = "event";
        } else if (visualItem instanceof TimeItem) {
            itemType = "time";
        } else if (visualItem instanceof MetaTagItem) {
            itemType = "metatag";
        } else if (visualItem instanceof TagItem) {
            itemType = "tag";
        } else if (visualItem instanceof BlogPostItem) {
            itemType = "blogpost";
        } else if (visualItem instanceof MicroPostItem) {
            itemType = "micropost";
        } else if (visualItem instanceof PictureItem) {
            itemType = "picture";
        }

        return itemType;
    }

    /**
     * Gets all preferred numbers for selecting set of related items from config
     * and saves it in corresponding fields.
     */
    protected void setRelatedItemPreferredNumbersFromConfigInItem(
            FlowVisualItem<? extends Item, ? extends FXMLController> visualItem) {
        String itemType = this.getStringForFlowVisualItem(visualItem);

        if (itemType != null) {
            String configPathPrefix = "view.flow.graph.relatedItems.";

            // related Persons
            String configPathRelatedPersons = configPathPrefix + itemType + ".relatedPersons";
            int preferredMinimalNumberOfRelatedPersons = this.configuration.getInt(configPathRelatedPersons);
            visualItem.setPreferredMinimalNumberOfRelatedPersons(preferredMinimalNumberOfRelatedPersons);

            // related Organizations
            String configPathRelatedOrganizations = configPathPrefix + itemType + ".relatedOrganizations";
            int preferredMinimalNumberOfRelatedOrganisations = this.configuration
                    .getInt(configPathRelatedOrganizations);
            visualItem.setPreferredMinimalNumberOfRelatedOrganisations(preferredMinimalNumberOfRelatedOrganisations);

            // related Tags
            String configPathRelatedTags = configPathPrefix + itemType + ".relatedTags";
            int preferredMinimalNumberOfRelatedTags = this.configuration.getInt(configPathRelatedTags);
            visualItem.setPreferredMinimalNumberOfRelatedTags(preferredMinimalNumberOfRelatedTags);

            // related Events
            String configPathRelatedEvents = configPathPrefix + itemType + ".relatedEvents";
            int preferredMinimalNumberOfRelatedEvents = this.configuration.getInt(configPathRelatedEvents);
            visualItem.setPreferredMinimalNumberOfRelatedEvents(preferredMinimalNumberOfRelatedEvents);

            // related MicroPosts
            String configPathRelatedMicroPosts = configPathPrefix + itemType + ".relatedMicroPosts";
            int preferredMinimalNumberOfRelatedMicroPosts = this.configuration.getInt(configPathRelatedMicroPosts);
            visualItem.setPreferredMinimalNumberOfRelatedMicroPosts(preferredMinimalNumberOfRelatedMicroPosts);

            // related BlogPosts
            String configPathRelatedBlogPosts = configPathPrefix + itemType + ".relatedBlogPosts";
            int preferredMinimalNumberOfRelatedBlogPosts = this.configuration.getInt(configPathRelatedBlogPosts);
            visualItem.setPreferredMinimalNumberOfRelatedBlogPosts(preferredMinimalNumberOfRelatedBlogPosts);

            // related Comments
            String configPathRelatedComments = configPathPrefix + itemType + ".relatedComments";
            int preferredMinimalNumberOfRelatedComments = this.configuration.getInt(configPathRelatedComments);
            visualItem.setPreferredMinimalNumberOfRelatedComments(preferredMinimalNumberOfRelatedComments);

            // related Pictures
            String configPathRelatedPictures = configPathPrefix + itemType + ".relatedPictures";
            int preferredMinimalNumberOfRelatedPictures = this.configuration.getInt(configPathRelatedPictures);
            visualItem.setPreferredMinimalNumberOfRelatedPictures(preferredMinimalNumberOfRelatedPictures);

            // maximum number
            int maximumNumberOfRelatedItems = this.configuration.getInt(configPathPrefix + "maxCount");
            visualItem.setMaximumNumberOfRelatedItems(maximumNumberOfRelatedItems);
        }
    }

    /**
     * Load values from configuration which indicate if item type should be
     * displayed or not
     */
    private void setDisplayItemsFromConfiguration() {
        this.displayPersons = this.configuration.getBoolean("view.flow.particles.person.visible", true);
        this.displayOrganizations = this.configuration.getBoolean("view.flow.particles.organization.visible", true);
        this.displayContents = this.configuration.getBoolean("view.flow.particles.content.visible", true);
        this.displayPictures = this.configuration.getBoolean("view.flow.particles.picture.visible", false);
        this.displayTime = this.configuration.getBoolean("view.flow.particles.time.visible", false);
    }

    /**
     * Loads metatags which should be used for items from configuration
     */
    private void setMetaTagsFromConfiguration() {
        this.microPostMetaTagNames = ConfigurationLoader.getStringArray(this.configuration,
                "view.flow.particles.content.micropost.metatag");
        this.blogPostMetaTagNames = ConfigurationLoader.getStringArray(this.configuration,
                "view.flow.particles.content.blogpost.metatag");
        this.contentMetaTagNames = ConfigurationLoader.getStringArray(this.configuration,
                "view.flow.particles.content.content.metatag");
        this.pictureMetaTagNames = ConfigurationLoader.getStringArray(this.configuration,
                "view.flow.particles.content.picture.metatag");
        this.timeMetaTagName = this.configuration.getString("view.flow.particles.time.metatag");
    }

    /**
     * Filters data from the CommunityMashup data set and keeps possible items
     * in the data items list
     */
    protected void updateDataList() {
        if (this.dataSet == null) {
            return;
        }
        Date lastChange = this.dataSet.getLastModified();

        if (this.lastDataSetChange != null && !lastChange.after(this.lastDataSetChange)) {
            // update only if there are some new elements
            return;
        }

        this.lastDataSetChange = lastChange;

        // TODO adjust selection of data
        this.dataItems = new LinkedList<>();
        this.dataItemsImportant = new LinkedList<>();
        this.dataItemsMostRecent = new LinkedList<>();

        this.logger.info("Loading objects for presentation:");

        // get data from mashup for organizations
        if (this.displayOrganizations) {
            int orgCount = 0;
            final EList<Organisation> organizations = this.getDataSet().getOrganisations();
            if (!organizations.isEmpty()) {
                for (Organisation organization : organizations) {
                    // just add organizations with images, one is always a qr
                    // code
                    if (organization.getImages().size() > 1) {
                        this.dataItems.add(organization);
                        orgCount++;
                    }
                }
                // dataItems.addAll(organizations);
            }
            this.logger.info("- orgs: " + orgCount);
        }

        // get data from mashup for persons
        if (this.displayPersons) {
            int persCount = 0;
            final EList<Person> persons = this.getDataSet().getPersons();
            if (!persons.isEmpty()) {
                for (Person person : persons) {
                    // just add persons with images and a minimum of information
                    if ((person.getImages().size() > 1)) {
                        this.dataItems.add(person);
                        persCount++;
                    }
                }
                // dataItems.addAll(persons);
            }
            this.logger.info("- pers: " + persCount);
        }

        // get data from mashup for content
        if (this.displayContents) {
            int contentCount = 0;
            EList<MetaTag> contentMetaTagList = new BasicEList<>();
            EList<String> contentMetaTags = new BasicEList<>();
            for (String contentMetaTagName : this.contentMetaTagNames) {
                Object o = this.getDataSet().getMetaTag(contentMetaTagName);
                if (o != null) {
                    contentMetaTagList.add(this.getDataSet().getMetaTag(contentMetaTagName));
                    contentMetaTags.add(contentMetaTagName);
                }
            }
            for (String contentMetaTagName : this.microPostMetaTagNames) {
                Object o = this.getDataSet().getMetaTag(contentMetaTagName);
                if (o != null) {
                    contentMetaTagList.add(this.getDataSet().getMetaTag(contentMetaTagName));
                    contentMetaTags.add(contentMetaTagName);
                }
            }
            for (String contentMetaTagName : this.blogPostMetaTagNames) {
                Object o = this.getDataSet().getMetaTag(contentMetaTagName);
                if (o != null) {
                    contentMetaTagList.add(this.getDataSet().getMetaTag(contentMetaTagName));
                    contentMetaTags.add(contentMetaTagName);
                }
            }
            final EList<Content> contentsContents = this.getDataSet().getContentsWithOneOfMetaTags(contentMetaTagList);
            if (!contentsContents.isEmpty()) {
                this.dataItems.addAll(contentsContents);
                contentCount += contentsContents.size();
            }
            this.logger.info("- contents: " + contentCount + " (" + contentMetaTags.toString() + ")");
        }

        // get data from mashup for pictures
        if (this.displayPictures) {
            int pictureCount = 0;
            EList<MetaTag> pictureMetaTagList = new BasicEList<>();
            for (String pictureMetaTagName : this.pictureMetaTagNames) {
                pictureMetaTagList.add(this.getDataSet().getMetaTag(pictureMetaTagName)); // "impression_image"
            }
            final EList<Content> pictureContents = this.getDataSet().getContentsWithOneOfMetaTags(pictureMetaTagList);

            if (!pictureContents.isEmpty()) {
                this.dataItems.addAll(pictureContents);
                pictureCount += pictureContents.size();
            }
            this.logger.info("- pictures: " + pictureCount);
        }

        Collections.shuffle(this.dataItems);

        // get items with meta tag "important"
        EList<MetaTag> importantMetaTagList = new BasicEList<>();
        importantMetaTagList.add(this.getDataSet().getMetaTag(this.importantMetaTag));
        final EList<InformationObject> importantItems = this.getDataSet()
                .getInformationObjectsWithOneOfMetaTags(importantMetaTagList);
        if (!importantItems.isEmpty()) {
            this.dataItemsImportant.addAll(importantItems);
        }

        // get the 10 most recently added items from dataItems
        LinkedList<InformationObject> tmplist = new LinkedList<>();
        tmplist.addAll(this.dataItems);
        tmplist.sort(Comparator.comparing(InformationObject::getLastModified));
        for (int i = 0; i < 10; i++) {
            if (tmplist.size() > i) {
                InformationObject io = tmplist.get(tmplist.size() - i - 1);
                this.dataItemsMostRecent.add(io);
            } else {
                break;
            }
        }

        this.logger.info("Loaded objects from CommunityMashup for presentation: " + this.dataItems.size() + " ("
                + this.importantMetaTag + ": " + this.dataItemsImportant.size() + ", mostrecent: "
                + this.dataItemsMostRecent.size() + ")");

    }

    /**
     * Add changed or newly created objects to the dataItemsUpdated list - to be
     * displayed as soon as possible in the flow
     */
    @Override
    public void dataSetChanged(Notification notification) {
        if (notification.getEventType() == Notification.REMOVE) {
            return;
        }
        Object o = notification.getNotifier();
        if (o instanceof org.sociotech.communitymashup.data.InformationObject) {
            this.logger.info("dataSetChanged: " + notification.toString());
            this.dataItemsUpdated.add((InformationObject) o);
        }
    }

    /**
     * Creates the factories for this view and sets the renderer for the visual
     * states
     */
    protected void createFactories() {

        this.factory = new TypeBasedVisualItemFactory<>();

        // Persons
        GenericItemFactory<Person, PersonItem> personFactory = new GenericItemFactory<>();
        personFactory.setRendererForVisualState(new FlowPersonRenderer(), VisualState.DETAIL);
        personFactory.setRendererForVisualState(new FlowPersonRenderer(), VisualState.MICRO);
        personFactory.setRendererForVisualState(new FlowPersonRenderer(), VisualState.PREVIEW);
        this.factory.setFactoryForType(Person.class, personFactory);

        // Organizations
        GenericItemFactory<Organisation, OrganizationItem> organizationFactory = new GenericItemFactory<>();
        organizationFactory.setRendererForVisualState(new FlowOrganizationRenderer(), VisualState.DETAIL);
        organizationFactory.setRendererForVisualState(new FlowOrganizationRenderer(), VisualState.MICRO);
        organizationFactory.setRendererForVisualState(new FlowOrganizationRenderer(), VisualState.PREVIEW);
        this.factory.setFactoryForType(Organisation.class, organizationFactory);

        // Content
        GenericItemFactory<Content, VisualItem<Content>> contentFactory = new GenericItemFactory<>();
        contentFactory.setRendererForVisualState(new FlowContentRenderer(this.configuration), VisualState.DETAIL);
        contentFactory.setRendererForVisualState(new FlowContentRenderer(this.configuration), VisualState.MICRO);
        contentFactory.setRendererForVisualState(new FlowContentRenderer(this.configuration), VisualState.PREVIEW);
        this.factory.setFactoryForType(Content.class, contentFactory);

        // MetaTag
        GenericItemFactory<MetaTag, VisualItem<MetaTag>> metaTagFactory = new GenericItemFactory<>();
        metaTagFactory.setRendererForVisualState(new FlowMetaTagRenderer(), VisualState.DETAIL);
        metaTagFactory.setRendererForVisualState(new FlowMetaTagRenderer(), VisualState.MICRO);
        metaTagFactory.setRendererForVisualState(new FlowMetaTagRenderer(), VisualState.PREVIEW);
        this.factory.setFactoryForType(MetaTag.class, metaTagFactory);

        // Tag
        GenericItemFactory<Tag, VisualItem<Tag>> tagFactory = new GenericItemFactory<>();
        tagFactory.setRendererForVisualState(new FlowTagRenderer(), VisualState.DETAIL);
        tagFactory.setRendererForVisualState(new FlowTagRenderer(), VisualState.MICRO);
        tagFactory.setRendererForVisualState(new FlowTagRenderer(), VisualState.PREVIEW);
        this.factory.setFactoryForType(Tag.class, tagFactory);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.physics.behaviors.position.
     * LeftBoundsCallback #leftBounds(javafx.geometry.Bounds,
     * org.sociotech.communitymirror.physics.PhysicsApplicable)
     */
    @Override
    public void leftBounds(Bounds bounds, PhysicsApplicable leavingObject) {

        if (leavingObject instanceof VisualItem<?>) {
            VisualItem<?> leavingItem = (VisualItem<?>) leavingObject;

            // log user activity consequence
            this.performVisualComponentLeftScreenLogging(leavingItem);

            if (leavingObject instanceof TimeItem) {
                // reposition time item -> should always be on screen
                leavingItem.setPositionX(this.getViewBounds().getMaxX());
                leavingItem.setPositionY(this.getViewBounds().getMaxY() / 3);
            } else {
                InformationObject item = (InformationObject) leavingItem.getDataItem();
                this.dataItemsInFlow.remove(item);
                leavingItem.destroy();
            }
        } else if (leavingObject instanceof VisualGroup) {
            // get left VisualGroup
            VisualGroup leavingGroup = (VisualGroup) leavingObject;

            // log user activity
            this.performVisualComponentLeftScreenLogging(leavingGroup);

            // get components of left VisualGroup
            List<VisualComponent> copiedVisualComponents = new LinkedList<>(leavingGroup.getVisualComponents());

            // destroy all components consequence
            for (VisualComponent component : copiedVisualComponents) {
                this.leftBounds(bounds, component);
            }
            // destroy left VisualGroup
            leavingGroup.destroy();
        }
    }

    /**
     * Visualizes two piles of visual items in the lower right and left corner
     * of the screen
     *
     * @param persons
     *            The person items to be visualized
     * @param organizations
     *            The organization items to be visualized
     * @param events
     *            The event items to be visualized
     */
    private void createPiles(List<? extends InformationObject> items) {

        List<VisualComponent> componentsRight = new LinkedList<>();
        List<VisualComponent> componentsLeft = new LinkedList<>();
        int counter = 1;

        // TODO duplicated code -> also in onPileEmpty

        for (InformationObject item : items) {
            // Produce VisualItem
            final FlowVisualItem<Item, ? extends FXMLController> visualItem = this.createFlowVisualItem(item,
                    VisualState.MICRO);

            if (visualItem != null) {
                // add Caching
                visualItem.setCache(true);
                visualItem.setCacheHint(CacheHint.SPEED);

                this.makeNewVisualItemInteractive(visualItem);

                if (counter % 2 == 0) {
                    componentsRight.add(visualItem);
                } else {
                    componentsLeft.add(visualItem);
                }
                counter++;
            } else {
                this.logger.debug("visual item is null, could not be produced");
            }
        }

        Collections.shuffle(componentsRight);
        Collections.shuffle(componentsLeft);

        this.rightPile = new VisualComponentsPile<>(this.configuration, componentsRight);
        this.leftPile = new VisualComponentsPile<>(this.configuration, componentsLeft);

        this.rightPile.init();
        this.leftPile.init();

        // position right pile
        this.rightPile.setPositionX(this.getWidth() - 300);
        this.rightPile.setPositionY(this.getHeight() - /* 200 */300);

        // position left pile
        this.leftPile.setPositionX(10);
        this.leftPile.setPositionY(this.getHeight() - /* 200 */300);

        this.rightPile.addPileEventListener(this);
        this.leftPile.addPileEventListener(this);

        // add right pile to area group
        VisualGroup visualGroup = this.getTopLevelVisualGroup("area");
        visualGroup.addVisualComponent(this.rightPile, true);
        visualGroup.addVisualComponent(this.leftPile, true);
    }

    /**
     * Return the topLevelVisualGroup for the given identifier - or the "flow"
     * group if non could be found
     */
    protected VisualGroup getTopLevelVisualGroup(String name) {
        VisualGroup visualGroup = this.topLevelVisualGroups.get(name);
        if (name == null) {
            this.logger.warn("Could not find top level group '" + name + "'. Using top level group 'flow'.");
            visualGroup = this.flowComponents;
        }
        return visualGroup;
    }

    /**
     * Makes new element interactive.
     *
     * @param visualItem
     *            Visual item to make interactive
     */
    protected void makeNewVisualItemInteractive(VisualItem<?> visualItem) {
        // make it interactive
        visualItem.makeDragable();
        if (this.flowItemRotateable) {
            visualItem.makeRotatable();
        }
        if (this.flowItemZoomable) {
            visualItem.makeZoomable();
        }
        visualItem.makeTouchable();
        visualItem.makeReactToOverlappingEvents();
        visualItem.makeMagnetic(new MagneticField(5, 5, MagneticPole.NORTH));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.visualitems.PileEventListener
     * #onPileLeft(org.sociotech.communitymirror.view.flow.visualitems.
     * WorkspaceNodeInteractionEvent)
     */
    @Override
    public <T extends Item, V extends FXMLController> void onPileLeft(WorkspaceNodeInteractionEvent<T, V> event) {

        FlowVisualItem<T, V> sourceNode = event.getSourceNode();

        // log user activity consequence
        FlowView.logActionUserConsequence("Pile has been left!", "pileLeft", sourceNode, null, null, null);

        // sourceNode.addPhysicsBehavior(driftBehavior);
        // sourceNode.addPhysicsBehavior(repositionOnScreenExit);

        // addListener
        sourceNode.addWorkspaceListener(this);

        // get fadeOutAndDieBehavior values from configuration
        long fadeOutTime = this.configuration.getInt("view.flow.fadeOutAndDieBehavior.previousStackItems.fadeOutTime");
        long waitToFadeOut = this.configuration
                .getInt("view.flow.fadeOutAndDieBehavior.previousStackItems.waitToFadeOut");

        // add fadeOutAndDieBehavior
        sourceNode.addPhysicsBehavior(
                FadeOutAndDieBehavior.createFadeOutAndDieBehavior(fadeOutTime, waitToFadeOut, sourceNode.getOpacity()));

        // destroy on screen exit
        this.destroyOnScreenExit(sourceNode);

        // workaround to transfer local (in group) coordinates of node into
        // coordinates of scene
        VisualGroup parentGroup = sourceNode.getParentVisualGroup();
        sourceNode.setPositionX(
                sourceNode.getPositionX() + parentGroup.localToScene(sourceNode.getBoundsInLocal()).getMinX());
        sourceNode.setPositionY(
                sourceNode.getPositionY() + parentGroup.localToScene(sourceNode.getBoundsInLocal()).getMinY());

        // remove node from its parent group (the pile)
        parentGroup.removeNode(sourceNode);

        // add to flow items
        this.flowComponents.addVisualComponent(sourceNode, true);
    }

    /**
     * Adds a behavior to the given visual item that will destroy it after
     * exiting the screen.
     *
     * @param visualItem
     *            Visual item to destroy after screen exit
     */
    protected <T extends Item, V extends FXMLController> void destroyOnScreenExit(VisualItem<?> visualItem) {
        visualItem.addPhysicsBehavior(CallBackOnBoundsExitBehavior
                .createCallBackOnBoundsExitBehaviorWithOffset(this.getViewBounds(), this, 0/*-50*/));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.visualitems.PileEventListener
     * #onPileEmpty
     * (org.sociotech.communitymirror.view.flow.components.VisualComponentsPile)
     */
    @Override
    public <T extends Item, V extends FXMLController> void onPileEmpty(VisualComponentsPile<T, V> pile) {

        // take 10 random items from existing dataset
        int start = (int) Math.floor(Math.random() * (this.dataItems.size() - 10));
        List<InformationObject> pileItems = this.dataItems.subList(start, start + 10);

        for (InformationObject item : pileItems) {
            this.addItemToPile(pile, item);
        }

    }

    /**
     * Adds a given data item to a given pile
     *
     * @param pile
     *            The Pile the item is added to
     * @param item
     *            The Data item to be added to the pile
     */
    public <T extends Item, V extends FXMLController> void addItemToPile(VisualComponentsPile<T, V> pile,
            InformationObject item) {
        if (item != null) {
            // Produce VisualItem
            final FlowVisualItem<Item, ? extends FXMLController> visualItem = this.createFlowVisualItem(item,
                    VisualState.MICRO);

            if (visualItem != null) {
                // add Caching
                visualItem.setCache(true);
                visualItem.setCacheHint(CacheHint.SPEED);

                this.makeNewVisualItemInteractive(visualItem);

                pile.addVisualComponentToPile(visualItem, false);
            } else {
                this.logger.debug("visual item is null, could not be produced");
            }

        }
    }

    /**
     * Looping update on the base spring system is needed to make spring forces
     * work.
     *
     * @author Eva Loesch
     */
    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.FXView#onUpdate(double, long)
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);

        if (this.rightPile != null && this.dataItems != null && this.rightPile.size() < 10) {
            this.addItemToPile(this.rightPile, this.dataItems.get(this.random.nextInt(this.dataItems.size())));
        }
        if (this.leftPile != null && this.dataItems != null && this.leftPile.size() < 10) {
            this.addItemToPile(this.leftPile, this.dataItems.get(this.random.nextInt(this.dataItems.size())));
        }
        if (this.flowComponents.size() < this.maxNumberFlowingItems) {
            // update the list of possible items
            this.updateDataList();
            try {
                this.renderRandomFlowVisualItem();
            } catch (Exception ex) {
                // there is no item to render - so set maxNumber accordingly
                this.logger.warn("failed in renderRandomFlowVisualItem", ex);
                this.maxNumberFlowingItems = this.flowComponents.size();
            }
        }

        // update spring system
        this.springSystem.loop();
    }

    /**
     * Method that is called by a workspace event listener in case of a zoom
     * zoomed smaller event (with zoom factor < 1) on visual items.
     *
     * @param WorkspaceNodeInteractionEvent
     *            <T, V> e : the workspace node interaction event that had
     *            occurred on one of the items of this workspace
     * @author Eva Loesch
     */
    @Override
    public <T extends Item, V extends FXMLController> void onWorkspaceNodeZoomZoomedSmaller(
            WorkspaceNodeInteractionEvent<T, V> event) {

        FlowVisualItem<T, V> sourceNode = event.getSourceNode();
        VisualState visualState = sourceNode.getVisualState();

        // check if item is in graph
        GraphNode existingNode = this.visualItemNodeMapping.get(sourceNode);
        if (existingNode != null && !visualState.isMicro()) {
            // go to previous state
            existingNode.previousState();
            return;
        }

        // else old implementation

        FlowVisualItem<? extends Item, ? extends FXMLController> parentNode = sourceNode.getParentNode();

        if (visualState instanceof Micro) {
            // tapped node may be part of a workspace or not
            // --> in either case do nothing
        } else if (visualState instanceof Preview) {
            // tapped node may be part of a workspace or not

            // --> if tapped item is NOT already part of a workspace create a
            // workspace
            if (parentNode == null) {
                // delete tapped item and create new item with VisualState MICRO
                FlowVisualItem<?, ?> newItem = this.transitionItem(sourceNode, VisualState.MICRO, this.factory);

                newItem.addPhysicsBehavior(
                        new DriftBehavior(-0.4, 0.0, 0.25, 0.25, 0.75 * (0.5 + 2 * this.random.nextDouble())));
                // stop move after interaction
                newItem.addPhysicsBehavior(this.stopMoveOnInteraction);
                // reset interaction time on zoom started events
                newItem.resetInteractionTime();

                // add listener
                newItem.addWorkspaceListener(this);
                // and add to flow view
                this.flowComponents.addVisualComponent(newItem, true);
            }
        }
    }

    /**
     * Method that is called by a workspace event listener in case of a zoom
     * zoomed bigger event (with zoom factor > 1) on visual items.
     *
     * @param WorkspaceNodeInteractionEvent
     *            <T, V> e : the workspace node interaction event that had
     *            occurred on one of the items of this workspace
     * @author Eva Loesch
     */
    @Override
    public <T extends Item, V extends FXMLController> void onWorkspaceNodeZoomZoomedBigger(
            WorkspaceNodeInteractionEvent<T, V> event) {

        VisualComponentEvent vcEvent = new VisualComponentEvent(event.getSourceNode(), event.getSourceEvent());
        event.getSourceNode().notifyListeners(vcEvent);
        if (!vcEvent.getSourceEvent().isConsumed()) {
            this.switchItemToNextVisualState(event.getSourceNode());
        }
    }

    /**
     * Method that is called by a workspace event listener in case of a touch
     * to close event (with zoom factor > 1) on visual items.
     *
     * @param WorkspaceNodeInteractionEvent
     *            <T, V> e : the workspace node interaction event that had
     *            occurred on one of the items of this workspace
     * @author Sebastian Pohle
     */
    @Override
    public <T extends Item, V extends FXMLController> void onWorkspaceNodeInDetailClosed(
            WorkspaceNodeInteractionEvent<T, V> event) {

        PhysicsApplicable timeOutObject = event.getSourceNode();

        if (!(timeOutObject instanceof VisualItem<?>)) {
            return;
        }

        // find mapping graph node
        GraphNode node = this.visualItemNodeMapping.get(timeOutObject);

        if (node != null) {
            // go to previous state - from Detail to Preview - which closes
            // graph
            node.previousState();
        }
    }

    /**
     * Method that is called by a workspace event listener in case of a touch
     * tapped event on visual items.
     *
     * @param WorkspaceNodeInteractionEvent
     *            <T, V> e : the workspace node interaction event that had
     *            occurred on one of the items of this workspace
     * @author Eva Loesch
     */
    @Override
    public <T extends Item, V extends FXMLController> void onWorkspaceNodeTouchTapped(
            WorkspaceNodeInteractionEvent<T, V> e) {
        VisualComponentEvent vcEvent = new VisualComponentEvent(e.getSourceNode(), e.getSourceEvent());
        e.getSourceNode().notifyListeners(vcEvent);
        if (!vcEvent.getSourceEvent().isConsumed()) {
            this.switchItemToNextVisualState(e.getSourceNode());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchHold
     * (org.sociotech.communityinteraction.events.touch.TouchHoldEvent)
     */
    @Override
    public void onTouchHold(TouchHoldEvent event) {
        // if configured then show item panel
        String panelMetaTag = this.configuration.getString("view.flow.panel.metatag");
        if (panelMetaTag != null && panelMetaTag.length() > 0) {
            this.showItemPanel(panelMetaTag);
        }
    }

    /**
     * Gets the event metatag
     *
     * @return eventMetatag
     */
    public String[] getContentMetaTagNames() {
        return this.contentMetaTagNames;
    }

    /**
     * Writes a log entry with information about a VisualComponent has left
     * screen as consequence of a user action.
     *
     * @param visualComponent
     *            : the VisualComponent that has left the screen
     */
    private void performVisualComponentLeftScreenLogging(VisualComponent visualComponent) {
        String message;
        String consequenceAction;

        if (visualComponent instanceof FlowVisualItem<?, ?>) {
            message = "Particle left screen!";
            consequenceAction = "particleLeftScreen";
        } else if (visualComponent instanceof Graph) {
            message = "Graph left screen!";
            consequenceAction = "graphLeftScreen";
        } else {
            message = "VisualComponent left screen!";
            consequenceAction = "visualComponentLeftScreen";
        }

        // if left visual component has drift behavior attached add this
        // information to message
        if (visualComponent.hasPhysicsBehavior(new DriftBehavior(0, 0).getClass())) {
            message = message + " (Left by DriftBehavior.)";
        }

        FlowView.logActionUserConsequence(message, consequenceAction, visualComponent, null, null, null);
    }

    /**
     * Switches the given flow visual item to the next higher visual state.
     *
     * @param sourceNode
     *            : the flow visual item that should be switched to the next
     *            higher visual state
     * @author Eva Loesch
     */
    private <T extends Item, V extends FXMLController> void switchItemToNextVisualState(
            FlowVisualItem<T, V> sourceNode) {
        // check if item is in graph
        GraphNode existingNode = this.visualItemNodeMapping.get(sourceNode);
        if (existingNode == null) {
            this.createGraph(sourceNode);
        } else {
            // go to next state
            existingNode.nextState();
        }
    }

    /**
     * Creates a new graph starting with the given visual item as first graph
     * node.
     *
     * @param visualItem
     *            Visual item as start node.
     */
    protected <T extends Item, V extends FXMLController> void createGraph(FlowVisualItem<T, V> visualItem) {

        // create graph with this flow view as item creator
        Graph graph = new Graph(this);

        // set number of related items
        graph.setMaximumNumberOfConnectedNodes(this.configuration.getInt("view.flow.graph.relatedItems.maxCount"));

        // set radius and layout factors
        graph.setRadius(this.configuration.getInt("view.flow.graph.layout.radius"));
        graph.setMicroRadiusFactor(this.configuration.getDouble("view.flow.graph.layout.microRadiusFactor"));
        graph.setPreviewRadiusFactor(this.configuration.getDouble("view.flow.graph.layout.previewRadiusFactor"));

        // set spring length factors
        graph.setMicroSpringLengthFactor(
                this.configuration.getDouble("view.flow.graph.layout.microSpringLengthFactor"));
        graph.setPreviewSpringLengthFactor(
                this.configuration.getDouble("view.flow.graph.layout.previewSpringLengthFactor"));
        graph.setDetailSpringLengthFactor(
                this.configuration.getDouble("view.flow.graph.layout.detailSpringLengthFactor"));

        // set animation of graph opening using springs
        graph.setRepositionAfterLayout(this.configuration.getBoolean("view.flow.graph.layout.animateOpening"));

        // set the background circle values
        graph.setBackgroundCircleFactor(this.configuration.getDouble("view.flow.graph.layout.backgroundCircleFactor"));
        graph.setBackgroundCircleColor(this.configuration.getString("view.flow.graph.layout.backgroundCircleColor"));
        graph.setBackgroundCircleOpacity(
                this.configuration.getDouble("view.flow.graph.layout.backgroundCircleOpacity"));

        // set the number of needed nodes for layouting
        graph.setNodesForLayout(this.configuration.getInt("view.flow.graph.layout.nodesForBackground"));

        // shared spring system
        graph.setSpringSystem(this.springSystem);

        // copy item
        VisualItem<?> newVisualItem = this.createVisualItemForGraph(visualItem.getDataItem(),
                visualItem.getVisualState());

        // transfer attributes relevant for graph node
        GraphNode.transferAttributes(visualItem, newVisualItem);

        // create graph node with new visual item
        GraphNode startNode = graph.createNode(newVisualItem);

        // keep node visual item mapping
        this.visualItemNodeMapping.put(newVisualItem, startNode);

        // delete old item if it is not a sticky item
        if (!this.stickyFlowItems.contains(visualItem)) {
            Item item = visualItem.getDataItem();
            this.dataItemsInFlow.remove(item);
            visualItem.destroy();
        }

        // make graph visible
        VisualGroup visualGroup = this.getTopLevelVisualGroup("graph");
        visualGroup.addVisualComponent(graph, true);

        // init graph
        graph.init();

        // directly go to next step in the start node
        startNode.nextState(); // needs to be executed before logging since this
                               // creates the conntected DataItems

        // send user activity log message about graph creation
        String connectedDataItemsIdentString = startNode.getConnectedDataItemsIdentString(); // List
                                                                                             // of
                                                                                             // all
                                                                                             // connected
                                                                                             // nodes
        FlowView.logActionUserConsequence("Graph has been created!" + connectedDataItemsIdentString, "graphCreated",
                visualItem, graph, null, null);
    }

    /**
     * Creates a new visual item with given VisualState and the dataItem of the
     * given old VisualItem WITHOUT caring about workspaces and WITHOUT deleting
     * the selected item.
     *
     * @param selectedItem
     *            : the visual item that should be replaced by a new visual item
     *            with given VisualState
     * @param newVisualState
     *            : the VisualState the new visual item should get
     *
     * @author Eva Loesch
     */
    private <T extends Item, V extends FXMLController> FlowVisualItem<?, ?> createNewItemFromExistingItem(
            FlowVisualItem<T, V> selectedItem, VisualState newVisualState,
            TypeBasedVisualItemFactory<Item, FlowVisualItem<Item, ? extends FXMLController>> factory) {
        FlowVisualItem<?, ?> newItem;

        newItem = this.createFlowVisualItem(selectedItem.getDataItem(), newVisualState);

        if (newItem != null) {
            this.makeNewVisualItemInteractive(newItem);

            // set Position
            newItem.setPositionX(selectedItem.getPositionX());
            newItem.setPositionY(selectedItem.getPositionY());

            // add Caching
            newItem.setCache(true);
            newItem.setCacheHint(CacheHint.SPEED);
        } else {
            LogManager.getLogger(FlowView.class.getName()).debug("visual item is null, could not be produced");
        }

        return newItem;
    }

    /**
     * Creates a new visual item with given VisualState and the dataItem of the
     * given old VisualItem.
     *
     * @param selectedItem
     *            : the visual item that should be replaced by a new visual item
     *            with given VisualState
     * @param newVisualState
     *            : the VisualState the new visual item should get
     *
     * @author Eva Loesch
     */
    public <T extends Item, V extends FXMLController> FlowVisualItem<?, ?> transitionItem(
            FlowVisualItem<T, V> selectedItem, VisualState newVisualState,
            TypeBasedVisualItemFactory<Item, FlowVisualItem<Item, ? extends FXMLController>> factory) {
        FlowVisualItem<?, ?> newItem = this.createNewItemFromExistingItem(selectedItem, newVisualState, factory);

        // get the parent node of the old item (may be null)
        FlowVisualItem<? extends Item, ? extends FXMLController> parentNode = selectedItem.getParentNode();
        if (parentNode != null) {
            // remove the old item from the list of child nodes of the parent
            // item
            parentNode.removeChildNode(selectedItem);

            // add the new item as child node to parent node
            parentNode.addChildNode(newItem);

            // set parent node
            newItem.setParentNode(parentNode);
        }

        // get circle group of old item (may be null)
        CircleVisualGroup circleGroup = selectedItem.getCircleGroup();
        if (circleGroup != null) {
            // set circle group for new item
            newItem.setCircleGroup(selectedItem.getCircleGroup());

            // remove oldComponent and add newComponent at position of
            // oldComponent
            try {
                circleGroup.substituteLayoutedComponent(selectedItem, newItem);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        selectedItem.destroy();

        return newItem;
    }

    /**
     * Creates a new flow visual item with random properties and adds it to the
     * flow.
     *
     * @return the rendered flow visual item
     *
     * @author Eva Lösch
     * @throws Exception
     *             if there is no unused data item that could be rendered
     */
    private FlowVisualItem<? extends Item, ? extends FXMLController> renderRandomFlowVisualItem() throws Exception {

        if (this.dataSet == null) {
            return null;
        }

        // get a random data item
        InformationObject item;
        try {
            item = this.getNotUsedRandomDataItem();
        } catch (Exception ex) {
            throw new Exception("There is no unused data item that could be rendered.", ex);
        }

        // get random scale
        double scaleX = 1;
        double scaleY = 1;
        // scale 10 percent of the new objects
        java.util.Random rand = new java.util.Random();
        int randint = rand.nextInt(10);
        if (randint == 8) {
            scaleX = 1.4;
            scaleY = 1.4;
        }
        if (randint > 8) {
            scaleX = 1.7;
            scaleY = 1.7;
        }

        // get random positionY
        double randomY = this.random.nextDouble();
        if (this.usePiles) {
            // prevent items flowing over lower stacks
            randomY = randomY * (this.getViewBounds().getMaxY() - 500);
        } else {
            randomY = randomY * (this.getViewBounds().getMaxY() - 225);
        }

        String direction = this.configuration.getString("view.flow.direction", "leftToRight&rightToLeft");

        switch (direction) {
        case "leftToRight":
            this.directionSwitcher = -1;
            break;
        case "rightToLeft":
            this.directionSwitcher = 1;
            break;
        case "leftToRight&rightToLeft":
            // switch direction switcher
            this.directionSwitcher *= -1;
            break;
        default:
            // switch direction switcher
            this.directionSwitcher *= -1;
            break;
        }

        // get random position x
        double randomX = this.directionSwitcher > 0 ? this.getViewBounds().getMaxX() : 0;
        randomX = (randomX + this.random.nextDouble() * 800.0) * this.directionSwitcher;

        // create random drift behavior
        DriftBehavior randomDriftBehavior = new DriftBehavior(-0.4 * this.directionSwitcher, 0.0, 5, 0.75,
                1.75 * (0.5 + 2 * this.random.nextDouble()));

        // create flow visual item
        FlowVisualItem<? extends Item, ? extends FXMLController> flowVisualItem = this.addFlowVisualItemToView(item,
                randomX, randomY, scaleX, scaleY, VisualState.PREVIEW, randomDriftBehavior);

        return flowVisualItem;
    }

    /**
     * Gets a random data item out of the list of dataItems, that is not in the
     * flow already.
     *
     * @return a random data item
     *
     * @author Eva Lösch, Michael Koch
     * @throws Exception
     *             if there is currently no unused data item
     */
    private InformationObject getNotUsedRandomDataItem() throws Exception {

        InformationObject item = null;

        // check if there are updated items waiting for being displayed
        if (this.dataItemsUpdated.size() > 0) {
            // take the first item from the list and remove it from list
            item = this.dataItemsUpdated.get(0);
            this.dataItemsUpdated.remove(item);
        } else {
            // take a random item
            // sometimes choose a random important or most recent data item
            int rand = this.random.nextInt(10);
            if ((rand > 7) && (this.dataItemsImportant.size() > 0)) {
                item = this.dataItemsImportant.get(this.random.nextInt(this.dataItemsImportant.size()));
            }
            if ((rand < 2) && (this.dataItemsMostRecent.size() > 0)) {
                item = this.dataItemsMostRecent.get(this.random.nextInt(this.dataItemsMostRecent.size()));
            }
        }

        // check if item is already displayed
        if (item != null) {
            if (this.dataItemsInFlow.contains(item)) {
                item = null;
            }
        }
        if (item != null) {
            return item;
        }

        // create temporary list of all items not yet in the flow
        List<InformationObject> eligibleDataItems = new LinkedList<>(this.dataItems);
        eligibleDataItems.removeAll(this.dataItemsInFlow);
        if (eligibleDataItems.size() == 0) {
            throw new Exception("Cannot return an unused data item because all are in use.");
        }

        item = eligibleDataItems.get(this.random.nextInt(eligibleDataItems.size()));

        return item;
    }

    /**
     * creates a new flow visual item with given properties and adds it to the
     * flow
     *
     * @param item
     * @param className
     *            - optional name of class to use for generating VisualItem
     * @param positionX
     * @param positionY
     * @param scaleX
     * @param scaleY
     * @param visualState
     * @param driftBehavior
     * @return the rendered flow visual item
     *
     * @author Eva Lösch, Michael Koch
     */
    protected FlowVisualItem<? extends Item, ? extends FXMLController> addFlowVisualItemToView(Item item,
            double positionX, double positionY, double scaleX, double scaleY, VisualState visualState,
            DriftBehavior driftBehavior) {
        return this.addFlowVisualItemToView(item, null, positionX, positionY, scaleX, scaleY, visualState,
                driftBehavior);
    }

    protected FlowVisualItem<? extends Item, ? extends FXMLController> addFlowVisualItemToView(Item item,
            String className, double positionX, double positionY, double scaleX, double scaleY, VisualState visualState,
            DriftBehavior driftBehavior) {
        // create flow visual item
        FlowVisualItem<? extends Item, ? extends FXMLController> flowVisualItem = null;

        if (item != null) {
            // produce flow visual item
            flowVisualItem = this.createFlowVisualItem(item, className, visualState);
            if (flowVisualItem == null) {
                return null;
            }

            // set scale
            flowVisualItem.setScaleX(scaleX);
            flowVisualItem.setScaleY(scaleY);

            // set position
            flowVisualItem.setPositionX(positionX);
            flowVisualItem.setPositionY(positionY);

            // add Caching
            flowVisualItem.setCache(true);
            flowVisualItem.setCacheHint(CacheHint.SPEED);

            flowVisualItem = this.addFlowVisualItemToView(flowVisualItem, driftBehavior);
        }

        return flowVisualItem;
    }

    public FlowVisualItem<? extends Item, ? extends FXMLController> addFlowVisualItemToView(
            FlowVisualItem<? extends Item, ? extends FXMLController> visualItem, DriftBehavior driftBehavior) {

        if (visualItem != null) {
            // add physics behaviors
            if (driftBehavior != null) {
                // add drift behavior
                visualItem.addPhysicsBehavior(driftBehavior);

                // add call back on screen exit behavior
                this.destroyOnScreenExit(visualItem);

                // add stop move on interaction behavior
                visualItem.addPhysicsBehavior(this.stopMoveOnInteraction);
            }

            // add interaction behaviors
            this.makeNewVisualItemInteractive(visualItem);

            // add data item to list of data items in flow
            if (visualItem.getDataItem() instanceof InformationObject) {
                this.dataItemsInFlow.add((InformationObject) visualItem.getDataItem());
            }

            // add flowVisualItem as VisualComponent and Node to flowItems
            this.flowComponents.addVisualComponent(visualItem, true);

            // addListener
            visualItem.addWorkspaceListener(this);
        }

        return visualItem;
    }

    /**
     * create a new visual component with given properties and add it to a layer
     *
     * @param className
     * @param positionX
     * @param positionY
     * @param scaleX
     * @param scaleY
     * @param layer
     * @return
     */
    protected VisualComponent addVisualComponentToView(String className, double positionX, double positionY,
            double scaleX, double scaleY, String layer) {
        return this.addVisualComponentToView(className, positionX, positionY, scaleX, scaleY, layer, null);
    }

    protected VisualComponent addVisualComponentToView(String className, double positionX, double positionY,
            double scaleX, double scaleY, String layer, String configPrefix) {
        VisualComponent visualComponent = this.createVisualComponent(className, positionX, positionY, scaleX, scaleY);
        if (configPrefix != null) {
            visualComponent.configure(this.configuration, configPrefix);
        }
        if (visualComponent != null) {
            visualComponent.makeTouchable();
            VisualGroup visualGroup = this.getTopLevelVisualGroup(layer);
            if (visualGroup != null) {
                visualGroup.addVisualComponent(visualComponent, true);
            }
        }
        return visualComponent;
    }

    protected VisualComponent addVisualComponentToView(VisualComponent visualComponent, double positionX,
            double positionY, double scaleX, double scaleY, String layer) {
        VisualGroup visualGroup = this.getTopLevelVisualGroup(layer);
        if (visualGroup != null) {
            visualGroup.addVisualComponent(visualComponent, true);
        } else {
            this.logger.warn("Top level group " + layer + " does not exist.");
        }
        return this.addVisualComponentToView(visualComponent, positionX, positionY, scaleX, scaleY, visualGroup);
    }

    protected VisualComponent addVisualComponentToView(VisualComponent visualComponent, double positionX,
            double positionY, double scaleX, double scaleY, VisualGroup visualGroup) {
        if (visualComponent != null) {
            // set scaling factor
            visualComponent.setScaleX(scaleX);
            visualComponent.setScaleY(scaleY);

            // set position
            visualComponent.setPositionX(positionX);
            visualComponent.setPositionY(positionY);

            // add caching
            visualComponent.setCache(true);
            visualComponent.setCacheHint(CacheHint.SPEED);

            visualComponent.makeTouchable();
            if (visualGroup != null) {
                visualGroup.addVisualComponent(visualComponent, true);
            } else {
                this.logger.warn("VisualGroup is null.");
            }
        }
        return visualComponent;
    }

    /**
     * creates a new visual component with given properties
     *
     * @param className
     *            - name of class to use for generating VisualComponent
     * @param positionX
     * @param positionY
     * @param scaleX
     * @param scaleY
     * @return the new visual component
     *
     * @author Michael Koch
     */
    protected VisualComponent createVisualComponent(String className, double positionX, double positionY, double scaleX,
            double scaleY) {
        // create visual component
        VisualComponent visualComponent = null;
        if (className != null) {
            // try to instantiate object from given class name
            try {
                visualComponent = (VisualComponent) Class.forName(className).getConstructor().newInstance();
                visualComponent.setThemeResourceLoader(this.themeResources);
                visualComponent.init();
            } catch (Throwable e) {
                this.logger.warn("Did not succeed to produce VisualComponent from class " + className, e);
                visualComponent = null;
            }
        }
        if (visualComponent == null) {
            return null;
        }

        // set scale
        visualComponent.setScaleX(scaleX);
        visualComponent.setScaleY(scaleY);

        // set position
        visualComponent.setPositionX(positionX);
        visualComponent.setPositionY(positionY);

        // add Caching
        visualComponent.setCache(true);
        visualComponent.setCacheHint(CacheHint.SPEED);

        return visualComponent;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * createVisualItem(org.sociotech.communitymashup.data.Item,
     * org.sociotech.communitymirror.visualstates.VisualState,
     * org.sociotech.communitymirror.view.flow.graph.Node)
     */
    @Override
    public VisualItem<?> createVisualItem(Item dataItem, VisualState visualState, GraphNode node) {
        // create visual item
        FlowVisualItem<Item, ? extends FXMLController> newVisualItem = this.createVisualItemForGraph(dataItem,
                visualState);

        // keep node visual item mapping
        this.visualItemNodeMapping.put(newVisualItem, node);

        return newVisualItem;
    }

    /**
     * Creates a visual item for usage in the graph
     *
     * @param dataItem
     *            Data item to be visualized
     * @param visualState
     *            State of the visualization
     * @return The new visual item
     */
    protected FlowVisualItem<Item, ? extends FXMLController> createVisualItemForGraph(Item dataItem,
            VisualState visualState) {
        FlowVisualItem<Item, ? extends FXMLController> newVisualItem = this.createFlowVisualItem(dataItem, null,
                visualState);

        long timeOut = 0;
        if (visualState instanceof Micro) {
            timeOut = this.configuration.getLong("view.flow.graph.close.microCloseTime");
        } else if (visualState instanceof Preview) {
            timeOut = this.configuration.getLong("view.flow.graph.close.previewCloseTime");
        } else if (visualState instanceof Detail) {
            timeOut = this.configuration.getLong("view.flow.graph.close.detailCloseTime");
        }

        // reset interaction time
        newVisualItem.resetInteractionTime();
        newVisualItem.addPhysicsBehavior(new TimeOutAndCallbackBehavior(timeOut, this));

        // add fade out to micro but do not destroy
        if (visualState instanceof Micro) {
            newVisualItem.addPhysicsBehavior(
                    new FadeOutAndDieBehavior(timeOut / 2, timeOut / 2, newVisualItem.getOpacity(), false));
        }

        // make interactive
        this.makeNewVisualItemInteractive(newVisualItem);

        // add this as workspace listener
        newVisualItem.addWorkspaceListener(this);

        // add Caching
        newVisualItem.setCache(true);
        newVisualItem.setCacheHint(CacheHint.SCALE_AND_ROTATE);
        return newVisualItem;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * getRelatedItemsFor(org.sociotech.communitymashup.data.Item)
     */
    @Override
    public Set<Item> getRelatedItemsFor(Item dataItem) {
        // produce a dummy flow visual item
        FlowVisualItem<Item, ? extends FXMLController> newVisualItem = this.createFlowVisualItem(dataItem, null,
                VisualState.DEFAULT);

        return newVisualItem.getAllRelatedItems();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.graph.creation.GraphVisualItemCreator#
     * getSelectedRelatedItemsToConnectFor
     * (org.sociotech.communitymashup.data.Item, java.util.Set)
     */
    @Override
    public Set<Item> getSelectedRelatedItemsToConnectFor(Item dataItem, Set<Item> connectedItems) {

        // produce a dummy flow visual item
        FlowVisualItem<Item, ? extends FXMLController> newVisualItem = this.createFlowVisualItem(dataItem, null,
                VisualState.DEFAULT);

        return newVisualItem.getSelectedSetOfRelatedItemsToConnect(connectedItems);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * destroyVisualItem(org.sociotech.communitymirror.visualitems.VisualItem)
     */
    @Override
    public void destroyVisualItem(VisualItem<?> visualItem) {
        // remove from node mapping
        GraphNode graphNode = this.visualItemNodeMapping.remove(visualItem);

        if (graphNode == null) {
            this.logger.warn("Tried to destroy a graph visual item that was not created before");
            return;
        }

        if (visualItem.getDataItem() instanceof InformationObject) {
            InformationObject item = (InformationObject) visualItem.getDataItem();
            this.dataItemsInFlow.remove(item);
        }

        // destroy visual item
        visualItem.destroy();

        Graph graph = graphNode.getGraph();

        // check if graph needs to be destroyed now
        if (!(graph.getComponentState() == ComponentState.DESTROYED) && graph.isNonVisible()) {

            // log user action consequence message
            FlowView.logActionUserConsequence("Graph has been destroyed!", "graphDestroyed", graph, null, null, null);

            // destroy
            graph.destroy();
            // remove from view
            VisualGroup visualGroup = this.getTopLevelVisualGroup("graph");
            visualGroup.removeVisualComponent(graph, true);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.physics.behaviors.timeout.TimeOutCallBack
     * #timedOut(org.sociotech.communitymirror.physics.PhysicsApplicable)
     */
    @Override
    public void timedOut(PhysicsApplicable timeOutObject) {
        // will be called when a graph node is not used for a given amount of
        // time
        if (!(timeOutObject instanceof VisualItem<?>)) {
            return;
        }

        // find mapping graph node
        GraphNode node = this.visualItemNodeMapping.get(timeOutObject);

        if (node != null) {
            // go to previous state
            node.previousState();
        }
    }

    /**
     * Log consequence of user action using the user activity logger.
     *
     * @param consequenceAction
     * @param visualComponent
     * @param searchTerm
     * @param numberOfSearchResults
     */
    public static void logActionUserConsequence(String message, String consequenceAction,
            VisualComponent visualComponent, VisualGroup parentVisualGroup, String searchTerm,
            String numberOfSearchResults) {
        // initialize details from visual component
        String visualComponentID = null;
        String currentPositionX = null;
        String currentPositionY = null;
        String parentGroupID = null;
        String parentGroupClassName = null;

        // get details from visual component
        // FlowView.getDetailsFromVisualComponent(visualComponent,
        // parentVisualGroup, visualComponentID, currentPositionX,
        // currentPositionY, parentGroupID, parentGroupClassName);
        if (visualComponent != null) {
            visualComponentID = visualComponent.getID();

            Bounds visualComponentBoundsInScene = visualComponent.localToScene(visualComponent.getBoundsInLocal());
            currentPositionX = Long.toString(Math.round(visualComponentBoundsInScene.getMinX()));
            currentPositionY = Long.toString(Math.round(visualComponentBoundsInScene.getMinY()));

            if (parentVisualGroup == null) {
                parentVisualGroup = visualComponent.getParentVisualGroup();
            }
            if (parentVisualGroup != null) {
                parentGroupID = parentVisualGroup.getID();
                parentGroupClassName = parentVisualGroup.getClass().getName();
            }
        }

        // initialize details from flow visual item
        String dataItemID = null;
        String dataType = null;
        String title = null;
        String fromVisualState = null;
        String toVisualState = null;
        String numberOfImages = null;
        String numberOfTags = null;

        // get details from flow visual item
        if (visualComponent instanceof FlowVisualItem) {
            @SuppressWarnings("unchecked")
            FlowVisualItem<? extends Item, ? extends FXMLController> flowVisualItem = (FlowVisualItem<? extends Item, ? extends FXMLController>) visualComponent;
            // FlowView.getDetailsFromVisualItem(flowVisualItem, dataItemID,
            // dataType, title, fromVisualState, toVisualState, numberOfImages,
            // numberOfTags);
            dataItemID = flowVisualItem.getDataItem().getIdent();
            dataType = flowVisualItem.getClass().getName();

            // get detail info from information object
            if (flowVisualItem.getDataItem() instanceof InformationObject) {
                InformationObject informationObject = (InformationObject) flowVisualItem.getDataItem();
                title = informationObject.getName();
                // log number of images that ar no qr-codes
                numberOfImages = "" + informationObject.getAttachedImagesWithoutMetaTagName("qr").size();
                // get number of attached tags
                numberOfTags = "" + informationObject.getMetaTags().size();
            }
            if (flowVisualItem.getDataItem() instanceof Tag) {
                Tag tag = (Tag) flowVisualItem.getDataItem();
                title = tag.getName();
                numberOfImages = null;
                // get number of references
                numberOfTags = "" + tag.getCount();
            }
        }

        // write log entry
        FlowView.logUserActivity(message, null, null, consequenceAction, visualComponentID, dataItemID, dataType, title,
                fromVisualState, toVisualState, numberOfImages, numberOfTags, currentPositionX, currentPositionY, null,
                null, null, null, null, null, null, null, null, null, parentGroupID, parentGroupClassName, searchTerm,
                numberOfSearchResults);
    }

    /**
     * Logs a user activity using the UserActivityLogger.
     *
     * @param :
     *            detail information to log
     */
    public static void logUserInteractionEvent(String message, String userAction, InteractionEvent event,
            VisualComponent visualComponent, String fromVisualState, String toVisualState) {
        // initialize details from visual component
        String visualComponentID = null;
        String currentPositionX = null;
        String currentPositionY = null;
        String parentGroupID = null;
        String parentGroupClassName = null;

        // get details from visual component
        // FlowView.getDetailsFromVisualComponent(visualComponent, null,
        // visualComponentID, currentPositionX, currentPositionY, parentGroupID,
        // parentGroupClassName);
        VisualGroup parentVisualGroup = null;
        if (visualComponent != null) {
            visualComponentID = visualComponent.getID();

            Bounds visualComponentBoundsInScene = visualComponent.localToScene(visualComponent.getBoundsInLocal());
            currentPositionX = Long.toString(Math.round(visualComponentBoundsInScene.getMinX()));
            currentPositionY = Long.toString(Math.round(visualComponentBoundsInScene.getMinY()));

            if (parentVisualGroup == null) {
                parentVisualGroup = visualComponent.getParentVisualGroup();
            }
            if (parentVisualGroup != null) {
                parentGroupID = parentVisualGroup.getID();
                parentGroupClassName = parentVisualGroup.getClass().getName();
            }
        }

        // initialize details from flow visual item
        String dataItemID = null;
        String dataType = null;
        String title = null;
        String numberOfImages = null;
        String numberOfTags = null;

        // get details from flow visual item
        if (visualComponent instanceof FlowVisualItem) {
            @SuppressWarnings("unchecked")
            FlowVisualItem<? extends Item, ? extends FXMLController> flowVisualItem = (FlowVisualItem<? extends Item, ? extends FXMLController>) visualComponent;
            // FlowView.getDetailsFromVisualItem(flowVisualItem, dataItemID,
            // dataType, title, fromVisualState, toVisualState, numberOfImages,
            // numberOfTags);
            dataItemID = flowVisualItem.getDataItem().getIdent();
            dataType = flowVisualItem.getClass().getName();

            // get detail info from information object
            if (flowVisualItem.getDataItem() instanceof InformationObject) {
                InformationObject informationObject = (InformationObject) flowVisualItem.getDataItem();
                title = informationObject.getName();
                // log number of images that ar no qr-codes
                numberOfImages = "" + informationObject.getAttachedImagesWithoutMetaTagName("qr").size();
                // get number of attached tags
                numberOfTags = "" + informationObject.getMetaTags().size();
            }
            if (flowVisualItem.getDataItem() instanceof Tag) {
                Tag tag = (Tag) flowVisualItem.getDataItem();
                title = tag.getName();
                numberOfImages = null;
                // get number of references
                numberOfTags = "" + tag.getCount();
            }
        }

        // initialize details from interaction event
        String gestureEvent = null;
        String zoomFactor = null;
        String dragStartPositionX = null;
        String dragStartPositionY = null;
        String dragEndPositionX = null;
        String dragEndPositionY = null;
        String touchTappedPositionX = null;
        String touchTappedPositionY = null;
        String rotationAngle = null;
        String rotationPositionX = null;
        String rotationPositionY = null;

        // get details from interaction event
        if (event != null) {
            // FlowView.getDetailsFromInteractionEvent(visualComponent, event,
            // gestureEvent, zoomFactor, dragStartPositionX, dragStartPositionY,
            // dragEndPositionX, dragEndPositionY, touchTappedPositionX,
            // touchTappedPositionY, rotationAngle, rotationPositionX,
            // rotationPositionY);
            if (event instanceof ZoomZoomedEvent) {
                ZoomZoomedEvent zoomEvent = (ZoomZoomedEvent) event;
                gestureEvent = "zoomZoomed";

                zoomFactor = "" + zoomEvent.getZoomFactor();
            } else if (event instanceof DragFinishedEvent) {
                DragFinishedEvent dragEvent = (DragFinishedEvent) event;
                gestureEvent = "dragFinished";

                if (visualComponent != null) {
                    dragStartPositionX = "" + visualComponent.getDragStartedSceneX();
                    dragStartPositionY = "" + visualComponent.getDragStartedSceneY();
                }
                dragEndPositionX = "" + dragEvent.getSceneX();
                dragEndPositionY = "" + dragEvent.getSceneY();
            } else if (event instanceof TouchTappedEvent) {
                gestureEvent = "touchTapped";

                TouchTappedEvent touchEvent = (TouchTappedEvent) event;

                touchTappedPositionX = Double.toString(touchEvent.getSceneX());
                touchTappedPositionY = Double.toString(touchEvent.getSceneY());
            } else if (event instanceof RotationEvent) {
                RotationFinishedEvent rotationEvent = (RotationFinishedEvent) event;
                gestureEvent = "rotationFinished";

                rotationAngle = Double.toString(rotationEvent.getAngle());
                rotationPositionX = Double.toString(rotationEvent.getSceneX());
                rotationPositionY = Double.toString(rotationEvent.getSceneY());
            } else if (event instanceof TouchHoldEvent) {
                TouchHoldEvent holdEvent = (TouchHoldEvent) event;
                gestureEvent = "touchHold";

                touchTappedPositionX = Double.toString(holdEvent.getSceneX());
                touchTappedPositionY = Double.toString(holdEvent.getSceneY());
            }
        }

        // write log entry
        FlowView.logUserActivity(message, userAction, gestureEvent, null, visualComponentID, dataItemID, dataType,
                title, fromVisualState, toVisualState, numberOfImages, numberOfTags, currentPositionX, currentPositionY,
                zoomFactor, dragStartPositionX, dragStartPositionY, dragEndPositionX, dragEndPositionY,
                touchTappedPositionX, touchTappedPositionY, rotationAngle, rotationPositionX, rotationPositionY,
                parentGroupID, parentGroupClassName, null, null);
    }

    /**
     * Generates a log entry for fixed set of detail information using the
     * UserActivityLogger
     *
     * @param :
     *            detail information to log
     */
    public static void logUserActivity(String message, String userAction, String gestureEvent, String consequenceAction,
            String visualComponentID, String dataItemID, String dataType, String title, String fromVisualState,
            String toVisualState, String numberOfImages, String numberOfTags, String currentPositionX,
            String currentPositionY, String zoomFactor, String dragStartPositionX, String dragStartPositionY,
            String dragEndPositionX, String dragEndPositionY, String touchTappedPositionX, String touchTappedPositionY,
            String rotationAngle, String rotationPositionX, String rotationPositionY, String parentGroupID,
            String parentGroupClassName, String searchTerm, String numberOfSearchResults) {
        // get timestamp
        Timestamp timeStamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        String timeStampString = timeStamp.toString();

        // get user activity logger and log message and details
        UserActivityLogger ul = CommunityMirror.getUserActivityLogger();
        ul.logActivity(ul.detail("TimeStamp", timeStampString), ul.detail("GestureEvent", gestureEvent),
                ul.detail("UserAction", userAction), ul.detail("ConsequenceAction", consequenceAction),
                ul.detail("Message", message), ul.detail("VisualComponentID", visualComponentID),
                ul.detail("DataItemID", dataItemID), ul.detail("DataType", dataType), ul.detail("Title", title),
                ul.detail("FromVisualState", fromVisualState), ul.detail("ToVisualState", toVisualState),
                ul.detail("NumberOfImages", numberOfImages), ul.detail("NumberOfTags", numberOfTags),
                ul.detail("CurrentPositionX", currentPositionX), ul.detail("CurrentPositionY", currentPositionY),
                ul.detail("ZoomFactor", zoomFactor), ul.detail("DragStartPositionX", dragStartPositionX),
                ul.detail("DragStartPositionY", dragStartPositionY), ul.detail("DragEndPositionX", dragEndPositionX),
                ul.detail("DragEndPositionY", dragEndPositionY),
                ul.detail("TouchTappedPositionX", touchTappedPositionX),
                ul.detail("TouchTappedPositionY", touchTappedPositionY), ul.detail("RotationAngle", rotationAngle),
                ul.detail("RotationPositionX", rotationPositionX), ul.detail("rotationPositionY", rotationPositionY),
                ul.detail("ParentGroupID", parentGroupID), ul.detail("ParentGroupClassName", parentGroupClassName),
                ul.detail("SearchTerm", searchTerm), ul.detail("NumberOfSearchResults", numberOfSearchResults));
    }

    /**
     * Show all items with a given metatag
     */
    public void showItemPanel(String metaTagName) {
        if (this.itemPanel == null) {
            this.preloadItemPanel(metaTagName);
        }
        if (this.itemPanel != null) {
            this.itemPanel.setVisible(true);
            // set sub components to visible
            List<VisualComponent> visualComponents = this.itemPanel.getVisualComponents();
            for (VisualComponent visualComponent : visualComponents) {
                visualComponent.setVisible(true);
            }
        }
    }

    VisualGroup itemPanel = null;

    protected void preloadItemPanel(String metaTagName) {

        int image_width_max = this.configuration.getInt("view.flow.panel.image.width.max", 100);
        int image_width = image_width_max;
        int image_height = (int) (image_width_max * 1.5);
        int image_spacing = 25;

        EList<MetaTag> metaTagList = new BasicEList<>();
        metaTagList.add(this.getDataSet().getMetaTag(metaTagName));
        final EList<InformationObject> items = this.getDataSet().getInformationObjectsWithOneOfMetaTags(metaTagList);
        int count = 0;
        List<InformationObject> itemsToShow = new ArrayList<>();
        if (!items.isEmpty()) {
            for (InformationObject item : items) {
                // just add items with images
                if ((item.getImages().size() > 1)) {
                    itemsToShow.add(item);
                    count++;
                }
            }
        }

        if (count < 1) {
            this.logger.warn("ItemPanel for metatag " + metaTagName + " resulted in 0 items for display.");
            return;
        }

        // sort person objects by family name
        Collections.sort(itemsToShow, new Comparator<InformationObject>() {
            @Override
            public int compare(InformationObject i1, InformationObject i2) {
                String s1 = i1.getName();
                String s2 = i2.getName();
                if (i1 instanceof Person) {
                    s1 = ((Person) i1).getName().substring(((Person) i1).getName().lastIndexOf(' '));
                }
                if (i2 instanceof Person) {
                    s2 = ((Person) i2).getName().substring(((Person) i2).getName().lastIndexOf(' '));
                }
                return s1.compareTo(s2);
            }
        });

        // get window/screen size
        double screenWidth = this.getWidth();
        double screenHeight = this.getHeight();

        // check if there is a image_width defined in the properties
        // if yes, then take this one and calculate cols, rows based on this
        // otherwise, calculate image_width based on count and screenWidth and
        // screenHeight
        image_width = this.configuration.getInt("view.flow.panel.image.width", 0);
        if (image_width == 0) {
            double image_width_tmp = (16.0 / 25.0) * (float) screenWidth
                    / Math.sqrt(((float) screenWidth / (float) screenHeight) * count * (7.0 / 5.0));
            image_width = (int) Math.floor(image_width_tmp);
            // more than a minimum value
            if (image_width < 40) {
                image_width = 40;
            }
            // not more than a maximum value
            if (image_width > image_width_max) {
                image_width = image_width_max;
            }
        }
        image_height = (int) Math.floor(1.5 * image_width);
        image_spacing = (int) Math.floor(0.25 * image_width);

        // calculate number of rows and columns
        int cols = (int) Math.ceil(Math.sqrt((screenWidth * count * (image_height + image_spacing))
                / ((image_width + image_spacing) * screenHeight))) + 1;
        int rows = (int) Math.ceil(count / cols) + 1;
        double panelWidth = cols * (image_width + image_spacing);
        double panelHeight = rows * (image_height + image_spacing);
        if (panelWidth > (screenWidth - (image_height + image_spacing))) {
            cols = (int) ((screenWidth - (image_height + image_spacing)) / (image_width + image_spacing));
            panelWidth = cols * (image_width + image_spacing);
        }
        if (panelHeight > (screenHeight - (image_height + image_spacing))) {
            rows = (int) ((screenHeight - (image_height + image_spacing)) / (image_height + image_spacing));
            panelHeight = rows * (image_height + image_spacing);
        }

        // center panel
        double posXstart = ((screenWidth - panelWidth) / 2.0);
        double posYstart = ((screenHeight - panelHeight) / 2.0);

        // create visual group for panel
        this.itemPanel = new VisualGroup();
        this.itemPanel.init();
        this.itemPanel.setVisible(false);
        this.addVisualComponent(this.itemPanel, true);

        // fill panel
        double posX = posXstart;
        double posY = posYstart;
        int col = 0;
        int row = 0;
        for (InformationObject item : itemsToShow) {
            ImageComponent imageComponent = new ImageComponent();
            imageComponent.setVisible(false);
            VisualComponent c = this.addVisualComponentToView(imageComponent, posX, posY, 1.0, 1.0, this.itemPanel);
            // get images of this item
            // create Metatag list
            LinkedList<String> metaTagList2 = new LinkedList<>();
            metaTagList2.add("profile_image_big_main");
            metaTagList2.add("profile_image_big");
            Image image = null;
            for (String metaTag : metaTagList2) {
                org.sociotech.communitymashup.data.Image mashupImage = item.getAttachedImageWithMetaTagName(metaTag);
                if (mashupImage != null) {
                    String fileUrl = mashupImage.getFileUrl();
                    image = ImageHelper.createImage(fileUrl, true);
                    if (image == null) {
                        continue;
                    }
                    break;
                }
            }
            Image categoryIcon = null;
            String categoryString = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons");
            String category_icons[] = null;
            if (categoryString != null) {
                category_icons = categoryString.split(",");
                EList<MetaTag> metaTags = item.getMetaTags();
                for (MetaTag metaTag : metaTags) {
                    for (String category : category_icons) {
                        if (category.equals(metaTag.getName())) {
                            categoryString = metaTag.getName();
                            break;
                        }
                    }
                }
            }
            if (categoryString != null) {
                String iconPath = "images/category_icon_" + categoryString + ".png";
                URL location;
                if (this.themeResources != null) {
                    location = this.themeResources.getResourceURL(iconPath);
                } else {
                    location = Resources.getResource("flow/" + iconPath);
                    this.logger.warn(this.getClass().getName() + " using deprecated resource path.");
                }
                // If the icon file does not exist, ImageHelper will simply
                // return null. Consequently, passing null into the controller
                // will hide the category icon.
                categoryIcon = ImageHelper.createImage(location, true);
            }

            String name = item.getName();
            if (item instanceof Person) {
                name = item.getName().substring(item.getName().lastIndexOf(' '));
            }
            ((org.sociotech.communitymirror.components.ImageComponent) c).configure(image, image_width, image_height,
                    name, "closeGroupAndAddItem(" + item.getIdent() + ")", categoryIcon);
            posX += image_width + image_spacing;
            col++;
            if (col >= cols) {
                col = 0;
                posX = posXstart;
                posY += image_height + image_spacing;
                row++;
                if (row >= rows) {
                    this.logger
                            .warn("Items do not fit on screen (showing only " + cols * rows + " from " + count + ")");
                    break;
                }
            }

        }

    }

    /**
     * Add a visual item for an item with given ident at given position
     */
    public void addVisualItem(String ident, double posX, double posY) {

        InformationObject item = (InformationObject) this.dataSet.getItemsWithIdent(ident);

        if (item != null) {
            this.addFlowVisualItemToView(item, posX, posY, 1.0, 1.0, VisualState.PREVIEW, null);
        }

    }

}
