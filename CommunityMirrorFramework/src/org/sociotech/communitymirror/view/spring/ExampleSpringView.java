package org.sociotech.communitymirror.view.spring;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymirror.physics.behaviors.RotationBehavior;
import org.sociotech.communitymirror.physics.behaviors.StayInBoundsBehavior;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.view.flow.components.ZoomableCircleVisualGroup;
import org.sociotech.communitymirror.view.spring.components.SpringConnectableRectangle;
import org.sociotech.communitymirror.view.spring.components.SpringConnectableVisualComponent;
import org.sociotech.communitymirror.visualgroups.layout.CircleVisualGroup;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.components.spring.BasicSpringLooper;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;

import com.facebook.rebound.BaseSpringSystem;
import com.facebook.rebound.SpringClock;

/**
 * Example view to show how to connect visual components with springs
 * 
 * @author Peter Lachenmaier
 */
public class ExampleSpringView extends FXView {
   
	
	private BaseSpringSystem springSystem;
	private Stack<VisualSpringConnection> connections = new Stack<>();
	
	public ExampleSpringView(double width, double height, Configuration configuration, DataSet dataSet) {
        super(width, height, configuration, dataSet);
    }

    @Override
    protected void onInit() {
        
        super.onInit();
		
        springSystem = new BaseSpringSystem(new SpringClock(), new BasicSpringLooper());
           
	    // create visual components
//        SpringConnectableVisualComponent center = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2);
//	    center.init();
//	    
//	    SpringConnectableVisualComponent vc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
//	    vc1.init();
//	    
//	    SpringConnectableVisualComponent vc2 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
//	    vc2.init();
//	    
//	    SpringConnectableVisualComponent vc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
//	    vc3.init();
//	    
//	    SpringConnectableVisualComponent vc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
//	    vc4.init();
//	    
//	    // connect the components
//	    VisualSpringConnection springConnection1 = VisualSpringConnection.connect(center, vc1, springSystem);
//	    VisualSpringConnection springConnection2 = VisualSpringConnection.connect(center, vc2, springSystem);
//	    VisualSpringConnection springConnection3 = VisualSpringConnection.connect(center, vc3, springSystem);
//	    VisualSpringConnection springConnection4 = VisualSpringConnection.connect(center, vc4, springSystem);
//	    
//	    VisualSpringConnection springConnectionA = VisualSpringConnection.connect(vc1, vc2, springSystem);
//	    VisualSpringConnection springConnectionB = VisualSpringConnection.connect(vc2, vc3, springSystem);
//	    VisualSpringConnection springConnectionC = VisualSpringConnection.connect(vc3, vc4, springSystem);
//	    VisualSpringConnection springConnectionD = VisualSpringConnection.connect(vc4, vc1, springSystem);
//	    
//	    connections.add(springConnection1);
//	    connections.add(springConnection2);
//	    connections.add(springConnection3);
//	    connections.add(springConnection4);
//	    
//	    connections.add(springConnectionA);
//	    connections.add(springConnectionB);
//	    connections.add(springConnectionC);
//	    connections.add(springConnectionD);
//	    
//	    // add all of them to let them appear
//	    this.addVisualComponent(springConnection1, true);
//	    this.addVisualComponent(springConnection2, true);
//	    this.addVisualComponent(springConnection3, true);
//	    this.addVisualComponent(springConnection4, true);
//	    
//	    this.addVisualComponent(springConnectionA, true);
//	    this.addVisualComponent(springConnectionB, true);
//	    this.addVisualComponent(springConnectionC, true);
//	    this.addVisualComponent(springConnectionD, true);
//	    
//	    this.addSpringConnectableComponent(vc1, true);
//	    this.addSpringConnectableComponent(vc2, true);
//	    this.addSpringConnectableComponent(vc3, true);
//	    this.addSpringConnectableComponent(vc4, true);
//	    this.addSpringConnectableComponent(center, true);
//		   
//	    
//	    // create visual components
//        SpringConnectableVisualComponent lCenter = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2, false);
//	    lCenter.init();
//	    
//	    SpringConnectableVisualComponent lVc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
//	    lVc1.init();
//	    
//	    SpringConnectableVisualComponent lVc2 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
//	    lVc2.init();
//	    
//	    SpringConnectableVisualComponent lVc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
//	    lVc3.init();
//	    
//	    SpringConnectableVisualComponent lVc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
//	    lVc4.init();
//	    
//	    // connect the components with keeping the layout
//	    VisualSpringConnection lSpringConnection1 = VisualSpringConnection.connect(lCenter, lVc1, springSystem, true);
//	    VisualSpringConnection lSpringConnection2 = VisualSpringConnection.connect(lCenter, lVc2, springSystem, true);
//	    VisualSpringConnection lSpringConnection3 = VisualSpringConnection.connect(lCenter, lVc3, springSystem, true);
//	    VisualSpringConnection lSpringConnection4 = VisualSpringConnection.connect(lCenter, lVc4, springSystem, true);
//	    
//	    connections.add(lSpringConnection1);
//	    connections.add(lSpringConnection2);
//	    connections.add(lSpringConnection3);
//	    connections.add(lSpringConnection4);
//	    
//	    lSpringConnection1.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lSpringConnection1.getHiddenLayoutSpring().getLine()); 
//	      
//	    lSpringConnection2.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lSpringConnection2.getHiddenLayoutSpring().getLine());
//	    
//	    lSpringConnection3.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lSpringConnection3.getHiddenLayoutSpring().getLine());
//	    
//	    lSpringConnection4.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lSpringConnection4.getHiddenLayoutSpring().getLine());
//	    
//	    // add all of them to let them appear
//	    this.addVisualComponent(lSpringConnection1, true);
//	    this.addVisualComponent(lSpringConnection2, true);
//	    this.addVisualComponent(lSpringConnection3, true);
//	    this.addVisualComponent(lSpringConnection4, true);
//	      
//	    this.addSpringConnectableComponent(lVc1, true);
//	    this.addSpringConnectableComponent(lVc2, true);
//	    this.addSpringConnectableComponent(lVc3, true);
//	    this.addSpringConnectableComponent(lVc4, true);
//	    this.addSpringConnectableComponent(lCenter, true);
//	    
//	    
//	    // create visual components
//        SpringConnectableVisualComponent fCenter = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2);
//	    fCenter.init();
//	    
//	    SpringConnectableVisualComponent fVc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
//	    fVc1.init();
//	    
//	    SpringConnectableVisualComponent fVc2 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
//	    fVc2.init();
//	    
//	    SpringConnectableVisualComponent fVc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
//	    fVc3.init();
//	    
//	    SpringConnectableVisualComponent fVc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
//	    fVc4.init();
//	    
//	    // connect the components without layout
//	    VisualSpringConnection fSpringConnection1 = VisualSpringConnection.connect(fCenter, fVc1, springSystem, false);
//	    VisualSpringConnection fSpringConnection2 = VisualSpringConnection.connect(fCenter, fVc2, springSystem, false);
//	    VisualSpringConnection fSpringConnection3 = VisualSpringConnection.connect(fCenter, fVc3, springSystem, false);
//	    VisualSpringConnection fSpringConnection4 = VisualSpringConnection.connect(fCenter, fVc4, springSystem, false);
//	    
//	    connections.add(fSpringConnection1);
//	    connections.add(fSpringConnection2);
//	    connections.add(fSpringConnection3);
//	    connections.add(fSpringConnection4);
//	    
//	    // add all of them to let them appear
//	    this.addVisualComponent(fSpringConnection1, true);
//	    this.addVisualComponent(fSpringConnection2, true);
//	    this.addVisualComponent(fSpringConnection3, true);
//	    this.addVisualComponent(fSpringConnection4, true);
//	      
//	    this.addSpringConnectableComponent(fVc1, true);
//	    this.addSpringConnectableComponent(fVc2, true);
//	    this.addSpringConnectableComponent(fVc3, true);
//	    this.addSpringConnectableComponent(fVc4, true);
//	    this.addSpringConnectableComponent(fCenter, true);
//	    
//	    // create visual components
//        SpringConnectableVisualComponent cCenter = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2);
//	    cCenter.init();
//	    
//	    SpringConnectableVisualComponent cVc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
//	    cVc1.init();
//	    
//	    SpringConnectableVisualComponent cVc2 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
//	    cVc2.init();
//	    
//	    SpringConnectableVisualComponent cVc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
//	    cVc3.init();
//	    
//	    SpringConnectableVisualComponent cVc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
//	    cVc4.init();
//	    
//	    // connect the components
//	    VisualSpringConnection cSpringConnection1 = VisualSpringConnection.connect(cCenter, cVc1, springSystem, true);
//	    VisualSpringConnection cSpringConnection2 = VisualSpringConnection.connect(cCenter, cVc2, springSystem);
//	    VisualSpringConnection cSpringConnection3 = VisualSpringConnection.connect(cCenter, cVc3, springSystem, true);
//	    VisualSpringConnection cSpringConnection4 = VisualSpringConnection.connect(cCenter, cVc4, springSystem);
//	    
//
//	    cSpringConnection1.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(cSpringConnection1.getHiddenLayoutSpring().getLine());
//	      
//	    cSpringConnection3.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(cSpringConnection3.getHiddenLayoutSpring().getLine());
//	    
//	    
//	    VisualSpringConnection cSpringConnectionA = VisualSpringConnection.connect(cVc1, cVc3, springSystem);
//	    VisualSpringConnection cSpringConnectionB = VisualSpringConnection.connect(cVc2, cVc4, springSystem);
//	    
//	    connections.add(cSpringConnection1);
//	    connections.add(cSpringConnection2);
//	    connections.add(cSpringConnection3);
//	    connections.add(cSpringConnection4);
//	    
//	    connections.add(cSpringConnectionA);
//	    connections.add(cSpringConnectionB);
//	    
//	    // add all of them to let them appear
//	    this.addVisualComponent(cSpringConnection1, true);
//	    this.addVisualComponent(cSpringConnection2, true);
//	    this.addVisualComponent(cSpringConnection3, true);
//	    this.addVisualComponent(cSpringConnection4, true);
//	    
//	    this.addVisualComponent(cSpringConnectionA, true);
//	    this.addVisualComponent(cSpringConnectionB, true);
//	    
//	    this.addSpringConnectableComponent(cVc1, true);
//	    this.addSpringConnectableComponent(cVc2, true);
//	    this.addSpringConnectableComponent(cVc3, true);
//	    this.addSpringConnectableComponent(cVc4, true);
//	    this.addSpringConnectableComponent(cCenter, true);
//	    
//	    // create visual components
//	    SpringConnectableRectangle lrCenter = new SpringConnectableRectangle(scene.getWidth() / 2, scene.getHeight() / 2, false);
//	    lrCenter.init();
//	    
//	    SpringConnectableVisualComponent lrVc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
//	    lrVc1.init();
//	    
		// SpringConnectableVisualComponent lrVc2 = new
		// SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
//	    lrVc2.init();
//	    
//	    SpringConnectableVisualComponent lrVc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
//	    lrVc3.init();
//	    
//	    SpringConnectableVisualComponent lrVc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
//	    lrVc4.init();
//	    
//	    // connect the components with keeping the layout
//	    VisualSpringConnection lrSpringConnection1 = VisualSpringConnection.connect(lrCenter, lrVc1, springSystem, true);
//	    VisualSpringConnection lrSpringConnection2 = VisualSpringConnection.connect(lrCenter, lrVc2, springSystem, true);
//	    VisualSpringConnection lrSpringConnection3 = VisualSpringConnection.connect(lrCenter, lrVc3, springSystem, true);
//	    VisualSpringConnection lrSpringConnection4 = VisualSpringConnection.connect(lrCenter, lrVc4, springSystem, true);
//	    
//	    connections.add(lrSpringConnection1);
//	    connections.add(lrSpringConnection2);
//	    connections.add(lrSpringConnection3);
//	    connections.add(lrSpringConnection4);
//	    
//	    lrSpringConnection1.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lrSpringConnection1.getHiddenLayoutSpring().getLine());
//	      
//	    lrSpringConnection2.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lrSpringConnection2.getHiddenLayoutSpring().getLine());
//	    
//	    lrSpringConnection3.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lrSpringConnection3.getHiddenLayoutSpring().getLine());
//	    
//	    lrSpringConnection4.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
//	    this.addNode(lrSpringConnection4.getHiddenLayoutSpring().getLine());
//	    
//	    // add all of them to let them appear
//	    this.addVisualComponent(lrSpringConnection1, true);
//	    this.addVisualComponent(lrSpringConnection2, true);
//	    this.addVisualComponent(lrSpringConnection3, true);
//	    this.addVisualComponent(lrSpringConnection4, true);
//	      
//	    this.addSpringConnectableComponent(lrVc1, true);
//	    this.addSpringConnectableComponent(lrVc2, true);
//	    this.addSpringConnectableComponent(lrVc3, true);
//	    this.addSpringConnectableComponent(lrVc4, true);
//	    this.addSpringConnectableRectangle(lrCenter, true);
	    
	    // create visual components
	    SpringConnectableRectangle lr2Center = new SpringConnectableRectangle(scene.getWidth() / 2, scene.getHeight() / 2, false);
	    lr2Center.init();
	    
	    SpringConnectableVisualComponent lr2Vc1 = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 100, scene.getHeight() / 2);
	    lr2Vc1.init();
	    
	    SpringConnectableVisualComponent lr2Vc2 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 - 100);
	    lr2Vc2.init();
	    
	    SpringConnectableVisualComponent lr2Vc3 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 100, scene.getHeight() / 2);
	    lr2Vc3.init();
	    
	    SpringConnectableVisualComponent lr2Vc4 = new SpringConnectableVisualComponent(scene.getWidth() / 2, scene.getHeight() / 2 + 100);
	    lr2Vc4.init();
	    
	    // connect the components with keeping the layout
	    VisualSpringConnection lr2SpringConnection1 = VisualSpringConnection.connect(lr2Center.getLeftSpringConnector(), lr2Vc1, springSystem, true);
	    VisualSpringConnection lr2SpringConnection2 = VisualSpringConnection.connect(lr2Center.getTopSpringConnector(), lr2Vc2, springSystem, true);
	    VisualSpringConnection lr2SpringConnection3 = VisualSpringConnection.connect(lr2Center.getRightSpringConnector(), lr2Vc3, springSystem, true);
	    VisualSpringConnection lr2SpringConnection4 = VisualSpringConnection.connect(lr2Center.getBottomSpringConnector(), lr2Vc4, springSystem, true);
	      
	    connections.add(lr2SpringConnection1);
	    connections.add(lr2SpringConnection2);
	    connections.add(lr2SpringConnection3);
	    connections.add(lr2SpringConnection4);
	    
	    lr2SpringConnection1.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
	    this.addNode(lr2SpringConnection1.getHiddenLayoutSpring().getLine());
	      
	    lr2SpringConnection2.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
	    this.addNode(lr2SpringConnection2.getHiddenLayoutSpring().getLine());
	    
	    lr2SpringConnection3.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
	    this.addNode(lr2SpringConnection3.getHiddenLayoutSpring().getLine());
	    
	    lr2SpringConnection4.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
	    this.addNode(lr2SpringConnection4.getHiddenLayoutSpring().getLine());
	    
	    // add all of them to let them appear
	    this.addVisualComponent(lr2SpringConnection1, true);
	    this.addVisualComponent(lr2SpringConnection2, true);
	    this.addVisualComponent(lr2SpringConnection3, true);
	    this.addVisualComponent(lr2SpringConnection4, true);
	      
	    this.addSpringConnectableComponent(lr2Vc1, true);
	    this.addSpringConnectableComponent(lr2Vc2, true);
	    this.addSpringConnectableComponent(lr2Vc3, true);
	    this.addSpringConnectableComponent(lr2Vc4, true);
	    this.addSpringConnectableRectangle(lr2Center, true);
	    
	    lr2Center.addPhysicsBehavior(new RotationBehavior(0.2, 0.0, 0.1, 1.0));
//	    lr2Center.addPhysicsBehavior(new SpiralDriftBehavior(0.2, 0.0, 0.0, 0.1, 1.0));
//	    // fall down
//	    lr2Center.addPhysicsBehavior(DriftBehavior.createVerticalDriftBehavior());
//		lr2Center.addPhysicsBehavior(StayInBoundsBehavior.createStayInBoundsBehaviorWithOffset(this.getViewBounds(), 0.0));
	    
		List<VisualComponent> componentsToAlign = new LinkedList<>();
		
		SpringConnectableVisualComponent centerComponent = new SpringConnectableVisualComponent(scene.getWidth() / 2 - 200, scene.getHeight() / 2 - 200);
		this.addSpringConnectableComponent(centerComponent, true);
		
		// keep center inside screen
		centerComponent.addPhysicsBehavior(StayInBoundsBehavior.createStayInBoundsBehaviorWithOffset(this.getViewBounds(), 0.0));
		
		int n = 12;
		
		for(int i = 0; i < n; i ++) {
			SpringConnectableVisualComponent connectableComponent = new SpringConnectableVisualComponent(0,0);
			componentsToAlign.add(connectableComponent);
			this.addSpringConnectableComponent(connectableComponent, true);
		}
	    
		CircleVisualGroup circleAlignGroup = new ZoomableCircleVisualGroup(componentsToAlign, centerComponent, 200, springSystem);
		circleAlignGroup.init();
		
		this.addVisualComponent(circleAlignGroup, true);
		// make it zoomable
		circleAlignGroup.makeZoomable();
		
		// create second one
		List<VisualComponent> componentsToAlign2 = new LinkedList<>();
		
		SpringConnectableVisualComponent centerComponent2 = new SpringConnectableVisualComponent(scene.getWidth() / 2 + 200, scene.getHeight() / 2 - 200);
		this.addSpringConnectableComponent(centerComponent2, true);
		
		n = 5;
		
		for(int i = 0; i < n; i ++) {
			SpringConnectableVisualComponent connectableComponent = new SpringConnectableVisualComponent(0,0);
			componentsToAlign2.add(connectableComponent);
			this.addSpringConnectableComponent(connectableComponent, true);
		}
	    
		// add one node of the first one
		componentsToAlign2.add(componentsToAlign.get(4));
		
		CircleVisualGroup circleAlignGroup2 = new ZoomableCircleVisualGroup(componentsToAlign2, centerComponent2, 150, springSystem);
		circleAlignGroup2.init();
		// make it zoomable
		circleAlignGroup2.makeZoomable();
				
		this.addVisualComponent(circleAlignGroup2, true);
		
		// add a unconnected component
		SpringConnectableVisualComponent unconnected = new SpringConnectableVisualComponent(50, 50, false);
	    unconnected.init();
	    this.addSpringConnectableComponent(unconnected, true);
		
	    // shuffle connections
	    Collections.shuffle(connections);
	    
	    scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent keyEvent) {
				if(keyEvent.getCode() == KeyCode.D) {
					connections.pop().disconnect();
					System.out.println("disconnecting");
				}
			}
		});
    }

    private void addSpringConnectableRectangle(SpringConnectableRectangle component, boolean addAsNode) {
    	this.addVisualComponent(component, addAsNode);
    	
    	// make it dragable
    	component.makeDragable();
	}

	private void addSpringConnectableComponent(SpringConnectableVisualComponent component, boolean addAsNode) {
		this.addVisualComponent(component, addAsNode);
		
		// make component dragable
		component.makeDragable();
		component.makeTouchable();
	}

	/* (non-Javadoc)
     * @see org.sociotech.communitymirror.view.FXView#onUpdate(double, long)
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
    	super.onUpdate(framesPerSecond, nanoTime);
        
        // update spring system
        springSystem.loop();
    }
}
