package org.sociotech.communitymirror.view.spring.components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;

/**
 * The simple sample implementation of a visual component that can be
 * connected by a spring {@link VisualSpringConnection}.
 *
 * @author Peter Lachenmaier
 *
 */
public class SpringConnectableVisualComponent extends VisualComponent {

	private double radius = 30.0;
	private Circle circle;
	private boolean fixed;

	/**
	 * Creates the component centered at the given position.
	 *
	 * @param centerX X of the center
	 * @param centerY Y of the center
	 */
	public SpringConnectableVisualComponent(double centerX, double centerY) {
		this(centerX, centerY, false);
	}

	/**
	 * Creates the component centered at the given position.
	 *
	 * @param centerX X of the center
	 * @param centerY Y of the center
	 * @param fixed True to fix the position
	 */
	public SpringConnectableVisualComponent(double centerX, double centerY, boolean fixed) {
		setPositionX(centerX);
		setPositionY(centerY);
		this.fixed = fixed;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();
		circle = new Circle();
		circle.setCenterX(0);
		circle.setCenterY(0);
		circle.setRadius(radius);
		circle.setFill(Color.WHITE);

		this.addNode(circle);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.springview.components.SpringConnectable#isFixed()
	 */
	@Override
	public boolean isFixed() {
		return fixed;
	}

	// implementation of HandleDrag

	@Override
	public void onDragDragged(DragDraggedEvent dragEvent) {
		// no inertial drag after release
		if(dragEvent.isInertia() && !fixed) {
			return;
		}

		super.onDragDragged(dragEvent);
	}
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#onDragStarted(org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent)
	 */
	@Override
	public void onDragStarted(DragStartedEvent dragEvent) {
		fix();

		// consume event
		dragEvent.consume();
	}

	/**
	 * Fixes this component to avoid spring forces
	 */
	private void fix() {
		// fix to avoid spring forces
		this.fixed = true;

		// change color to indicate fix
		circle.setFill(Color.RED);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchPressed(org.sociotech.communityinteraction.events.touch.TouchPressedEvent)
	 */
	@Override
	public void onTouchPressed(TouchPressedEvent event) {
		fix();
		event.consume();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchReleased(org.sociotech.communityinteraction.events.touch.TouchReleasedEvent)
	 */
	@Override
	public void onTouchReleased(TouchReleasedEvent event) {
		releaseFix();
		event.consume();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#onDragFinished(org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent)
	 */
	@Override
	public void onDragFinished(DragFinishedEvent dragEvent) {
		releaseFix();

		// consume event
		dragEvent.consume();
	}

	/**
	 * Releases a possible fix on this component
	 */
	private void releaseFix() {
		// enable spring forces again after drag
		this.fixed = false;

		// reset fix color
		circle.setFill(Color.WHITE);
	}
}
