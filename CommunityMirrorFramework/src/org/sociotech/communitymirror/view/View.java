package org.sociotech.communitymirror.view;

import org.sociotech.communitymirror.visualitems.VisualGroup;

/**
 * Abstract super class of all views. The view is a visual group itself to maintain
 * the list of contained visual components and propagate events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class View extends VisualGroup {
	
}
