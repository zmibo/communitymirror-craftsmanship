package org.sociotech.communitymirror.view.picture.visualitems;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.Animation.Status;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.CacheHint;
import javafx.scene.Group;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * A visual representation for images.
 *
 * @author Peter Lachenmaier
 */
public class PicureItem extends VisualItem<org.sociotech.communitymashup.data.Image> {

	private double currentSepia;

	private ImageView backgroundImage;
	private ImageView foregroundImage;
	private Group foregroundGroup;

	private double height;

	protected boolean initialized = false;

	private SepiaTone sepiaTone;

	private ScaleTransition scaleUpTransition;

	private ScaleTransition scaleDownTransition;

	private final double upperSepia = 0.8;

	private Timeline colorImageAnimation;

	private Timeline deColorImageAnimation;

	private static final boolean smooth = true;

	protected double scaleFactor = 1.6;

	private boolean opened = false;

	public PicureItem(org.sociotech.communitymashup.data.Image dataItem, VisualState state, double height) {
		super(dataItem);
		this.setVisualState(state);
		this.height = height;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		String fileUrl = this.getDataItem().getFileUrl();
		if(fileUrl == null) {
			// nothing can be done
			return;
		}

		final GaussianBlur gaussianBlur = new GaussianBlur();
        gaussianBlur.setRadius(11);

        double backgroundOverhang = 20.0;
        double imageHeight = height;
        double x = 0;
        double y = 0;

        Image image = ImageHelper.createImage(fileUrl, smooth);
        foregroundImage = new ImageView(image);
        //image can be resized to preferred width
        foregroundImage.setFitHeight(imageHeight - 2*backgroundOverhang);
        foregroundImage.setPreserveRatio(true);

        sepiaTone = new SepiaTone();
        sepiaTone.setLevel(upperSepia);
        foregroundImage.setEffect(sepiaTone);

        foregroundImage.setX(x + backgroundOverhang);
        foregroundImage.setY(y + backgroundOverhang);

        foregroundImage.setCache(true);
        foregroundImage.setCacheHint(CacheHint.SCALE);

        // put image in a group to be extended
        foregroundGroup = new Group();
        foregroundGroup.getChildren().add(foregroundImage);

		scaleUpTransition = new ScaleTransition();
		scaleUpTransition.setDuration(Duration.seconds(2));
		scaleUpTransition.setNode(foregroundGroup);
		scaleUpTransition.setToX(scaleFactor);
		scaleUpTransition.setToY(scaleFactor);
		scaleUpTransition.setCycleCount(1);
		scaleUpTransition.setAutoReverse(false);

		scaleDownTransition = new ScaleTransition();
		scaleDownTransition.setDuration(Duration.seconds(2));
		scaleDownTransition.setNode(foregroundGroup);
		scaleDownTransition.setToX(1);
		scaleDownTransition.setToY(1);
		scaleDownTransition.setCycleCount(1);
		scaleDownTransition.setAutoReverse(false);

        colorImageAnimation = new Timeline();
        colorImageAnimation.setCycleCount(1);
        colorImageAnimation.setAutoReverse(false);
        final KeyValue kv = new KeyValue(sepiaTone.levelProperty(), 0);
        final KeyFrame kf = new KeyFrame(Duration.seconds(2), kv);
        colorImageAnimation.getKeyFrames().add(kf);

        deColorImageAnimation = new Timeline();
        deColorImageAnimation.setCycleCount(1);
        deColorImageAnimation.setAutoReverse(false);
        deColorImageAnimation.setDelay(Duration.seconds(8));
        final KeyValue kv2 = new KeyValue(sepiaTone.levelProperty(), upperSepia);
        final KeyFrame kf2 = new KeyFrame(Duration.seconds(2), kv2);
        deColorImageAnimation.getKeyFrames().add(kf2);
        deColorImageAnimation.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				//reactivate caching
				setCache(true);
			}
		});

        // manage animation state
        manageAnimation(scaleUpTransition);
        manageAnimation(scaleDownTransition);
        manageAnimation(colorImageAnimation);
        manageAnimation(deColorImageAnimation);

        // make the image touchable
        this.makeTouchable();

        backgroundImage = new ImageView(image);
        //image can be resized to preferred width
        backgroundImage.setFitHeight(imageHeight);
        backgroundImage.setFitWidth(foregroundImage.getBoundsInLocal().getWidth() + 2* backgroundOverhang);

        // set blur
        backgroundImage.setEffect(gaussianBlur);
        backgroundImage.setOpacity(0.4);

        backgroundImage.setX(x);
        backgroundImage.setY(y);

        // do a little performance boost
        backgroundImage.setCache(true);
        backgroundImage.setCacheHint(CacheHint.SPEED);

        backgroundImage.setBlendMode(BlendMode.DIFFERENCE);

        this.addNode(backgroundImage);
        this.addNode(foregroundGroup);

        this.setCache(true);
        this.setCacheHint(CacheHint.SPEED);
	}

	/**
	 * Returns if this picture item is opened.
	 *
	 * @return True if this picture item is openend.
	 */
	public boolean isOpened() {
		return opened;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchTapped(org.sociotech.communityinteraction.events.touch.TouchTappedEvent)
	 */
	@Override
	public void onTouchTapped(TouchTappedEvent event) {
		if(opened) {
			close();
		} else {
			open();
		}
		event.consume();
	}

	/**
	 * Returns the current sepia for the foreground image
	 *
	 * @return The current sepia for the foreground image
	 */
	protected double getCurrentSepia() {
		return currentSepia;
	}

	/**
	 * Returns the background image.
	 *
	 * @return The background image.
	 */
	protected ImageView getBackgroundImage() {
		return backgroundImage;
	}

	/**
	 * Returns the foreground image.
	 *
	 * @return The foreground image.
	 */
	public ImageView getForegroundImage() {
		return foregroundImage;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualItem#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();

		// remove images from tree
		this.getChildren().removeAll(foregroundImage, backgroundImage);
	}

	/**
	 * Will be called when a open (interaction) is performed
	 */
	protected void open() {

		// put the image to the front
		bringToFront();

		// deactivate caching before playing animations
		this.setCache(false);

		// stop possible scale down
		scaleDownTransition.stop();
		// and scale up
		scaleUpTransition.play();

		// stop possible de color animation
		deColorImageAnimation.stop();

		// color image
		colorImageAnimation.play();

		opened = true;
	}

	/**
	 * Brings this element to the front
	 */
	protected void bringToFront() {
		foregroundGroup.toFront();
	}

	/**
	 * Will be called when a close (interaction) is performed
	 */
	protected void close() {

		scaleUpTransition.stop();
		colorImageAnimation.stop();

		scaleDownTransition.play();
		deColorImageAnimation.play();

		opened = false;
	}

	/**
	 * Returns the foreground group.
	 *
	 * @return The foreground group.
	 */
	public Group getForegroundGroup() {
		return foregroundGroup;
	}

	/**
	 * Returns the sepia tone of the foreground image.
	 *
	 * @return The sepia tone of the foreground image.
	 */
	public SepiaTone getSepiaTone() {
		return sepiaTone;
	}

	/**
	 * Returns whether this element is currently in an animation or not
	 *
	 * @return Whether this element is currently in an animation or not
	 */
	public boolean isInAnimation() {
		return scaleDownTransition.getStatus() == Status.RUNNING ||
				scaleUpTransition.getStatus() == Status.RUNNING ||
				colorImageAnimation.getStatus() == Status.RUNNING ||
				deColorImageAnimation.getStatus() == Status.RUNNING;
	}

	/**
	 * Returns true if the foreground image is defined.
	 *
	 * @return True if foreground image is defined.
	 */
	public boolean isValid() {
		return foregroundImage != null && foregroundImage.getBoundsInLocal().getWidth() > 10;
	}
}
