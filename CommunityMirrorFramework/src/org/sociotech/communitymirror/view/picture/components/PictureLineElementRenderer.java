package org.sociotech.communitymirror.view.picture.components;

import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * A renderer for picture line elements.
 * 
 * @author Peter Lachenmaier
 */
public class PictureLineElementRenderer extends VisualItemRenderer<Image, PictureLineElement> {

	private double height;

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer#renderItem(org.sociotech.communitymashup.data.Item, org.sociotech.communitymirror.visualstates.VisualState)
	 */
	@Override
	public PictureLineElement renderItem(Image item, VisualState state) {
		return new PictureLineElement(item, state, height);
	}

	/**
	 * The height for new rendered images.
	 * 
	 * @param height Height for new rendered images.
	 */
	public void setHeight(double height) {
		this.height = height;
	}

}
