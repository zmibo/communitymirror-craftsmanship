package org.sociotech.communitymirror.view.picture.components;

import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymirror.view.picture.visualitems.PicureItem;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * A visual item that can be used as line element.
 * 
 * @author Peter Lachenmaier
 *
 */
public class PictureLineElement extends PicureItem implements ItemLineElement {

	private ItemLine line;

	/**
	 * Creates a new line element with the given state and height as representation
	 * of the given data item.
	 * 
	 * @param dataItem Data item to be represented
	 * @param state Visual state
	 * @param height Supposed height 
	 */
	public PictureLineElement(Image dataItem, VisualState state, double height) {
		super(dataItem, state, height);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getX()
	 */
	@Override
	public double getX() {
		return this.getLayoutX();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setX(double)
	 */
	@Override
	public void setX(double x) {
		this.setLayoutX(x);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setY(double)
	 */
	@Override
	public void setY(double y) {
		this.setLayoutY(y);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getWidth()
	 */
	@Override
	public double getWidth() {
		return this.getBoundsInLocal().getWidth();
		
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#setLine(org.sociotech.communitymirror.view.picture.components.ItemLine)
	 */
	@Override
	public void setLine(ItemLine itemLine) {
		this.line = itemLine;		
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#getLine()
	 */
	@Override
	public ItemLine getLine() {
		return line;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.visualitems.PicureItem#foregroundTouched()
	 */
	@Override
	protected void open() {
		super.open();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.visualitems.PicureItem#foregroundReleased()
	 */
	@Override
	protected void close() {
		super.close();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.visualitems.PicureItem#bringToFront()
	 */
	@Override
	protected void bringToFront() {
		super.bringToFront();
		//bring this to front
		this.toFront();
		// bring line to front
		//line.toFront();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.ItemLineElement#isValid()
	 */
	@Override
	public boolean isValid() {
		return super.isValid();
	}
	
}
