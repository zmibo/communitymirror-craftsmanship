package org.sociotech.communitymirror.view.picture.components;

/**
 * The interface that every element must implement to be used in a item line.
 * 
 * @author Peter Lachenmaier
 */
public interface ItemLineElement {
	public void destroy();
	public double getX();
	public void setX(double x);
	public void setY(double y);
	public double getWidth();
	public void setLine(ItemLine itemLine);
	public ItemLine getLine();
	public boolean isInAnimation();
	public boolean isValid();
}
