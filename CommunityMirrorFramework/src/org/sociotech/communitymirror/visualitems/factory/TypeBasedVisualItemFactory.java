package org.sociotech.communitymirror.visualitems.factory;

import java.util.HashMap;
import java.util.Map;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Item factory that chooses a concrete item factory based on the type of the element to produce
 *
 * @author Peter Lachenmaier
 */
public class TypeBasedVisualItemFactory<T extends Item, V extends VisualItem<T>> extends VisualItemFactory<T, V>{

	/**
	 * Map containing a mapping from types to concrete factories
	 */
	private Map<String, VisualItemFactory<T, V>> typeSpecificFactories = new HashMap<>();
		
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.factory.VisualItemFactory#getRenderer(org.sociotech.communitymashup.data.Item, org.sociotech.communitymirror.visualstates.VisualState)
	 */
	@Override
	protected VisualItemRenderer<T, V> getRenderer(T item, VisualState state) {
		VisualItemFactory<T, V> typeFactory = typeSpecificFactories.get(item.getClass().getSimpleName());
		
		if(typeFactory != null) {
			return typeFactory.getRenderer(item, state);
		}
		
		return null;
	}

	/**
	 * Set the factory for a special type.
	 * 
	 * @param type Type to set the factory for
	 * @param factory Factory that should be used to produce items of the given type
	 */
	@SuppressWarnings("unchecked")
	public <U extends T, W extends VisualItem<U>> void setFactoryForType(Class<U> type, VisualItemFactory<U, W> factory) {
		// we add an "Impl" extension as Items are EMF elements and the concrete renderable items will be implementation objects
		// while initialization happens with the interfaces
		typeSpecificFactories.put(type.getSimpleName() + "Impl", (VisualItemFactory<T, V>) factory);
	}
	
// sample code for how to use it	
//	public static void main(String[] args) {
//		TypeBasedVisualItemFactory<InformationObject, VisualItem<InformationObject>> factory = new TypeBasedVisualItemFactory<>();
//		
//		GenericItemFactory<Person, PersonItem> personFactory = new GenericItemFactory<>();
//		GenericItemFactory<Content, CommentItem> contentFactory = new GenericItemFactory<>();
//		
//		factory.setFactoryForType(Person.class, personFactory);
//		factory.setFactoryForType(Content.class, contentFactory);
//		
//		//GenericItemFactory<WebAccount, VisualItem<WebAccount>> webAccountFactory = new GenericItemFactory<>();
//		//factory.setFactoryForType(WebAccount.class, webAccountFactory);
//		
//		factory.produceVisualItem(DataFactory.eINSTANCE.createPerson(), VisualState.DETAIL);
//		factory.produceVisualItem(DataFactory.eINSTANCE.createContent(), VisualState.DETAIL);
//		factory.produceVisualItem(DataFactory.eINSTANCE.createOrganisation(), VisualState.DETAIL);
//		
//		
//	}
}
