package org.sociotech.communitymirror.visualitems.user.bodyCondition;

public class BodyOrientation {

	private float[] orientation;

	public BodyOrientation(float[] orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the orientation
	 */
	public float[] getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(float[] orientation) {
		this.orientation = orientation;
	}

}
