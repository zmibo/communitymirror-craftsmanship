package org.sociotech.communitymirror.visualitems.user.interactionZone;

public enum InteractionZoneType {
	
	TouchZone, CloserBodyZone, FarerBodyZone, UntrackedZone

}
