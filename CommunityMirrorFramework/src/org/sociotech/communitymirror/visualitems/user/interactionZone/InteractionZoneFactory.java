package org.sociotech.communitymirror.visualitems.user.interactionZone;

import org.apache.commons.configuration2.CombinedConfiguration;
import org.sociotech.communitymirror.configuration.ConfigurationLoader;

public class InteractionZoneFactory {

	public static InteractionZone createInteractionZone(double posY) {
		// load configuration
		CombinedConfiguration configuration = ConfigurationLoader
				.load("configuration/main_configuration.xml");
		double minY = configuration.getDouble("interactionZone.TouchZone.minY");
		double maxY = configuration.getDouble("interactionZone.TouchZone.maxY");
		if (posY >= minY && posY < maxY) {
			return new InteractionZone(minY, maxY,
					InteractionZoneType.TouchZone);
		}
		minY = configuration.getDouble("interactionZone.CloserBodyZone.minY");
		maxY = configuration.getDouble("interactionZone.CloserBodyZone.maxY");
		if (posY < maxY) {
			return new InteractionZone(minY, maxY,
					InteractionZoneType.CloserBodyZone);
		}
		minY = configuration.getDouble("interactionZone.FarerBodyZone.minY");
		maxY = configuration.getDouble("interactionZone.FarerBodyZone.maxY");
		if (posY < maxY) {
			return new InteractionZone(minY, maxY,
					InteractionZoneType.FarerBodyZone);
		}
		minY = configuration.getDouble("interactionZone.UntrackedZone.minY");
		maxY = configuration.getDouble("interactionZone.UntrackedZone.maxY");
		return new InteractionZone(minY, maxY,
				InteractionZoneType.UntrackedZone);
	}

	public static InteractionZone createUntrackedZone() {
		// load configuration
		CombinedConfiguration configuration = ConfigurationLoader
				.load("configuration/main_configuration.xml");
		double minY = configuration
				.getDouble("interactionZone.UntrackedZone.minY");
		double maxY = configuration
				.getDouble("interactionZone.UntrackedZone.maxY");
		return new InteractionZone(minY, maxY,
				InteractionZoneType.UntrackedZone);
	}
}
