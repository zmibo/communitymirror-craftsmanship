package org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URI;
import java.nio.ByteBuffer;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;

import com.google.common.io.Resources;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritablePixelFormat;

public class HeadOnlyImageCreator extends VisualUserRepresentationImageCreator {

	public HeadOnlyImageCreator() {

		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.
	 * imageCreators.VisualUserRepresentationImageCreator#
	 * createRepresentatonImage(javafx.scene.image.Image)
	 */
	@Override
	public Image createRepresentatonImage(Image userImage) {

		return detectFace(userImage);
	}

	private Image detectFace(Image fxImage) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//		System.out.println("\nRunning FaceDetector");

		try {
			String xmlUri = new URI(Resources.getResource("userRepresentation/xml/haarcascade_frontalface_alt.xml").toString()).getPath();		
			String xmlFilePath = new File(xmlUri).getCanonicalPath();
			CascadeClassifier faceDetector = new CascadeClassifier(xmlFilePath);
			
			Mat image = imageToMat(fxImage);

			MatOfRect faceDetections = new MatOfRect();
			faceDetector.detectMultiScale(image, faceDetections);

//			System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

			if (faceDetections.toArray().length > 0) {
				Rect rect = faceDetections.toArray()[0];

				// int posX = rect.x -rect.width/4;
				// int posY = rect.y - rect.height/2;
				// int width = (int)(rect.width * 1.5);
				// int height = rect.height * 2;

				int posX = rect.x;
				int posY = rect.y;
				int width = rect.width;
				int height = rect.height;

				Rect rectCrop = new Rect(posX, posY, width, height);
				Mat faceOnly = new Mat(image, rectCrop);

				return matToImage(faceOnly);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
					
		return null;
	}

	private Mat imageToMat(Image image) {
		Mat mat = null;
		if(image!=null){
			int width = (int) image.getWidth();
			int height = (int) image.getHeight();
			byte[] buffer = new byte[width * height * 4];

			PixelReader reader = image.getPixelReader();
			WritablePixelFormat<ByteBuffer> format = WritablePixelFormat.getByteBgraInstance();
			reader.getPixels(0, 0, width, height, format, buffer, 0, width * 4);

			mat = new Mat(height, width, CvType.CV_8UC4);
			mat.put(0, 0, buffer);
		}
		return mat;
	}

	private Image matToImage(Mat mat) {
		MatOfByte byteMat = new MatOfByte();
		Highgui.imencode(".bmp", mat, byteMat);
		return new Image(new ByteArrayInputStream(byteMat.toArray()));
	}
}
