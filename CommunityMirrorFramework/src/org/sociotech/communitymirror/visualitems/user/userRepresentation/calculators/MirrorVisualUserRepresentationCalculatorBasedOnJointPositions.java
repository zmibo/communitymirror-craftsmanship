package org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyPosition;

public class MirrorVisualUserRepresentationCalculatorBasedOnJointPositions extends VisualUserRepresentationCalculator {

	private Configuration configuration;

	private double displayWidthInPixel;
	private double displayHeightInPixel;

	private int displayWidthInMillimeter;
	private int displayHeightInMillimeter;

	private int offsetKinectOriginXInMillimeter;
	private int offsetKinectOriginYInMillimeter;

	public MirrorVisualUserRepresentationCalculatorBasedOnJointPositions() {
		super();
		this.configuration = CommunityMirror.getConfiguration();
		this.displayWidthInPixel = viewBounds.getWidth();
		this.displayHeightInPixel = viewBounds.getHeight();
		this.displayWidthInMillimeter = this.configuration.getInt("display.widthInMillimeter");
		this.displayHeightInMillimeter = this.configuration.getInt("display.heightInMillimeter");
		this.offsetKinectOriginXInMillimeter = this.configuration
				.getInt("sensing.kinect.offsetKinectOriginXFromScreenOriginXInMillimeter");
		this.offsetKinectOriginYInMillimeter = this.configuration
				.getInt("sensing.kinect.offsetKinectOriginYFromScreenOriginYInMillimeter");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.
	 * calculators.VisualUserRepresentationCalculator#calculatePositionX(org.
	 * sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionX(User user, double imageWidth) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double onScreenUpperLeftPositionXInPixel = 0;
		if (bodyPosition != null) {
			// (1) get user positionX (in mm) in kinect coordinate system
			float userPositionXInMillimeter = bodyPosition.getPosX1();
			// (2) calculate position X (in mm) in screen coordinate system
			double onScreenPositionXInMillimeter = userPositionXInMillimeter + this.offsetKinectOriginXInMillimeter;
			// (3) calculate position X in pixel
			double onScreenJointPositionXInPixel = onScreenPositionXInMillimeter * this.displayWidthInPixel
					/ this.displayWidthInMillimeter;
			// (4) calculate posX of left upper corner of user image
			onScreenUpperLeftPositionXInPixel = onScreenJointPositionXInPixel - bodyPosition.getPosXInUserImage();
			// (5) correct position according to scale
			onScreenUpperLeftPositionXInPixel = correctPositionAccordingToScale(onScreenUpperLeftPositionXInPixel,
					onScreenJointPositionXInPixel, calculateScale(user), imageWidth);
		}
		return onScreenUpperLeftPositionXInPixel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.
	 * calculators.VisualUserRepresentationCalculator#calculatePositionY(org.
	 * sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionY(User user, double imageHeight) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double onScreenUpperLeftPositionYInPixel = 0;
		if (bodyPosition != null) {
			// (1) get user positionY (in mm) in kinect coordinate system
			float jointPositionYInMillimeter = bodyPosition.getPosY1();
			// (2) calculate position Y (in mm) in screen coordinate system
			double onScreenJointPositionYInMillimeter = jointPositionYInMillimeter * (-1)
					- this.offsetKinectOriginYInMillimeter;
			// (3) calculate position Y in pixel
			double onScreenJointPositionYInPixel = onScreenJointPositionYInMillimeter * this.displayHeightInPixel
					/ this.displayHeightInMillimeter;
			// (4) calculate posX of left upper corner of user image
			onScreenUpperLeftPositionYInPixel = onScreenJointPositionYInPixel - bodyPosition.getPosYInUserImage();
			// (5) correct position according to scale
			onScreenUpperLeftPositionYInPixel = correctPositionAccordingToScale(onScreenUpperLeftPositionYInPixel,
					onScreenJointPositionYInPixel, calculateScale(user), imageHeight);
		}
		return onScreenUpperLeftPositionYInPixel;
	}

	/**
	 * Corrects a given position of a image according to the scale of that
	 * image.
	 * 
	 * @param upperLeftCorner
	 *            position of the upper left corner of the image before scale
	 * @param jointPositionInImage
	 *            position of the joint in the image before scale
	 * @param scale
	 *            the scale that will be done to the image
	 * @param unscaledLength
	 *            the length (width or height) of the image before scale
	 * @return the position of the upper left corner after scale
	 */
	private double correctPositionAccordingToScale(double upperLeftCorner, double jointPositionInImage, double scale,
			double unscaledLength) {
		double positionMiddle = upperLeftCorner + unscaledLength / 2;
		double diff = Math.abs(positionMiddle - jointPositionInImage);
		double diffAfterScale = diff * scale;
		double shift = diffAfterScale - diff;
		if (jointPositionInImage < positionMiddle) {
			return upperLeftCorner + shift;
		} else {
			return upperLeftCorner - shift;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.
	 * calculators.VisualUserRepresentationCalculator#calculateScale(org.
	 * sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculateScale(User user) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double scale = 1;
		if (bodyPosition != null) {
			// (1) get user joint y-positions in kinect coordinate system
			float userPositionYInMillimeter1 = bodyPosition.getPosY1();
			float userPositionYInMillimeter2 = bodyPosition.getPosY2();
			float distanceInMillimeter = Math.abs(Math.abs(userPositionYInMillimeter2) - Math.abs(userPositionYInMillimeter1));
//			System.out.println("millimeter -> x1: " + userPositionYInMillimeter1 + ", x2: " + userPositionYInMillimeter2
//					+ " , diff: " + distanceInMillimeter);
			// (2) get user joint y-positions in image
			float userPositionYInPixel1 = bodyPosition.getPosYInUserImage();
			float userPositionYInPixel2 = bodyPosition.getPosY2InUserImage();
			float distanceInPixel = Math.abs(userPositionYInPixel2 - userPositionYInPixel1);
//			System.out.println("pixel -> x1: " + userPositionYInPixel1 + ", x2: " + userPositionYInPixel2 + " , diff: "
//					+ distanceInPixel);
			// (3) convert distance in millimeter in pixels according to screen
			double distanceInPixelAfterScale = distanceInMillimeter * this.displayHeightInPixel
					/ this.displayHeightInMillimeter;
			// (4) calculate scale
			scale = distanceInPixelAfterScale / distanceInPixel;
//			System.out.println("scale: " + scale + ", distAfterScale: " + distanceInPixelAfterScale);
		}

		return scale;
	}
}
