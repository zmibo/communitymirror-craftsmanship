package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers.VisualUserRepresentationFXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.VisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

/**
 * Abstract super class of all states that a fxml visual user representation can
 * be in.
 *
 * @param <V>
 *            The type of the visual user representation fxml controller that
 *            controlls a visual user representation that is currently in this
 *            state.
 * @author evalosch
 *
 */
public abstract class FXMLVisualUserRepresentationState<V extends VisualUserRepresentationFXMLController>
		extends VisualUserRepresentationState {

	/**
	 * The fxml controller that controlles the binding of the visual user
	 * representation that is connected to this state to a corresponding
	 * .fxml-file.
	 */
	protected V fxmlController;

	public V getFxmlController() {
		return fxmlController;
	}

	/**
	 * URL of the fxml-file that is used by the fxml controller of this state.
	 */
	private final URL fxmlLocation;

	/**
	 * List of nodes that represent the fxml elements of the fxml file.
	 */
	private List<Node> nodesFromFXML;

	/**
	 * The fxml loader that loads the fxml elements from the fxml-file that is
	 * used by the fxml controller of this state.
	 */
	protected FXMLLoader fxmlLoader;

	/**
	 * The anchor pane retrieved from loading the fxml loader.
	 */
	protected AnchorPane fxmlAnchorPane;

	public FXMLVisualUserRepresentationState(InteractionPhase interactionPhase,
			URL fxmlLocation,
			VisualUserRepresentation visualUserRepresentation,
			VisualUserRepresentationImageCreator imageCreator,
			VisualUserRepresentationCalculator calculator) {
		super(visualUserRepresentation, interactionPhase, calculator, imageCreator);

		this.fxmlLocation = fxmlLocation;
		this.nodesFromFXML = new LinkedList<Node>();

		this.initializeFXMLLoader();
	}

	/**
	 * Initializes the fxml loader and sets its location.
	 */
	private void initializeFXMLLoader() {
		this.fxmlLoader = new FXMLLoader();
		this.fxmlLoader.setLocation(fxmlLocation);
		try {
			this.fxmlAnchorPane = (AnchorPane) this.fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets bindings and values within the fxml controller like it is expected
	 * in the interaction phase.
	 */
	protected void setupFXMLController() {
		this.fxmlController = this.fxmlLoader.getController();
		if(this.fxmlController != null){
			this.fxmlController.bindToUserRepresentationImage(visualUserRepresentation.getRepresentationImageView());
		}
		else{
			System.out.println("fxmlController is null!");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#onActivation()
	 */
	@Override
	public void onActivation() {
		super.onActivation();
		this.setupFXMLController();
		this.addFXMLElementsToVisualUserRepresentation();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#onDeactivation()
	 */
	@Override
	public void onDeactivation() {
		super.onDeactivation();
		this.removeFXMLElementsFromVisualUserRepresentation();
	}

	/**
	 * Adds all children of the anchor pane of the fxml-file as nodes to the
	 * list of nodes of this state if it is still empty.
	 */
	private void fillListOfNodesFromFXML() {
		if (this.fxmlAnchorPane != null && nodesFromFXML.size() == 0) {
			for (Node node : this.fxmlAnchorPane.getChildren()) {
				// add node to list of nodes
				nodesFromFXML.add(node);
			}
		}
	}

	/**
	 * Adds the fxml elements of the fxml file as nodes to the visual user
	 * representation.
	 */
	private void addFXMLElementsToVisualUserRepresentation() {
		fillListOfNodesFromFXML();
		for (Node node : nodesFromFXML) {
			visualUserRepresentation.addNode(node);
		}
	}

	/**
	 * Removes the nodes representing the fxml elements from the user visual
	 * representation.
	 */
	private void removeFXMLElementsFromVisualUserRepresentation() {
		fillListOfNodesFromFXML();
		for (Node node : nodesFromFXML) {
			visualUserRepresentation.removeNode(node);
		}
	}
}
