package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.MirrorVisualUserRepresentationCalculatorBasedOnJointPositions;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers.FifthPhaseVisualUserRepresentationFXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.FifthPhaseVisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

import com.google.common.io.Resources;

/**
 * State of a visual user representation that is represented like is expected
 * for users in the fifth interaction phase.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentationInFifthInteractionPhase
		extends
		FXMLVisualUserRepresentationState<FifthPhaseVisualUserRepresentationFXMLController> {

	public VisualUserRepresentationInFifthInteractionPhase(
			VisualUserRepresentation visualUserRepresentation) {
		super(
				InteractionPhase.FifthInteractionPhase,
				Resources
						.getResource("userRepresentation/fxml/fifthInteractionPhase.fxml"),
				visualUserRepresentation,
				//new HeadOnlyImageCreator(),
				new FifthPhaseVisualUserRepresentationImageCreator(),
				//new AdjustedVisualUserRepresentationCalculator(),
				new MirrorVisualUserRepresentationCalculatorBasedOnJointPositions());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .FXMLVisualUserRepresentationState#setupFXMLController()
	 */
	@Override
	protected void setupFXMLController() {
		super.setupFXMLController();
		// this.fxmlController.setPersonImage(this.visualUserRepresentation.getRepresentationImage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#fillListOfInteractionBehaviors()
	 */
	@Override
	protected void fillListOfInteractionBehaviors() {
		// TODO Auto-generated method stub

	}
}
