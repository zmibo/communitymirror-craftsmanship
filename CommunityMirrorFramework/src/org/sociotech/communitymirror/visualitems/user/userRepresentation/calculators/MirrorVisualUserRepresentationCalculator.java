  package org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyPosition;

public class MirrorVisualUserRepresentationCalculator extends
VisualUserRepresentationCalculator {
	
	private Configuration configuration;
	
	private double displayWidthInPixel;
	
	@SuppressWarnings("unused")
	private double displayHeightInPixel;
	
	private int displayWidthInMillimeter;
	
	@SuppressWarnings("unused")
	private int displayHeightInMillimeter;
	
	@SuppressWarnings("unused")
	private int offsetKinectOriginXInMillimeter;
	
	@SuppressWarnings("unused")
	private int offsetKinectOriginYInMillimeter;
	
	public MirrorVisualUserRepresentationCalculator(){
		super();
		
		this.configuration = CommunityMirror.getConfiguration();
		//this.cameraWidthInPixel =;
		this.displayWidthInPixel = 5000;//viewBounds.getWidth();
		this.displayHeightInPixel = viewBounds.getHeight();
		this.displayWidthInMillimeter = this.configuration.getInt("display.widthInMillimeter");
		this.displayHeightInMillimeter = this.configuration.getInt("display.heightInMillimeter");
		this.offsetKinectOriginXInMillimeter = this.configuration.getInt("sensing.kinect.offsetKinectOriginXFromScreenOriginXInMillimeter");
		this.offsetKinectOriginYInMillimeter = this.configuration.getInt("sensing.kinect.offsetKinectOriginYFromScreenOriginYInMillimeter");
	}
	
	

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculatePositionX(org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionX(User user, double imageWidth) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double finalDisplayPositionXInPixel = 0;
		if (bodyPosition != null) {
			float userPositionZInMillimeter = bodyPosition.getPosZ1();
			float relativeCameraPositionXInPixel = bodyPosition.getPosX1();
			
			// transfer cameraPosition into displayPosition
			double displayPositionXInPixel = relativeCameraPositionXInPixel * displayWidthInPixel;
			System.out.println("displayPositionXInPixel: " + displayPositionXInPixel);
			
			// calculate width of tracked area in real world
			double realWorldWidthInMillimeter = (Math.tan(Math.toRadians(57/2)) * userPositionZInMillimeter) * (displayWidthInPixel/displayWidthInMillimeter);
			System.out.println("Math.tan(Math.toRadians(57/2): " + Math.tan(Math.toRadians(57/2)));
			System.out.println("userPositionZInMillimeter: " + userPositionZInMillimeter);
			System.out.println("realWorldWidthInMillimeter: " + realWorldWidthInMillimeter);
			
			// correct display positions to match real world positions
			double realWorldPositionXInPixel = displayPositionXInPixel * (realWorldWidthInMillimeter / displayWidthInMillimeter);
			System.out.println("realWorldPositionXInPixel: " + realWorldPositionXInPixel);
			
			// shift real world positions to final display positions
			finalDisplayPositionXInPixel = realWorldPositionXInPixel + (realWorldWidthInMillimeter - displayWidthInMillimeter)/2 * (displayWidthInPixel/displayWidthInMillimeter);
			System.out.println("__finalDisplayPositionXInPixel: " + finalDisplayPositionXInPixel);
			
			//finalDisplayPositionXInPixel = displayPositionXInPixel;
		}
		return finalDisplayPositionXInPixel;
		//return 5000;
	}
	
	/* (non-Javadoc)
//	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculatePositionX(org.sociotech.communitymirror.visualitems.user.User)
//	 */
//	@Override
//	public double calculatePositionX(User user, double imageWidth) {
//		BodyPosition bodyPosition = user.getBodyPosition();
//		double onScreenPositionXInPixel = 0;
//		if (bodyPosition != null) {
//			float userPositionZInMillimeter = bodyPosition.getPosZ();
//			// calculate position X
//			onScreenPositionXInPixel = bodyPosition.getPosX() * this.displayWidthInPixel;
//			System.out.println("onScreenPositionXInPixel: " + onScreenPositionXInPixel + ", pixel width: " + this.displayWidthInPixel);
//			
//			
//			double trackedWidthInMillimeter = (Math.tan(57/2) * userPositionZInMillimeter);
//			
//			double trackedWidthInPixel = trackedWidthInMillimeter * (this.displayWidthInPixel/this.displayWidthInMillimeter);
//			
//			
//			
//			
//			
////			// calculate offset between posX of user in camera image and in real world
////			double offsetInMillimeter = this.displayWidthInMillimeter/2 - (Math.tan(57/2) * userPositionZInMillimeter);
////			System.out.println("offsetInMillimeter: " + offsetInMillimeter);
////			double offsetInPixel = offsetInMillimeter * (this.displayWidthInPixel/this.displayWidthInMillimeter);
////			System.out.println("offsetInPixel: " + offsetInPixel);
//			
//			// calculate position X on screen
//			if(onScreenPositionXInPixel > this.displayWidthInMillimeter/2){
//				onScreenPositionXInPixel = (onScreenPositionXInPixel  - this.displayWidthInMillimeter/2)/(this.displayWidthInMillimeter/2) * trackedWidthInPixel - (trackedWidthInPixel - this.displayWidthInMillimeter/2);
//			}
//			else if(onScreenPositionXInPixel < this.displayWidthInMillimeter/2){
//				onScreenPositionXInPixel = trackedWidthInPixel;
//			}
//		}
////		// correct position according to scale
////		onScreenPositionXInPixel = onScreenPositionXInPixel + (calculateScaleX(user) - 1) * (imageWidth / calculateScaleX(user)) / 2;
//		System.out.println("onScreenPositionXInPixel: " + onScreenPositionXInPixel);
//		return onScreenPositionXInPixel;
//	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculatePositionY(org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionY(User user,  double imageHeight) {
//		BodyPosition bodyPosition = user.getBodyPosition();
//		double onScreenPositionYInPixel = 0;
//		// default implementation: use positionY of user body position
//		if (bodyPosition != null) {
//			float userPositionYInMillimeter = bodyPosition.getPosY();
//			// calculate position Y
//			double onScreenPositionYInMillimeter = (userPositionYInMillimeter + this.offsetKinectOriginYInMillimeter) * (-1);
//			onScreenPositionYInPixel = onScreenPositionYInMillimeter * (this.displayHeightInPixel/this.displayHeightInMillimeter);
//		}
//		// correct position according to scale
//		onScreenPositionYInPixel = onScreenPositionYInPixel + (calculateScaleY(user) - 1) * (imageHeight / calculateScaleY(user)) / 2;
//		return onScreenPositionYInPixel;
		return 100;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculateScale(org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculateScale(User user) {
		// TODO override default implementation
		return super.calculateScale(user);
	}
}
