package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import java.util.LinkedList;
import java.util.List;

import javafx.scene.image.Image;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.events.stateChange.State;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.VisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

/**
 * Abstract super class of all states that a visual user representation can be
 * in.
 * 
 * @author evalosch
 *
 */
public abstract class VisualUserRepresentationState extends State {

	/**
	 * The visual user representation that is connected to this state.
	 */
	protected final VisualUserRepresentation visualUserRepresentation;

	/**
	 * The interaction phase in which the user is while he is represented by
	 * this visual user representation.
	 */
	private final InteractionPhase interactionPhase;

	/**
	 * The calculator that calculates data for the visual user representation
	 * that is currently in this state.
	 */
	private final VisualUserRepresentationCalculator calculator;

	/**
	 * The image creator that is used to create images for the visual user
	 * representation that is currently in this state.
	 */
	private final VisualUserRepresentationImageCreator imageCreator;

	/**
	 * List of all interaction behaviors that are added to the visual user
	 * representation that is within this visual user representation state.
	 */
	protected List<InteractionBehavior<?>> interactionBehaviors;

	public VisualUserRepresentationState(
			VisualUserRepresentation visualUserRepresentation,
			InteractionPhase interactionPhase,
			VisualUserRepresentationCalculator calculator,
			VisualUserRepresentationImageCreator imageCreator) {
		this.visualUserRepresentation = visualUserRepresentation;
		this.interactionPhase = interactionPhase;
		this.calculator = calculator;
		this.imageCreator = imageCreator;
		this.interactionBehaviors = new LinkedList<InteractionBehavior<?>>();
	}

	/**
	 * Is called if the state of the visual user representation is set to this
	 * state.
	 */
	public void onActivation() {
		this.addInteractionBehaviorsToVisualUserRepresentation();
	}

	/**
	 * Is called if the state of the visual user representation is changed from
	 * this state to another state
	 */
	public void onDeactivation() {
		this.removeInteractionBehaviorsFromVisualUserRepresentation();
	}

	/**
	 * @return the interactionPhase
	 */
	public InteractionPhase getInteractionPhase() {
		return interactionPhase;
	}

	/**
	 * @return the calculator
	 */
	public VisualUserRepresentationCalculator getCalculator() {
		return calculator;
	}

	/**
	 * @return the imageCreator
	 */
	public VisualUserRepresentationImageCreator getImageCreator() {
		return imageCreator;
	}

	public double calculatePositionX(User user, double imageWidth) {
		return this.getCalculator().calculatePositionX(user, imageWidth);
	}

	public double calculatePositionY(User user, double imageHeight) {
		return this.getCalculator().calculatePositionY(user, imageHeight);
	}

	public double calculateScale(User user) {
		return this.getCalculator().calculateScale(user);
	}

	public Image createRepresentatonImage(Image userImage) {
		return this.getImageCreator().createRepresentatonImage(userImage);
	}

	/**
	 * adds the behavior to the visual user representation that is in this
	 * visual user representation state
	 */
	private void addInteractionBehaviorsToVisualUserRepresentation() {
		// if list is still empty fill the list
		if (this.interactionBehaviors.size() == 0) {
			fillListOfInteractionBehaviors();
		}
		for (InteractionBehavior<?> interactionBehavior : this.interactionBehaviors) {
			this.visualUserRepresentation
					.addInteractionBehavior(interactionBehavior);
		}
	}

	/**
	 * removes all behaviors from the visual user representation that is in this
	 * visual user representation state
	 */
	private void removeInteractionBehaviorsFromVisualUserRepresentation() {
		if (this.interactionBehaviors != null) {
			for (InteractionBehavior<?> interactionBehavior : this.interactionBehaviors) {
				this.visualUserRepresentation
						.removeInteractionBehavior(interactionBehavior);
			}
		}
	}

	/**
	 * Fills the list of interaction behaviors of the visual user representation
	 * that are specific for this visual user representation state.
	 */
	protected abstract void fillListOfInteractionBehaviors();
}
