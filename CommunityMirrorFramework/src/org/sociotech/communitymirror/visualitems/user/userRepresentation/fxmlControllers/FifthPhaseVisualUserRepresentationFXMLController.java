package org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML controller for visual user representations of users that are in the
 * fifth interaction phase. The FXML controller defines the binding between fxml
 * ui elements from visualUserRepresentation.fxml and the data of a
 * corresponding instance of visual user representation.
 *
 * @author evalosch
 *
 */
public class FifthPhaseVisualUserRepresentationFXMLController extends
		VisualUserRepresentationFXMLController {

	/*
	 * (non-Javadoc)
	 *
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		super.initialize(arg0, arg1);
	}
}
