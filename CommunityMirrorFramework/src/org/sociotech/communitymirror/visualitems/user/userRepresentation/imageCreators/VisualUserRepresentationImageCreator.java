package org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators;

import javafx.scene.image.Image;

public abstract class VisualUserRepresentationImageCreator {

	public Image createRepresentatonImage(Image userImage) {
		// default implementation: use the user image as it is
		return userImage;
	}

	public boolean isRGBImage() {
		// default implementation: create a RGB image
		return true;
	}

}
