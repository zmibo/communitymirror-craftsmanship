package org.sociotech.communitymirror.visualitems.user.userState;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneType;

/**
 * A concrete user state, that says that the user is in the fifth interaction
 * phase.
 * 
 * @author evalosch
 *
 */
public class UserInFifthInteractionPhase extends UserState {

	public UserInFifthInteractionPhase() {
		super(InteractionPhase.FifthInteractionPhase);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userState.UserState#isReached
	 * (org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public boolean isReached(User user) {
		// TODO define under which conditions this user state is reached by a
		// given user
		boolean userHasTouched = false;
		if(user.getCurrentInteractionZone().getType() == InteractionZoneType.TouchZone && userHasTouched){
			return true;
		}
		return false;
	}
}
