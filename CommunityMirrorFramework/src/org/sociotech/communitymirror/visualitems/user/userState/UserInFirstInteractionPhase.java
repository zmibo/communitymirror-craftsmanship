package org.sociotech.communitymirror.visualitems.user.userState;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneType;

/**
 * A concrete user state, that says that the user is in the first interaction
 * phase.
 * 
 * @author evalosch
 *
 */
public class UserInFirstInteractionPhase extends UserState {

	public UserInFirstInteractionPhase() {
		super(InteractionPhase.FirstInteractionPhase);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userState.UserState#isReached
	 * (org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public boolean isReached(User user) {
		// TODO define under which conditions this user state is reached by a
		// given user
		if(user.getCurrentInteractionZone().getType() == InteractionZoneType.UntrackedZone){
			return true;
		}
		return false;
	}
}
