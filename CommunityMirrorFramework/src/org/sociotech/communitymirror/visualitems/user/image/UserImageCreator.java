package org.sociotech.communitymirror.visualitems.user.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javafx.scene.image.Image;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that creates user images out of bigger image data.
 * 
 * @author evalosch
 *
 */
public class UserImageCreator {

	/**
	 * The log4j logger reference.
	 */
	private final static Logger logger = LogManager
			.getLogger(UserImageCreator.class.getName());
	
	/**
	 * Creates a depth user image to a given user out of a given depth image
	 * data. Cuts the area out of the given depth data, that shows the given
	 * user.
	 * 
	 * @param user
	 *            The user for whom an user image is created.
	 * @param depth_frame
	 *            The depth image data from which the user image is created.
	 * @param player_index
	 * @param xyz
	 * @param uv
	 * @return The created depth user image.
	 */
	public static Image createDepthUserImage(short[] depth_frame,
			byte[] player_index, float[] xyz, float[] uv) {
		// TODO: create user image from depth data
		Image image = null;
		return image;
	}

	/**
	 * Creates a color user image to a given user out of a given color image
	 * data. Cuts the area out of the given image data, that shows the given
	 * user.
	 * 
	 * @param user
	 *            The user for whom an user image is created.
	 * @param color_data
	 *            The color image data from which the user image is created.
	 * @return The created user image.
	 */
	public static Image createColorUserImage(byte[] color_data) {
		Image image = getJavaFXImage(color_data, 1280, 960);
		// TODO: take only the area that shows the given user
		return image;
	}

	/**
	 * Creates a JavaFX Image with given width and height from a given byte
	 * array in bgra format.
	 * 
	 * @param rawPixels
	 *            The byte array in bgra format from which to create the image.
	 * @param width
	 *            The width of the image to create.
	 * @param height
	 *            The height of the image to create.
	 * @return The created JavaFx Image.
	 */
	public static Image getJavaFXImage(byte[] rawPixels, int width, int height) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			ImageIO.write(
					(RenderedImage) createBufferedImage(rawPixels, width,
							height), "png", out);
			out.flush();
		} catch (IOException ex) {
			logger.warn("user image could not be created!");
		}
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		return new Image(in);
	}

	private static BufferedImage createBufferedImage(byte[] pixels, int width,
			int height) {
		SampleModel sm = getIndexSampleModel(width, height);
		DataBuffer db = new DataBufferByte(pixels, width * height, 0);
		WritableRaster raster = Raster.createWritableRaster(sm, db, null);
		IndexColorModel cm = getDefaultColorModel();
		BufferedImage image = new BufferedImage(cm, raster, false, null);
		return image;
	}

	private static SampleModel getIndexSampleModel(int width, int height) {
		IndexColorModel icm = getDefaultColorModel();
		WritableRaster wr = icm.createCompatibleWritableRaster(1, 1);
		SampleModel sampleModel = wr.getSampleModel();
		sampleModel = sampleModel.createCompatibleSampleModel(width, height);
		return sampleModel;
	}

	private static IndexColorModel getDefaultColorModel() {
		byte[] r = new byte[256];
		byte[] g = new byte[256];
		byte[] b = new byte[256];
		for (int i = 0; i < 256; i++) {
			r[i] = (byte) i;
			g[i] = (byte) i;
			b[i] = (byte) i;
		}
		IndexColorModel defaultColorModel = new IndexColorModel(8, 256, r, g, b);
		return defaultColorModel;
	}

	public static void findMinAndMax(short[] pixels, int width, int height) {
		int size = width * height;
		int value;
		int min = 65535;
		int max = 0;
		for (int i = 0; i < size; i++) {
			value = pixels[i] & 0xffff;
			if (value < min)
				min = value;
			if (value > max)
				max = value;
		}
	}
}
