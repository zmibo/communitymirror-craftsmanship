package org.sociotech.communitymirror.visualitems.renderer;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Abstract super class for all visual item renderer.
 *
 * @author Peter Lachenmaier
 *
 * @param <T> The type of data items that can be rendererd.
 * @param <V> The type of visual items that will be created.
 */
public abstract class VisualItemRenderer<T extends Item, V extends VisualItem<T>> {

	/**
	 * Renders the given item.
	 *
	 * @param item Item to be rendered.
	 * @param state Visual state of the item
	 * @return The create visual item representing the given data item.
	 */
	public abstract V renderItem(T item, VisualState state);

	// TODO: should this one have a "changeState(VisualItem<T> visualItem, VisualState newState)" method ??
}
