package org.sociotech.communitymirror.visualitems;

import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.dataconnection.DataItemAdapter;
import org.sociotech.communitymirror.dataconnection.DataItemChangedInterface;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * The abstract superclass of all visual items. A visual item is a
 * visual representation of a concrete data item. Several visual items
 * can exist for one data item.
 * 
 * @author Peter Lachenmaier
 * 
 * @param <T> The type of the used data item. Must be a subclass of {@link Item} 
 */
public abstract class VisualItem<T extends Item> extends VisualComponent implements DataItemChangedInterface {
	
	/**
	 * The visual state of this visual item. Initially {@link VisualState#DEFAULT}. 
	 */
	protected VisualState visualState = VisualState.DEFAULT;

	/**
	 * The reference to the CommunityMashup data item  
	 */
	protected final T dataItem;
	
	/**
	 * Map that keeps a mapping of used properties to data attributes.
	 */
	protected Map<EAttribute, StringProperty> attributePropertyMap = null;
	
	/**
	 * Map that keeps a mapping of used properties to operation results.
	 */
	protected Map<EOperation, StringProperty> operationPropertyMap = null;
	
	/**
	 * Logger reference
	 */
	protected Logger logger = LogManager.getLogger(VisualItem.class.getName());

	/**
	 * Indicates if there is currently a data change
	 */
	private boolean inUpdate = false;

	/**
	 * Reference to the adapter observing changes of the data item.
	 */
	private DataItemAdapter itemAdapter;
	
	/**
	 * Creates a visual item as a representation of the given item.
	 * 
	 * @param dataItem The data item to represent.
	 */
	public VisualItem(T dataItem) {
		this.dataItem = dataItem;
		
		// connect data item to observe changes
		connectDataItem();
	}
	
	/**
	 * Returns the used CommunityMashup data item.
	 * 
	 * @return The CommunityMashup data item.
	 */
	public T getDataItem() {
		return dataItem;
	}
	
	/**
	 * Initializes a data adapter to observe changes of the data item.
	 */
	private void connectDataItem() {
		itemAdapter = new DataItemAdapter(dataItem, this);
	}
	
	/**
	 * Binds the given property to the property bound to the attribute. The binding will be
	 * created bidirectional. This means that data changes will be reflected to the given property
	 * and property changes will be reflected to the data item.
	 *  
	 * @param attributeFeatureID The feature id of the attribute that should be bound.
	 * @param property The property to bind the attribute to.
	 */
	public void bindAttributeBidirectional(int attributeFeatureID, StringProperty property) {
		if(property == null) {
			throw new IllegalArgumentException("Property can not be null");
		}
		
		StringProperty attributeProperty = getStringPropertyForAttribute(attributeFeatureID);
		if(attributeProperty == null) {
			logger.warn("Could not bind property. No property for an attribute with id " + attributeFeatureID);
			return;
		}
		
		// bind the property bidirectional
		property.bindBidirectional(attributeProperty);
	}
	
	/**
	 * Binds the given property to the property bound to the attribute. The binding will be
	 * created only unidirectional. This means that data changes will be reflected to the given property,
	 * but no property changes will be reflected to the data item. This causes the given property to be
	 * no more changeable.
	 *  
	 * @param attributeFeatureID The feature id of the attribute that should be bound.
	 * @param property The property to bind the attribute to.
	 */
	public void bindAttributeUnidirectional(int attributeFeatureID, StringProperty property) {
		if(property == null) {
			throw new IllegalArgumentException("Property can not be null");
		}
		
		StringProperty attributeProperty = getStringPropertyForAttribute(attributeFeatureID);
		if(attributeProperty == null) {
			logger.warn("Could not bind property. No property for an attribute with id " + attributeFeatureID);
			return;
		}
		
		// bind the property unidirectional
		property.bind(attributeProperty);
	}
	
	/**
	 * Returns the property bound to the data attribute identified by the given featureID.
	 * If it does not exist yet, it will be created and bound.
	 * 
	 * @param featureID The feature id 
	 * @return The property for the given feature.
	 */
	protected StringProperty getStringPropertyForAttribute(final int featureID) {
		// get matching attribute from data item
		EStructuralFeature structuralFeature = dataItem.eClass().getEStructuralFeature(featureID);
		
		// currently only attributes are allowed
		if(!(structuralFeature instanceof EAttribute)) {
			logger.error("Tried to access feature " + featureID + " of class " + dataItem.eClass().getName() + " as attribute, but it is no attribute");
			throw new IllegalArgumentException("The feature " + featureID + " of class " + dataItem.eClass().getName() + " is no attribute");
		}
		
		if(attributePropertyMap == null) {
			// create map on first use
			attributePropertyMap = new HashMap<>();
		} else if(attributePropertyMap.containsKey(structuralFeature)) {
			// created before, so simply return
			return attributePropertyMap.get(structuralFeature);
		}
		
		// cast to attribute
		EAttribute attribute = (EAttribute) structuralFeature;
						
		// create new property
		StringProperty attributeProperty = new SimpleStringProperty();

		// initially set the value of the property
		try {
			attributeProperty.setValue("" + dataItem.eGet(attribute));
		} catch (Exception e) {
			logger.warn("Could not set the inital value from attribute " + attribute + " of class " + dataItem.eClass().getName(), e);
		}
		
		// create listener to bind property changes to data item attribute
		attributeProperty.addListener(new AttributeChangeListener(attribute));

		// add it to map
		attributePropertyMap.put(attribute, attributeProperty);
		
		// and return it
		return attributeProperty;
	}

	/**
	 * Binds the given property to the property bound to the operation result. The binding will be
	 * created only unidirectional. This means that data changes will be reflected to the given property,
	 * but no property changes will be reflected to the data item. This causes the given property to be
	 * no more changeable. Be careful in usage. Every data item change will lead to an execution of the 
	 * operation. This may lead to performance issues.
	 * 
	 * Note: Do not use operations with side effects, like setters.
	 * 
	 * Note: Only operations without parameters are supported.
	 * 
	 * @param operationID EMF id of the operation that should be bound.
	 * @param property The property to bind the operation result to.
	 */
	public void bindOperationResult(int operationID, StringProperty property) {
		bindOperationResult(dataItem.eClass().getEOperation(operationID), property);
	}
	
	/**
	 * Binds the given property to the property bound to the operation result. The binding will be
	 * created only unidirectional. This means that data changes will be reflected to the given property,
	 * but no property changes will be reflected to the data item. This causes the given property to be
	 * no more changeable. Be careful in usage. Every data item change will lead to an execution of the 
	 * operation. This may lead to performance issues.
	 * 
	 * Note: Do not use operations with side effects, like setters.
	 * 
	 * Note: Only operations without parameters are supported.
	 * 
	 * @param operation EMF operation that should be bound.
	 * @param property The property to bind the operation result to.
	 */
	public void bindOperationResult(EOperation operation, StringProperty property) {
		if(property == null) {
			throw new IllegalArgumentException("Property can not be null");
		}
		
		StringProperty operationProperty = getStringPropertyForOperation(operation);
		if(operationProperty == null) {
			logger.warn("Could not bind property. No property for operation " + operation);
			return;
		}
		
		// bind the property unidirectional
		property.bind(operationProperty);
	}
	
	/**
	 * Returns the property bound to the operation identified by the given operationID.
	 * If it does not exist yet, it will be created and bound.
	 * 
	 * @param operationID The operation id 
	 * @return The property for the given operation.
	 */
	protected StringProperty getStringPropertyForOperation(final int operationID) {
		// get matching operation from data item
		EOperation operation = dataItem.eClass().getEOperation(operationID);
		
		if(operation == null) {
			logger.warn("No operation for id " + operationID + " in class " + dataItem.eClass().getName());
			return null;
		}
		
		return getStringPropertyForOperation(operation);
	}
	
	/**
	 * Returns the property bound to the given operation.
	 * If it does not exist yet, it will be created and bound.
	 * 
	 * @param operation The operation 
	 * @return The property for the given operation.
	 */
	protected StringProperty getStringPropertyForOperation(EOperation operation) {
		
		// check empty parameters, otherwise log messages and throw exception
		EList<EParameter> operationParameters = operation.getEParameters();
		if(operationParameters != null && !operationParameters.isEmpty()) {
			logger.error("The operation " + operation + " of class " + dataItem.eClass().getName() + " has parameters, which is not allowed.");
			throw new IllegalArgumentException("The operation " + operation + " of class " + dataItem.eClass().getName() + " has parameters, which is not allowed.");
		}
		
		if(operationPropertyMap == null) {
			// create map on first use
			operationPropertyMap = new HashMap<>();
		} else if(operationPropertyMap.containsKey(operation)) {
			// created before, so simply return
			return operationPropertyMap.get(operation);
		}
		
		// create new property
		StringProperty operationProperty = new SimpleStringProperty();

		// initially set the value of the property
		try {
			operationProperty.setValue("" + dataItem.eInvoke(operation, null));
		} catch (Exception e) {
			logger.warn("Could not set the inital value from operation " + operation + " of class " + dataItem.eClass().getName(), e);
		}
		
		// add it to map
		operationPropertyMap.put(operation, operationProperty);
		
		// and return it
		return operationProperty;
	}

	/**
	 * Method that handles the property change to the data item.
	 * 
	 * @param changedAttribute The attribute that should be changed depending on a change of the property mapped to the attribute.
	 * @param oldValue The old value of the property.
	 * @param newValue The new value of the property.
	 */
	private void propertyForAttributeChanged(EAttribute changedAttribute, String oldValue, String newValue) {
		// check if it is a value change
		// be nice and update only on changed attribute to avoid notification storms
		try {
			if(newValue != null && !newValue.equals(dataItem.eGet(changedAttribute))) {
				// set the new value for the attribute
				dataItem.eSet(changedAttribute, newValue);
			}
		} catch (Exception e) {
			logger.warn("Exeption (" + e.getMessage() + ") at the attempt to change the attribute " + changedAttribute + " due to a property change.", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.dataconnection.DataItemChangedInterface#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification) {
		// data object changed
		if(notification == null) {
			// nothing to do
			return;
		}
		
		if(isInUpdate()) {
			// block other (maybe looped) changes to avoid infinite loops 
			return;
		}
		
		// indicate start of change
		startUpdate();

		// check if bound attribute has changed
		Object feature = notification.getFeature();
		// only attributes are supported
		if(feature instanceof EAttribute) {
			EAttribute attribute = (EAttribute) feature;
			try {
				if(attributePropertyMap != null && attributePropertyMap.containsKey(attribute)) {
					StringProperty propertyToChange = attributePropertyMap.get(attribute);
					String newValue = notification.getNewStringValue();
					// change property value if differen
					if(propertyToChange.getValue() != null && !propertyToChange.getValue().equals(newValue)) {
						// set only on real change to avoid unnecessary notifications
						propertyToChange.setValue(newValue);
					}
				}
			} catch (Exception e) {
				logger.warn("Exception (" + e.getMessage() + ") + while propagating a data item change to JavaFX property", e);
			}
			
		}
		
		// call all bound methods and set the results to the properties
		// we need to call all methods, cause we do not know if the change
		// has affected the result:
		if(operationPropertyMap != null) {
			for(EOperation operation : operationPropertyMap.keySet()) {
				try {
					StringProperty propertyToChange = operationPropertyMap.get(operation);
					// execute operation and make string of it
					String newValue = "" + dataItem.eInvoke(operation, null);
					// change property value if differen
					if(propertyToChange.getValue() != null && !propertyToChange.getValue().equals(newValue)) {
						// set only on real change to avoid unnecessary notifications
						propertyToChange.setValue(newValue);
					}
				} catch (Exception e) {
					logger.warn("Exception (" + e.getMessage() + ") + while propagating operation result of " + operation + " after data item change to JavaFX property", e);
				}
			}
		}
		
		// indicate the end of change
		endUpdate();

		// call method to let subclasses handle changes
		onDataChange(notification);
	}
	
	/**
	 * This method can be overwritten in subclasses to react on data item changes. This method will be called after
	 * all changes to bound properties are handled.
	 * 
	 * @param notification The EMF notification containing change details.
	 */
	protected void onDataChange(Notification notification) {
		// nop
	}
	
	
	/**
	 * Inner class to keep the attribute that should get the changed value.
	 * 
	 * @author Peter Lachenmaier
	 */
	private class AttributeChangeListener implements ChangeListener<String> {

		private EAttribute changingAttribute = null;
		
		/**
		 * Creates a new change listener and keeps the given changing attribute
		 * to pass it as parameter on change.
		 * 
		 * @param changingAttribute Attribute to be changed.
		 */
		public AttributeChangeListener(EAttribute changingAttribute) {
			this.changingAttribute = changingAttribute;
		}
		
		/* (non-Javadoc)
		 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void changed(ObservableValue<? extends String> arg0,
				String oldValue, String newValue) {
			propertyForAttributeChanged(changingAttribute, oldValue, newValue);
		}
	}
	
	/**
	 * Method to indicate the end of a change.
	 */
	private synchronized void  endUpdate() {
		this.inUpdate = false;
	}

	/**
	 * Method to indicate the beginning of a change.
	 */
	private synchronized void startUpdate() {
		this.inUpdate = true;
	}

	/**
	 * Returns whether a change is currently processed or not.
	 * 
	 * @return Whether a change is currently processed or not.
	 */
	private synchronized boolean isInUpdate() {
		return this.inUpdate;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		
		super.onDestroy();
		
		// disconnect the data adapter
		itemAdapter.disconnect();
		
		// unbind all properties
		if(attributePropertyMap != null) {
			for(EAttribute key : attributePropertyMap.keySet()) {
				StringProperty property = attributePropertyMap.get(key);
				property.unbind();
			}
		}
	}
	
	/**
	 * Returns the visual state.
	 * 
	 * @return The visual state.
	 */
	public VisualState getVisualState() {
		return visualState;
	}

	/**
	 * Sets the given visual state as visual state. Set to {@link VisualState#DEFAULT}
	 * if not defined.
	 * 
	 * @param visualState Visual state.
	 */
	public void setVisualState(VisualState visualState) {
		if(this.visualState == visualState) {
			return;
		}
		
		if(visualState == null) {
			this.visualState = VisualState.DEFAULT;
		}
		else {
			this.visualState = visualState;
		}
		
		onVisualStateChanged(this.visualState);
	}
	
	/**
	 * Method to be implemented in subclasses for applying visual changes linked to visual state change
	 * @param visualState The visual state
	 */
	protected void onVisualStateChanged(VisualState visualState){
		
	}
	
	/**
	 * Adds the given node as child to this visual item.
	 */
	public void addNode(Node node) {
		this.getChildren().add(node);
	}

    /**
     * Removes the given node from this visual item.
     */
    public void removeNode(Node node) {
        this.getChildren().remove(node);
    }

}
