package org.sociotech.communitymirror.visualitems.components.base;

import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: i21bmabu
 * Date: 31.10.13
 * Time: 09:44
 * To change this template use File | Settings | File Templates.
 */
public class CMFToggleButton extends CMFButton<ToggleButton> {

    public CMFToggleButton(String title) {
        super(new ToggleButton(title));
    }

    public CMFToggleButton(String title, ImageView imageView) {
        super(new ToggleButton(title,imageView));
    }

    public void setSelected(boolean selected) {
        getButtonBase().setSelected(selected);
    }

    public void setToggleGroup(ToggleGroup toggleGroup) {
        getButtonBase().setToggleGroup(toggleGroup);
    }

    /*TODO public void setFocused(boolean focused) {
        ToggleButton buttonBase = getButtonBase();//.setFocused(focused);
        buttonBase.setF
    }*/
}
