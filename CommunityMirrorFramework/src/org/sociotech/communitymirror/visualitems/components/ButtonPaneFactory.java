/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.visualitems.components;

import javafx.geometry.Orientation;
import javafx.scene.control.Control;
import javafx.scene.layout.TilePane;
import org.sociotech.communitymirror.visualitems.components.base.CMFButton;

import java.util.LinkedList;
import java.util.List;

public final class ButtonPaneFactory {

    private ButtonPaneFactory() {
    }

    public static TilePane createTilePane(int spacing, List<? extends CMFButton<?>> controlList, Orientation orientation) {

        List<Control> jfxControlList = new LinkedList<>();
        for(CMFButton<?> c : controlList) {
            jfxControlList.add(c.getButtonBase());
        }

        TilePane tilePane = new TilePane();
        tilePane.setVgap(spacing);
        tilePane.setHgap(spacing);
        tilePane.setLayoutX(spacing);
        tilePane.setLayoutY(spacing);
        tilePane.setPrefColumns(controlList.size());
        tilePane.setOrientation(orientation);
        tilePane.getChildren().addAll(jfxControlList);
        return tilePane;
    }

}
