package org.sociotech.communitymirror.visualitems.components.spring;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Implementation of a hidden component that can be connected by springs.
 * 
 * @author Peter Lachenmaier
 */
public class HiddenSpringConnectable implements SpringConnectable{

	private DoubleProperty positionX;
	private DoubleProperty positionY;
	private boolean fixed;
	
	/**
	 * Creates a hidden spring connectable component at the given position.
	 * 
	 * @param x Position X
	 * @param y Position Y
	 * @param fixed Whether the position of this component should react on spring changes or not
	 */
	public HiddenSpringConnectable(double x, double y, boolean fixed) {
		positionX = new SimpleDoubleProperty(x);
		positionY = new SimpleDoubleProperty(y);
		this.fixed = fixed;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.springview.components.SpringConnectable#getSpringConnectorXProperty()
	 */
	@Override
	public DoubleProperty getSpringConnectorXProperty() {
		return positionX;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.springview.components.SpringConnectable#getSpringConnectorYProperty()
	 */
	@Override
	public DoubleProperty getSpringConnectorYProperty() {
		return positionY;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.springview.components.SpringConnectable#isFixed()
	 */
	@Override
	public boolean isFixed() {
		return fixed;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable#getWeight()
	 */
	@Override
	public double getWeight() {
		// small weight
		return 0.5;
	}

}
