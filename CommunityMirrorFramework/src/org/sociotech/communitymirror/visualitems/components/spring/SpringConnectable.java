package org.sociotech.communitymirror.visualitems.components.spring;

import javafx.beans.property.DoubleProperty;

/**
 * The interface that needs to be implemented to allow a connection
 * with a {@link VisualSpringConnection}.
 * 
 * @author Peter Lachenmaier
 *
 */
public interface SpringConnectable {

	/**
	 * The X position property to connect the spring to.
	 * 
	 * @return X position property.
	 */
	public DoubleProperty getSpringConnectorXProperty();
	
	/**
	 * The Y position property to connect the spring to.
	 * 
	 * @return Y position property.
	 */
	public DoubleProperty getSpringConnectorYProperty();
		
	/**
	 * Returns whether the position of this component is fixed or not.
	 * 
	 * @return Whether the position of this component is fixed or not.
	 */
	public boolean isFixed();
	
	/**
	 * Returns the weight of this component. Springs pulling less on heavier components.
	 * 
	 * @return The weight of this component.
	 */
	public double getWeight();
}
