package org.sociotech.communitymirror.visualitems.components.map;

import java.net.URL;
import java.util.ResourceBundle;

import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;

// old controller for first mapitem, not used anymore, just available if a new metatag for mapitem will be added

public class MapItemController extends FXMLController implements Initializable {

	@FXML
	ImageView mapImage;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

}
