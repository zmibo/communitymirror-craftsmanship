package org.sociotech.communitymirror.visualitems.components.map;

import javafx.scene.web.WebView;

import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * A sample browser component that shows usage of CommunityMirror interaction handling.
 *
 * @author Peter Lachenmaier
 */
public class BrowserVisualComponent extends VisualComponent {

	/**
	 * The url rendered in the html view.
	 */
	private final String websiteUrl = "https://www.google.de/maps";
	private WebView webView;
	private int zoomLevel = 100;

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {

		webView = new WebView();
		webView.setPrefHeight(350);
		webView.setPrefWidth(350);

		webView.setContextMenuEnabled(false);
		webView.setOpacity(0.8);
		webView.getEngine().load(websiteUrl);

		// add the web view
		this.addNode(webView);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onZoomZoomed(org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent)
	 */
	@Override
	public void onZoomZoomed(ZoomZoomedEvent zoomEvent) {
		zoomLevel *= zoomEvent.getZoomFactor();
		if(zoomLevel < 25) {
			zoomLevel = 25;
		}
		else if(zoomLevel > 200) {
			zoomLevel = 200;
		}
		System.out.println("zoom " + zoomLevel);

		webView.getEngine().executeScript("document.body.style.zoom='" + zoomLevel + "%';");
	}

}
