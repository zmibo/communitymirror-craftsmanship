package org.sociotech.communitymirror.visualitems.components.map;

import java.net.URL;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import org.sociotech.communitymashup.data.DataSet;

import com.google.common.io.Resources;

// Webview that loads the necesarry html file(s) to finally display the map  

public class Browser extends Region {

	private Double positionX;
	private Double positionY;

	final WebView browser = new WebView();
	final WebEngine webEngine = browser.getEngine();

	public Browser(DataSet dataSet, Double positionX, Double positionY) {
		// apply the styles
		getStyleClass().add("browser");
		// load the web page
		URL location = Resources.getResource("map/index.html");

		webEngine.load(location.toString());

		// add the web view to the scene
		getChildren().add(browser);

		this.positionX = positionX;
		this.positionY = positionY;

	}

	@SuppressWarnings("unused")
	private Node createSpacer() {
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		return spacer;
	}

	@Override
	protected void layoutChildren() {
		double w = getWidth();
		double h = getHeight();
		layoutInArea(browser, positionX, positionY, w, h, 0, HPos.CENTER,
				VPos.CENTER);
	}

	@Override
	protected double computePrefWidth(double height) {
		return 1000;
	}

	
	 
	
	@Override
	protected double computePrefHeight(double width) {
		return 800;
	}

}