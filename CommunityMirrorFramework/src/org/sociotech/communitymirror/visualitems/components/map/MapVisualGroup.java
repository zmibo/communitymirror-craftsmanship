
package org.sociotech.communitymirror.visualitems.components.map;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;
import org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.Location;
import org.sociotech.communitymirror.view.View;
import org.sociotech.communitymirror.visualitems.VisualGroup;

// mapvisualgroup contains the mapvisualcomponent which renders the map icon

public class MapVisualGroup extends VisualGroup implements PositionedEventConsumer, ManageInteractionBehaviors,
        HandleDrag, HandleScroll, HandleRotate, HandleZoom, HandleTouch {

    private DataSet dataSet;

    private String listOfMarkerData;

    private View view;

    private MapVisualGroup mapVisualGroup;

    private MapVisualComponent mapComponent;

    public View getView() {
        return this.view;
    }

    private Double positionX;

    private Double positionY;

    public MapVisualGroup getMapVisualGroup() {
        return this.mapVisualGroup;
    }

    public MapVisualGroup(DataSet dataSet, long fadeOutTime, long waitToFadeOut, View view, Double positionX,
            Double positionY) {
        this.dataSet = dataSet;
        this.view = view;
        this.positionX = positionX;
        this.positionY = positionY;

    }

    // creates the javafx webview

    public View createWebView(View view, DataSet dataset, Double PositionX, Double PositionY) {
        Browser browser = new Browser(dataset, PositionX, PositionY);
        view.addNode(browser);

        return view;

    }

    // creates the list as javascript file to enter the gmarker into the map

    public void createList(DataSet dataSet) {

        // List for Locations with longitude and latitude for all persons in the
        // dataset (filtered by regular expressions)
        this.listOfMarkerData = "var markers = [";
        for (int i = 0; i < dataSet.getPersons().size(); i++) {

            List<Location> loc = dataSet.getPersons().get(i).getLocations();

            for (Location l : loc) {

                if (dataSet.getPersons().get(i).getName().matches("[0-9a-zA-Z\\s]*")) {
                    if (l.getLatitude().matches(
                            "(^-?[1][8][0]\\.[0-9]{0,7}[^0-9]*)|(^-?([1][0-7][0-9])\\.[0-9]{0,7}[^0-9]*)|(^-?[0-9]{1,2}\\.[0-9]{0,7}[^0-9]*)")) {
                        if (l.getLongitude()
                                .matches("(^-?[9][0]\\.[0]{0,7}[^0-9]*)|(^-?[0-8]?[0-9]\\.[0-9]{0,7}[^0-9]*)")) {

                            this.listOfMarkerData = this.listOfMarkerData + "['" + dataSet.getPersons().get(i).getName()
                                    + "','" + l.getLatitude() + "','" + l.getLongitude() + "','Person']," + "\n";

                        }
                    }
                }
            }
        }

        // List for Locations with longitude and latitude for all oragnisations
        // in the dataset (filtered by regular expressions)

        for (int i = 0; i < dataSet.getOrganisations().size(); i++) {

            List<Location> loc = dataSet.getOrganisations().get(i).getLocations();

            for (Location l : loc) {

                if (dataSet.getOrganisations().get(i).getName().matches("[0-9a-zA-Z\\s]*")) {
                    if (l.getLatitude().matches(
                            "(^-?[1][8][0]\\.[0]{0,7}[^0-9]*)|(^-?([1][0-7][0-9])\\.[0-9]{0,7}[^0-9]*)|(^-?[0-9]{1,2}\\.[0-9]{0,7}[^0-9]*)")) {
                        if (l.getLongitude()
                                .matches("(^-?[9][0]\\.[0]{0,7}[^0-9]*)|(^-?[0-8]?[0-9]\\.[0-9]{0,7}[^0-9]*)")) {

                            this.listOfMarkerData = this.listOfMarkerData + "['"
                                    + dataSet.getOrganisations().get(i).getName() + "','" + l.getLatitude() + "','"
                                    + l.getLongitude() + "','Unternehmen']," + "\n";
                        }
                    }
                }
            }

        }
        this.listOfMarkerData = this.listOfMarkerData + "];";

        // new created file "markers.js" for use of the gmarker within the
        // webview

        PrintWriter markerData;
        try {
            markerData = new PrintWriter("src/main/resources/map/js/markers.js");
            markerData.println(this.listOfMarkerData);

            markerData.close();

        } catch (FileNotFoundException e) {
            e.getStackTrace();
        }

    }

    @Override
    protected void onInit() {

        super.onInit();

        // new instance of the mapicon

        this.mapComponent = new MapVisualComponent();
        this.mapComponent.init();

        // this.addPhysicsBehavior(FadeOutAndDieBehavior.createFadeOutAndDieBehavior(fadeOutTime,
        // waitToFadeOut,this.getOpacity()));

        // this.addPhysicsBehavior(new SpiralDriftBehavior(0.2, 0.0, 0.0,
        // 0.1, 1.0));

        this.makeTouchable();
        // this.makeDragable();
        this.makeRotatable();
        this.makeScrollable();
        this.makeZoomable();

        this.addVisualComponent(this.mapComponent, true);

    }

    @Override
    public void onTouchTapped(TouchTappedEvent event) {

        // ontouchtapped creates the webview for the mapcomponent
        // getting the coordinates for creating the webview at the clicked
        // position
        this.positionX = event.getSceneX();
        this.positionY = event.getSceneY();

        this.createWebView(this.getView(), this.dataSet, this.positionX, this.positionY);

    }

}
