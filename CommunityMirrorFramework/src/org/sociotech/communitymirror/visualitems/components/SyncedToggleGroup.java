/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.visualitems.components;

import java.util.ArrayList;
import java.util.List;

/**
 * Ensures that all ToggleButtons will be toggled together.
 */
public class SyncedToggleGroup {

    private final List<SyncedToggleButton> m_toggleButtons = new ArrayList<SyncedToggleButton>();

    public SyncedToggleGroup() {

    }

    public synchronized void add(SyncedToggleButton syncedToggleButton) {
        m_toggleButtons.add(syncedToggleButton);
    }

    synchronized List<SyncedToggleButton> getToggleButtons() {
        return m_toggleButtons;
    }

    public void setSelected(boolean selected) {
        for (SyncedToggleButton button : getToggleButtons()) {
            button.setSelected(selected);
        }
    }
}
