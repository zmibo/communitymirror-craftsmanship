package org.sociotech.communitymirror.visualstates;

/**
 * The visual state indication that the visual item is in the detailed visualization. 
 * 
 * @author Peter Lachenmaier
 */
public class Detail extends VisualState {

}
