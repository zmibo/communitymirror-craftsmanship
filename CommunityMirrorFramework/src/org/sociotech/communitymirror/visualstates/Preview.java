package org.sociotech.communitymirror.visualstates;

/**
 * The visual state indication that the visual item is in the information flow. 
 * 
 * @author Peter Lachenmaier
 */
public class Preview extends VisualState {

}
