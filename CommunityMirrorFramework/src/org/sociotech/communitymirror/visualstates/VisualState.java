package org.sociotech.communitymirror.visualstates;

/**
 * The abstract super class of every visual state.
 * 
 * @author Peter Lachenmaier
 */
public abstract class VisualState {
	public static final VisualState MICRO = new Micro();
	public static final VisualState PREVIEW = new Preview();
	public static final VisualState DETAIL = new Detail();
	
	public static final VisualState FOCUSED = new Focused();
	public static final VisualState SPOTLIGHT = new Spotlight();
	
	/**
	 * The default visual state.
	 */
	public static final VisualState DEFAULT = PREVIEW;
	
	/**
	 * Returns whether the current visual state is MICRO
	 * 
	 * @return Whether the current visual state is MICRO
	 */
	public boolean isMicro() {return this instanceof Micro;}
	
	/**
	 * Returns whether the current visual state is PREVIEW
	 * 
	 * @return Whether the current visual state is PREVIEW
	 */
	public boolean isPreview() {return this instanceof Preview;}
	
	/**
	 * Returns whether the current visual state is DETAIL
	 * 
	 * @return Whether the current visual state is DETAIL
	 */
	public boolean isDetail() {return this instanceof Detail;}
	
	/**
	 * Returns whether the current visual state is FOCUSED
	 * 
	 * @return Whether the current visual state is FOCUSED
	 */
	public boolean isFocused() {return this instanceof Focused;}
	
	/**
	 * Returns whether the current visual state is SPOTLIGHT
	 * 
	 * @return Whether the current visual state is SPOTLIGHT
	 */
	public boolean isSpotlight() {return this instanceof Spotlight;}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
