
package org.sociotech.communitymirror.context;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymirror.CommunityMirror;

/**
 * Class for channeling access to different context information that is used in
 * the CM for adapting (personalizing) information. In addition to providing
 * context information the class also provides methods for using the context
 * information to do adaption / selection.
 *
 * @author Michael Koch
 *
 */
public class ContextManager {

    /**
     * A reference to a ContextManager object (if one has been initialized)
     */
    public static ContextManager contextManager;

    /**
     * The location of the CommunityMirror
     */
    protected Location location;

    /**
     * The registered users
     */
    protected Map<String, Person> users;

    /**
     *
     * @param communityMirror
     */
    public ContextManager(CommunityMirror communityMirror) {
        ContextManager.contextManager = this;

        // load context information from configuration
        this.loadFromConfiguration();

        // initalize data structures
        this.users = new HashMap<>();

    }

    protected void loadFromConfiguration() {
        Configuration configuration = CommunityMirror.getConfiguration();
        // location
        String locname = configuration.getString("context.location.name");
        double latitude = configuration.getDouble("context.location.latitude");
        double longitude = configuration.getDouble("context.location.longitude");
        this.location = new Location(locname, latitude, longitude);
    }

    public Location getLocation() {
        return this.location;
    }

    public void registerUser(Person userItem) {
        this.users.put(userItem.getName(), userItem);
    }

    public void unregisterUser(Person userItem) {
        String userName = userItem.getName();
        this.unregisterUser(userName);
    }

    public void unregisterUser(String userName) {
        this.users.remove(userName);
    }

    public Collection<Person> getUsers() {
        return this.users.values();
    }

}
