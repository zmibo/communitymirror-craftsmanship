
package org.sociotech.communitymirror.context;

public class Location {
    private String name;
    private double longitude;
    private double latitude;

    /**
     * create and initialize a point with given name and (latitude, longitude)
     * specified in degrees
     *
     * @param name
     * @param latitude
     * @param longitude
     */
    public Location(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    /**
     * return distance between this location and that location measured in
     * meters
     */
    public double distanceTo(Location that) {
        final double earthRadius = 6371000; // in meters
        final double lat1 = Math.toRadians(this.latitude);
        final double lon1 = Math.toRadians(this.longitude);
        final double lat2 = Math.toRadians(that.latitude);
        final double lon2 = Math.toRadians(that.longitude);
        double dLat = lat2 - lat1;
        double dLng = lon2 - lon1;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        return dist;
    }

    // return string representation of this point
    @Override
    public String toString() {
        return this.name + " (" + this.latitude + ", " + this.longitude + ")";
    }

}
