
package org.sociotech.communitymirror.components.web.mashup;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.io.Resources;

/**
 * Custom URLConnection for CommunityMirror - to be used in JavaFX
 * WebView/WebEngine for loading images.
 *
 * @author kochm
 *
 */

public class CMURLConnection extends URLConnection {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(CMURLConnection.class);

    public CMURLConnection(URL url) {
        super(url);
        // TODO Auto-generated constructor stub
    }

    private byte[] data;

    @Override
    public void connect() throws IOException {
        if (this.connected) {
            return;
        }
        this.loadImage();
        this.connected = true;
    }

    @Override
    public String getHeaderField(String name) {
        if ("Content-Type".equalsIgnoreCase(name)) {
            return this.getContentType();
        } else if ("Content-Length".equalsIgnoreCase(name)) {
            return "" + this.getContentLength();
        }
        return null;
    }

    @Override
    public String getContentType() {
        String fileName = this.getURL().getFile();
        String ext = fileName.substring(fileName.lastIndexOf('.'));
        return "image/" + ext; // TODO: switch based on file-type
    }

    @Override
    public int getContentLength() {
        return this.data.length;
    }

    @Override
    public long getContentLengthLong() {
        return this.data.length;
    }

    @Override
    public boolean getDoInput() {
        return true;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        this.connect();
        return new ByteArrayInputStream(this.data);
    }

    private void loadImage() throws IOException {
        if (this.data != null) {
            return;
        }

        URL url = this.getURL();
        this.logger.info("loading image from custom url: " + url.toExternalForm());

        // TBD - load image from internal sources - currently only a dummy image
        // is loaded
        URL imageUrl = Resources.getResource("themes/flow2/images/icon-blank.png");
        BufferedImage bufferedImage = ImageIO.read(imageUrl);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        baos.flush();
        this.data = baos.toByteArray();

    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        // this might be unnecessary - the whole method can probably be omitted
        // for our purposes
        return new ByteArrayOutputStream();
    }

    @Override
    public java.security.Permission getPermission() throws IOException {
        return null; // we need no permissions to access this URL
    }

}
