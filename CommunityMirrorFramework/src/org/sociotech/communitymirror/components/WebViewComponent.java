
package org.sociotech.communitymirror.components;

import java.net.URL;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;

/**
 * VisualComponent showing a WebView
 *
 * @author Michael Koch
 *
 */

public class WebViewComponent extends VisualComponent {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(WebViewComponent.class);

    private WebView webView;

    private String action;

    public WebViewComponent() {
        super();
        // initializing the WebView will be done in configure()
    }

    /**
     * Configure visual component using the given configuration and
     * configuration prefix. This method is called after the component is
     * created - and before it is inserted in the scene.
     *
     * @author Michael Koch
     */
    @Override
    public void configure(Configuration configuration, String confPrefix) {
        String urlString = configuration.getString(confPrefix + ".url");
        int width = 300;
        int height = 350;
        try {
            String tmps = configuration.getString(confPrefix + ".width");
            width = Integer.parseInt(tmps);
            tmps = configuration.getString(confPrefix + ".height");
            height = Integer.parseInt(tmps);
        } catch (Exception e) {}
        this.action = configuration.getString(confPrefix + ".action");
        this.configure(urlString, width, height);
    }

    public void configure(String urlString, int width, int height) {
        this.configure(urlString, width, height, null);
    }

    public void configure(String urlString, int width, int height, Image image) {
        try {
            URL url = this.getClass().getClassLoader().getResource(urlString);
            if (url == null && urlString.startsWith("http")) {
                url = new URL(urlString);
            }
            if (url == null) {
                this.logger.warn("Could not open url " + urlString + " in WebView");
                return;
            }

            this.webView = new WebView();
            this.webView.getEngine().load(url.toString());
            this.webView.setMaxSize(width, height);
            this.getChildren().add(this.webView);

            if (image != null) {
                ImageView imageView = new ImageView(image);
                imageView.setPreserveRatio(true);
                imageView.setFitWidth(50);
                imageView.setFitHeight(50);
                imageView.setLayoutX(-30);
                imageView.setLayoutY(-30);
                this.getChildren().add(imageView);
            }

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Close WebView ...
     */
    @Override
    public void onTouchPressed(TouchPressedEvent event) {
        if (this.action == null || this.action.startsWith("close")) {
            // if touch was near to the right border, it might have been an
            // interaction with the scroll bar ... so ignore it
            if (event.getSceneX() > this.webView.getLocalToSceneTransform().transform(this.webView.getWidth(), 0).getX()
                    - 15) {
                return;
            }

            this.webView.getEngine().load(null);
            this.destroy();
        }
    }

}
