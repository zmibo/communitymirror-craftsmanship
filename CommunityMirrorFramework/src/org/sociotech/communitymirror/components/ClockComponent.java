
package org.sociotech.communitymirror.components;

import java.util.Calendar;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communitymirror.visualitems.VisualComponent;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 * VisualComponent showing a simple digital clock
 *
 * @author Michael Koch
 *
 */

public class ClockComponent extends VisualComponent {

    public ClockComponent() {
        super();
    }

    String action = null;

    /**
     * Configure visual component using the given configuration and
     * configuration prefix. This method is called after the component is
     * created - and before it is inserted in the scene.
     *
     * @author Michael Koch
     */
    @Override
    public void configure(Configuration configuration, String confPrefix) {

        DigitalClock clock = new DigitalClock();
        clock.setTextFill(Color.WHITE);
        clock.setFont(Font.font("Arial", 30));
        this.getChildren().add(clock);

    }

    /**
     * The digital clock as a separate private class extending the JavaFX Label
     * class
     *
     * @author kochm
     *
     */
    class DigitalClock extends Label {
        public DigitalClock() {
            this.bindToTime();
        }

        // the digital clock updates once a second.
        private void bindToTime() {
            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    Calendar time = Calendar.getInstance();
                    String hourString = ClockComponent.pad(2, '0', time.get(Calendar.HOUR_OF_DAY) + "");
                    String minuteString = ClockComponent.pad(2, '0', time.get(Calendar.MINUTE) + "");
                    String secondString = ClockComponent.pad(2, '0', time.get(Calendar.SECOND) + "");
                    DigitalClock.this.setText(hourString + ":" + minuteString + ":" + secondString);
                }
            }), new KeyFrame(Duration.seconds(1)));
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
        }
    }

    public static String pad(int fieldWidth, char padChar, String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length(); i < fieldWidth; i++) {
            sb.append(padChar);
        }
        sb.append(s);

        return sb.toString();
    }

}
