
package org.sociotech.communitymirror.components.bus;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.components.bus.schedulereaders.BusScheduleReader;
import org.sociotech.communitymirror.view.flow.components.VisualComponentsPile;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 * Class for a bus departure component
 *
 * @author Julian Fietkau
 *
 */
public class BusComponent<I extends Item, V extends FXMLController> extends VisualComponent {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(VisualComponentsPile.class.getName());

    private String departureBusStop;
    private BusScheduleReader busSchedule;
    private int secondsBetweenUpdates = 10;
    private long lastUpdated;

    private int numberOfDeparturesToShow;

    public BusController controller;

    /**
     * Creates a new Bus departures component
     */
    public BusComponent() {
        this.departureBusStop = "Musterstraße";

        this.busSchedule = null;

        this.numberOfDeparturesToShow = 3;

        this.lastUpdated = 0;
    }

    @Override
    public void setConfiguration(Configuration configuration) {
        super.setConfiguration(configuration);

        if (configuration != null) {
            if (configuration.getString("components.bus.departurebusstop") != null) {
                this.departureBusStop = configuration.getString("components.bus.departurebusstop");
                this.controller.setHeader("N\u00E4chste Busse ab " + this.departureBusStop + ":");
            }
            if (configuration.getString("components.bus.schedulereader") != null) {
                String scheduleReaderClassName = configuration.getString("components.bus.schedulereader");
                try {
                    if (!scheduleReaderClassName.contains(".")) {
                        scheduleReaderClassName = "org.sociotech.communitymirror.components.bus.schedulereaders."
                                + scheduleReaderClassName;
                    }
                    this.busSchedule = (BusScheduleReader) Class.forName(scheduleReaderClassName)
                            .getConstructor(String.class).newInstance(this.departureBusStop);
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                        | InvocationTargetException | NoSuchMethodException | SecurityException
                        | ClassNotFoundException e) {
                    this.logger.error(
                            "Invalid class name given for bus schedule reader, no departure times will be displayed.");
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualGroup#onUpdate()
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);

        if (this.controller != null) {
            if (this.lastUpdated < System.currentTimeMillis() - this.secondsBetweenUpdates * 1000
                    && this.busSchedule != null) {
                this.busSchedule.update();
                this.controller.showDepartures(
                        this.busSchedule.getUpcomingDepartures(this.departureBusStop, this.numberOfDeparturesToShow));
                this.lastUpdated = System.currentTimeMillis();
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();

        AnchorPane pane;
        FXMLLoader loader = new FXMLLoader();
        try {
            URL location;
            String fxmlPath = "fxml/bus.fxml";
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            loader.setLocation(location);
            pane = (AnchorPane) loader.load();

            this.controller = loader.getController();

            this.controller.setThemeResources(this.themeResources);

            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = pane.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(pane.getChildren().get(0));
            }

        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for Bus component. Reason:");
            this.logger.error("    " + e.getMessage());
            for (int i = 0; i < 10 && i < e.getStackTrace().length; i++) {
                this.logger.error("    " + e.getStackTrace()[i]);
            }
        }
    }

}
