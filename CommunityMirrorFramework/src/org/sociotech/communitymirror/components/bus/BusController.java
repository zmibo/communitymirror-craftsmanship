
package org.sociotech.communitymirror.components.bus;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.configuration.ThemeResourceLoader;
import org.sociotech.communitymirror.view.flow.components.VisualComponentsPile;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Controller class for bus departure component
 *
 * @author Julian Fietkau
 *
 */
public class BusController extends FXMLController implements Initializable {

    @FXML
    Text header;

    @FXML
    ImageView busPictogram;

    @FXML
    Rectangle backgroundRect;

    @FXML
    VBox busDepartures;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(VisualComponentsPile.class.getName());

    private ThemeResourceLoader themeResources;

    public void setThemeResources(ThemeResourceLoader themeResources) {
        this.themeResources = themeResources;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setHeader(String text) {
        if (this.header != null) {
            this.header.setText(text);
            this.header.setWrappingWidth(this.busPictogram.getFitWidth());
        }
    }

    private String formatDepartureTime(OffsetDateTime departureTime) {
        String result;
        Duration until = Duration.between(OffsetDateTime.now(), departureTime);
        if (until.getSeconds() < 1 * 60) {
            result = "sofort";
        } else if (until.getSeconds() < 2 * 60) {
            result = "in 1 Minute";
        } else if (until.getSeconds() < 15 * 60) {
            result = "in " + (int) Math.floor(until.getSeconds() / 60) + " Minuten";
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
            result = departureTime.format(formatter);
        }
        return result;
    }

    private Pane createNewEntry(BusDeparture busDeparture) {
        AnchorPane pane = null;
        BusEntryController entryController = null;
        FXMLLoader loader = new FXMLLoader();
        try {
            URL location;
            String fxmlPath = "fxml/busEntry.fxml";
            if (this.themeResources == null) {
                throw new IOException(this.getClass().getSimpleName() + " requires a ThemeResourceLoader.");
            }
            location = this.themeResources.getResourceURL(fxmlPath);
            loader.setLocation(location);
            pane = (AnchorPane) loader.load();

            entryController = loader.getController();

            entryController.setLineNumber(busDeparture.getBusLine());
            entryController.setLineDestination(busDeparture.getLineDestination());
            entryController.setDepartureTime(this.formatDepartureTime(busDeparture.getPlannedDeparture()));
        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for Bus component. Reason:");
            this.logger.error("    " + e.getMessage());
            for (int i = 0; i < 10 && i < e.getStackTrace().length; i++) {
                this.logger.error("    " + e.getStackTrace()[i]);
            }
        }

        entryController.hashCode();

        return pane;
    }

    public void showDepartures(List<BusDeparture> departures) {
        this.busDepartures.getChildren().clear();
        for (BusDeparture departure : departures) {
            Pane newEntry = this.createNewEntry(departure);
            this.busDepartures.getChildren().add(newEntry);
        }
    }

    public double getWidth() {
        return this.backgroundRect.getWidth();
    }

    public double getHeight() {
        return this.backgroundRect.getHeight();
    }
}
