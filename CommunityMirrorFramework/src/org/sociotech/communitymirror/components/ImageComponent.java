
package org.sociotech.communitymirror.components;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.view.View;
import org.sociotech.communitymirror.view.flow.FlowView;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * VisualComponent showing an image - e.g. for displaying logos in the front
 * layer
 *
 * @author Michael Koch
 *
 */

public class ImageComponent extends VisualComponent {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(ImageComponent.class);

    private Image image;

    public ImageComponent() {
        super();
        // loading the image will be done in configure()
    }

    String action = null;

    /**
     * Configure visual component using the given configuration and
     * configuration prefix. This method is called after the component is
     * created - and before it is inserted in the scene.
     *
     * @author Michael Koch
     */
    @Override
    public void configure(Configuration configuration, String confPrefix) {

        // display a simple ImageView
        String filename = configuration.getString(confPrefix + ".filename");
        if (filename == null) {
            this.logger.warn("No filename found for ImageComponent - " + confPrefix + ".filename");
            return;
        }
        try {
            URL url = this.getClass().getClassLoader().getResource(filename);
            InputStream input = url.openStream();
            this.image = new Image(input);

            // Setting the image view
            ImageView imageView = new ImageView(this.image);

            // Setting the preserve ratio of the image view
            imageView.setPreserveRatio(true);

            this.getChildren().add(imageView);

            this.action = configuration.getString(confPrefix + ".action");

        } catch (Exception e) {
            this.logger.error("Failed loading file '" + filename + "' for ImageComponent", e);
        }
    }

    /**
     * Configure visual component using a given Image object.
     * This method is called after the component is
     * created - and before it is inserted in the scene.
     *
     * @author Michael Koch
     */
    public void configure(Image image, int width, int height, String labelText, String action, Image categoryIcon) {

        this.image = image;

        // display a simple ImageView
        // Setting the image view
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(width);
        imageView.setFitHeight(height - 20.0);

        this.getChildren().add(imageView);

        if (labelText != null) {
            Label label = new Label(labelText);
            label.setLayoutY(height - 10.0);
            label.setFont(Font.font("Arial", 12));
            label.setTextFill(Color.WHITE);
            this.getChildren().add(label);
        }
        if (categoryIcon != null) {
            ImageView categoryView = new ImageView(categoryIcon);
            categoryView.setLayoutX(width - 30.0);
            categoryView.setLayoutY(height - 50.0);
            categoryView.setPreserveRatio(true);
            categoryView.setFitHeight(40);
            categoryView.setFitWidth(40);
            this.getChildren().add(categoryView);
        }

        this.action = action;
    }

    @Override
    public void onTouchPressed(TouchPressedEvent event) {
        super.onTouchPressed(event);

        if (!this.isVisible()) {
            return;
        }

        // if there is an action defined, then perform it ...
        if (this.action != null && this.action.length() > 0) {
            if (this.action.startsWith("showWebView(")) {
                String urlString = this.action.substring(12, this.action.length() - 1);
                int pos = urlString.indexOf(",");
                int width = 200;
                int height = 200;
                if (pos > 0) {
                    String tmps = urlString.substring(pos + 1);
                    urlString = urlString.substring(0, pos);
                    pos = tmps.indexOf(",");
                    if (pos > 0) {
                        try {
                            width = Integer.parseInt(tmps.substring(0, pos));
                            height = Integer.parseInt(tmps.substring(pos + 1));
                        } catch (Exception e) {}
                    }
                }
                WebViewComponent webView = new WebViewComponent();
                webView.configure(urlString, width, height, this.image);

                // calculate x, y position for new component
                Point2D posXY = ImageComponent.calculatePosition(this, webView);
                webView.setPositionX(posXY.getX());
                webView.setPositionY(posXY.getY());

                webView.makeTouchable();

                this.getParentVisualGroup().addVisualComponent(webView, true);
            }
            if (this.action.startsWith("closeGroupAndAddItem(")) {

                VisualGroup visualGroup = this.getParentVisualGroup();
                visualGroup.setVisible(false);
                List<VisualComponent> visualComponents = visualGroup.getVisualComponents();
                for (VisualComponent visualComponent : visualComponents) {
                    visualComponent.setVisible(false);
                    // visualGroup.removeVisualComponent(visualComponent);
                }

                // add a VisualItem for the Item referenced in brackets
                View view = this.getParentView();
                if (view != null && view instanceof FlowView) {
                    int pos = this.action.indexOf("(");
                    if (pos > 0) {
                        int pos2 = this.action.indexOf(")");
                        String ident = this.action.substring(pos + 1, pos2);
                        ((FlowView) view).addVisualItem(ident, this.getPositionX(), this.getPositionY());
                    }
                }
            }
            if (this.action.equals("close")) {

                this.getChildren().clear();

            }
            if (this.action.equals("closeGroup")) {

                VisualGroup visualGroup = this.getParentVisualGroup();
                visualGroup.setVisible(false);
                /*
                 * List<VisualComponent> visualComponents =
                 * visualGroup.getVisualComponents();
                 * for (VisualComponent visualComponent : visualComponents) {
                 * visualGroup.removeVisualComponent(visualComponent);
                 * }
                 */
            }
            if (this.action.startsWith("showItemPanel(")) {

                int pos = this.action.indexOf("(");
                if (pos > 0) {
                    View view = this.getParentView();
                    int pos2 = this.action.indexOf(")");
                    String metatag = this.action.substring(pos + 1, pos2);
                    ((FlowView) view).showItemPanel(metatag);
                }

            }

        }
    }

    /**
     * Calculate where a new component should be placed (if it should be
     * centered on an old component but stay inside the screen bounds
     */
    public static Point2D calculatePosition(VisualComponent oldC, VisualComponent newC) {
        // calculate x, y position for new component
        // component should be centered above old component
        double posX = oldC.getPositionX();
        double posY = oldC.getPositionY();
        Bounds oldBounds = oldC.getLayoutBounds();
        Bounds newBounds = newC.getLayoutBounds();
        double deltaX = (oldBounds.getWidth() - newBounds.getWidth()) / 2;
        double deltaY = (oldBounds.getHeight() - newBounds.getHeight()) / 2;
        posX += deltaX;
        posY += deltaY;
        // full component should be visible
        if (posX < 0) {
            posX = 0;
        }
        if (posY < 0) {
            posY = 0;
        }
        Rectangle2D screenBounds = null;
        Screen usedScreen = null;
        try {
            int screenIndex = CommunityMirror.getConfiguration().getInt("mirror.screen", 0);
            usedScreen = Screen.getScreens().get(screenIndex);
        } catch (Exception e) {
            // set screen to primary screen
            usedScreen = Screen.getPrimary();
        }
        screenBounds = usedScreen.getBounds();
        if (posX + newBounds.getWidth() > screenBounds.getWidth()) {
            posX = screenBounds.getWidth() - newBounds.getWidth();
        }
        if (posY + newBounds.getHeight() > screenBounds.getHeight()) {
            posY = screenBounds.getHeight() - newBounds.getHeight();
        }
        return new Point2D(posX, posY);
    }

}
