/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.utils;

import javafx.geometry.Point2D;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Generates random values or value sets.
 *
 * @author Martin Burkhard
 */
public final class RandomFactory {

    private static Random s_random;

    private RandomFactory() {
    }

    public static boolean getRandomBoolean() {
        Random rand = getRandomInstance();
        return rand.nextBoolean();
    }

    public static int getRandomInt(int n) {
        Random rand = getRandomInstance();
        return rand.nextInt(n);
    }

    public static double getRandomDouble() {
        Random rand = getRandomInstance();
        return rand.nextDouble();
    }

    public static Point2D getRandomPosition(double width, double height) {

        return new Point2D(getRandomDouble() * width, getRandomDouble() * height);
    }

    /**
     * Uses SecureRandom to shuffle collection.
     *
     * @param list Collection to be shuffled
     */
    public static void shuffle(List<?> list) {
        Collections.shuffle(list, getRandomInstance());
    }

    private static Random getRandomInstance() {

        // Generate new random instance
        if(s_random == null) {
            try {
                // Generate new random number using Secure Random
                s_random = SecureRandom.getInstance("SHA1PRNG");

            } catch (NoSuchAlgorithmException e) {
                // Generate new random number using Default Random
                s_random = new Random();
                s_random.setSeed(Calendar.getInstance().getTimeInMillis());
            }
        }

        // Return random instance
        return s_random;
    }
}
