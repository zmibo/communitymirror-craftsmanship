package org.sociotech.communitymirror.controllers.user;

import java.util.List;

import org.sociotech.communitymirror.controllers.Controller;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyCondition;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyOrientation;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyPosition;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneFactory;
import org.sociotech.communitymirror.visualitems.user.userState.UserState;

/**
 * A visual component controller that controlles a list of users
 *
 * @author evalosch
 *
 */
public class UserController extends Controller {

	/**
	 * The list of users that is controlled by this user controller.
	 */
	private List<User> users;

	// private long timeUntilNewSkeletonFrameEventIsExpected = 500;
	// private long timeOfLastSkeletonFrameEvent = -1;

	/**
	 * Creates a new user controller controlling a given list of users
	 *
	 * @param users
	 *            The list of users that is controlled by this user
	 *            representation controller
	 */
	public UserController(List<User> users, FXView view) {
		this.users = users;
	}

	/**
	 * @return the users
	 */
	public List<User> getUsers() {
		return users;
	}

	/**
	 * This method is called by each UserImageCreationTask that is created
	 * whenever a new image from the kinect is received within KinectProcessing.
	 *
	 * @param userId
	 * @param isTracked
	 * @param position
	 * @param orientation
	 * @param userImage
	 */
	public void updateUser(int userId, boolean isTracked, float[] position1, float[] position2,
			float[] orientation, float[] positionInUserImage, float[] position2InUserImage) {

		// get user
		User user = this.getUsers().get(userId);
		// update tracking status
		user.setIsTracked(isTracked);
		if (isTracked) {
			// update position and orientation
			this.updateBodyCondition(user, position1, position2, orientation, positionInUserImage, position2InUserImage);
			// update the interaction zone the user is interaction zone
			this.updateInteractionZone(user);
		}
		// at last: update user state
		this.updateUserState(user);
	}

	private void updateInteractionZone(User user) {
		double posZ = user.getBodyPosition().getPosZ1();
		if (!user.getCurrentInteractionZone().containsPosition(posZ)) {
			user.setCurrentInteractionZone(InteractionZoneFactory
					.createInteractionZone(posZ));
		}
	}

	private void updateBodyCondition(User user, float[] position1, float[] position2,
			float[] orientation, float[] positionInUserImage, float[] position2InUserImage) {
		BodyCondition bodyCondition = user.getBodyCondition();
		if (bodyCondition == null) {
			bodyCondition = new BodyCondition(new BodyOrientation(orientation),
					new BodyPosition(position1, position2, positionInUserImage, position2InUserImage));
			user.setBodyCondition(bodyCondition);
		} else {
			bodyCondition.updateBodyPosition(position1, position2, positionInUserImage, position2InUserImage);
			bodyCondition.updateBodyOrientation(orientation);
		}

		// BodyPosition bodyPosition = user.getBodyPosition();
		// System.out.println("body position x:" + bodyPosition.getPosX()
		// + "; y:" + bodyPosition.getPosY()
		// + "; z:" + bodyPosition.getPosZ());

	}

	/**
	 * Updates a users' user state: Sets the user state of a given user to the
	 * reached state out of the list of reachable states if a new state has been
	 * reached.
	 *
	 * @param user
	 *            The user of whom the state has to been updated.
	 */
	private void updateUserState(User user) {
		UserState reachedUserState = user.getReachedUserState();
		if (reachedUserState != null) {
			user.setState(reachedUserState);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.sociotech.communitymirror.controllers.VisualComponentController#update
	 * ()
	 */
	@Override
	public void update() {
		// checkInComingSkeletonFrameRate();
	}

	// private void checkInComingSkeletonFrameRate() {
	// if (this.timeOfLastSkeletonFrameEvent > -1) {
	// if (System.currentTimeMillis() - this.timeOfLastSkeletonFrameEvent >
	// this.timeUntilNewSkeletonFrameEventIsExpected) {
	// setAllUsersToNotTracked();
	// }
	// }
	// }
	//
	// private void setAllUsersToNotTracked() {
	// for (User user : this.getUsers()) {
	// if (user.isTracked()) {
	// user.setIsTracked(false);
	// }
	// }
	// }

}
