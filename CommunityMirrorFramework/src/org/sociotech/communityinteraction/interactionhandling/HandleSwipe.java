package org.sociotech.communityinteraction.interactionhandling;

/**
 * Interface that must be implemented to handle swipes.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleSwipe extends HandleInteraction {

}
