package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.kinect.ColorFrameEvent;
import org.sociotech.communityinteraction.events.kinect.DepthFrameEvent;
import org.sociotech.communityinteraction.events.kinect.SkeletonFrameEvent;

/**
 * Interface that must be implemented to handle kinect frames.
 * 
 * @author evalosch
 *
 */
public interface HandleKinectFrame extends HandleSensing{

	/**
	 * Will be called if a skeleton frame is received from the kinect.
	 * 
	 * @param event Skeleton frame event that has to be handled.
	 */
	public void onSkeletonFrame(SkeletonFrameEvent event);

	/**
	 * Will be called if a color frame will be received from the kinect.
	 * 
	 * @param event Color frame event that has to be handled.
	 */
	public void onColorFrame(ColorFrameEvent event);

	/**
	 * Will be called if a depth frame is received by the kinect.
	 * 
	 * @param event Depth frame event that has to be handled.
	 */
	public void onDepthFrame(DepthFrameEvent event);
}
