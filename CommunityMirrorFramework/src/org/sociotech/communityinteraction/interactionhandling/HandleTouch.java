package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;

/**
 * Interface that must be implemented to handle touch interaction.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleTouch extends HandleInteraction {

	/**
	 * Will be called when a touch is pressed on this component.
	 * 
	 * @param event Touch pressed event.
	 */
	public void onTouchPressed(TouchPressedEvent event);
	
	/**
	 * Will be called when a touch is released on this component.
	 * 
	 * @param event Touch released event.
	 */
	public void onTouchReleased(TouchReleasedEvent event);
	
	/**
	 * Will be called when a touch is moved on this component.
	 * 
	 * @param event Touch moved event.
	 */
	public void onTouchMoved(TouchMovedEvent event);
	
	/**
	 * Will be called when a tap is executed on this component.
	 * 
	 * @param event Touch tapped event.
	 */
	public void onTouchTapped(TouchTappedEvent event);
	
	/**
	 * Will be called when a hold is executed on this component.
	 * 
	 * @param event Touch hold event.
	 */
	public void onTouchHold(TouchHoldEvent event);
}
