package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;

/**
 * Interface that must be implemented to handle zoom.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleZoom extends HandleInteraction {

	/**
	 * Will be called when a zoom is started on this component.
	 * 
	 * @param zoomEvent Zoom started event.
	 */
	public void onZoomStarted(ZoomStartedEvent zoomEvent);

	/**
	 * Will be called when a zoom is performed on this component.
	 * 
	 * @param zoomEvent Zoom zoomed event.
	 */
	public void onZoomZoomed(ZoomZoomedEvent zoomEvent);

	/**
	 * Will be called when a zoom is finished on this component.
	 * 
	 * @param zoomEvent Zoom finished event.
	 */
	public void onZoomFinished(ZoomFinishedEvent zoomEvent);

}
