package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.communication.MessageReceivedEvent;

/**
 * Interface that must be implemented by components that handle communication events.
 * 
 * @author Eva Lösch
 *
 */
public interface HandleCommunication extends HandleInteraction {

	/**
	 * Will be called if a message received event occurs.
	 * 
	 * @param event
	 *            The message received event to handle.
	 */
	public void onMessageReceived(MessageReceivedEvent event);

}
