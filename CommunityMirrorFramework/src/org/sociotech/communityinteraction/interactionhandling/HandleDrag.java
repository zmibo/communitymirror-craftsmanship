package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;

/**
 * Interface that must be implemented to handle drags.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleDrag extends HandleInteraction {
	/**
	 * Will be called when a drag is started on this component.
	 * 
	 * @param dragEvent Drag started event.
	 */
	public void onDragStarted(DragStartedEvent dragEvent);

	/**
	 * Will be called when a drag is performed on this component.
	 * 
	 * @param dragEvent Drag performed event.
	 */
	public void onDragDragged(DragDraggedEvent dragEvent);

	/**
	 * Will be called when a drag is finished on this component.
	 * 
	 * @param dragEvent Drag finished event.
	 */
	public void onDragFinished(DragFinishedEvent dragEvent);

}
