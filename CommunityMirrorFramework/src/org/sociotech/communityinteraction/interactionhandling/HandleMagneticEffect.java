package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionStartedEvent;

/**
 * Interface that must be implemented by components that handle magnetic effect
 * events.
 * 
 * @author evalosch
 *
 */
public interface HandleMagneticEffect extends HandleInteraction {

	/**
	 * Will be called if a magnetic attraction started event occurs.
	 * 
	 * @param event
	 *            The magnetic attraction started event to handle.
	 */
	public void onMagneticAttractionStarted(MagneticAttractionStartedEvent event);

	/**
	 * Will be called if a magnetic attraction finished event occurs.
	 * 
	 * @param event
	 *            The magnetic attraction finished event to handle.
	 */
	public void onMagneticAttractionFinished(
			MagneticAttractionFinishedEvent event);

	/**
	 * Will be called if a magnetic repulsion started event occurs.
	 * 
	 * @param event
	 *            The magnetic repulsion started event to handle.
	 */
	public void onMagneticRepulsionStarted(MagneticRepulsionStartedEvent event);

	/**
	 * Will be called if a magnetic repulsion finished event occurs.
	 * 
	 * @param event
	 *            The magnetic repulsion finished event to handle.
	 */
	public void onMagneticRepulsionFinished(MagneticRepulsionFinishedEvent event);

}
