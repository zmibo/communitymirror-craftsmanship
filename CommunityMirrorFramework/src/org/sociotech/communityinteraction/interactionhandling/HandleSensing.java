package org.sociotech.communityinteraction.interactionhandling;

/**
 * Super interface for handling all kinds of sensing events.
 * 
 * @author evalosch
 *
 */
public interface HandleSensing {

}
