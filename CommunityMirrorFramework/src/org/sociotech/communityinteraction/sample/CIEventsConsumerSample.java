package org.sociotech.communityinteraction.sample;

import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.sociotech.communityinteraction.behaviorfactories.DirectBehaviorFactory;
import org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory;
import org.sociotech.communityinteraction.eventcreators.CIEventCreator;
import org.sociotech.communityinteraction.eventcreators.GWEventCreator;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.visualizer.CIEventVisualizer;

/**
 * Sample that shows how ci events are generated and handled. The UI consists of
 * two shapes and a log.
 */
public class CIEventsConsumerSample extends Application implements CIEventConsumer {

	private ObservableList<String> events = FXCollections.observableArrayList();
	private InteractionBehaviorFactory behaviorFactory;
	private CIEventVisualizer interactionVisualizer;
	private CIEventCreator eventCreator;
	private CIEventsConsumerComponent comp1;
	private CIEventsConsumerComponent comp2;

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {

		Group visualizationGroup = new Group();
		AnchorPane root = new AnchorPane();
		
		String windowTitle = "CommunityInteraction Sample";
		
		primaryStage.setTitle(windowTitle);
		primaryStage.setFullScreen(true);
		
		double width  = primaryStage.getWidth();
		double height = primaryStage.getHeight();
		
		root.getChildren().add(visualizationGroup);
		
		Scene scene = new Scene(root, width, height);
		
		// create components
		comp1 = new InteractableRectangle();
		comp1.setLayoutX(100);
		comp1.setLayoutY(100);
		
		comp2 = new InteractableRectangle();
		comp2.setLayoutX(300);
		comp2.setLayoutY(300);
		
		// Create the log that shows events
		ListView<String> log = createLog(events);
		AnchorPane.setBottomAnchor(log, 5.0);
		AnchorPane.setLeftAnchor(log, 5.0);
		AnchorPane.setRightAnchor(log, 5.0);

		root.getChildren().addAll(comp1, comp2, log);
		
		primaryStage.setTitle(windowTitle);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		// create factory for interaction behaviors
		//behaviorFactory = new MouseToTouchBehaviorFactory();
		behaviorFactory = new DirectBehaviorFactory();

		// create event creator
		//eventCreator = new FXEventCreator(scene);
		eventCreator = new GWEventCreator(scene, windowTitle, "resources/basic_manipulation.gml", "C:\\Program Files (x86)\\Ideum\\GestureworksCore\\GestureworksCore32.dll");

		// create interaction visualizer
		interactionVisualizer = new CIEventVisualizer(behaviorFactory, visualizationGroup);
		
		// register visualizer for interaction events at creator
		eventCreator.registerEventConsumer(interactionVisualizer);
		// register this also as event consumer
		eventCreator.registerEventConsumer(this);
		
		// add interaction behaviors to components
		comp1.addInteractionBehavior(behaviorFactory.createRotateBehavior(comp1));
		comp1.addInteractionBehavior(behaviorFactory.createZoomBehavior(comp1));
		comp1.addInteractionBehavior(behaviorFactory.createDragBehavior(comp1));
		
		comp2.addInteractionBehavior(behaviorFactory.createRotateBehavior(comp2));
		comp2.addInteractionBehavior(behaviorFactory.createZoomBehavior(comp2));
		comp2.addInteractionBehavior(behaviorFactory.createDragBehavior(comp2));
		
		// register components as event consumers
		eventCreator.registerEventConsumer(comp1);
		eventCreator.registerEventConsumer(comp2);
	}

	/**
	 * Creates a log that shows the events.
	 */
	private ListView<String> createLog(ObservableList<String> messages) {
		final ListView<String> log = new ListView<String>();
		log.setPrefSize(500, 200);
		log.setItems(messages);

		return log;
	}

	/**
	 * Adds a message to the log.
	 * 
	 * @param message
	 *            Message to be logged
	 */
	private void log(String message) {
		// Limit log to 50 entries, delete from bottom and add to top
		if (events.size() == 50) {
			events.remove(49);
		}
		events.add(0, message);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		List<CIEventConsumer> possibleConsumers = CIListHelper.createConsumerList();
		
		// add this
		possibleConsumers.add(this);
		
		// if it is a position event then try to find the matching 
		
		return possibleConsumers;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		// simply log
		log(event.toString());
	}
}
