package org.sociotech.communityinteraction.sample;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Represents an interactable rectangle.
 * 
 * @author Peter Lachenmaier
 */
public class InteractableRectangle extends CIEventsConsumerComponent {
	private final Rectangle rectangle;
	
	/**
	 * Constructs the interactable rectangle
	 */
	public InteractableRectangle() {
		rectangle = new Rectangle();
		
		// add rectangle to this group
		this.getChildren().add(rectangle);
		
		// set some basic attributes
		rectangle.setWidth(100);
		rectangle.setHeight(100);
		rectangle.setFill(Color.DARKBLUE);
	}

	/**
	 * Returns the rectangle node for manipulation
	 * 
	 * @return The rectangle node.
	 */
	public Rectangle getRectangle() {
		return rectangle;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.PositionedEventConsumer#isTarget(java.lang.Object)
	 */
	@Override
	public boolean isTarget(Object target) {
		if(target == rectangle) {
			// allow interactions on rectangle
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.PositionedEventConsumer#getZIndex()
	 */
	@Override
	public int getZIndex() {
		return 0;
	}
}
