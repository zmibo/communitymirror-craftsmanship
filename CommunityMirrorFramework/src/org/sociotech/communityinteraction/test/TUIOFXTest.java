
package org.sociotech.communityinteraction.test;

import org.tuiofx.Configuration;
import org.tuiofx.TuioFX;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.ZoomEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * Class for testing TUIOFX features - without the overhead of
 * CommunityInteractionFramework and CommunityMirrorFramework
 *
 * For information on TUIOFX see http://tuiofx.org or
 * https://github.com/TUIOFX/tuiofx
 *
 * TUIOFX needs
 * - TUIO 1.1. Java API
 * - Simple Logging Facade for Java (SLF4J)
 * - Guava: Google Core Libraries for Java
 *
 * @author kochm
 *
 */

public class TUIOFXTest extends Application {

    public static void main(String[] args) {
        TuioFX.enableJavaFXTouchProperties();
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        final Pane root = new Pane();
        final Scene scene = new Scene(root, 1920, 1080);

        final Label label1 = new Label("Hello TUIOFX");
        label1.setFont(Font.font("Helvetica", FontWeight.BLACK, 90));
        label1.setTextFill(Color.DARKRED);

        label1.setOnZoom(new EventHandler<ZoomEvent>() {
            @Override
            public void handle(ZoomEvent event) {
                double scaleX = label1.getScaleX() * event.getZoomFactor();
                double scaleY = label1.getScaleY() * event.getZoomFactor();
                label1.setScaleX(scaleX);
                label1.setScaleY(scaleY);
            }
        });
        label1.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                label1.setTranslateX(label1.getTranslateX() + event.getDeltaX());
                label1.setTranslateY(label1.getTranslateY() + event.getDeltaY());
            }
        });
        label1.setOnRotate(new EventHandler<RotateEvent>() {
            @Override
            public void handle(RotateEvent event) {
                label1.setRotate(label1.getRotate() + event.getAngle());
            }
        });

        final Label label2 = new Label("Hello TUIOFX");
        label2.setFont(Font.font("Helvetica", FontWeight.BLACK, 75));
        label2.setTextFill(Color.GREEN);

        label2.setOnZoom(new EventHandler<ZoomEvent>() {
            @Override
            public void handle(ZoomEvent event) {
                double scaleX = label2.getScaleX() * event.getZoomFactor();
                double scaleY = label2.getScaleY() * event.getZoomFactor();
                label2.setScaleX(scaleX);
                label2.setScaleY(scaleY);
            }
        });
        label2.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                label2.setTranslateX(label2.getTranslateX() + event.getDeltaX());
                label2.setTranslateY(label2.getTranslateY() + event.getDeltaY());
            }
        });
        label2.setOnRotate(new EventHandler<RotateEvent>() {
            @Override
            public void handle(RotateEvent event) {
                label2.setRotate(label1.getRotate() + event.getAngle());
            }
        });

        root.getChildren().add(label1);
        root.getChildren().add(label2);

        TuioFX tuiofx = new TuioFX(primaryStage, Configuration.pqLabs());
        tuiofx.start();

        primaryStage.setTitle("Hello TUIOFX");
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);
        primaryStage.show();

    }

}
