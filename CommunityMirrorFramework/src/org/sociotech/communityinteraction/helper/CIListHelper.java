package org.sociotech.communityinteraction.helper;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Helper class containing method for the work with lists of {@link CIEvent}s.
 * 
 * @author Peter Lachenmaier
 */
public class CIListHelper {

	/**
	 * Unmodifiable empty list of event consumers that will always be returned when an empty list is needed.  
	 */
	private static final List<CIEventConsumer> emptyConsumerList = Collections.unmodifiableList(new LinkedList<CIEventConsumer>());
	
	// using type specific methods to allow easy later changing of list types
	
	/**
	 * Creates a list containing only the given event. If the given event is null
	 * than an empty list will be returned.
	 * 
	 * @param event Event to add to list
	 * @return List with the one event
	 */
	public static List<CIEvent> listFromEvent(CIEvent event) {
		return linkedListFromObject(event);
	}
	
	/**
	 * Creates a list containing only the given event consumer. If the given event consumer is null
	 * than an empty list will be returned.
	 * 
	 * @param eventConsumer Event consumer to add to list
	 * @return List with the one event consumer
	 */
	public static List<CIEventConsumer> listFromEventConsumer(CIEventConsumer eventConsumer) {
		return linkedListFromObject(eventConsumer);
	}
	
	/**
	 * Creates a linked list containing only the object. If the given object is null
	 * than an empty list will be returned.
	 * 
	 * @param object Object to add to list
	 * @return List with the one object
	 */
	private static <T> List<T> linkedListFromObject(T object) {
		// create new empty list
		List<T> result = new LinkedList<>();
		if(object != null) {
			result.add(object);
		}
		return result;
	}
	
	/**
	 * Returns an unmodifiable empty list of event consumers. Always the same list will be
	 * returned for performance reasons.
	 * 
	 * @return An unmodifiable empty list.
	 */
	public static List<CIEventConsumer> emptyUnmodifiableEventConsumerList() {
		return emptyConsumerList;
	}
	
	/**
	 * Returns a new empty list of event consumers.
	 * 
	 * @return An empty list for event consumers.
	 */
	public static List<CIEventConsumer> createConsumerList() {
		return new LinkedList<>();
		// list without duplicates
		//return SetUniqueList.decorate(new LinkedList<CIEventConsumer>());
	}
}
