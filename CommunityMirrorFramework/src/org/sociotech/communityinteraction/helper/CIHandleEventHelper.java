package org.sociotech.communityinteraction.helper;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.CommunicationEvent;
import org.sociotech.communityinteraction.events.communication.MessageReceivedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.kinect.ColorFrameEvent;
import org.sociotech.communityinteraction.events.kinect.DepthFrameEvent;
import org.sociotech.communityinteraction.events.kinect.KinectFrameEvent;
import org.sociotech.communityinteraction.events.kinect.SkeletonFrameEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingStartedEvent;
import org.sociotech.communityinteraction.events.touch.TouchEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.exception.UnknownEventException;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleKinectFrame;
import org.sociotech.communityinteraction.interactionhandling.HandleMagneticEffect;
import org.sociotech.communityinteraction.interactionhandling.HandleCommunication;
import org.sociotech.communityinteraction.interactionhandling.HandleOverlapping;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;

/**
 * Helper class containing helper methods for easier handling of events.
 * 
 * @author Peter Lachenmaier
 */
public class CIHandleEventHelper {

	/**
	 * Uses the given touch handler to handle the given touch event. This method
	 * chooses the correct handler method depending on the type of the given
	 * event. An {@link UnknownEventException} will be thrown if no method is
	 * implemented for the type of the given event.
	 * 
	 * @param touchEvent
	 *            Event to handle.
	 * @param touchHandler
	 *            Handler for the event.
	 */
	public static void handleTouchEventWith(TouchEvent touchEvent,
			HandleTouch touchHandler) {
		if (touchEvent == null || touchHandler == null) {
			// can not handle
			return;
		}

		// call matching method depending on touch type
		if (touchEvent instanceof TouchPressedEvent) {
			touchHandler.onTouchPressed((TouchPressedEvent) touchEvent);
		} else if (touchEvent instanceof TouchReleasedEvent) {
			touchHandler.onTouchReleased((TouchReleasedEvent) touchEvent);
		} else if (touchEvent instanceof TouchMovedEvent) {
			touchHandler.onTouchMoved((TouchMovedEvent) touchEvent);
		} else if (touchEvent instanceof TouchTappedEvent) {
			touchHandler.onTouchTapped((TouchTappedEvent) touchEvent);
		} else if (touchEvent instanceof TouchHoldEvent) {
			touchHandler.onTouchHold((TouchHoldEvent) touchEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(touchEvent);
		}
	}

	/**
	 * Uses the given rotation handler to handle the given rotation event. This
	 * method chooses the correct handler method depending on the type of the
	 * given event. An {@link UnknownEventException} will be thrown if no method
	 * is implemented for the type of the given event.
	 * 
	 * @param rotationEvent
	 *            Event to handle.
	 * @param rotationHandler
	 *            Handler for the event.
	 */
	public static void handleRotationEventWith(RotationEvent rotationEvent,
			HandleRotate rotationHandler) {
		if (rotationEvent == null || rotationHandler == null) {
			// can not handle
			return;
		}

		// call matching method depending on rotation type
		if (rotationEvent instanceof RotationStartedEvent) {
			rotationHandler
					.onRotationStarted((RotationStartedEvent) rotationEvent);
		} else if (rotationEvent instanceof RotationRotatedEvent) {
			rotationHandler
					.onRotationRotated((RotationRotatedEvent) rotationEvent);
		} else if (rotationEvent instanceof RotationFinishedEvent) {
			rotationHandler
					.onRotationFinished((RotationFinishedEvent) rotationEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(rotationEvent);
		}
	}

	/**
	 * Uses the given zoom handler to handle the given zoom event. This method
	 * chooses the correct handler method depending on the type of the given
	 * event. An {@link UnknownEventException} will be thrown if no method is
	 * implemented for the type of the given event.
	 * 
	 * @param zoomEvent
	 *            Event to handle.
	 * @param zoomHandler
	 *            Handler for the event.
	 */
	public static void handleZoomEventWith(ZoomEvent zoomEvent,
			HandleZoom zoomHandler) {
		if (zoomEvent == null || zoomHandler == null) {
			// can not handle
			return;
		}

		// call matching method depending on zoom type
		if (zoomEvent instanceof ZoomStartedEvent) {
			zoomHandler.onZoomStarted((ZoomStartedEvent) zoomEvent);
		} else if (zoomEvent instanceof ZoomZoomedEvent) {
			zoomHandler.onZoomZoomed((ZoomZoomedEvent) zoomEvent);
		} else if (zoomEvent instanceof ZoomFinishedEvent) {
			zoomHandler.onZoomFinished((ZoomFinishedEvent) zoomEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(zoomEvent);
		}
	}

	/**
	 * Uses the given drag handler to handle the given drag event. This method
	 * chooses the correct handler method depending on the type of the given
	 * event. An {@link UnknownEventException} will be thrown if no method is
	 * implemented for the type of the given event.
	 * 
	 * @param dragEvent
	 *            Event to handle.
	 * @param dragHandler
	 *            Handler for the event.
	 */
	public static void handleDragEventWith(DragEvent dragEvent,
			HandleDrag dragHandler) {
		if (dragEvent == null || dragHandler == null) {
			// can not handle
			return;
		}

		// call matching method depending on zoom type
		if (dragEvent instanceof DragStartedEvent) {
			dragHandler.onDragStarted((DragStartedEvent) dragEvent);
		} else if (dragEvent instanceof DragDraggedEvent) {
			dragHandler.onDragDragged((DragDraggedEvent) dragEvent);
		} else if (dragEvent instanceof DragFinishedEvent) {
			dragHandler.onDragFinished((DragFinishedEvent) dragEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(dragEvent);
		}
	}

	/**
	 * Uses the given scroll handler to handle the given scroll event. This
	 * method chooses the correct handler method depending on the type of the
	 * given event. An {@link UnknownEventException} will be thrown if no method
	 * is implemented for the type of the given event.
	 * 
	 * @param scrollEvent
	 *            Event to handle.
	 * @param scrollHandler
	 *            Handler for the event.
	 */
	public static void handleScrollEventWith(ScrollEvent scrollEvent,
			HandleScroll scrollHandler) {
		if (scrollEvent == null || scrollHandler == null) {
			// can not handle
			return;
		}

		// call matching method depending on zoom type
		if (scrollEvent instanceof ScrollStartedEvent) {
			scrollHandler.onScrollStarted((ScrollStartedEvent) scrollEvent);
		} else if (scrollEvent instanceof ScrollScrolledEvent) {
			scrollHandler.onScrollScrolled((ScrollScrolledEvent) scrollEvent);
		} else if (scrollEvent instanceof ScrollFinishedEvent) {
			scrollHandler.onScrollFinished((ScrollFinishedEvent) scrollEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(scrollEvent);
		}
	}

	/**
	 * Uses the given overlapping handler to handle the given overlapping event.
	 * This method chooses the correct handler method depending on the type of
	 * the given event. An {@link UnknownEventException} will be thrown if no
	 * method is implemented for the type of the given event.
	 * 
	 * @param event
	 *            Event to handle.
	 * @param interactionHandler
	 *            Handler of the Event.
	 * 
	 * @author: Eva Loesch
	 */
	public static void handleOverlappingEventWith(
			OverlappingEvent overlappingEvent,
			HandleOverlapping overlappingHandler) {
		if (overlappingEvent == null || overlappingHandler == null) {
			// can not handle
			return;
		}
		// call matching method depending on overlapping type
		if (overlappingEvent instanceof OverlappingStartedEvent) {
			overlappingHandler
					.onOverlappingStarted((OverlappingStartedEvent) overlappingEvent);
		} else if (overlappingEvent instanceof OverlappingFinishedEvent) {
			overlappingHandler
					.onOverlappingFinished((OverlappingFinishedEvent) overlappingEvent);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(overlappingEvent);
		}
	}

	/**
	 * Uses the given magnetic effect handler to handle the given magnetic
	 * effect event. This method chooses the correct handler method depending on
	 * the type of the given event. An {@link UnknownEventException} will be
	 * thrown if no method is implemented for the type of the given event.
	 * 
	 * @param event
	 *            Event to handle.
	 * @param magneticEffectHandler
	 *            Handler of the Event
	 * 
	 * @author: Eva Loesch
	 */
	public static void handleMagneticEffectEventWith(CIEvent event,
			HandleMagneticEffect magneticEffectHandler) {
		if (event == null || magneticEffectHandler == null) {
			// can not handle
			return;
		}
		// call matching method depending on magnetic effect type
		if (event instanceof MagneticRepulsionStartedEvent) {
			magneticEffectHandler
					.onMagneticRepulsionStarted((MagneticRepulsionStartedEvent) event);
		} else if (event instanceof MagneticRepulsionFinishedEvent) {
			magneticEffectHandler
					.onMagneticRepulsionFinished((MagneticRepulsionFinishedEvent) event);
		} else if (event instanceof MagneticAttractionStartedEvent) {
			magneticEffectHandler
					.onMagneticAttractionStarted((MagneticAttractionStartedEvent) event);
		} else if (event instanceof MagneticAttractionFinishedEvent) {
			magneticEffectHandler
					.onMagneticAttractionFinished((MagneticAttractionFinishedEvent) event);
		}

		else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(event);
		}
	}

	/**
	 * Uses the given kinect frame handler to handle the given kinect frame
	 * event. This method chooses the correct handler method depending on the
	 * type of the given event. An {@link UnknownEventException} will be thrown
	 * if no method is implemented for the type of the given event.
	 * 
	 * @param event
	 *            Event to Handle.
	 * @param kinectFrameHandler
	 *            Handler of the Event.
	 * 
	 * @author: Eva Loesch
	 */
	public static void handleKinectFrameEventWith(KinectFrameEvent event,
			HandleKinectFrame kinectFrameHandler) {
		if (event == null || kinectFrameHandler == null) {
			// can not handle
			return;
		}
		// call matching method depending on kinectFrame type
		if (event instanceof SkeletonFrameEvent) {
			kinectFrameHandler.onSkeletonFrame((SkeletonFrameEvent) event);
		} else if (event instanceof ColorFrameEvent) {
			kinectFrameHandler.onColorFrame((ColorFrameEvent) event);
		} else if (event instanceof DepthFrameEvent) {
			kinectFrameHandler.onDepthFrame((DepthFrameEvent) event);
		} else {
			// throw exception to show unknown event type
			// may only happen when extending event hierarchy
			throw new UnknownEventException(event);
		}
	}
	
	/**
	 * Uses the given communicationHandler to handle the given CommunicationEvent.
	 * 
	 * @param event
	 *            Event to Handle.
	 * @param communicationHandler
	 *            Handler of the Event.
	 * 
	 * @author: Eva Lösch
	 */
	public static void handleCommunicationEventWith(CommunicationEvent event,
			HandleCommunication communicationHandler) {
		if (event == null || communicationHandler == null) {
			// can not handle
			return;
		}
		// handle message received event
		if(event instanceof MessageReceivedEvent){
			communicationHandler.onMessageReceived((MessageReceivedEvent)event);
		}
	}
}
