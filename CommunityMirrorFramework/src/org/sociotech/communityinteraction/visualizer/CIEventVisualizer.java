package org.sociotech.communityinteraction.visualizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javafx.scene.Group;

import org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory;
import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.touch.TouchEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.helper.CIEventConsumerHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors;
import org.sociotech.communityinteraction.visualizer.visualizations.TouchVisualization;

/**
 * JavaFX based visualizer for CIEvents.
 * 
 * @author Peter Lachenmaier
 */
public class CIEventVisualizer implements CIEventConsumer, HandleTouch,
		ManageInteractionBehaviors {

	/**
	 * The list of interaction behaviors. The behaviors are used to visualize
	 * the resulting Event and not only the initial one. All interaction
	 * behaviors are possible event consumers.
	 */
	private List<InteractionBehavior<?>> interactionBehaviors;

	/**
	 * Factory for interaction behaviors
	 */
	private InteractionBehaviorFactory behaviorFactory;

	/**
	 * The java fx group to add visualizations to
	 */
	private Group mainGroup;

	/**
	 * The map that keeps all currently active touch visualizations
	 */
	private Map<Integer, TouchVisualization> visualTouches = new ConcurrentHashMap<>();

	/**
	 * Creates a new visualizer that draws on the given scene.
	 * 
	 * @param behaviorFactory
	 *            Factory for behaviors
	 * @param parent
	 *            Parent group to add visualization nodes to.
	 */
	public CIEventVisualizer(InteractionBehaviorFactory behaviorFactory,
			Group parent) {
		this.behaviorFactory = behaviorFactory;
		this.mainGroup = parent;

		// create list for interaction behaviors
		interactionBehaviors = new LinkedList<>();

		// create and add behaviors
		this.addInteractionBehavior(this.behaviorFactory
				.createTouchBehavior(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers
	 * (org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		List<CIEventConsumer> result = CIListHelper.createConsumerList();

		// the visualizer is always a consumer
		result.add(this);

		// find consumer from behaviors
		result.addAll(CIEventConsumerHelper.collectConsumersFromBehaviors(
				interactionBehaviors, event));

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(
	 * org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		// simply log
		// System.out.println(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interactionhandling.HandleTouchInteraction
	 * #
	 * onTouchPressed(org.sociotech.communityinteraction.events.TouchPressedEvent
	 * )
	 */
	@Override
	public void onTouchPressed(TouchPressedEvent event) {
		addVisualTouch(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interactionhandling.HandleTouchInteraction
	 * #
	 * onTouchReleased(org.sociotech.communityinteraction.events.TouchReleasedEvent
	 * )
	 */
	@Override
	public void onTouchReleased(TouchReleasedEvent event) {
		removeVisualTouch(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interactionhandling.HandleTouchInteraction
	 * #onTouchMoved(org.sociotech.communityinteraction.events.TouchMovedEvent)
	 */
	@Override
	public void onTouchMoved(TouchMovedEvent event) {
		updateVisualTouch(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interactionhandling.HandleTouchInteraction
	 * #onTouchTapped(org.sociotech.communityinteraction.events.touch.
	 * TouchTappedEvent)
	 */
	@Override
	public void onTouchTapped(TouchTappedEvent event) {
		updateVisualTouch(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
	 * onTouchHold
	 * (org.sociotech.communityinteraction.events.touch.TouchHoldEvent)
	 */
	@Override
	public void onTouchHold(TouchHoldEvent event) {
		updateVisualTouch(event);
	}

	/**
	 * Removes the visualization for the id of the given touch event.
	 * 
	 * @param event
	 *            Identifiable event.
	 */
	private void removeVisualTouch(TouchEvent event) {
		Integer key = new Integer(event.getTouchID());
		TouchVisualization visualTouch = visualTouches.get(key);
		if (visualTouch != null) {
			// update position
			visualTouch.setPositionX(event.getSceneX());
			visualTouch.setLayoutY(event.getSceneY());

			// remove from fx group and lookup map
			mainGroup.getChildren().remove(visualTouch);
			visualTouches.remove(key);
		}
	}

	/**
	 * Updates the visualization for the id of the given touch event.
	 * 
	 * @param event
	 *            Identifiable event.
	 */
	private void updateVisualTouch(TouchEvent event) {
		Integer key = new Integer(event.getTouchID());
		TouchVisualization visualTouch = visualTouches.get(key);
		if (visualTouch != null) {
			// update position
			visualTouch.setPositionX(event.getSceneX());
			visualTouch.setLayoutY(event.getSceneY());
		}
	}

	/**
	 * Adds a visualization for the id of the given touch event.
	 * 
	 * @param event
	 *            Identifiable event.
	 */
	private void addVisualTouch(TouchEvent event) {
		Integer key = new Integer(event.getTouchID());
		TouchVisualization visualTouch = visualTouches.get(key);
		if (visualTouch == null) {
			visualTouch = new TouchVisualization(event);

			// add to fx group and lookup map
			mainGroup.getChildren().add(visualTouch);
			visualTouches.put(key, visualTouch);
		}

		// update position
		visualTouch.setPositionX(event.getSceneX());
		visualTouch.setLayoutY(event.getSceneY());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
	 * #addInteractionBehavior
	 * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
	 */
	@Override
	public void addInteractionBehavior(
			InteractionBehavior<?> interactionBehavior) {
		interactionBehaviors.add(interactionBehavior);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
	 * #getInteractionBehaviors()
	 */
	@Override
	public List<InteractionBehavior<?>> getInteractionBehaviors() {
		return this.interactionBehaviors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
	 * #removeInteractionBehavior
	 * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
	 */
	@Override
	public void removeInteractionBehavior(
			InteractionBehavior<?> interactionBehavior) {
		interactionBehaviors.remove(interactionBehavior);
	}
}
