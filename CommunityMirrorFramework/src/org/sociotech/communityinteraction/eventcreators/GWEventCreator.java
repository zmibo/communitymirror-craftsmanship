
package org.sociotech.communityinteraction.eventcreators;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.touch.TouchEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.gestureworks.Core;
import org.sociotech.communitymirror.gestureworks.GWCUtils.GestureEvent;
import org.sociotech.communitymirror.gestureworks.GWCUtils.PointEvent;
import org.sociotech.communitymirror.gestureworks.GWCUtils.Status;

import javafx.scene.Scene;

/**
 * Event creator that produces CommunityInteraction events from GestureWorks
 * events.
 *
 * @author Peter Lachenmaier
 */
public class GWEventCreator extends CIEventCreator {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(GWEventCreator.class.getName());

    /**
     * Referece to the gesture works core instance.
     */
    protected org.sociotech.communitymirror.gestureworks.Core gwCore;

    /**
     * Stores the mapping from consumer ids to consumers
     */
    protected Map<String, CIEventConsumer> consumerIDMapping;

    /**
     * Copied list to iterate over it while letting new consumers to register
     * and unregister.
     */
    protected List<PositionedEventConsumer> positionedConsumersCopy;

    /**
     * Initializes the GestureWorks events creator.
     *
     * @param scene
     *            JavaFX scene to determine window size.
     * @param windowName
     *            Name used for main window.
     * @param gmlPath
     *            Path to gesture definition.
     * @param gwDllPath
     *            Path to the gestureworks dll.
     */
    public GWEventCreator(Scene scene, String windowName, String gmlPath, String gwDllPath) {
        try {
            // initialize gesture works
            this.gwCore = new Core(gwDllPath);

            this.gwCore.initializeGestureWorks((int) scene.getWidth(), (int) scene.getHeight());

            // register window with core
            this.gwCore.registerWindowForTouchByName(windowName);

            // get full path for gmlPath
            URL url = CommunityMirror.class.getClassLoader().getResource(gmlPath);
            if (url != null) {
                String tmps = url.toString();
                if (tmps.startsWith("file:/C:")) {
                    gmlPath = tmps.substring(6);
                } else if (tmps.startsWith("file:")) {
                    gmlPath = tmps.substring(5);
                }
            }

            // load gesture definition
            boolean gmlLoaded = this.gwCore.loadGML(gmlPath);
            if (!gmlLoaded) {
                this.logger.warn("Could not load GestureWorks GML file from " + gmlPath);
            }
        } catch (Exception e) {
            this.logger.error("Could not initialize GestureWorks due to exception " + e.getMessage(), e);
        }

        // create id mapping map
        this.consumerIDMapping = new HashMap<>();

        // frame processing timer
        this.initializeTimer();
    }

    /**
     * Initializes the timer for the gestureworks update thread.
     */
    private void initializeTimer() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new UpdateGW(), 1000, 64);
        // TODO stop timer / thread in the end
    }

    /**
     * Inner class representing the gestureworks update task.
     */
    private class UpdateGW extends TimerTask {
        @Override
        public void run() {
            try {
                GWEventCreator.this.gwCore.processFrame();
                GWEventCreator.this.processPointEvents(GWEventCreator.this.gwCore.consumePointEvents());
                GWEventCreator.this.processGestureEvents(GWEventCreator.this.gwCore.consumeGestureEvents());
            } catch (Exception e) {
                GWEventCreator.this.logger.warn(
                        "Could not process current interaction frame. Trying to continue. (" + e.getMessage() + ")");
            }
        }
    }

    /**
     * Processes the given list of point events and maps them to ci events.
     *
     * @param events
     *            Gestureworks point events
     */
    private void processPointEvents(ArrayList<PointEvent> events) {

        if (events.isEmpty()) {
            return;
        }

        for (PointEvent event : events) {

            this.logger.debug("gw point event: " + event);

            PositionedEventConsumer targetConsumer = null;

            // check if touch is inside of a positioned consumer
            for (PositionedEventConsumer consumer : this.getPositionedConsumersCopy()) {
                try {
                    if (consumer.isOnPosition(event.position.x, event.position.y)) {
                        this.gwCore.addTouchPoint(consumer.getID(), event.point_id);
                        // System.out.println("Touch " + event.point_id + " on "
                        // + consumer.getID() );
                        // keep target
                        targetConsumer = consumer;
                        // use only first consumer -> assume list is ordered by
                        // z-index
                        break;
                    }
                } catch (Exception e) {
                    // do nothing -> continue with next consumer
                }
            }

            TouchEvent touchEvent = null;

            switch (event.status) {
            case Status.TOUCHADDED:
                // create new touch pressed event
                touchEvent = new TouchPressedEvent(event.position.x, event.position.y);
                break;
            case Status.TOUCHUPDATE:
                // create new touch moved event
                touchEvent = new TouchMovedEvent(event.position.x, event.position.y);
                break;
            case Status.TOUCHREMOVED:
                // create new touch released event
                touchEvent = new TouchReleasedEvent(event.position.x, event.position.y);
                break;
            default:
                break;
            }

            if (touchEvent != null) {
                // set id
                touchEvent.setTouchID(event.point_id);

                // set target
                touchEvent.setTargetConsumers(targetConsumer);

                // publish the event in the main thread
                this.publishLater(touchEvent);
            }
        }
    }

    /**
     * Processes the given list of gesture events, maps them to ci events and
     * publishes the ci events.
     *
     * @param events
     *            List of gestureworks events
     */
    private void processGestureEvents(ArrayList<GestureEvent> events) {

        for (GestureEvent event : events) {

            this.logger.debug("gw gesture event: " + event);

            // TODO map number of fingers

            // Phase describes the current state of the gesture:
            //
            // 0 = GESTURE_BEGIN : The beginning of a gesture.
            // 1 = GESTURE_ACTIVE : The gesture is currently being generated by
            // direct manipulation.
            // 2 = GESTURE_RELEASE : Direct manipulation has ceased.
            // 3 = GESTURE_PASSIVE : The gesture is being generated by indirect
            // manipulation (e.g. inertial filters).
            // 4 = GESTURE_END : The gesture has completed. This is the last
            // event.

            // System.out.println("Gesture " + event.gesture_id + " in " +
            // event.phase + " on " + event.target);

            if (event.gesture_id.equalsIgnoreCase("n-drag")) {
                // create new drag event
                DragEvent dragEvent;

                if (event.phase == 0) {
                    dragEvent = new DragStartedEvent(event.x, event.y);
                } else if (event.phase == 4 || event.phase == 2) {
                    dragEvent = new DragFinishedEvent(event.x, event.y);
                } else if (event.phase == 3) {
                    dragEvent = new DragDraggedEvent(event.x, event.y);
                    dragEvent.setInertia(true);
                } else {
                    dragEvent = new DragDraggedEvent(event.x, event.y);
                }

                // set delta
                dragEvent.setDeltaX(event.values.castValues()[0]);
                dragEvent.setDeltaY(event.values.castValues()[1]);

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, dragEvent);

                // publish it on main thread
                this.publishLater(dragEvent);

            } else if (event.gesture_id.equalsIgnoreCase("n-rotate")) {
                // create new rotate event
                RotationEvent rotateEvent;

                if (event.phase == 0) {
                    rotateEvent = new RotationStartedEvent(event.x, event.y);
                } else if (event.phase == 4) {
                    rotateEvent = new RotationFinishedEvent(event.x, event.y);
                } else if (event.phase == 3) {
                    rotateEvent = new RotationRotatedEvent(event.x, event.y);
                    rotateEvent.setInertia(true);
                } else {
                    rotateEvent = new RotationRotatedEvent(event.x, event.y);
                }

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, rotateEvent);

                // set angle depending on phase
                rotateEvent.setAngle(event.values.castValues()[0]);

                // publish it on main thread
                this.publishLater(rotateEvent);

            } else if (event.gesture_id.equalsIgnoreCase("n-scale")) {

                // create new zoom event depending on phase
                ZoomEvent zoomEvent;

                if (event.phase == 0) {
                    zoomEvent = new ZoomStartedEvent(event.x, event.y);
                } else if (event.phase == 4) {
                    zoomEvent = new ZoomFinishedEvent(event.x, event.y);
                } else if (event.phase == 3) {
                    zoomEvent = new ZoomZoomedEvent(event.x, event.y);
                    zoomEvent.setInertia(true);
                } else {
                    zoomEvent = new ZoomZoomedEvent(event.x, event.y);
                }

                // set zoom factor
                zoomEvent.setZoomFactor(1 + event.values.castValues()[0]);

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, zoomEvent);

                // publish it on main thread
                this.publishLater(zoomEvent);

            } else if (event.gesture_id.equalsIgnoreCase("n-scroll")) {
                // create new scroll event
                ScrollEvent scrollEvent;

                if (event.phase == 0) {
                    scrollEvent = new ScrollStartedEvent(event.x, event.y);
                } else if (event.phase == 4 || event.phase == 2) {
                    scrollEvent = new ScrollFinishedEvent(event.x, event.y);
                } else if (event.phase == 3) {
                    scrollEvent = new ScrollScrolledEvent(event.x, event.y);
                    scrollEvent.setInertia(true);
                } else {
                    scrollEvent = new ScrollScrolledEvent(event.x, event.y);
                }

                // set delta
                scrollEvent.setDeltaX(event.values.castValues()[0]);
                scrollEvent.setDeltaY(event.values.castValues()[1]);

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, scrollEvent);

                // publish it on main thread
                this.publishLater(scrollEvent);
            } else if (event.gesture_id.equalsIgnoreCase("n-hold")) {
                // create new hold event
                TouchHoldEvent holdEvent = new TouchHoldEvent(event.values.castValues()[1],
                        event.values.castValues()[2]);

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, holdEvent);

                // publish it on main thread
                this.publishLater(holdEvent);
            } else if (event.gesture_id.equalsIgnoreCase("n-tap")) {
                // create new hold event
                TouchTappedEvent tapEvent = new TouchTappedEvent(event.values.castValues()[1],
                        event.values.castValues()[2]);

                // try to resolve the target for the gesture directly
                this.mapEventTarget(event, tapEvent);

                // publish it on main thread
                this.publishLater(tapEvent);
            }

            // TODO Gestures
            // n-flick
            // n-swipe
        }
    }

    /**
     * Maps the target of the given gesture works event to a registered event
     * consumer if possible.
     * If no mapping can be performed, the target of the ci event stays null.
     *
     * @param event
     *            Gestureworks event to map
     * @param ciEvent
     *            Event to set the target
     */
    private void mapEventTarget(GestureEvent event, CIEvent ciEvent) {
        // System.out.println(event.gesture_id + ": " + "Phase: " + event.phase
        // + " Target: " + event.target + " Consumer: " +
        // consumerIDMapping.get(event.target));
        ciEvent.setTargetConsumers(this.consumerIDMapping.get(event.target));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.eventcreators.CIEventCreator#
     * addPositionedEventConsumer(org.sociotech.communityinteraction.interfaces.
     * PositionedEventConsumer)
     */
    @Override
    protected void addPositionedEventConsumer(PositionedEventConsumer consumer) {
        super.addPositionedEventConsumer(consumer);
        this.registerConsumerAtGestureworks(consumer);
    }

    /**
     * Registers the given consumer for events at gestureworks core.
     *
     * @param consumer
     *            Consumer to register.
     */
    protected void registerConsumerAtGestureworks(PositionedEventConsumer consumer) {
        String consumerID = consumer.getID();

        // keep mapping from id to consumer
        this.consumerIDMapping.put(consumerID, consumer);

        try {
            // register consumer at gestureworks
            this.gwCore.registerTouchObject(consumerID);
        } catch (Exception e) {
            this.logger.warn("Could not register consumer " + consumerID + " at GestureWorks due to exception "
                    + e.getMessage());
            return;
        }

        try {
            // TODO room for performance enhancements when only adding gesture
            // that are possible on consumer
            this.gwCore.addGesture(consumerID, "n-drag");
            this.gwCore.addGesture(consumerID, "n-rotate");
            this.gwCore.addGesture(consumerID, "n-scale");
            this.gwCore.addGesture(consumerID, "n-scroll");
            this.gwCore.addGesture(consumerID, "n-hold");
            this.gwCore.addGesture(consumerID, "n-tap");
        } catch (Exception e) {
            this.logger.warn("Could not add gestures for consumer " + consumerID + " at GestureWorks due to exception "
                    + e.getMessage());
            return;
        }
    }

    /**
     * Returns all positioned consumers.
     *
     * @return All positioned consumers.
     */
    private List<PositionedEventConsumer> getPositionedConsumersCopy() {
        // get positioned returns z-index ordered list
        this.positionedConsumersCopy = new LinkedList<>(this.getPositionedConsumers());
        return this.positionedConsumersCopy;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.eventcreators.CIEventCreator#
     * removePositionedEventConsumer(org.sociotech.communityinteraction.
     * interfaces.PositionedEventConsumer)
     */
    @Override
    protected void removePositionedEventConsumer(PositionedEventConsumer consumer) {
        super.removePositionedEventConsumer(consumer);

        this.unregisterEventConsumerAtGestureWorks(consumer);
    }

    /**
     * Unregisters the given event consumer at gestureworks.
     *
     * @param consumer
     *            Consumer to unregister
     */
    protected void unregisterEventConsumerAtGestureWorks(PositionedEventConsumer consumer) {
        // remove from consumer id mapping
        this.consumerIDMapping.remove(consumer.getID());

        // unregister consumer
        try {
            if (!this.gwCore.deregisterTouchObject(consumer.getID())) {
                this.logger.warn("Could not deregister gestureworks object with id " + consumer.getID());
            }
        } catch (Exception e) {
            this.logger.warn("Exception " + e.getMessage() + " while deregistering gestureworks object with id "
                    + consumer.getID());
            // continue
        }
    }

}
