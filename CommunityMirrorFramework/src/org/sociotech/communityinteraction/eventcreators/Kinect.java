package org.sociotech.communityinteraction.eventcreators;

import java.util.LinkedList;
import java.util.List;

import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that represents a kinect and receives kinect frames using j4k sdk.
 * 
 * @author evalosch
 *
 */
public class Kinect extends J4KSDK {

	/**
	 * The kinect frame event creator that creates and publishes kinect frame
	 * events coming from this kinect.
	 */
	private KinectFrameEventCreator kinectFrameEventCreator;

	private final Logger logger = LogManager.getLogger(Kinect.class.getName());

	public Kinect(KinectFrameEventCreator kinectFrameEventCreator) {
		this.kinectFrameEventCreator = kinectFrameEventCreator;
		logger.debug("kinect initialized!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ufl.digitalworlds.j4k.J4KSDK#onColorFrameEvent(byte[])
	 */
	@Override
	public void onColorFrameEvent(byte[] color_data) {
		logger.debug("kinect: on color frame event");

		// let the kinect frame event creator create and publish a new color
		// frame event forwarding the received color frame
		this.kinectFrameEventCreator
				.createAndPublishColorFrameEvent(color_data);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ufl.digitalworlds.j4k.J4KSDK#onDepthFrameEvent(short[], byte[],
	 * float[], float[])
	 */
	@Override
	public void onDepthFrameEvent(short[] depth_frame, byte[] player_index,
			float[] xyz, float[] uv) {
		logger.debug("kinect: on depth frame event");

		// let the kinect frame event creator create and publish a new depth
		// frame event forwarding the received depth frame
		// this.kinectFrameEventCreator.createAndPublishDepthFrameEvent(
		// depth_frame, player_index, uv, uv);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ufl.digitalworlds.j4k.J4KSDK#onSkeletonFrameEvent(boolean[],
	 * float[], float[], byte[])
	 */
	@Override
	public void onSkeletonFrameEvent(boolean[] skeleton_tracked,
			float[] joint_positions, float[] joint_orientations,
			byte[] joint_state) {
		logger.debug("kinect: on skeleton frame event");
		//create skeletons
		List<Skeleton> skeletons = new LinkedList<Skeleton>();
		for (int i = 0; i < skeleton_tracked.length; i++) {
			skeletons.add(Skeleton.getSkeleton(i, skeleton_tracked,
					joint_positions, joint_orientations, joint_state, this));
		}
		// let the kinect frame event creator create and publish a new skeleton
		// frame event forwarding the received skeleton frame
		this.kinectFrameEventCreator
				.createAndPublishSkeletonFrameEvent(skeletons);
	}
}
