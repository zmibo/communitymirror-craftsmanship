package org.sociotech.communityinteraction.behaviorfactories;

import org.sociotech.communityinteraction.behaviors.DragBehavior;
import org.sociotech.communityinteraction.behaviors.ScrollBehavior;
import org.sociotech.communityinteraction.behaviors.TouchBehavior;
import org.sociotech.communityinteraction.behaviors.ZoomBehavior;
import org.sociotech.communityinteraction.behaviors.manipulation.RotateBehavior;
import org.sociotech.communityinteraction.behaviors.mousetox.MouseToDragBehavior;
import org.sociotech.communityinteraction.behaviors.mousetox.MouseToRotateBehavior;
import org.sociotech.communityinteraction.behaviors.mousetox.MouseToScrollBehavior;
import org.sociotech.communityinteraction.behaviors.mousetox.MouseToTouchBehavior;
import org.sociotech.communityinteraction.behaviors.mousetox.MouseToZoomBehavior;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;

/**
 * Factory that create behaviors that map mouse to touch.
 * 
 * @author Peter Lachenmaier
 */
public class MouseToXBehaviorFactory extends InteractionBehaviorFactory {

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createTouchBehaviour()
	 */
	@Override
	public TouchBehavior createTouchBehavior(HandleTouch handler) {
		return new MouseToTouchBehavior(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createDragBehavior(org.sociotech.communityinteraction.interactionhandling.HandleDrag)
	 */
	@Override
	public DragBehavior createDragBehavior(HandleDrag handler) {
		return new MouseToDragBehavior(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createScrollBehavior(org.sociotech.communityinteraction.interactionhandling.HandleScroll)
	 */
	@Override
	public ScrollBehavior createScrollBehavior(HandleScroll handler) {
		return new MouseToScrollBehavior(handler);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createZoomBehavior(org.sociotech.communityinteraction.interactionhandling.HandleZoom)
	 */
	@Override
	public ZoomBehavior createZoomBehavior(HandleZoom handler) {
		return new MouseToZoomBehavior(handler);
	}
	
	@Override
	public RotateBehavior createRotateBehavior(HandleRotate handler) {
		return new MouseToRotateBehavior(handler);
	}
}
