package org.sociotech.communityinteraction.behaviors;

import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleOverlapping;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * @author Eva Lösch
 *
 */
public class OverlappingBehavior extends InteractionBehavior<HandleOverlapping> {

	public OverlappingBehavior(HandleOverlapping handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof OverlappingEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof OverlappingEvent)){
			return;
		}
		CIHandleEventHelper.handleOverlappingEventWith((OverlappingEvent) event, this.getInteractionHandler());
	}

}
