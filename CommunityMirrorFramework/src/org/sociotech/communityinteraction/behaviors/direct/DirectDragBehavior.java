package org.sociotech.communityinteraction.behaviors.direct;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.DragBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;


/**
 * A behavior that directly applies received drag events.
 *  
 * @author Peter Lachenmaier
 */
public class DirectDragBehavior extends DragBehavior {

	/**
	 * Constructs the drag behavior for the given handler.
	 * 
	 * @param handler Handler that handles the drag interaction.
	 */
	public DirectDragBehavior(HandleDrag handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof DragEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof DragEvent)) {
			return;
		}
		// let the event be handled by the handler
		CIHandleEventHelper.handleDragEventWith((DragEvent) event, this.getInteractionHandler());
	}

}
