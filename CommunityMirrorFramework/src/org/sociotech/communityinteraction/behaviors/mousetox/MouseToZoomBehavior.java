package org.sociotech.communityinteraction.behaviors.mousetox;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.ZoomBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior that maps scroll to zoom while mouse interaction. Scroll with shift pressed is mapped
 * to zoom
 * 
 * @author Peter Lachenmaier
 */
public class MouseToZoomBehavior extends ZoomBehavior {
	
	/**
	 * Creates a mouse to zoom behavior for the given handler.
	 * 
	 * @param handler Handler for the zoom events created by this behaviors.
	 */
	public MouseToZoomBehavior(HandleZoom handler) {
		super(handler);

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!isEnabled()) {
			// can only consume if enabled
			return null;
		}
		
		if(!(event instanceof ScrollEvent)) {
			// accept only scroll events
			return null;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		if(!scrollEvent.isShiftDown()) {
			// only use with shift pressed
			return null;
		}
		
		// TODO maybe use always the same list that is final and contains only this object -> method getThisInList 
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		
		
		if(!(event instanceof ScrollEvent)) {
			// accept only scroll events
			return;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		
		ZoomEvent zoomEvent;
		
		if(event instanceof ScrollStartedEvent) {
			zoomEvent = new ZoomStartedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
		} else if(event instanceof ScrollScrolledEvent) {
			zoomEvent = new ZoomZoomedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
			// map y scroll to zoom factor
			zoomEvent.setZoomFactor(1.0 + scrollEvent.getDeltaY() / 2500);
			zoomEvent.setInertia(scrollEvent.isInertia());
		} else if(event instanceof ScrollFinishedEvent) {
			zoomEvent = new ZoomFinishedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
		} else {
			return;
		}
		
		
		// let the event be handled by the interaction handler using the helper implementation
		CIHandleEventHelper.handleZoomEventWith(zoomEvent , this.getInteractionHandler());
		
		// set consumed state
		event.setConsumed(zoomEvent.isConsumed());
	}
}
