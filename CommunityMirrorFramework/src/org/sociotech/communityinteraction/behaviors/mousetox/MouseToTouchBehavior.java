package org.sociotech.communityinteraction.behaviors.mousetox;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.TouchBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.mouse.MouseClickedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseDraggedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseEvent;
import org.sociotech.communityinteraction.events.mouse.MouseMovedEvent;
import org.sociotech.communityinteraction.events.mouse.MousePressedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.exception.UnknownEventException;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior that maps mouse to touch interaction.
 * 
 * @author Peter Lachenmaier
 */
public class MouseToTouchBehavior extends TouchBehavior {

	/**
	 * The touch id mapped from the primary mouse button.
	 */
	public static int PRIMARY_BUTTON_TOUCH_ID = 1;
	
	/**
	 * The touch id mapped from the secondary mouse button.
	 */
	public static int SECONDARY_BUTTON_TOUCH_ID = 2;
	
	/**
	 * The touch id mapped from the middle mouse button.
	 */
	public static int MIDDLE_BUTTON_TOUCH_ID = 3;
	
	/**
	 * The touch id mapped when no button is pressed
	 */
	public static int NO_BUTTON_TOUCH_ID = -1;
	
	/**
	 * Keeps drag state
	 */
	private boolean isDragged = false;
	
	/**
	 * Creates a mouse to touch behavior for the given handler.
	 * 
	 * @param handler Handler for the touch events created by this behaviors.
	 */
	public MouseToTouchBehavior(HandleTouch handler) {
		super(handler);

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!isEnabled()) {
			// can only consume if enabled
			return null;
		}
		
		if(!(event instanceof MouseEvent)) {
			// accept only mouse events
			return null;
		}
		
		// TODO maybe use always the same list that is final and contains only this object -> method getThisInList 
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		
		if(!(event instanceof MouseEvent)) {
			// accept only mouse events
			return;
		}
		
		MouseEvent mouseEvent = (MouseEvent) event;
		
		// transform the mouse event to an touch event
		TouchEvent touchEvent = transformMouseToTouchEvent(mouseEvent); 
		
		if(touchEvent == null) {
			return;
		}
		
		// let the event be handled by the interaction handler using the helper implementation
		CIHandleEventHelper.handleTouchEventWith(touchEvent, this.getInteractionHandler());
		
		// copy consume state from mapped event
		mouseEvent.setConsumed(touchEvent.isConsumed());
		
		if(mouseEvent instanceof MousePressedEvent && mouseEvent.getButton() == MouseEvent.MIDDLE_BUTTON) {
			//send also a hold on middle button
			TouchEvent holdEvent = new TouchHoldEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			holdEvent.setTouchID(touchEvent.getTouchID());
			// let the event be handled by the interaction handler using the helper implementation
			CIHandleEventHelper.handleTouchEventWith(holdEvent, this.getInteractionHandler());
		}
	}

	/**
	 * Transforms the given mouse event to a mapped touch event.
	 * 
	 * @param mouseEvent Mouse event to transform
	 * 
	 * @return New Touch event that represents the mouse event. 
	 */
	protected TouchEvent transformMouseToTouchEvent(MouseEvent mouseEvent) {
		TouchEvent transformedEvent = null;
		
		int touchID = getTouchIDForMouseEvent(mouseEvent);
		
		if(mouseEvent instanceof MouseClickedEvent) {
			// click maps to tap if there was no drag between
			if(!isDragged) {
				transformedEvent = new TouchTappedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
				transformedEvent.setTouchID(touchID);
			}
		} else if(mouseEvent instanceof MousePressedEvent) {
			// mouse release maps to touch release
			transformedEvent = new TouchPressedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			transformedEvent.setTouchID(touchID);
			// reset drag state
			isDragged = false;
		} else if(mouseEvent instanceof MouseReleasedEvent) {
			// mouse release maps to touch release
			transformedEvent = new TouchReleasedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			transformedEvent.setTouchID(touchID);
		} else if(mouseEvent instanceof MouseMovedEvent) {
			// mouse moves do not map to touch
		} else if(mouseEvent instanceof MouseDraggedEvent) {
			// mouse drag maps to touch move
			transformedEvent = new TouchMovedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			transformedEvent.setTouchID(touchID);
			// indicate a drag
			isDragged =  true;
		} else {
			// throw exception to indicate the use of unsupported events
			// should only happen when new events are implemented
			throw new UnknownEventException(mouseEvent);
		}
		
		return transformedEvent;
	}

	/**
	 * Returns the matching touch id for the given mouse event by looking
	 * at the pressed buttons.
	 * 
	 * @param mouseEvent Mouse event to find a matching touch id for.
	 * 
	 * @return The touch id for the mouse event.
	 */
	protected int getTouchIDForMouseEvent(MouseEvent mouseEvent) {
		int touchID = NO_BUTTON_TOUCH_ID ;
		
		if(mouseEvent.getButton() == MouseEvent.PRIMARY_BUTTON) {
			touchID = PRIMARY_BUTTON_TOUCH_ID;
		} else if(mouseEvent.getButton() == MouseEvent.SECONDARY_BUTTON) {
			touchID = SECONDARY_BUTTON_TOUCH_ID;
		} else if(mouseEvent.getButton() == MouseEvent.MIDDLE_BUTTON) {
			touchID = MIDDLE_BUTTON_TOUCH_ID;
		}
		
		return touchID;
	}
}
