package org.sociotech.communityinteraction.behaviors.mousetox;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.ScrollBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior that maps scroll while mouse interaction. Prevents scrolling when shift or alt
 * is pressed, cause then scroll events are uses for zoom and rotate.
 * 
 * @author Peter Lachenmaier
 */
public class MouseToScrollBehavior extends ScrollBehavior {
	
	/**
	 * Creates a mouse to scroll behavior for the given handler.
	 * 
	 * @param handler Handler for the scroll events created by this behaviors.
	 */
	public MouseToScrollBehavior(HandleScroll handler) {
		super(handler);

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!isEnabled()) {
			// can only consume if enabled
			return null;
		}
		
		if(!(event instanceof ScrollEvent)) {
			// accept only scroll events
			return null;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		if(scrollEvent.isAltDown() || scrollEvent.isShiftDown()) {
			// do not use the ones with shift or alt pressed in parallel -> zoom and rotate
			return null;
		}
		
		// TODO maybe use always the same list that is final and contains only this object -> method getThisInList 
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		
		if(!(event instanceof ScrollEvent)) {
			// accept only mouse events
			return;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		
		// let the event be handled by the interaction handler using the helper implementation
		CIHandleEventHelper.handleScrollEventWith(scrollEvent, this.getInteractionHandler());
	}
}
