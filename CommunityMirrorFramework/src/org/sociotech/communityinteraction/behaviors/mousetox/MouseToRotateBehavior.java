package org.sociotech.communityinteraction.behaviors.mousetox;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.manipulation.RotateBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior that maps scroll to rotate while mouse interaction. Scroll with alt pressed is mapped
 * to rotate.
 * 
 * @author Peter Lachenmaier
 */
public class MouseToRotateBehavior extends RotateBehavior {
	
	/**
	 * Creates a mouse scroll to rotate behavior for the given handler.
	 * 
	 * @param handler Handler for the rotate events created by this behaviors.
	 */
	public MouseToRotateBehavior(HandleRotate handler) {
		super(handler);

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!isEnabled()) {
			// can only consume if enabled
			return null;
		}
		
		if(!(event instanceof ScrollEvent)) {
			// accept only scroll events
			return null;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		if(!scrollEvent.isAltDown()) {
			// only use with alt pressed
			return null;
		}
		
		// TODO maybe use always the same list that is final and contains only this object -> method getThisInList 
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
				
		if(!(event instanceof ScrollEvent)) {
			// accept only scroll events
			return;
		}
		
		ScrollEvent scrollEvent = (ScrollEvent) event;
		
		RotationEvent rotateEvent;
		
		if(event instanceof ScrollStartedEvent) {
			rotateEvent = new RotationStartedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
		} else if(event instanceof ScrollScrolledEvent) {
			rotateEvent = new RotationRotatedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
			// map y scroll to rotation angle
			rotateEvent.setAngle(scrollEvent.getDeltaY() / 10);
			rotateEvent.setInertia(scrollEvent.isInertia());
		} else if(event instanceof ScrollFinishedEvent) {
			rotateEvent = new RotationFinishedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());
		} else {
			return;
		}
		
		
		// let the event be handled by the interaction handler using the helper implementation
		CIHandleEventHelper.handleRotationEventWith(rotateEvent, this.getInteractionHandler());
		
		// set consumed state
		event.setConsumed(rotateEvent.isConsumed());
	}
}
