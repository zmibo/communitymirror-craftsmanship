package org.sociotech.communityinteraction.behaviors.mousetox;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.DragBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.PositionEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseDraggedEvent;
import org.sociotech.communityinteraction.events.mouse.MousePressedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseReleasedEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior that maps mouse drag to touch drags.
 * 
 * @author Peter Lachenmaier
 */
public class MouseToDragBehavior extends DragBehavior {

	/**
	 * The last x position of the mouse interaction to calculate the delta.
	 */
	private double lastX;
	
	/**
	 * The last y position of the mouse interaction to calculate the delta.
	 */
	private double lastY;
	
	
	/**
	 * Indicates that the drag gesture is finished by mouse released event
	 */
	private boolean finished = false;
	
	/**
	 * Indicates that the drag gesture is started by mouse pressed event
	 */
	private boolean started = false;

	/**
	 * Indicates that the drag gesture is currently performed
	 */
	private boolean inDrag = false;

	/**
	 * Constructs a mouse to drag behavior for the given handler.
	 * 
	 * @param handler Hander for transformed mouse drag events
	 */
	public MouseToDragBehavior(HandleDrag handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!isEnabled()) {
			// can only consume if enabled
			return null;
		}
		
		if(event instanceof MousePressedEvent) {
			// keep start for drag delta
			lastX = ((MousePressedEvent) event).getSceneX();
			lastY = ((MousePressedEvent) event).getSceneY();
			// indicate a drag start
			inDrag = false;
			started = true;
			// return null to process start at first drag event
			return null;
		} else	if(event instanceof MouseReleasedEvent && inDrag) {
			// indicate a drag end
			finished = true;
		}
		else if(!(event instanceof MouseDraggedEvent)) {
			// accept only mouse drag events
			return null;
		}
		
		// TODO react on mouse exited and send a drag finished
		
		// TODO maybe use always the same list that is final and contains only this object -> method getThisInList 
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!((event instanceof MouseDraggedEvent) || (event instanceof MouseReleasedEvent) || (event instanceof MousePressedEvent))) {
			// accept only mouse drag events
			return;
		}
		
		// cast
		PositionEvent mouseEvent = (PositionEvent) event;
		
		// transform the mouse event to a drag gesture event
		
		DragEvent dragEvent;
		if(started && !inDrag) {
			// send start event
			dragEvent = new DragStartedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			inDrag = true;
		} else if(finished) {
			// send finished event
			dragEvent = new DragFinishedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			inDrag = false;
			started = false;
			finished = false;
		} else if (started){		
			dragEvent = new DragDraggedEvent(mouseEvent.getSceneX(), mouseEvent.getSceneY());
			inDrag = true;
		} else {
			// do nothing if not started
			return;
		}
		
		// calculate and set delta
		double deltaX = mouseEvent.getSceneX() - lastX;
		dragEvent.setDeltaX(deltaX);
		double deltaY = mouseEvent.getSceneY() - lastY;
		dragEvent.setDeltaY(deltaY);
		//System.out.println("dx: " + deltaX + ", dy: " + deltaY);
		
		// update last x
		lastX = mouseEvent.getSceneX();
		lastY = mouseEvent.getSceneY();
		
		// TODO total delta
		
		// let the event be handled by the interaction handler using the helper implementation
		CIHandleEventHelper.handleDragEventWith(dragEvent, this.getInteractionHandler());
		
		// if drag event was consumed then the mouse event should also be consumed
		event.setConsumed(dragEvent.isConsumed());
	}

}
