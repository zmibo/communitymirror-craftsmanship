package org.sociotech.communityinteraction.behaviors;

import org.sociotech.communityinteraction.interactionhandling.HandleZoom;

/**
 * Behavior to add zoom interaction to a component.
 * 
 * @author Peter Lachenmaier
 */
public abstract class ZoomBehavior extends InteractionBehavior<HandleZoom> {

	/**
	 * Creates the zoom behavior for the given handler.
	 * 
	 * @param handler Handler for zoom events.
	 */
	public ZoomBehavior(HandleZoom handler) {
		super(handler);
	}

}
