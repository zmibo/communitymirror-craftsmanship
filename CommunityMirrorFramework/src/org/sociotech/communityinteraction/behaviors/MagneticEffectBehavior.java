package org.sociotech.communityinteraction.behaviors;

import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticEffectEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleMagneticEffect;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public class MagneticEffectBehavior extends InteractionBehavior<HandleMagneticEffect> {
	
	private MagneticField magneticField;
	
	/**
	 * @return the magneticField
	 */
	public MagneticField getMagneticField() {
		return magneticField;
	}

	public MagneticEffectBehavior(HandleMagneticEffect handler, MagneticField magneticField) {
		super(handler);
		this.magneticField = magneticField;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof MagneticEffectEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof MagneticEffectEvent)){
			return;
		}
		CIHandleEventHelper.handleMagneticEffectEventWith(event, this.getInteractionHandler());
	}
}
