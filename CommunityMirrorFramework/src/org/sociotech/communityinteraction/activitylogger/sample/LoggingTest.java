package org.sociotech.communityinteraction.activitylogger.sample;

import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;

public class LoggingTest {
	public static void main(String[] args) {
		UserActivityLogger ul = new UserActivityLogger();
		ul.logActivity("Started");
		ul.logActivity("Message with details.", ul.detail("Key", "Value"));
	}
}
