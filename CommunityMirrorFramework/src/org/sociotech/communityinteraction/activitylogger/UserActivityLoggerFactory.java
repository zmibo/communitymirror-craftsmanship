package org.sociotech.communityinteraction.activitylogger;

import org.sociotech.communitymirror.view.flow.activityLogger.CSVUserActivityLogger;

/**
 * Factory to produce user activity logger.
 * 
 * @author Eva Lösch
 */
public class UserActivityLoggerFactory {
	
	/**
	 * Creates a new default user activity logger.
	 * 
	 * @return The default user activity logger.
	 */
	private UserActivityLogger createDefaultUserActivityLogger() {
		return new UserActivityLogger();
	}
	
	/**
	 * Creates a new csv user activity logger.
	 * 
	 * @return The csv user activity logger.
	 */
	private UserActivityLogger createCSVUserActivityLogger() {
		return new CSVUserActivityLogger();
	}
	
	/**
     * Creates a new (subclass of) user activity logger according to corresponding specification within configuration file.
     *
     * @return The user activity logger
     */
	public UserActivityLogger createUserActivityLogger(String userActivityLoggerClassName)
	{			
		if(userActivityLoggerClassName == null) {
			return null;
		}
			
		// create depending the given class name
		switch (userActivityLoggerClassName) {
			case "UserActivityLogger":
				return createDefaultUserActivityLogger();
			case "CSVUserActivityLogger":
				return createCSVUserActivityLogger();
			default:
				return createDefaultUserActivityLogger();
		}
	}

}
