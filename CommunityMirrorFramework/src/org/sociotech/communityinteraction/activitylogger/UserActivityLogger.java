package org.sociotech.communityinteraction.activitylogger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualComponent;


/**
 * Logger for user activity.
 *
 * @author Peter Lachenmaier
 *
 */
public class UserActivityLogger {

	 /**
     * Use log4j loggers for writing
     */
    private Logger messageLogger = LogManager.getLogger(UserActivityLogger.class.getName() + ".message");
    private Logger detailLogger = LogManager.getLogger(UserActivityLogger.class.getName() + ".detail");

    /**
     * Get the message logger.
     * @return the messageLogger
     */
    protected Logger getMessageLogger()
    {
    	return this.messageLogger;
    }

    /**
     * Get the detail logger.
     * @return the detailLogger
     */
    protected Logger getDetailLogger()
    {
    	return this.detailLogger;
    }

    /**
     * Logs a user activity.
     *
     * @param message Description of the user activity
     */
    public void logActivity(String message) {
    	if(message == null) return;

    	// log as info
    	this.getMessageLogger().info("Message - " + message);
    }

    /**
     * Logs the activity and all given details.
     *
     * @param message Message to log
     * @param details Details in key value pairs
     */
    public void logActivity(String message, ActivityKeyValue<?> ... details) {
    	// log message
    	this.logActivity(message);

    	logDetails(details);
    }


    /**
     * Logs all given details.
     *
     * @param details Details in key value pairs
     */
    public void logActivity(ActivityKeyValue<?> ... details) {
		// log details
    	logDetails(details);
    }

    /**
	 * Logs the given details.
	 *
	 * @param details Details to log
	 */
	protected void logDetails(ActivityKeyValue<?>... details) {
		// log all details
    	for(ActivityKeyValue<?> detail : details) {
    		this.logDetail(detail);
    	}
	}

    /**
     * Logs the activity and detail of the visual component
     *
     * @param message Message to log
     * @param visualComponent Visual component to log
     */
    public void logActivity(String message, VisualComponent visualComponent) {
    	this.logActivity(message);
    	logDetail(detail("VisualComponent class", visualComponent.getClass().getSimpleName()));
    	logDetail(detail("VisualComponent value", visualComponent));
    }

    /**
     * Logs the activity and detail of the visual component
     *
     * @param message Message to log
     * @param visualComponent Visual component to log
     * @param details Additional details
     */
    public void logActivity(String message, VisualComponent visualComponent, ActivityKeyValue<?> ... details) {
    	this.logActivity(message, visualComponent);
    	logDetails(details);
    }

    /**
     * Logs the activity and detail of the visual component
     *
     * @param message Message to log
     * @param visualComponent Visual component to log
     * @param data The CommunityMashup data item
     */
    public void logActivity(String message, VisualComponent visualComponent, Item data) {
    	this.logActivity(message, visualComponent);
    	logDetail(detail("Data ", data));
    }

    /**
     * Logs the activity and detail of the visual component
     *
     * @param message Message to log
     * @param visualComponent Visual component to log
     * @param data The CommunityMashup data item
     * @param details Additional details
     */
    public void logActivity(String message, VisualComponent visualComponent, Item data, ActivityKeyValue<?> ... details) {
    	this.logActivity(message, visualComponent, data);
    	logDetails(details);
    }
	/**
	 * Logs one key value detail.
	 *
	 * @param detail Detail to log
	 */
	private void logDetail(ActivityKeyValue<?> detail) {
		// use detail logger
		this.getDetailLogger().info(" Detail - " + detail);
	}

	/**
	 * Factory method for a new key value pair.
	 *
	 * @param key Key
	 * @param value Value
	 * @return The created key value pair object.
	 */
	public <T> ActivityKeyValue<T> detail(String key, T value) {
		return new ActivityKeyValue<T>(key, value);
	}
}
