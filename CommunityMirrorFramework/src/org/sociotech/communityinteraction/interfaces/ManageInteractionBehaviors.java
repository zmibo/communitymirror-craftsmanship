package org.sociotech.communityinteraction.interfaces;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;

/**
 * Interface that defines methods for management of interaction behaviors.
 * 
 * @author Peter Lachenmaier
 */
public interface ManageInteractionBehaviors {

	/**
	 * Adds the given interaction behavior to this component.
	 * 
	 * @param interactionBehavior
	 *            Interaction behavior to add.
	 */
	public void addInteractionBehavior(
			InteractionBehavior<?> interactionBehavior);

	/**
	 * Gets the list of all interaction behaviors added to this component.
	 * 
	 * @return the list of all interaction behaviors of this components
	 */
	public List<InteractionBehavior<?>> getInteractionBehaviors();

	/**
	 * Removes an interaction behavior from the list of all interaction
	 * behaviors added to this component.
	 */
	void removeInteractionBehavior(InteractionBehavior<?> interactionBehavior);
}
