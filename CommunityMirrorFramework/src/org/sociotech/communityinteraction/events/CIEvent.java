package org.sociotech.communityinteraction.events;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Abstract super class of all CommunityInteraction events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class CIEvent {
	
	/**
	 * Whether this event is filtered out or not. If it is filtered, it will not
	 * be passed to possible other consumers in the {@link CIEventConsumer#getConsumers(CIEvent)}
	 * step.
	 */
	protected boolean filtered = false;

	/**
	 * Whether this event is already consumed or not. If it is consumed, it will not
	 * be passed to possible other consumers in the @link {@link CIEventConsumer#handle(CIEvent)}
	 * step.
	 */
	protected boolean consumed = false;
	
	/**
	 * If known, the direct reference to the targeted consumer.
	 */
	protected List<CIEventConsumer> targetConsumers;
	
	/**
	 * Whether this event is filtered out or not. If it is filtered, it will not
	 * be passed to possible other consumers in the {@link CIEventConsumer#getConsumers(CIEvent)}
	 * step.
	 *
	 * @return True if this event is filtered.
	 */
	public boolean isFiltered() {
		return filtered;
	}

	/**
	 * Sets the filtered value.
	 * 
	 * @param filtered True to mark the event as filtered.
	 */
	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}
	
	/**
	 * Call this method to filter out this event to not get passed to possible other
	 * consumers in the {@link CIEventConsumer#getConsumers(CIEvent)} step.
	 */
	public void filter() {
		setFiltered(true);
	}

	/**
	 * Whether this event is already consumed or not. If it is consumed, it will not
	 * be passed to possible other consumers in the @link {@link CIEventConsumer#handle(CIEvent)}
	 * step.
	 *
	 * @return True if this event is already consumed.
	 */
	public boolean isConsumed() {
		return consumed;
	}

	/**
	 * Sets the consumed value.
	 * 
	 * @param consumed True to mark the event as consumed.
	 */
	public void setConsumed(boolean consumed) {
		this.consumed = consumed;
	}
	
	/**
	 * Call this method to consume this event and to not get passed to possible other
	 * consumers in the {@link CIEventConsumer#handle(CIEvent)} step.
	 */
	public void consume() {
		setConsumed(true);
	}

	/**
	 * Returns the targeted consumer for this event.
	 *  
	 * @return The targeted consumer for this event. Null if it is unknown.
	 */
	public List<CIEventConsumer> getTargetConsumers() {
		return targetConsumers;
	}

	/**
	 * Sets the list of target consumers for this event.
	 * 
	 * @param targetConsumers the list of target consumers.
	 */
	public void setTargetConsumers(List<CIEventConsumer> targetConsumers) {
		this.targetConsumers = targetConsumers;
	}
	
	/**
	 * Creates an empty list of CIEventConsumer, adds the given target consumer to the list and sets the target consumer for this event to the new list.
	 * 
	 * @param targetConsumer the target consumer to add to the list.
	 */
	public void setTargetConsumers(CIEventConsumer targetConsumer) {
		if(targetConsumer != null){
			List<CIEventConsumer> targetConsumers = new LinkedList<CIEventConsumer>();
			targetConsumers.add(targetConsumer);
			this.targetConsumers = targetConsumers;
		}
	}
}
