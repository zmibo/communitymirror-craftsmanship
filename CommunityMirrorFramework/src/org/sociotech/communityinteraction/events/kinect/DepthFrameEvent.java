package org.sociotech.communityinteraction.events.kinect;

/**
 * A kinect frame event that is triggered if a depth frame is received by the kinect.
 * 
 * @author evalosch
 *
 */
public class DepthFrameEvent extends KinectFrameEvent {

	private short[] depth_frame;
	private byte[] player_index;
	private float[] xyz;
	private float[] uv;
	
	public DepthFrameEvent(short[] depth_frame, byte[] player_index, 
			float[] xyz, float[] uv) {
		this.depth_frame = depth_frame;
		this.player_index = player_index;
		this.xyz = xyz;
		this.uv = uv;
	}

	/**
	 * @return the depth_frame
	 */
	public short[] getDepth_frame() {
		return depth_frame;
	}

	/**
	 * @return the player_index
	 */
	public byte[] getPlayer_index() {
		return player_index;
	}

	/**
	 * @return the xyz
	 */
	public float[] getXyz() {
		return xyz;
	}

	/**
	 * @return the uv
	 */
	public float[] getUv() {
		return uv;
	}

	/**
	 * @param depth_frame the depth_frame to set
	 */
	public void setDepth_frame(short[] depth_frame) {
		this.depth_frame = depth_frame;
	}

	/**
	 * @param player_index the player_index to set
	 */
	public void setPlayer_index(byte[] player_index) {
		this.player_index = player_index;
	}

	/**
	 * @param xyz the xyz to set
	 */
	public void setXyz(float[] xyz) {
		this.xyz = xyz;
	}

	/**
	 * @param uv the uv to set
	 */
	public void setUv(float[] uv) {
		this.uv = uv;
	}

}
