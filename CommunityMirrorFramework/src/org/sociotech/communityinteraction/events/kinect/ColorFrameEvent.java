package org.sociotech.communityinteraction.events.kinect;

/**
 * A kinect frame event that is triggered if a color frame is received by the kinect.
 * 
 * @author evalosch
 *
 */
public class ColorFrameEvent extends KinectFrameEvent {

	private byte[] color_data;
	
	public ColorFrameEvent(byte[] color_data) {
		this.color_data = color_data;
	}
	
	/**
	 * @return the color_data
	 */
	public byte[] getColor_data() {
		return color_data;
	}

	/**
	 * @param color_data the color_data to set
	 */
	public void setColor_data(byte[] color_data) {
		this.color_data = color_data;
	}

}
