package org.sociotech.communityinteraction.events;

/**
 * An event that occurs during a sensing activity (e.g. with the Kinect).
 * 
 * @author evalosch
 *
 */
public abstract class SensingEvent extends CIEvent {

}
