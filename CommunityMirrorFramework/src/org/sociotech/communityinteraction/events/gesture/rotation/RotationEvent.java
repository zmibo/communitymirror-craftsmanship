package org.sociotech.communityinteraction.events.gesture.rotation;

import org.sociotech.communityinteraction.events.gesture.GestureEvent;

/**
 * Abstract super class of all rotation events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class RotationEvent extends GestureEvent {

	/**
	 * The current angle of a rotation. 
	 */
	private double angle;
	
	/**
	 * Constructs a rotation event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public RotationEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Returns the angle of the rotation.
	 * 
	 * @return The angle of the rotation.
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * Sets the angle of the rotation.
	 * 
	 * @param angle Rotation angle.
	 */
	public void setAngle(double angle) {
		this.angle = angle;
	}
}
