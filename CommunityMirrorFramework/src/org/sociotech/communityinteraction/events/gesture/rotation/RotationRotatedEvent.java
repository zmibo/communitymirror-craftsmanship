package org.sociotech.communityinteraction.events.gesture.rotation;

/**
 * Representation of a rotation (change).
 * 
 * @author Peter Lachenmaier
 */
public class RotationRotatedEvent extends RotationEvent {

	/**
	 * Constructs a rotate event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public RotationRotatedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
