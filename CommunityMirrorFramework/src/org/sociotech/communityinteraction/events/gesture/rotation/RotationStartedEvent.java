package org.sociotech.communityinteraction.events.gesture.rotation;

/**
 * Representation of a starting rotation.
 * 
 * @author Peter Lachenmaier
 */
public class RotationStartedEvent extends RotationEvent {

	/**
	 * Constructs a rotate started event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public RotationStartedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
