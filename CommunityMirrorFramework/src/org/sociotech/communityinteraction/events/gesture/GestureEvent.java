package org.sociotech.communityinteraction.events.gesture;

import org.sociotech.communityinteraction.events.PositionEvent;

/**
 * Abstract super class of all CommunityInteraction gesture events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class GestureEvent extends PositionEvent {

	/**
	 * Indicates if this gesture event was created by indirect manipulation. 
	 */
	private boolean isInertia = false;
	
	/**
	 * Constructs a gesture event for the given scene position.
	 * 
	 * @param sceneX Scene x
	 * @param sceneY Scene y
	 */
	public GestureEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Indicates if this gesture event was created by indirect manipulation. 
	 * 
	 * @return True if indirect manipulation
	 */
	public boolean isInertia() {
		return isInertia;
	}

	/**
	 * Sets the value for the indirect manipulation indicator.
	 * 
	 * @param isInertia True for indirect manipulation.
	 */
	public void setInertia(boolean isInertia) {
		this.isInertia = isInertia;
	}

}
