package org.sociotech.communityinteraction.events.gesture.drag;

/**
 * Representation of a starting drag.
 * 
 * @author Peter Lachenmaier
 */
public class DragStartedEvent extends DragEvent {

	/**
	 * Constructs a drag started event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public DragStartedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
