package org.sociotech.communityinteraction.events.gesture.drag;

/**
 * Representation of a drag (change).
 * 
 * @author Peter Lachenmaier
 */
public class DragDraggedEvent extends DragEvent {

	/**
	 * Constructs a drag event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public DragDraggedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
