package org.sociotech.communityinteraction.events.gesture.drag;

/**
 * Representation of a finished drag.
 * 
 * @author Peter Lachenmaier
 */
public class DragFinishedEvent extends DragEvent {

	/**
	 * Constructs a drag finished event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public DragFinishedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
