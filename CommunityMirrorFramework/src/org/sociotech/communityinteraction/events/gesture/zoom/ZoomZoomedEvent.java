package org.sociotech.communityinteraction.events.gesture.zoom;

/**
 * Representation of a zoom (change).
 * 
 * @author Peter Lachenmaier
 */
public class ZoomZoomedEvent extends ZoomEvent {

	/**
	 * Constructs a zoom zoomed event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ZoomZoomedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
