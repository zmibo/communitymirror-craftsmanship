package org.sociotech.communityinteraction.events.gesture.zoom;

/**
 * Representation of a starting zoom.
 * 
 * @author Peter Lachenmaier
 */
public class ZoomStartedEvent extends ZoomEvent {

	/**
	 * Constructs a zoom started event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ZoomStartedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
