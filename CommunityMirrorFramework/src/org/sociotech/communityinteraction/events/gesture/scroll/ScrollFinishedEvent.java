package org.sociotech.communityinteraction.events.gesture.scroll;

/**
 * Representation of a finished scroll.
 * 
 * @author Peter Lachenmaier
 */
public class ScrollFinishedEvent extends ScrollEvent {

	/**
	 * Constructs a scroll finished event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ScrollFinishedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
