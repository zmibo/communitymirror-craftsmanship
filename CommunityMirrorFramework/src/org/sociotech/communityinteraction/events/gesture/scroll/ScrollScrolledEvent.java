package org.sociotech.communityinteraction.events.gesture.scroll;

/**
 * Representation of a performed scroll.
 * 
 * @author Peter Lachenmaier
 */
public class ScrollScrolledEvent extends ScrollEvent {

	/**
	 * Constructs a scroll performed event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ScrollScrolledEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
