package org.sociotech.communityinteraction.events.gesture.scroll;

import org.sociotech.communityinteraction.events.gesture.GestureEvent;

/**
 * Abstract super class of all zoom events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class ScrollEvent extends GestureEvent {
	
	/**
	 * The current delta in x direction. 
	 */
	private double deltaX;

	/**
	 * The current delta in y direction. 
	 */
	private double deltaY;

	/**
	 * Constructs a zoom event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ScrollEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Returns the current delta in x direction.
	 * 
	 * @return The current delta in x direction.
 	 */
	public double getDeltaX() {
		return deltaX;
	}

	/**
	 * Sets the current delta in x direction.
	 * 
	 * @param deltaX The delta in x direction.
	 */
	public void setDeltaX(double deltaX) {
		this.deltaX = deltaX;
	}

	/**
	 * Returns the current delta in y direction.
	 * 
	 * @return The current delta in y direction.
 	 */
	public double getDeltaY() {
		return deltaY;
	}

	/**
	 * Sets the current delta in y direction.
	 * 
	 * @param deltaY The delta in y direction.
	 */
	public void setDeltaY(double deltaY) {
		this.deltaY = deltaY;
	}
}
