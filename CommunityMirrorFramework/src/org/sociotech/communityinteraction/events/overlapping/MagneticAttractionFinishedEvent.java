package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public class MagneticAttractionFinishedEvent extends MagneticAttractionEvent {

	public MagneticAttractionFinishedEvent(Intersection intersection, CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
