package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public class MagneticAttractionStartedEvent extends MagneticAttractionEvent {

	public MagneticAttractionStartedEvent(Intersection intersection, CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
