package org.sociotech.communityinteraction.events.overlapping;


public class OverlappingFinishedEvent extends OverlappingEvent {

	public OverlappingFinishedEvent(Intersection overlapping) {
		super(overlapping);
	}

}
