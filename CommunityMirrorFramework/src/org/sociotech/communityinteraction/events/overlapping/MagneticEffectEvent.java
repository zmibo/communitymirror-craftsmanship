package org.sociotech.communityinteraction.events.overlapping;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public abstract class MagneticEffectEvent extends IntersectionEvent {
	
	private CIEventConsumer targetConsumer;

	public MagneticEffectEvent(Intersection intersection, CIEventConsumer targetConsumer) {
		super(intersection);
		this.targetConsumer = targetConsumer;
		
		List<CIEventConsumer> consumers = new LinkedList<CIEventConsumer>();
		consumers.add(targetConsumer);
		this.setTargetConsumers(consumers);
	}
	
	/**
	 * @return the targetConsumer
	 */
	public CIEventConsumer getTargetConsumer() {
		return targetConsumer;
	}

}
