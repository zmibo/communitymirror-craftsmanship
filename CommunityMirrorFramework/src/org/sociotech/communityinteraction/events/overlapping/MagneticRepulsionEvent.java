package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public abstract class MagneticRepulsionEvent extends MagneticEffectEvent {

	public MagneticRepulsionEvent(Intersection intersection, CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
