package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public class MagneticRepulsionStartedEvent extends MagneticRepulsionEvent {

	public MagneticRepulsionStartedEvent(Intersection intersection,  CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
