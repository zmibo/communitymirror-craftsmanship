package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.events.InteractionEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

public abstract class IntersectionEvent extends InteractionEvent {
	
	protected Intersection intersection;

	public IntersectionEvent(Intersection intersection) {
		this.intersection = intersection;
	}
	
	/**
	 * @return the intersection
	 */
	public Intersection getIntersection() {
		return intersection;
	}
	
	/**
	 * @return the firstComponent
	 */
	public VisualComponent getFirstComponent() {
		return intersection.getFirstComponent();
	}
	
	/**
	 * @return the secondComponent
	 */
	public VisualComponent getSecondComponent() {
		return intersection.getSecondComponent();
	}

}
