package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * object that represents an intersection of two visual components
 * 
 * @author evalosch
 *
 */
public class Intersection {

	/**
	 * the first intersection visual component of this intersection object
	 */
	private VisualComponent firstComponent;

	/**
	 * the second intersection visual component of this intersection object
	 */
	private VisualComponent secondComponent;

	public Intersection(VisualComponent firstComponent,
			VisualComponent secondComponent) {
		this.firstComponent = firstComponent;
		this.secondComponent = secondComponent;
	}

	/**
	 * Returns the other visual component that is - besigdes a given visual
	 * component - part of this intersection.
	 * 
	 * @param component
	 * @return first/second visual component of this intersection if the given
	 *         component equals the second/first visual component of this
	 *         intersection or null if the given component doesn't equal the
	 *         firts or the second component of this intersection
	 */
	public VisualComponent getOppenent(VisualComponent component) {
		if (this.firstComponent.equals(component)) {
			return this.secondComponent;
		} else if (this.secondComponent.equals(component)) {
			return this.firstComponent;
		}
		return null;
	}

	/**
	 * Compares two intersection objects to each other and returns true if both
	 * objects do contain the same components.
	 * 
	 * @param object
	 *            the (intersection) objects to compare with this intersection
	 *            object
	 * @return true if both objects do contain the same components else false
	 */
	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (this.getClass() != object.getClass()) {
			return false;
		}
		final Intersection intersection = (Intersection) object;
		if (this.firstComponent.equals(
				intersection.firstComponent)
				&& this.secondComponent.equals(
						intersection.secondComponent)
				|| this.firstComponent.equals(
						intersection.secondComponent)
				&& (this.secondComponent
						.equals(intersection.firstComponent))) {
			return true;
		}
		return false;
	}

	/**
	 * @return the firstComponent
	 */
	public VisualComponent getFirstComponent() {
		return firstComponent;
	}

	/**
	 * @param firstComponent
	 *            the firstComponent to set
	 */
	public void setFirstComponent(VisualComponent firstComponent) {
		this.firstComponent = firstComponent;
	}

	/**
	 * @return the secondComponent
	 */
	public VisualComponent getSecondComponent() {
		return secondComponent;
	}

	/**
	 * @param secondComponent
	 *            the secondComponent to set
	 */
	public void setSecondComponent(VisualComponent secondComponent) {
		this.secondComponent = secondComponent;
	}
}
