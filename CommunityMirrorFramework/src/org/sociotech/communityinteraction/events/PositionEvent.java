package org.sociotech.communityinteraction.events;

/**
 * User interaction event that happens on a specific scene position.
 * 
 * @author Peter Lachenmaier
 *
 */
public abstract class PositionEvent extends InteractionEvent {
	
	/**
	 * Constructs the position event at the given coordinates.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public PositionEvent(double sceneX, double sceneY) {
		this.setSceneX(sceneX);
		this.setSceneY(sceneY);
	}
	
	/**
	 * Constructs a position event at the same position as the given one.
	 * 
	 * @param positionEvent Other position event to get position from.
	 */
	public PositionEvent(PositionEvent positionEvent) {
		this(positionEvent.getSceneX(), positionEvent.getSceneY());
	}
	
	/**
	 * The scene x position. 
	 */
	protected double sceneX;
	
	/**
	 * The scene y position. 
	 */
	protected double sceneY;

	/**
	 * Returns the x position of this event.
	 * 
	 * @return The x position of this event.
	 */
	public double getSceneX() {
		return sceneX;
	}

	/**
	 * Sets the x position of this event.
	 * 
	 * @param sceneX The scene x
	 */
	public void setSceneX(double sceneX) {
		this.sceneX = sceneX;
	}

	/**
	 * Returns the y position of this event.
	 * 
	 * @return The y position of this event.
	 */
	public double getSceneY() {
		return sceneY;
	}

	/**
	 * Sets the y position of this event.
	 * 
	 * @param sceneX The scene y
	 */
	public void setSceneY(double sceneY) {
		this.sceneY = sceneY;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getCanonicalName() + " (" + getSceneX() + ", " + getSceneY() + ")";
	}
	
	
}
