package org.sociotech.communityinteraction.events.mouse;

/**
 * Representation of a mouse move.
 * 
 * @author Peter Lachenmaier
 */
public class MouseMovedEvent extends MouseEvent {

	/**
	 * Constructs a mouse moved event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MouseMovedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
