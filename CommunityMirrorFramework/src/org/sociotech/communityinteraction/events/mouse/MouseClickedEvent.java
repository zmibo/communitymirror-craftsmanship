package org.sociotech.communityinteraction.events.mouse;

/**
 * Representation of a mouse click.
 * 
 * @author Peter Lachenmaier
 */
public class MouseClickedEvent extends MouseEvent {

	/**
	 * Constructs a mouse click event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MouseClickedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
