package org.sociotech.communityinteraction.events.mouse;

/**
 * Representation of a mouse button released event.
 * 
 * @author Peter Lachenmaier
 */
public class MouseReleasedEvent extends MouseEvent {

	/**
	 * Constructs a mouse released event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MouseReleasedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
