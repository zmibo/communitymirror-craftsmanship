package org.sociotech.communityinteraction.events.touch;

import org.sociotech.communityinteraction.events.PositionEvent;

/**
 * Abstract super class of all touch events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class TouchEvent extends PositionEvent {

	/**
	 * Counter to create new ids 
	 */
	private static int touchIDCounter = 0;
	
	/**
	 * The id of the touch point.
	 */
	private int touchID = 0;
	
	/**
	 * Constructs a touch event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Returns the id of the touch point.
	 * 
	 * @return Touch point id.
	 */
	public int getTouchID() {
		return touchID;
	}

	/**
	 * Sets the id of the touch point.
	 * 
	 * @param touchID The touch point id.
	 */
	public void setTouchID(int touchID) {
		this.touchID = touchID;
	}
	
	/**
	 * Creates and sets a new touch id for this touch event.
	 */
	public void createAndSetNewTouchID() {
		// inc counter and set
		this.setTouchID(touchIDCounter++);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.events.PositionEvent#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + " (id: " + touchID + ")";
	}
}
