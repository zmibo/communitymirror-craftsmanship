package org.sociotech.communityinteraction.events.touch;

/**
 * Representation of a touched finger move.
 * 
 * @author Peter Lachenmaier
 */
public class TouchMovedEvent extends TouchEvent {

	/**
	 * Constructs a touch moved event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchMovedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
