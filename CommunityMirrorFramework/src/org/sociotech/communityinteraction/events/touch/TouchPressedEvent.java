package org.sociotech.communityinteraction.events.touch;

/**
 * Representation of a touch press.
 * 
 * @author Peter Lachenmaier
 */
public class TouchPressedEvent extends TouchEvent {

	/**
	 * Constructs a touch pressed event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchPressedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
