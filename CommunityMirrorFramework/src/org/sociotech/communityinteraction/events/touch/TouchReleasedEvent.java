package org.sociotech.communityinteraction.events.touch;

/**
 * Representation of a touch release.
 * 
 * @author Peter Lachenmaier
 */
public class TouchReleasedEvent extends TouchEvent {

	/**
	 * Constructs a touch released event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchReleasedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
